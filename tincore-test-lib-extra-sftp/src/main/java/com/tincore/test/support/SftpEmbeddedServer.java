package com.tincore.test.support;

/*-
 * #%L
 * tincore-test-lib-extra-sftp
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.SshServer;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.common.file.virtualfs.VirtualFileSystemFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.UserAuth;
import org.apache.sshd.server.auth.UserAuthPassword;
import org.apache.sshd.server.command.ScpCommandFactory;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.sftp.SftpSubsystem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * public static void main(String args[]) throws Exception { SftpEmbeddedServer server = new SftpEmbeddedServer(2121, new File("embedded_sftp/sshHosts.pen"), new SftpEmbeddedServerUser("admin", "admin", new
 * File("embedded_sftp/home/admin"))); server.start(); }
 */
@Slf4j
public class SftpEmbeddedServer {

    private final List<SftpEmbeddedServerUser> users;
    private final SshServer sshServer = SshServer.setUpDefaultServer();

    public SftpEmbeddedServer(int port, File hostsKeysFile, SftpEmbeddedServerUser user, SftpEmbeddedServerUser... otherUsers) {
        users = Stream.concat(Stream.of(user), Stream.of(otherUsers)).collect(Collectors.toList());
        users.stream().filter(u -> StringUtils.isBlank(u.password)).forEach(this::setUpUserHome);
        sshServer.setPort(port);
        SimpleGeneratorHostKeyProvider keyPairProvider = new SimpleGeneratorHostKeyProvider(hostsKeysFile.getAbsolutePath());
        keyPairProvider.setAlgorithm("RSA");
        sshServer.setKeyPairProvider(keyPairProvider);

        sshServer.setCommandFactory(new ScpCommandFactory());

        List<NamedFactory<UserAuth>> userAuthFactories = new ArrayList<>();
        userAuthFactories.add(new UserAuthPassword.Factory());
        sshServer.setUserAuthFactories(userAuthFactories);
        sshServer.setPasswordAuthenticator(
            (username, password, session) -> {
                Optional<SftpEmbeddedServerUser> authorizedUser = users.stream().filter(u -> username.equals(u.username) && password.equals(u.password)).findFirst();
                authorizedUser.ifPresent(this::setUpUserHome);
                return authorizedUser.isPresent();
            });
        List<NamedFactory<Command>> namedFactoryList = new ArrayList<>();
        namedFactoryList.add(new SftpSubsystem.Factory());
        sshServer.setSubsystemFactories(namedFactoryList);
    }

    public void setUpUserHome(SftpEmbeddedServerUser user) {
        File home = user.home;
        home.mkdirs();
        sshServer.setFileSystemFactory(new VirtualFileSystemFactory(home.getAbsolutePath()));
    }

    @SneakyThrows
    public void start() {
        sshServer.start();
        log.info("Server started at port {}", sshServer.getPort());
    }

    @SneakyThrows
    public void stop() {
        sshServer.stop();
        log.info("Server stopped");
    }

    @Data
    @AllArgsConstructor
    public static class SftpEmbeddedServerUser {
        private String username;
        private String password;
        private File home;
    }
}
