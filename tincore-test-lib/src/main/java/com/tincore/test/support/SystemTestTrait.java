package com.tincore.test.support;

/*-
 * #%L
 * tincore-test-lib
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public interface SystemTestTrait {

    static int getSystemPropertyAsInt(String key) {
        return Optional.ofNullable(System.getProperty(key)).map(Integer::parseInt).orElseThrow(() -> new RuntimeException(key + " mandatory system property not set"));
    }

    static String initSystemProperty(String key, String value) {
        String staticValue = Optional.ofNullable(System.getProperty(key, value)).filter(StringUtils::isBlank).orElse(value);
        return System.setProperty(key, staticValue);
    }

    static void initSystemProperty(String key1, String value1, String key2, String value2) {
        initSystemProperty(key1, value1);
        initSystemProperty(key2, value2);
    }

    static void initSystemProperty(String key1, String value1, String key2, String value2, String key3, String value3) {
        initSystemProperty(key1, value1, key2, value2);
        System.setProperty(key3, value3);
    }

    static void initSystemProperty(String key1, String value1, String key2, String value2, String key3, String value3, String key4, String value4) {
        initSystemProperty(key1, value1, key2, value2, key3, value3);
        System.setProperty(key4, value4);
    }
}
