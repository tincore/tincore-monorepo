package com.tincore.test.support.junit2bdd;

/*-
 * #%L
 * tincore-test-lib
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.qameta.allure.Allure;
import io.qameta.allure.util.ResultsUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.qameta.allure.util.ResultsUtils.STORY_LABEL_NAME;

public interface BehaviourTestListener {
    String JUNIT2BDD_REPORT_PATH = "junit2bdd.reportPath";
    String FILE_PREFIX = "TEST-";
    boolean ENFORCE_WELL_FORMED_SCENARIOS = Boolean.parseBoolean(System.getProperty("junit2bdd.enforce", "true"));

    default Scenario createScenario(Class<?> testClass, String methodName, Behaviour behaviour, String displayName, boolean skipped, Throwable throwable, List<BehaviourDefinitionStyle> behaviourDefinitionStyles) {
        BehaviourGroup behaviourGroup = testClass.getAnnotation(BehaviourGroup.class);

        Scenario scenario = new Scenario(testClass, methodName, skipped, throwable);
        scenario.setDescriptionDisplayName(displayName);

        if (behaviourGroup != null) {
            scenario.scenarioBase = Optional.of(behaviourGroup.scenario()).filter(StringUtils::isNotBlank).orElse(null);
            scenario.scenarioTags = behaviourGroup.tags();
        }

        behaviourDefinitionStyles.stream()
            .filter(s -> s.isMatch(methodName))
            .findFirst()
            .ifPresent(
                s -> {
                    String givenSeparator = s.getGivenSeparator();
                    String whenSeparator = s.getWhenSeparator();
                    String thenSeparator = s.getThenSeparator();

                    String otherSeparators = s.getOtherSeparators();

                    int givenIndex = StringUtils.indexOf(methodName, givenSeparator);
                    int whenIndex = StringUtils.indexOf(methodName, whenSeparator, givenIndex);
                    int thenIndex = StringUtils.indexOf(methodName, thenSeparator, Math.max(givenIndex, whenIndex));

                    String descriptionSection = StringUtils.substring(methodName, 0, givenIndex);
                    String givenSection = StringUtils.substring(methodName, givenIndex + givenSeparator.length(), whenIndex > 0 ? whenIndex : thenIndex);
                    String whenSection = whenIndex > 0 ? StringUtils.substring(methodName, whenIndex + whenSeparator.length(), thenIndex) : null;
                    String thenSection = StringUtils.substring(methodName, thenIndex + thenSeparator.length());

                    String methodBasedDescription = toHumanReadable(descriptionSection.replaceFirst("test", ""), otherSeparators);
                    scenario.description = Optional.ofNullable(methodBasedDescription).orElse(scenario.scenarioBase);

                    scenario.given = toHumanReadable(givenSection, otherSeparators);
                    scenario.when = toHumanReadable(whenSection, otherSeparators);
                    scenario.then = toHumanReadable(thenSection, otherSeparators);
                });

        if (behaviour != null) {
            scenario.description = Optional.of(behaviour.scenario()).filter(StringUtils::isNotBlank).orElse(scenario.description);
            scenario.given = Optional.of(behaviour.given()).filter(StringUtils::isNotBlank).orElse(scenario.given);
            scenario.when = Optional.of(behaviour.when()).filter(StringUtils::isNotBlank).orElse(scenario.when);
            scenario.then = Optional.of(behaviour.then()).filter(StringUtils::isNotBlank).orElse(scenario.then);
            scenario.tags = behaviour.tags();
        }

        return scenario;
    }

    default void flushScenarios(Map<String, Scenario> scenarios) {
        Optional<Path> reportDirectory = getConfig().getReportDirectory();
        writeAllTxt(scenarios.values(), reportDirectory);
        writeAllXml(scenarios.values(), reportDirectory);
        scenarios.clear();
    }

    BehaviourTestListenerConfig getConfig();

    default void toAllure(Scenario scenario) {
        try {
            Allure.description(scenario.toTxtDescription());
            Allure.descriptionHtml(scenario.toHtmlDescription());
            if (StringUtils.isNotBlank(scenario.description)) {
                Allure.label(STORY_LABEL_NAME, StringUtils.capitalize(scenario.description));
            } else {
                Allure.label(ResultsUtils.TAG_LABEL_NAME, "BAD_TEST_NAME");
            }
            if (StringUtils.isNotBlank(scenario.scenarioBase)) {
                Allure.label(ResultsUtils.FEATURE_LABEL_NAME, scenario.scenarioBase);
            }
            Stream.of(scenario.scenarioTags).forEach(t -> Allure.label(ResultsUtils.TAG_LABEL_NAME, t));
            Stream.of(scenario.tags).forEach(t -> Allure.label(ResultsUtils.TAG_LABEL_NAME, t));

        } catch (NullPointerException e) {
            // Allure not running
        }
    }

    default String toHumanReadable(String string, String otherSeparators) {
        if (StringUtils.isBlank(string)) {
            return null;
        }

        String preformattedString = StringUtils.replaceChars(string, otherSeparators, " ");

        String[] words = StringUtils.splitByCharacterTypeCamelCase(preformattedString);
        return Stream.of(words)
            .filter(s -> !StringUtils.isAllBlank(s))
            .map(
                w -> {
                    long upperCaseCount = w.chars().map(i -> (char) i).filter(Character::isUpperCase).count();

                    if (upperCaseCount > 1) {
                        return w;
                    } else {
                        return w.toLowerCase();
                    }
                })
            .collect(Collectors.joining(" "));
    }

    default void writeAllTxt(Collection<Scenario> scenarios, Optional<Path> reportDirectory) {
        Optional<String> fileName = scenarios.stream().findFirst().map(s -> FILE_PREFIX + s.getTestClass() + ".txt");
        if (fileName.isEmpty()) {
            return;
        }

        reportDirectory
            .map(d -> d.resolve(fileName.get()))
            .ifPresent(
                f -> {
                    try {
                        Scenario first = scenarios.stream().findFirst().get();
                        List<Scenario> sorted = scenarios.stream().sorted().collect(Collectors.toList());

                        long total = sorted.size();
                        long succeeded = sorted.stream().filter(s -> s.throwable == null).count();
                        long skipped = sorted.stream().filter(Scenario::isSkipped).count();

                        StringBuilder content = new StringBuilder();
                        content.append("-------------------------------------------------------------------------------\n")
                            .append(" Behaviour set: ")
                            .append(first.testClass.getName())
                            .append("\n")
                            .append(" Result ")
                            .append((succeeded < total) ? "FAILED" : "SUCCESS")
                            .append(" Total (")
                            .append(succeeded)
                            .append("/")
                            .append(total)
                            .append(") ")
                            .append(" Skipped (")
                            .append(skipped)
                            .append(") ")
                            .append("\n")
                            .append("-------------------------------------------------------------------------------\n");

                        sorted.forEach(
                            s -> {
                                content.append(s.toTxtDescription()).append(s.toTxtFooter());
                            });

                        content.append("\n")
                            .append("-------------------------------------------------------------------------------\n")
                            .append(" Result ")
                            .append((succeeded < total) ? "FAILED" : "SUCCESS")
                            .append(" Total (")
                            .append(succeeded)
                            .append("/")
                            .append(total)
                            .append(") ")
                            .append(" Skipped (")
                            .append(skipped)
                            .append(") ")
                            .append("\n")
                            .append("-------------------------------------------------------------------------------\n");

                        Files.write(f, content.toString().getBytes());
                    } catch (IOException e1) {
                        throw new RuntimeException(e1);
                    }
                });
    }

    default void writeAllXml(Collection<Scenario> scenarios, Optional<Path> reportDirectory) {
        Optional<String> fileName = scenarios.stream().findFirst().map(s -> FILE_PREFIX + s.getTestClass() + ".xml");
        if (!fileName.isPresent()) {
            return;
        }

        reportDirectory
            .map(d -> d.resolve(fileName.get()))
            .ifPresent(
                f -> {
                    try {
                        Scenario scenario = scenarios.stream().findFirst().get();
                        List<Scenario> sorted = scenarios.stream().sorted().collect(Collectors.toList());
                        long total = sorted.size();
                        long failed = sorted.stream().filter(s -> s.throwable != null).count();
                        long succeeded = sorted.stream().filter(s -> s.throwable == null).count();
                        long skipped = sorted.stream().filter(Scenario::isSkipped).count();

                        StringBuilder content = new StringBuilder();
                        content.append("<behaviours>\n");
                        content.append("\t<name>").append(scenario.testClass.getName()).append("</name>\n");
                        content.append("\t<result>").append((succeeded < total) ? "FAILED" : "SUCCESS").append("</result>\n");
                        content.append("\t<total>").append(total).append("</total>\n");
                        content.append("\t<failed>").append(failed).append("</failed>\n");
                        content.append("\t<succeded>").append(succeeded).append("</succeded>\n");
                        content.append("\t<skipped>").append(skipped).append("</skipped>\n");
                        content.append("\t<scenarios>\n");

                        sorted.forEach(
                            s -> {
                                StringBuilder sb = new StringBuilder();
                                sb.append("\t\t<scenario>\n");
                                if (StringUtils.isNotBlank(s.scenarioBase)) {
                                    sb.append("\t\t\t<base>").append(s.scenarioBase.trim()).append("</base>\n");
                                }
                                if (StringUtils.isNotBlank(s.description)) {
                                    sb.append("\t\t\t<description>").append(s.description.trim()).append("</description>\n");
                                } else {
                                    sb.append("\t\t\t<error>").append("No scenario attribute found in @Behaviour annotation and test name cannot be parsed as test<scenario>Given...When...Then...").append("</error>\n");
                                }

                                if (StringUtils.isNotBlank(s.given)) {
                                    sb.append("\t\t\t<given>").append(s.given).append("</given>\n");
                                }
                                if (StringUtils.isNotBlank(s.when)) {
                                    sb.append("\t\t\t<when>");
                                    sb.append(s.when);
                                    sb.append("</when>\n");
                                }
                                if (StringUtils.isNotBlank(s.then)) {
                                    sb.append("\t\t\t<then>");
                                    sb.append(s.then);
                                    sb.append("</then>\n");
                                }
                                sb.append("\t\t\t<test>").append(s.descriptionDisplayName).append("</test>\n");

                                if (s.scenarioTags != null && s.scenarioTags.length > 0) {
                                    sb.append("\t\t\t<tags>").append(String.join(",", s.scenarioTags)).append("</tags>\n");
                                }
                                if (s.tags != null && s.tags.length > 0) {
                                    sb.append("\t\t\t<tags>").append(String.join(",", s.tags)).append("</tags>\n");
                                }

                                if (s.throwable != null) {
                                    sb.append("\t\t\t<result>").append("ERROR").append("</result>\n");
                                    sb.append("\t\t\t<stacktrace>\n");
                                    StringWriter sw = new StringWriter();
                                    PrintWriter pw = new PrintWriter(sw);
                                    s.throwable.printStackTrace(pw);
                                    sb.append("\t").append(sw);
                                    sb.append("\t\t\t</stacktrace>\n");

                                } else if (s.isSkipped()) {
                                    sb.append("\t\t\t<result>").append("SKIP").append("</result>\n");
                                } else {
                                    sb.append("\t\t\t<result>").append("SUCESS").append("</result>\n");
                                }
                                sb.append("\t\t</scenario>\n");
                                content.append(sb);
                            });

                        content.append("\t</scenarios>\n");
                        content.append("</behaviours>\n");
                        Files.write(f, content.toString().getBytes());
                    } catch (IOException e1) {
                        throw new RuntimeException(e1);
                    }
                });
    }

    @Data
    class Scenario implements Comparable<Scenario> {
        private final Class<?> testClass;
        private final String testMethodName;
        private final boolean skipped;
        private final Throwable throwable;

        private String scenarioBase;
        private String description;
        private String[] scenarioTags;
        private String given;
        private String when;
        private String then;
        private String[] tags;
        private String descriptionDisplayName;

        private void appendHtmlL1(String value, String label, StringBuilder sb) {
            if (StringUtils.isNotBlank(value)) {
                sb.append("<br/>&emsp;").append(label).append("&nbsp;");
                sb.append(value.replaceAll(" and ", "<br/>&emsp;&nbsp;&nbsp;<b>and</b>&nbsp;"));
            }
        }

        private void appendTxtL1(String value, String label, StringBuilder sb) {
            if (StringUtils.isNotBlank(value)) {
                sb.append("\n\t").append(label).append(" ");
                sb.append(value.replaceAll(" and ", "\n\t  and "));
            }
        }

        @Override
        public int compareTo(Scenario scenario) {
            return toId().compareTo(scenario.toId());
        }

        public boolean isWellFormed() {
            return StringUtils.isNotBlank(description);
        }

        String toHtmlDescription() {
            StringBuilder sb = new StringBuilder();
            if (throwable != null) {
                sb.append("<p style='color:red'>");
            } else if (skipped) {
                sb.append("<p style='color:orange'>");
            } else {
                sb.append("<p style='color:green'>");
            }
            sb.append("<b>Scenario </b>");
            if (StringUtils.isNotBlank(scenarioBase)) {
                sb.append(scenarioBase).append(" : ");
            }
            if (StringUtils.isNotBlank(description)) {
                sb.append(description);
            } else {
                sb.append("No scenario attribute found in @Behaviour annotation and test name cannot be parsed as test<scenario>Given...When...Then...");
            }
            if (StringUtils.isNotBlank(then)) {
                appendHtmlL1(given, "<b>Given</b>", sb);
                appendHtmlL1(when, "<b>When</b>", sb);
                appendHtmlL1(then, "<b>Then</b>", sb);
                return sb.toString();
            }
            sb.append("</p>");
            return sb.toString();
        }

        public String toId() {
            return testClass + "." + testMethodName;
        }

        public String toMessage() {
            return toTxtDescription() + toTxtFooter();
        }

        public String toTxtDescription() {
            StringBuilder sb = new StringBuilder();
            sb.append("\nScenario: ");
            if (StringUtils.isNotBlank(scenarioBase)) {
                sb.append(scenarioBase).append(" : ");
            }
            if (StringUtils.isNotBlank(description)) {
                sb.append(description);
            } else {
                sb.append("No scenario attribute found in @Behaviour annotation and test name cannot be parsed as test<scenario>Given...When...Then...");
            }
            if (StringUtils.isNotBlank(then)) {
                appendTxtL1(given, "Given", sb);
                appendTxtL1(when, "When", sb);
                appendTxtL1(then, "Then", sb);
                return sb.toString();
            }
            return sb.toString();
        }

        public String toTxtFooter() {
            StringBuilder sb = new StringBuilder();
            sb.append("\n\n\t").append(descriptionDisplayName);
            if (scenarioTags != null && scenarioTags.length > 0) {
                sb.append("\n\t[").append(String.join(",", scenarioTags)).append("]");
            }
            if (tags != null && tags.length > 0) {
                sb.append("\n\t[").append(String.join(",", tags)).append("]");
            }
            if (throwable != null) {
                sb.append("\n\t<<< ERROR!");
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                throwable.printStackTrace(pw);
                sb.append("\n\t").append(sw);
            } else if (skipped) {
                sb.append("\n\t<<< SKIPPED");
            } else {
                sb.append("\n\t<<< SUCCESS");
            }
            sb.append("\n");
            return sb.toString();
        }
    }

    @Data
    class BehaviourTestListenerConfig {

        private final Optional<Path> reportDirectory;
        private final List<BehaviourDefinitionStyle> behaviourDefinitionStyles;

        public BehaviourTestListenerConfig(BehaviourDefinitionStyle... behaviourDefinitionStyles) {
            this.behaviourDefinitionStyles = Stream.of(behaviourDefinitionStyles).collect(Collectors.toList());

            Optional<Path> path = Optional.ofNullable(System.getProperty(JUNIT2BDD_REPORT_PATH, null))
                .map(Paths::get);
            path.ifPresent(
                p -> {
                    try {
                        Files.createDirectories(p);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            this.reportDirectory = path;
        }
    }
}
