package com.tincore.test.support.junit2bdd.junit5;

/*-
 * #%L
 * tincore-test-lib
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.Behaviour;
import com.tincore.test.support.junit2bdd.BehaviourDefinitionStyle;
import com.tincore.test.support.junit2bdd.BehaviourTestListener;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.tincore.test.support.junit2bdd.BehaviourDefinitionStyle.*;

public class BehaviourTestExtension implements TestWatcher, BeforeEachCallback, AfterAllCallback, BehaviourTestListener {

    private final BehaviourTestListenerConfig config;

    private final Map<String, Scenario> scenarios = new HashMap<>();

    private BehaviourTestExtension(BehaviourDefinitionStyle... behaviourDefinitionStyles) {
        this.config = new BehaviourTestListenerConfig(behaviourDefinitionStyles);
    }

    public BehaviourTestExtension() {
        this(SNAKE_CASE_CAPITAL, SNAKE_CASE, CAMEL_CASE);
    }

    @Override
    public void afterAll(ExtensionContext context) {
        flushScenarios(scenarios);
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        Scenario scenario = createScenario(context, false, null);
        if (!scenario.isWellFormed() && ENFORCE_WELL_FORMED_SCENARIOS) {
            throw new RuntimeException("Enforce BDD definition error " + scenario.toMessage());
        }
    }

    private Scenario createScenario(ExtensionContext context, boolean skipped, Throwable e) {
        Method method = context.getTestMethod().get();
        Behaviour behaviour = method.getAnnotation(Behaviour.class);
        Class<?> testClass = context.getTestClass().get();
        return createScenario(testClass, method.getName(), behaviour, context.getDisplayName(), skipped, e, config.getBehaviourDefinitionStyles());
    }

    private void displayMessage(Scenario scenario) {
        scenarios.put(scenario.toId(), scenario);
        String message = scenario.toMessage();
        System.out.println(message);
        toAllure(scenario);
    }

    public BehaviourTestListenerConfig getConfig() {
        return config;
    }

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        displayMessage(createScenario(context, true, null));
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        displayMessage(createScenario(context, false, cause));
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        displayMessage(createScenario(context, false, null));
    }
}
