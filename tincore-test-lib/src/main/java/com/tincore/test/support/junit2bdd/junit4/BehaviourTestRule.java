package com.tincore.test.support.junit2bdd.junit4;

/*-
 * #%L
 * tincore-test-lib
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.Behaviour;
import com.tincore.test.support.junit2bdd.BehaviourDefinitionStyle;
import com.tincore.test.support.junit2bdd.BehaviourTestListener;
import org.junit.AssumptionViolatedException;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.HashMap;

import static com.tincore.test.support.junit2bdd.BehaviourDefinitionStyle.CAMEL_CASE;
import static com.tincore.test.support.junit2bdd.BehaviourDefinitionStyle.SNAKE_CASE;

public class BehaviourTestRule extends TestWatcher implements BehaviourTestListener {

    private final BehaviourTestListenerConfig config;

    private final HashMap<String, Scenario> scenarios = new HashMap<>();

    private BehaviourTestRule(BehaviourDefinitionStyle... behaviourDefinitionStyles) {
        this.config = new BehaviourTestListenerConfig(behaviourDefinitionStyles);
    }

    public BehaviourTestRule() {
        this(CAMEL_CASE, SNAKE_CASE);
    }

    private Scenario createScenario(Description description, boolean skipped, Throwable e) {
        Class<?> testClass = description.getTestClass();
        String displayName = description.getDisplayName();
        String methodName = description.getMethodName();
        Behaviour behaviour = description.getAnnotation(Behaviour.class);
        return createScenario(testClass, methodName, behaviour, displayName, skipped, e, config.getBehaviourDefinitionStyles());
    }

    private void displayMessage(Scenario scenario) {
        scenarios.put(scenario.toId(), scenario);
        String message = scenario.toMessage();
        if (!scenario.isWellFormed() && ENFORCE_WELL_FORMED_SCENARIOS) {
            throw new RuntimeException("Enforce BDD definition error " + message);
        }
        System.out.println(message);
        toAllure(scenario);
    }

    @Override
    protected void failed(Throwable e, Description description) {
        super.failed(e, description);
        if (description.isSuite()) {
            flushScenarios(scenarios);
        } else {
            displayMessage(createScenario(description, false, e));
        }
    }

    @Override
    public BehaviourTestListenerConfig getConfig() {
        return config;
    }

    @Override
    protected void skipped(AssumptionViolatedException e, Description description) {
        super.skipped(e, description);
        if (description.isSuite()) {
            flushScenarios(scenarios);
        } else {
            displayMessage(createScenario(description, true, null));
        }
    }

    @Override
    protected void succeeded(Description description) {
        super.succeeded(description);
        if (description.isSuite()) {
            flushScenarios(scenarios);
        } else {
            displayMessage(createScenario(description, false, null));
        }
    }
}
