package com.tincore.test.support;

/*-
 * #%L
 * tincore-test-lib
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.Principal;
import java.time.*;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.CREATE;

public interface TestFixtureTrait {
    Exception EXCEPTION = new Exception("test");

    LocalDateTime NOW = LocalDateTime.now();
    ZonedDateTime NOWZ = ZonedDateTime.now();

    LocalDate TODAY = LocalDate.now();
    LocalDate YESTERDAY = TODAY.minusDays(1);

    String CRON_EVERY_SECOND = "* * * * * ?";

    int FILENAME_DEFAULT_RANDOM_LENGTH = 6;
    int STRING_DEFAULT_RANDOM_SUFFIX_LENGTH = 5;

    default Path createFile(Path targetDirectoryPath, String fileName, String data) {
        return createFile(targetDirectoryPath.resolve(fileName), data);
    }

    @SneakyThrows
    default Path createFile(Path targetPath, String data) {
        Files.createDirectories(targetPath.getParent());
        Files.write(targetPath, data.getBytes(Charset.defaultCharset()), CREATE);
        return targetPath;
    }

    default String createFilename(String extension) {
        return RandomStringUtils.randomAlphabetic(FILENAME_DEFAULT_RANDOM_LENGTH) + "." + extension;
    }

    default List<Path> createFiles(Path path, String... fileNames) {
        return Stream.of(fileNames).map(f -> createFile(path, f, "someData " + f)).sorted().collect(Collectors.toList());
    }

    default InputStream createInputStream(String content) {
        return new ByteArrayInputStream(content.getBytes());
    }

    default Principal createPrincipal() {
        return () -> "someNames";
    }

    default String createString(String prefix) {
        return prefix + RandomStringUtils.randomAlphabetic(STRING_DEFAULT_RANDOM_SUFFIX_LENGTH);
    }

    default XMLGregorianCalendar createXMLGregorianCalendar(LocalDateTime localDateTime) {
        Date utilDate = Date.from(localDateTime.toInstant(ZoneOffset.UTC));
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(utilDate);
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    default String getCron() {
        return CRON_EVERY_SECOND;
    }

    default ZonedDateTime toZonedDateTime(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault());
    }

    @Data
    class TestAutoCloseable implements AutoCloseable {

        private String value;
        private boolean closed;

        @Override
        public void close() {
            closed = true;
        }
    }

}
