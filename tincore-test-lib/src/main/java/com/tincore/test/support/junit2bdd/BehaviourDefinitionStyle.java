package com.tincore.test.support.junit2bdd;

/*-
 * #%L
 * tincore-test-lib
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;

import java.util.regex.Pattern;

@Data
public class BehaviourDefinitionStyle {
    public static final BehaviourDefinitionStyle CAMEL_CASE = new BehaviourDefinitionStyle("Given", "When", "Then", null);
    public static final BehaviourDefinitionStyle SNAKE_CASE = new BehaviourDefinitionStyle("_given", "_when", "_then", "_");
    public static final BehaviourDefinitionStyle SNAKE_CASE_CAPITAL = new BehaviourDefinitionStyle("_Given", "_When", "_Then", "_");

    private final String givenSeparator;
    private final String whenSeparator;
    private final String thenSeparator;
    private final String otherSeparators;
    private final Pattern pattern;

    public BehaviourDefinitionStyle(String givenSeparator, String whenSeparator, String thenSeparator, String otherSeparators) {
        this.givenSeparator = givenSeparator;
        this.whenSeparator = whenSeparator;
        this.thenSeparator = thenSeparator;
        this.otherSeparators = otherSeparators;
        pattern = Pattern.compile(".*" + givenSeparator + ".*(" + whenSeparator + ".*|)" + thenSeparator + ".*$");
    }

    public boolean isMatch(String methodName) {
        return pattern.matcher(methodName).matches();
    }
}
