package com.tincore.test.support.junit2bdd.junit5;

/*-
 * #%L
 * tincore-test-lib
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.Behaviour;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

// @Disabled
@BehaviourGroup(scenario = "behaviourGroup")
@ExtendWith(BehaviourTestExtension.class)
public class BehaviourTestExtensionTest {

    @Test
    public void otherTestNameGivenCamelCaseWithoutTestPrefixThenExecutesSuccessfully() {
        // Evaluate test name
    }

    @Test
    public void otherTestName_givenSnakeCaseWithoutTestPrefix_thenExecutesSuccessfully() {
        // Evaluate test name
    }

    @Test
    @Behaviour(given = "testWithAnnotation", then = "testSucceeds")
    public void testBehavioralNamedTestGivenCamelCaseAndAnnotationWithoutScenarioThenExecutesSuccesfully() {
        // Evaluate test name
    }

    @Test
    @Behaviour(scenario = "behaviourExtension", given = "testWithAnnotation", then = "testSucceeds")
    public void testBehavioralNamedTestWithNonMatchingNameButAnnotationWithScenario() {
        // Evaluate test name
    }

    @Test
    public void testTestNameGivenCamelCaseThenExecutesSuccessfully() {
        // Evaluate test name
    }

    @Test
    public void testTestNameGivenCamelCaseWhenHasWhenThenExecutesSuccessfully() {
        // Evaluate test name
    }

    @Test
    public void testTestName_givenSnakeCase_thenExecutesSuccessfully() {
        // Evaluate test name
    }

    @Test
    public void testTestName_givenSnakeCase_whenHasWhen_thenExecutesSuccessfully() {
        // Evaluate test name
    }
}
