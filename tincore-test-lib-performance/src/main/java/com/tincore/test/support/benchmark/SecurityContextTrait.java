package com.tincore.test.support.benchmark;

/*-
 * #%L
 * tincore-test-lib-performance
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.User;
//
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;

// SAMPLE - MOVE TO WHERE MAY BE NEEDED
public interface SecurityContextTrait {

//    default SecurityContext createSecurityContext(String username, String password, String... authorities) {
//        List<SimpleGrantedAuthority> grantedAuthorities = Stream.of(authorities).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
////                    grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + role));
////
//        User principal = new User(username, password, true, true, true, true, grantedAuthorities);
//        Authentication authentication = new UsernamePasswordAuthenticationToken(principal, principal.getPassword(), principal.getAuthorities());
//        SecurityContext context = SecurityContextHolder.createEmptyContext();
//        context.setAuthentication(authentication);
//        return context;
//    }

}
