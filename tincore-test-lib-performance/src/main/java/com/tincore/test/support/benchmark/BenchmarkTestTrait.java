package com.tincore.test.support.benchmark;

/*-
 * #%L
 * tincore-test-lib-performance
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Tag;
import org.openjdk.jmh.profile.ClassloaderProfiler;
import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.profile.StackProfiler;
import org.openjdk.jmh.results.RunResult;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.ChainedOptionsBuilder;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.text.DecimalFormat;
import java.util.Collection;

import static com.tincore.test.support.TagConstants.PERFORMANCE;
import static org.assertj.core.api.Assertions.assertThat;

@Tag(PERFORMANCE)
public interface BenchmarkTestTrait {

    DecimalFormat FORMAT = new DecimalFormat("0.000");
    int _100 = 100;

    default void assertDeviationWithin(RunResult result, double referenceScore, double maxDeviation) {
        double score = result.getPrimaryResult().getScore();
        double deviation = Math.abs(score / referenceScore - 1);
        assertThat(deviation).isLessThanOrEqualTo(maxDeviation).withFailMessage("Deviation " + FORMAT.format(deviation * _100) + "% exceeds maximum allowed deviation " + FORMAT.format(maxDeviation * _100) + "%");
    }

    default ChainedOptionsBuilder createOptionsDefaultBuilder() {
        ChainedOptionsBuilder optionsBuilder = new OptionsBuilder()
// Specify which benchmarks to run.
// You can be more specific if you'd like to run only one benchmark per test.
                .include(this.getClass().getName() + ".*")
// Set the following options as needed
//                .mode(Mode.AverageTime) // Annotation
//                .timeUnit(TimeUnit.MICROSECONDS) // Annotation
//                .warmupTime(TimeValue.seconds(1))
//                .warmupIterations(2)
//                .measurementTime(TimeValue.seconds(1))
//                .measurementIterations(2)
//                .threads(2)
//                .forks(1)
                .shouldFailOnError(false)
                .shouldDoGC(false)
                .resultFormat(ResultFormatType.TEXT)
                .result("/dev/null") // set this to a valid filename if you want reports
                //.jvmArgs("-server")
                //.jvmArgs("-XX:+UnlockDiagnosticVMOptions", "-XX:+PrintInlining")
                .addProfiler(ClassloaderProfiler.class)
                .addProfiler(GCProfiler.class)
                .addProfiler(StackProfiler.class);

        return optionsBuilder;
    }

    default void onResultsReceived(Collection<RunResult> runResults) {
//    public static final double REFERENCE_SCORE = 37.132;
//        System.out.println("onResultsReceived "+runResults);
//        assertFalse(runResults.isEmpty());
//        for (RunResult runResult : runResults) {
//            assertDeviationWithin(runResult, REFERENCE_SCORE, 0.05);
//        }
    }

    default void runBenchmarks() throws RunnerException {
        Options options = setUpOptions(createOptionsDefaultBuilder()).build();
        Collection<RunResult> runResults = new Runner(options).run();
        onResultsReceived(runResults);
    }

    default ChainedOptionsBuilder setUpOptions(ChainedOptionsBuilder optionsBuilder) {
        return optionsBuilder;
    }


}
