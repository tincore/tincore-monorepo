package com.tincore.util.form;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.Test;

import java.time.Month;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class FormMapperTraitTest extends BehaviourAwareTest {
    private final FormMapperTrait formMapperTrait = new FormMapperTrait() {};

    private final UUID uuid = UUID.randomUUID();

    @Test
    void test_toDate_Given_longMillis_Then_returnsDateWithMillisEqualToLong() {
        long l = System.currentTimeMillis();
        assertThat(formMapperTrait.toDate(l).getTime()).isEqualTo(l);
    }

    @Test
    void test_toLocalDate_Given_isoStringDate_Then_returnsLocalDateWithMatchingYearMonthAndDay() {
        assertThat(formMapperTrait.toLocalDate("2001-11-22"))
            .satisfies(
                l -> {
                    assertThat(l.getYear()).isEqualTo(2001);
                    assertThat(l.getMonth()).isEqualTo(Month.NOVEMBER);
                    assertThat(l.getDayOfMonth()).isEqualTo(22);
                });
    }

    @Test
    void test_toLocalDate_Given_nullString_Then_returnsNull() {
        assertThat(formMapperTrait.toLocalDate(null)).isNull();
    }

    @Test
    void test_toLong_Given_date_Then_returnsDateMillis() {
        Date date = new Date();
        assertThat(formMapperTrait.toLong(date)).isEqualTo(date.getTime());
    }

    @Test
    void test_toLong_Given_nullDate_Then_returnsZero() {
        assertThat(formMapperTrait.toLong(null)).isEqualTo(0);
    }

    @Test
    void test_toString_Given_charsequenceNull_Then_returnsNull() {
        assertThat(formMapperTrait.toString((CharSequence) null)).isNull();
    }

    @Test
    void test_toString_Given_charsequence_Then_returnsStringWithSameContent() {
        assertThat(formMapperTrait.toString(new StringBuilder("test"))).isEqualTo("test");
    }

    @Test
    void test_toString_Given_nullOptionalString_Then_returnsNull() {
        assertThat(formMapperTrait.toString((Optional<String>) null)).isNull();
    }

    @Test
    void test_toString_Given_optionalStringEmpty_Then_returnsNull() {
        assertThat(formMapperTrait.toString(Optional.empty())).isNull();
    }

    @Test
    void test_toString_Given_optionalString_Then_returnsOptionalContent() {
        Optional o = Optional.of("something");
        assertThat(formMapperTrait.toString(o)).isEqualTo(o.get());
    }

    @Test
    void test_toString_Given_uuidNull_Then_returnsNull() {
        assertThat(formMapperTrait.toString((UUID) null)).isNull();
    }

    @Test
    void test_toString_Given_uuid_Then_returnsUuidAsString() {
        assertThat(formMapperTrait.toString(uuid)).isEqualTo(uuid.toString());
    }

    @Test
    void test_toUuid_Given_null_Then_returnsNull() {
        assertThat(formMapperTrait.toUuid(null)).isNull();
    }

    @Test
    void test_toUuid_Given_stringUuid_Then_returnsUuid() {
        assertThat(formMapperTrait.toUuid(uuid.toString())).isEqualTo(uuid);
    }

    @Test
    void test_toZonedDateTime_Given_nullString_Then_returnsNull() {
        assertThat(formMapperTrait.toZonedDateTime(null)).isNull();
    }

    @Test
    void test_toZonedDate_Given_isoStringZonedDateTimeWithZoneId_Then_returnsLocalDateAndDateValuesMatchWithString() {
        assertThat(formMapperTrait.toZonedDateTime("2007-12-03T10:15:30+01:00[Europe/Paris]"))
            .satisfies(
                l -> {
                    assertThat(l.getYear()).isEqualTo(2007);
                    assertThat(l.getMonth()).isEqualTo(Month.DECEMBER);
                    assertThat(l.getDayOfMonth()).isEqualTo(3);
                    assertThat(l.getHour()).isEqualTo(10);
                    assertThat(l.getMinute()).isEqualTo(15);
                    assertThat(l.getSecond()).isEqualTo(30);
                    assertThat(l.getZone()).isEqualTo(ZoneId.of("Europe/Paris"));
                });
    }

    @Test
    void test_toZonedDate_Given_isoStringZonedDateTimeWithoutZoneId_Then_returnsLocalDateAndDateValuesMatchWithString() {
        assertThat(formMapperTrait.toZonedDateTime("2007-12-03T10:15:30+01:00"))
            .satisfies(
                l -> {
                    assertThat(l.getYear()).isEqualTo(2007);
                    assertThat(l.getMonth()).isEqualTo(Month.DECEMBER);
                    assertThat(l.getDayOfMonth()).isEqualTo(3);
                    assertThat(l.getHour()).isEqualTo(10);
                    assertThat(l.getMinute()).isEqualTo(15);
                    assertThat(l.getSecond()).isEqualTo(30);
                    assertThat(l.getZone()).isEqualTo(ZoneId.of("+01:00"));
                });
    }
}
