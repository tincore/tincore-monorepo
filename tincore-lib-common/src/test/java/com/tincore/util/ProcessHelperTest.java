package com.tincore.util;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.junit5.BehaviourTestExtension;
import org.apache.commons.lang3.SystemUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assumptions.assumeThat;

@ExtendWith(BehaviourTestExtension.class)
class ProcessHelperTest {

    @Test
    void test_execWithStringConsumerAndTimeoutGreaterThanZeroAndProcessExecutedOutofTimeoutLimit_Given_executed_Then_throwsInterruptedException() throws Exception {
        assumeThat(SystemUtils.IS_OS_LINUX).isTrue();

        StringBuilder response = new StringBuilder();
        assertThatExceptionOfType(IllegalThreadStateException.class).isThrownBy(() -> ProcessHelper.exec("sleep 1", response::append, 1));
    }

    @Test
    void test_execWithStringConsumerAndTimeoutGreaterThanZeroAndProcessExecutedWithingTimeoutLimit_Given_executed_Then_returnsProcessOutput() throws Exception {
        assumeThat(SystemUtils.IS_OS_LINUX).isTrue();

        StringBuilder response = new StringBuilder();
        ProcessHelper.exec("sleep 1", response::append, 1500);

        assertThat(response).isEqualToIgnoringNewLines("");
    }

    // it is flaky. Sometimes response = ""
    @Test
    @Disabled
    void test_execWithStringConsumer_Given_executed_Then_returnsProcessOutput() throws Exception {
        assumeThat(SystemUtils.IS_OS_LINUX).isTrue();

        StringBuilder response = new StringBuilder();
        ProcessHelper.exec("which ls", response::append);

        assertThat(response).endsWith("/ls");
    }
}
