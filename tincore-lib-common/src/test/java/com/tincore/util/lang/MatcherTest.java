package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class MatcherTest {

    @Test
    void test_matcherGetting_apply_Given_when_and_noOtherwise_When_noMatch_Then_throwsIllegalStateException() {
        var matcher = Matcher.<String, String>getting()
            .when(t -> t.equals("a"), "aMatch");

        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> matcher.apply("x"));
    }

    @Test
    void test_matcherGetting_apply_Given_when_with_nullOtherwise_and_notMatchingValue_Then_returnsNull() {
        var matcher = Matcher.<String, String>getting()
            .when(t -> t.equals("a"), "aMatch")
            .otherwise(null);

        assertThat(matcher.apply("x")).isNull();
    }

    @Test
    void test_matcherGetting_get_Given_when_and_otherwiseNull_When_noMatch_Then_returnsEmpty() {
        var matcher = Matcher.<String, String>getting()
            .when(t -> t.equals("a"), "aMatch")
            .otherwise(null);

        assertThat(matcher.find("x")).isEmpty();
    }

    @Test
    void test_matcherGetting_get_Given_when_and_otherwise_Then_returnsFirstMatchingValue() {
        var matcher = Matcher.<String, String>getting()
            .when(t -> t.equals("a"), "aMatch")
            .when(t -> t.equals("b"), "bMatch")
            .when(t -> t.equals("b"), "b2Match")
            .otherwise("cMatch");

        assertThat(matcher.find("a")).hasValue("aMatch");
        assertThat(matcher.find("b")).hasValue("bMatch");
        assertThat(matcher.find("x")).hasValue("cMatch");
    }

    @Test
    void test_matcherGetting_get_Given_when_without_otherwise_and_not_matchingValue_Then_returnsEmpty() {
        var matcher = Matcher.<String, String>getting()
            .when(t -> t.equals("a"), "aMatch");

        assertThat(matcher.find("x")).isEmpty();
    }
}
