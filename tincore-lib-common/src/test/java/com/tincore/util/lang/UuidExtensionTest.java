package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "Java Utility")
class UuidExtensionTest extends BehaviourAwareTest {

    @Test
    void test_uuidToBase64_Given_uuid_Then_returns22charsRepresentationAndCanBeDecoded() {
        UUID uuid = UUID.randomUUID();
        assertThat(UuidExtension.toString64(uuid)).hasSize(22).satisfies(v -> assertThat(UuidExtension.fromString64(v)).isEqualTo(uuid));
    }
}
