package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import static com.tincore.util.lang.ExceptionMapper.wrapAll;
import static com.tincore.util.lang.function.ThrowingRunnable.uRun;
import static com.tincore.util.lang.function.ThrowingRunnable.uRunnable;
import static org.assertj.core.api.Assertions.*;

class ThrowingRunnableTest {

    public static final ThrowingRunnable<RuntimeException> ILLEGAL_ARGUMENT_THROWING_RUNNABLE = () -> {
        throw new IllegalArgumentException("test");
    };

    @Test
    void test_uRun_Given_throwsError_Then_rethrows() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> uRun(() -> {
            throw new Error("unchecked");
        }, e -> e));
    }

    @Test
    void test_uRunnableWithMapAll_Given_mapAllEqualsFalse_and_uncheckedException_Then_throwsUncheckedException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> uRunnable(ILLEGAL_ARGUMENT_THROWING_RUNNABLE, e -> e).run()).withMessage("test");
    }

    @Test
    void test_uRunnableWithMapAll_Given_mapAllEqualsTrue_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uRunnable(ILLEGAL_ARGUMENT_THROWING_RUNNABLE, wrapAll(e -> new IllegalStateException(e))).run()).havingCause().withMessage("test");
    }

    @Test
    void test_uRunnable_Given_noException_Then_executesSuccessfully() {
        var ref = new AtomicReference<String>();
        uRunnable(() -> ref.set("x")).run();
        assertThat(ref).hasValue("x");
    }

    @Test
    void test_uRunnable_Given_throwingExceptionRunnable_Then_convertsCheckedIntoUncheckedException() {
        assertThatException().isThrownBy(() -> uRunnable(() -> {
            throw new Exception("test");
        }).run()).havingCause().withMessage("test");
    }

    @Test
    void test_uRunnable_Given_throwsError_Then_rethrows() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> uRunnable(() -> {
            throw new Error("unchecked");
        }).run());
    }

    @Test
    void test_uRunnable_Given_throwsUncheckedException_Then_throwsException() {
        assertThatException().isThrownBy(((ThrowingRunnable<IOException>) () -> {
            throw new RuntimeException("unchecked");
        })::run).withMessage("unchecked");
    }

}
