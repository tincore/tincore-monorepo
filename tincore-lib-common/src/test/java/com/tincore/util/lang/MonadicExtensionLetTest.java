package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.TestFixtureTrait;
import com.tincore.util.lang.tuple.Pair;
import lombok.experimental.ExtensionMethod;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.test.support.TestFixtureTrait.EXCEPTION;
import static com.tincore.util.lang.SequenceExtensionTest.*;
import static com.tincore.util.lang.function.FunctionExtension.contradiction;
import static com.tincore.util.lang.function.FunctionExtension.tautology;
import static com.tincore.util.lang.function.ThrowingFunction.uFunction;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtensionMethod({MonadicExtension.class, SequenceExtension.class})
class MonadicExtensionLetTest {

    public static final List<String> NULL_COLLECTION = null;
    public static final Map<String, String> NULL_MAP = null;
    public static final Function<String, String> DUPLICATE_STRING = x -> x + x;
    private static final String A1 = "a1";
    private static final String A2 = "a2";
    private static final String A3 = "a3";
    private static final String A4 = "a4";
    private static final String AB = "ab";
    private static final List<Pair<String, String>> NULL_ENTRY_COLLECTION = null;
    private static final Pair<String, String> NULL_ENTRY = null;

    private static List<String> createList(String... elements) {
        return Stream.of(elements).collect(Collectors.toList());
    }

    @Test
    void test_letComprehension_Given_value_Then_returnsMappingApplicationThatIsAwareOfMappedValue() {
        String actual = A.letComprehension(String::length, "%s:%d"::formatted);
        assertThat(actual).isEqualTo(A + ":1");
    }

    @Test
    void test_letComprehension_three_Given_value_Then_returnsMappingApplicationThatIsAwareOfMappedValue() {
        String actual = A.letComprehension(t -> t + "t", "%s(%s)"::formatted, "%s[%s|%s]"::formatted);
        assertThat(actual).isEqualTo("a[at|a(at)]");
    }

    @Test
    void test_letEachEntryValue_Given_null_Then_returnsNull() {
        assertThat(NULL_ENTRY_COLLECTION.letEachEntryValue(v -> "*" + v)).isNull();
    }

    @Test
    void test_letEachEntryValue_collection_Given_values_Then_returnsListWithMappedValues() {
        assertThat(List.of(
            Pair.of(A, "1"),
            Pair.of(B, "2"),
            Pair.of(C, "3"),
            Pair.of(A, "4"))
            .letEachEntryValue(v -> "*" + v)
            .asContravariantEntry()).containsExactly(
                Pair.of(A, "*1"),
                Pair.of(B, "*2"),
                Pair.of(C, "*3"),
                Pair.of(A, "*4"));
    }

    @Test
    void test_letEachEntry_Given_null_Then_returnsNull() {
        assertThat(NULL_ENTRY_COLLECTION.letEachEntry((k, v) -> "*" + v)).isNull();
    }

    @Test
    void test_letEachEntry_collection_Given_values_Then_returnsListWithMappedValues() {
        assertThat(List.of(
            Pair.of(A, "1"),
            Pair.of(B, "2"),
            Pair.of(C, "3"),
            Pair.of(A, "4")).letEachEntry((k, v) -> k + v))
            .containsExactly(
                A + "1",
                B + "2",
                C + "3",
                A + "4");
    }

    @Test
    void test_letEachEntry_map_Given_values_Then_returnsListWithMappedValues() {
        assertThat(Map.of(
            A,
            "1",
            B,
            "2",
            C,
            "3").letEachEntry((k, v) -> k + v))
            .containsExactlyInAnyOrder(
                A + "1",
                B + "2",
                C + "3");
    }

    @Test
    void test_letEachIf_Given_null_Then_returnsNull() {
        assertThat(NULL_COLLECTION.letEachIf(true, x -> x)).isNull();
        assertThat(NULL_COLLECTION.letEachIf(tautology(), x -> x)).isNull();
        assertThat(NULL_COLLECTION.letEachIf(false, x -> x)).isNull();
        assertThat(NULL_COLLECTION.letEachIf(contradiction(), x -> x)).isNull();

        var actual = NULL_COLLECTION.letEachIf(tautology(), x -> x, x -> x);
        assertThat(actual).isNull();
        var actual1 = NULL_COLLECTION.letEachIf(contradiction(), x -> x, x -> x);
        assertThat(actual1).isNull();
        var actual2 = NULL_COLLECTION.letEachIf(true, x -> x, x -> x);
        assertThat(actual2).isNull();
        var actual3 = NULL_COLLECTION.letEachIf(false, x -> x, x -> x);
        assertThat(actual3).isNull();
    }

    @Test
    void test_letEachIf_booleanOneFunction_Given_matchFirstPredicate_Then_executesFunction() {
        assertThat(List.of(A).letEachIf(true, APPEND_1)).containsExactly(A1);
    }

    @Test
    void test_letEachIf_booleanOneFunction_Given_noMatchFirstPredicate_Then_doesNotExecuteFunction() {
        assertThat(List.of(A).letEachIf(false, APPEND_1)).containsExactly(A);
    }

    @Test
    void test_letEachIf_onePredicateOneFunction_Given_matchFirstPredicate_Then_executesFunction() {
        assertThat(List.of(A).letEachIf(A::equals, APPEND_1)).containsExactly(A1);
        assertThat(List.of(A).letEachIf(tautology(), APPEND_1)).containsExactly(A1);
    }

    @Test
    void test_letEachIf_onePredicateOneFunction_Given_noMatchFirstPredicate_Then_doesNotExecuteFunction() {
        assertThat(List.of(A).letEachIf(contradiction(), APPEND_1)).containsExactly(A);
    }

    @Test
    void test_letEachIf_onePredicateTwoFunctions_Given_matchFirstPredicate_Then_executesFirstFunction_and_doesNotExecuteSecondFunction() {
        assertThat(List.of(A).letEachIf(A::equals, APPEND_1, APPEND_2)).containsExactly(A1);
        assertThat(List.of(A).letEachIf(tautology(), APPEND_1, APPEND_2)).containsExactly(A1);
    }

    @Test
    void test_letEachIf_onePredicateTwoFunctions_Given_noMatchFirstPredicate_Then_doesNotExecutesFirstFunction_and_executesSecondFunction() {
        assertThat(List.of(A).letEachIf(contradiction(), APPEND_1, APPEND_2)).containsExactly(A2);
    }

    @Test
    void test_letEachPresent_Given_collection_Then_returnsListOfPresentOptionals() {
        assertThat(List.of(Optional.of(A), Optional.of(B), Optional.of(C), Optional.empty()).letEachPresent()).containsExactly(A, B, C);
    }

    @Test
    void test_letEachPresent_Given_null_Then_returnsNull() {
        assertThat(((List<Optional<String>>) null).letEachPresent()).isNull();
    }

    @Test
    void test_letEachPresent_mapper_Given_collection_Then_returnsListOfPresentOptionals() {
        assertThat(Arrays.asList(A, B, C, null).letEachPresent(s -> Optional.ofNullable(s).filter(A::equals))).containsExactly(A);
    }

    @Test
    void test_letEachPresent_mapper_Given_null_Then_returnsNull() {
        assertThat(((List<String>) null).letEachPresent(Optional::ofNullable)).isNull();
    }

    @Test
    void test_letEachToPair_Given_nullValues_Then_returnsNullElements() {
        assertThat(Arrays.asList(null, B, null, C).letEachToPair(x -> "l" + x, x -> "r" + x))
            .containsExactly(
                null,
                Pair.of("l" + B, "r" + B),
                null,
                Pair.of("l" + C, "r" + C));
    }

    @Test
    void test_letEachToPair_Given_null_Then_returnsNull() {
        assertThat(NULL_COLLECTION.letEachToPair(x -> x, x -> x)).isNull();
    }

    @Test
    void test_letEachToPair_Given_values_Then_returnsCollectionOfPairs() {
        assertThat(List.of(A, B, C).letEachToPair(x -> "l" + x, x -> "r" + x))
            .containsExactly(
                Pair.of("l" + A, "r" + A),
                Pair.of("l" + B, "r" + B),
                Pair.of("l" + C, "r" + C));
    }

    @Test
    void test_letEachToPair_map_Given_null_Then_returnsNull() {
        assertThat(NULL_MAP.letEachToPair(x -> x, x -> x)).isNull();
    }

    @Test
    void test_letEachToPair_map_Given_values_Then_returnsCollectionOfPairs() {
        assertThat(Map.of(A, "1", B, "2", C, "3").letEachToPair(x -> "l" + x, x -> "r" + x))
            .containsExactlyInAnyOrder(
                Pair.of("l" + A, "r1"),
                Pair.of("l" + B, "r2"),
                Pair.of("l" + C, "r3"));
    }

    @Test
    void test_letEach_Given_collection_Then_returnsListWithMappedValues() {
        assertThat(List.of(A, B, C, A).letEach(v -> "*" + v)).containsExactly("*" + A, "*" + B, "*" + C, "*" + A);
    }

    @Test
    void test_letEach_Given_null_Then_returnsNull() {
        assertThat(NULL_COLLECTION.letEach(v -> "*" + v)).isNull();
    }

    @Test
    void test_letEntry_Given_entry_Then_returnsMappedEntry() {
        var actual = Pair.of(A, B).letEntry((k, v) -> k + v);
        assertThat(actual).isEqualTo(A + B);
    }

    @Test
    void test_letEntry_Given_null_Then_returnsNull() {
        var actual = NULL_ENTRY.letEntry((k, v) -> "*" + v);
        assertThat(actual).isNull();
    }

    @Test
    void test_letFirst_Given_notEmpty_Then_returnsFirstValue() {
        assertThat(createList(null, A).letFirst()).isNull();
        assertThat(createList(A, B).letFirst()).isEqualTo(A);
    }

    @Test
    void test_letFirst_Given_null_Then_returnsNull() {
        assertThat(((List<String>) null).letFirst()).isNull();
    }

    @Test
    void test_letFirst_array_Given_notEmpty_Then_returnsFirstValue() {
        assertThat(new String[]{null, A}.letFirst()).isNull();
        assertThat(new String[]{A, B}.letFirst()).isEqualTo(A);
    }

    @Test
    void test_letFirst_array_Given_null_Then_returnsNull() {
        assertThat(((String[]) null).letFirst()).isNull();
    }

    @Test
    void test_letFirst_array_mapper_Given_notEmptyl_Then_returnsFirstValueMapped() {
        var actual = new String[]{null, A}.letFirst(DUPLICATE_STRING);
        assertThat(actual).isNull();
        var actual1 = new String[]{A, B}.letFirst(DUPLICATE_STRING);
        assertThat(actual1).isEqualTo(A + A);
    }

    @Test
    void test_letFirst_array_mapper_Given_null_Then_returnsNull() {
        var actual = ((String[]) null).letFirst(x -> x);
        assertThat(actual).isNull();
    }

    @Test
    void test_letFirst_mapper_Given_notEmpty_Then_returnsFirstValueMapped() {
        var actual = createList(null, A).letFirst(DUPLICATE_STRING);
        assertThat(actual).isNull();
        var actual1 = createList(A, B).letFirst(DUPLICATE_STRING);
        assertThat(actual1).isEqualTo(A + A);
    }

    @Test
    void test_letFirst_mapper_Given_null_Then_returnsNull() {
        var actual = ((List<String>) null).letFirst(x -> x);
        assertThat(actual).isNull();
    }

    @Test
    void test_letIf_Given_null_Then_returnsNull() {
        assertThat(NULL.letIf(true, x -> x)).isNull();
        assertThat(NULL.letIf(tautology(), x -> x)).isNull();
        assertThat(NULL.letIf(false, x -> x)).isNull();
        assertThat(NULL.letIf(contradiction(), x -> x)).isNull();

        var actual = NULL.letIf(tautology(), x -> x, x -> x);
        assertThat(actual).isNull();
        var actual1 = NULL.letIf(contradiction(), x -> x, x -> x);
        assertThat(actual1).isNull();
        var actual2 = NULL.letIf(true, x -> x, x -> x);
        assertThat(actual2).isNull();
        var actual3 = NULL.letIf(false, x -> x, x -> x);
        assertThat(actual3).isNull();

        assertThat(NULL.letIf(Matcher.<String, String>applying()
            .when(tautology(), APPEND_1))).isNull();
    }

    @Test
    void test_letIf_Matcher_threePredicatesFourFunctions_Given_matchSecondPredicate_Then_executesSecondFunction_and_doesNotExecuteOtherFunctions() {
        assertThat(A.letIf(Matcher.<String, String>applying()
            .when(contradiction(), APPEND_1)
            .when(tautology(), APPEND_2)
            .when(tautology(), APPEND_3)
            .when(tautology(), APPEND_4))).isEqualTo(A2);
    }

    @Test
    void test_letIf_booleanOneFunction_Given_matchFirstPredicate_Then_executesFunction() {
        assertThat(A.letIf(true, APPEND_1)).isEqualTo(A1);
    }

    @Test
    void test_letIf_booleanOneFunction_Given_noMatchFirstPredicate_Then_doesNotExecuteFunction() {
        assertThat(A.letIf(false, APPEND_1)).isEqualTo(A);
    }

    @Test
    void test_letIf_onePredicateOneFunction_Given_matchFirstPredicate_Then_executesFunction() {
        assertThat(A.letIf(tautology(), APPEND_1)).isEqualTo(A1);
    }

    @Test
    void test_letIf_onePredicateOneFunction_Given_noMatchFirstPredicate_Then_doesNotExecuteFunction() {
        assertThat(A.letIf(contradiction(), APPEND_1)).isEqualTo(A);
    }

    @Test
    void test_letIf_onePredicateTwoFunctions_Given_matchFirstPredicate_Then_executesFirstFunction_and_doesNotExecuteSecondFunction() {
        assertThat(A.letIf(tautology(), APPEND_1, APPEND_2)).isEqualTo(A1);
    }

    @Test
    void test_letIf_onePredicateTwoFunctions_Given_noMatchFirstPredicate_Then_doesNotExecutesFirstFunction_and_executesSecondFunction() {
        assertThat(A.letIf(contradiction(), APPEND_1, APPEND_2)).isEqualTo(A2);
    }

    @Test
    void test_letIf_threePredicatesFourFunctions_Given_matchFirstPredicate_Then_executesFirstFunction_and_doesNotExecuteOtherFunctions() {
        assertThat(A.letIf(tautology(), APPEND_1, tautology(), APPEND_2, tautology(), APPEND_3, APPEND_4)).isEqualTo(A1);
    }

    @Test
    void test_letIf_threePredicatesFourFunctions_Given_matchSecondPredicate_Then_executesSecondFunction_and_doesNotExecuteOtherFunctions() {
        assertThat(A.letIf(contradiction(), APPEND_1, tautology(), APPEND_2, tautology(), APPEND_3, APPEND_4)).isEqualTo(A2);
    }

    @Test
    void test_letIf_threePredicatesFourFunctions_Given_noMatchSecondPredicate_Then_executesThirdFunction_and_doesNotExecuteOtherFunctions() {
        assertThat(A.letIf(contradiction(), APPEND_1, contradiction(), APPEND_2, tautology(), APPEND_3, APPEND_4)).isEqualTo(A3);
    }

    @Test
    void test_letIf_threePredicatesFourFunctions_Given_noMatchThirdPredicate_Then_executesLastFunction_and_doesNotExecuteOtherFunctions() {
        assertThat(A.letIf(contradiction(), APPEND_1, contradiction(), APPEND_2, contradiction(), APPEND_3, APPEND_4)).isEqualTo(A4);
    }

    @Test
    void test_letIf_twoPredicatesThreeFunctions_Given_matchFirstPredicate_Then_executesFirstFunction_and_doesNotExecuteOtherFunctions() {
        assertThat(A.letIf(tautology(), APPEND_1, tautology(), APPEND_2, APPEND_3)).isEqualTo(A1);
    }

    @Test
    void test_letIf_twoPredicatesThreeFunctions_Given_matchSecondPredicate_Then_executesSecondFunction_and_doesNotExecuteOtherFunctions() {
        assertThat(A.letIf(contradiction(), APPEND_1, tautology(), APPEND_2, APPEND_3)).isEqualTo(A2);
    }

    @Test
    void test_letIf_twoPredicatesThreeFunctions_Given_noMatchSecondPredicate_Then_executeslastFunction_and_doesNotExecuteOtherFunctions() {
        assertThat(A.letIf(contradiction(), APPEND_1, contradiction(), APPEND_2, APPEND_3)).isEqualTo(A3);
    }

    @Test
    void test_letLast_Given_notEmpty_Then_returnsLastValue() {
        var actual = createList(A, null).letLast();
        assertThat(actual).isNull();
        var actual1 = createList(A, B).letLast();
        assertThat(actual1).isEqualTo(B);
    }

    @Test
    void test_letLast_Given_null_Then_returnsNull() {
        var actual = ((List<String>) null).letLast();
        assertThat(actual).isNull();
    }

    @Test
    void test_letLast_array_Given_notEmpty_Then_returnsLastValue() {
        var actual = new String[]{null, A}.letLast();
        assertThat(actual).isEqualTo(A);
        var actual1 = new String[]{A, B}.letLast();
        assertThat(actual1).isEqualTo(B);
    }

    @Test
    void test_letLast_array_Given_null_Then_returnsNull() {
        var actual = ((String[]) null).letLast();
        assertThat(actual).isNull();
    }

    @Test
    void test_letLast_array_mapper_Given_notEmpty_Then_returnsLastValueMapped() {
        var actual = new String[]{null, A}.letLast(DUPLICATE_STRING);
        assertThat(actual).isEqualTo(A + A);
        var actual1 = new String[]{A, B}.letLast(DUPLICATE_STRING);
        assertThat(actual1).isEqualTo(B + B);
    }

    @Test
    void test_letLast_array_mapper_Given_null_Then_returnsNull() {
        var actual = ((String[]) null).letLast(x -> x);
        assertThat(actual).isNull();
    }

    @Test
    void test_letLast_mapper_Given_notEmpty_Then_returnsLastValueMapped() {
        var actual = createList(A, null).letLast(DUPLICATE_STRING);
        assertThat(actual).isNull();
        var actual1 = createList(A, B).letLast(DUPLICATE_STRING);
        assertThat(actual1).isEqualTo(B + B);
    }

    @Test
    void test_letLast_mapper_Given_null_Then_returnsNull() {
        var actual = ((List<String>) null).letLast(x -> x);
        assertThat(actual).isNull();
    }

    @Test
    void test_letResource_Given_exception_Then_constructsResource_and_throwsException_and_closesResource() {
        var reference = new AtomicReference<TestFixtureTrait.TestAutoCloseable>();
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> A.letResource(s -> {
            TestFixtureTrait.TestAutoCloseable testAutoCloseable = new TestFixtureTrait.TestAutoCloseable();
            testAutoCloseable.setValue(s);
            reference.set(testAutoCloseable);
            return testAutoCloseable;
        },
            ExceptionMapper.wrapChecked(),
            uFunction(t -> {
                throw EXCEPTION;
            })))
            .withCause(EXCEPTION);

        assertThat(reference).hasValueSatisfying(r -> {
            assertThat(r.isClosed()).isTrue();
            assertThat(r.getValue()).isEqualTo(A);
        });
    }

    @Test
    void test_letResource_Given_noException_Then_constructsResource_and_appliesValue_and_closesResource() {
        var reference = new AtomicReference<TestFixtureTrait.TestAutoCloseable>();
        var result = A.letResource(
            s -> {
                TestFixtureTrait.TestAutoCloseable testAutoCloseable = new TestFixtureTrait.TestAutoCloseable();
                reference.set(testAutoCloseable);
                return testAutoCloseable;
            },
            ExceptionMapper.wrapChecked(),
            t -> B);

        assertThat(reference.get().isClosed()).isTrue();
        assertThat(result).isEqualTo(B);
    }

    @Test
    void test_letToPair_Given_null_Then_returnsNull() {
        assertThat(NULL.letToPair(x -> x, x -> x)).isNull();
    }

    @Test
    void test_letToPair_Given_value_Then_returnsPairs() {
        assertThat(A.letToPair(x -> "l" + x, x -> "r" + x))
            .isEqualTo(Pair.of("l" + A, "r" + A));
    }

    @Test
    void test_letTry_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(RuntimeException.class)
            .isThrownBy(() -> A.letTry(a -> {
                throw EXCEPTION;
            }))
            .withCause(EXCEPTION);
    }

    @Test
    void test_letTry_Given_noException_Then_executesConsumer() {
        String actual = A.letTry(a -> B, (v, e) -> new IllegalArgumentException(e));
        assertThat(actual).isEqualTo(B);
    }

    @Test
    void test_letTry_exceptionMapper_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> A.letTry(a -> {
            throw EXCEPTION;
        }, (v, e) -> new IllegalArgumentException(e)));
    }

    @Test
    void test_letWith_Given_null_Then_returnsFunctionAppliedToCurrentObjectAndParameter() {
        var actual = NULL.letWith(B, CONCAT_STRINGS);
        assertThat(actual).isNull();
    }

    @Test
    void test_letWith_Given_value_Then_returnsFunctionAppliedToCurrentObjectAndParameter() {
        var actual = A.letWith(B, CONCAT_STRINGS);
        assertThat(actual).isEqualTo(AB);
    }

    @Test
    void test_letWith_predicate_Given_nullCombined_Then_returnsValue() {
        assertThat(A.letWith(NULL, Objects::nonNull, (a, b) -> a + b)).isEqualTo(A);
    }

    @Test
    void test_letWith_predicate_Given_nullValue_Then_returnsNull() {
        assertThat(NULL.letWith(B, Objects::nonNull, (a, b) -> "x")).isNull();
    }

    @Test
    void test_letWith_predicate_Given_valueValue_Then_returnsValueCombined() {
        assertThat(A.letWith(B, Objects::nonNull, (a, b) -> a + b)).isEqualTo("ab");
    }

    @Test
    void test_let_Given_nullValue_Then_returnsNull() {
        var actual = NULL.let(x -> 0);
        assertThat(actual).isNull();
    }

    @Test
    void test_let_Given_value_Then_returnsAppliedValue() {
        var actual = A.let(x -> 0);
        assertThat(actual).isZero();
    }

}
