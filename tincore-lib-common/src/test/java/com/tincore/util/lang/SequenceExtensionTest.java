package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.tuple.Pair;
import lombok.experimental.ExtensionMethod;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.tincore.test.support.TestFixtureTrait.EXCEPTION;
import static com.tincore.util.lang.MonadicExtensionLetTest.NULL_COLLECTION;
import static com.tincore.util.lang.SequenceExtension.toSortedMap;
import static com.tincore.util.lang.function.FunctionExtension.contradiction;
import static com.tincore.util.lang.function.FunctionExtension.tautology;
import static org.assertj.core.api.Assertions.*;

@ExtensionMethod({SequenceExtension.class})
class SequenceExtensionTest {

    public static final Function<String, String> APPEND_1 = p -> p + "1";
    public static final Function<String, String> APPEND_2 = p -> p + "2";
    public static final Function<String, String> APPEND_3 = p -> p + "3";
    public static final Function<String, String> APPEND_4 = p -> p + "4";

    public static final Function<String, String> ADD_SQUARE_BRACKETS = s -> "[" + s + "]";
    public static final Function<String, String> ADD_PARENTHESES = s -> "(" + s + ")";
    public static final BiFunction<String, String, String> CONCAT_STRINGS = (a, b) -> a + b;

    public static final String A = "a";
    public static final String B = "b";
    public static final String C = "c";
    public static final String D = "d";
    public static final String E = "e";
    public static final String F = "f";
    public static final String G = "g";
    public static final String H = "h";
    public static final String NULL = null;

    public static final String[] NULL_ARRAY = null;

    public static final Predicate<String> IS_LETTER_A = s -> s.equals(A);
    public static final Predicate<String> IS_LETTER_B = s -> s.equals(B);

    private final AtomicBoolean boolean1Called = new AtomicBoolean();
    private final Consumer<String> booleanConsumer1 = r -> boolean1Called.set(true);
    private final AtomicReference<String> stringConsumer1CalledValue = new AtomicReference<>();
    private final Consumer<String> stringConsumer1 = stringConsumer1CalledValue::set;

    private static void assertThatEntryEquals(Entry<?, ?> p, Object keyEquals, Object valueEquals) {
        assertThat(p.getKey()).isEqualTo(keyEquals);
        assertThat(p.getValue()).isEqualTo(valueEquals);
    }

    private void assertThatValueSatisfies(List<? extends Entry<String, Integer>> l, int index, String expectedKey, int expectedValue) {
        assertThat(l.get(index)).satisfies(e -> {
            assertThat(e.getKey()).isEqualTo(expectedKey);
            assertThat(e.getValue()).isEqualTo(expectedValue);
        });
    }

    private <T> Iterable<T> createIterable(T... values) {
        return () -> List.of(values).iterator();
    }

    @Test
    void test_all_Given_allMatch_Then_returnsTrue() {
        assertThat(List.of(A, A, A).all(IS_LETTER_A)).isTrue();
        assertThat(new String[]{A, A, A}.all(IS_LETTER_A)).isTrue();
    }

    @Test
    void test_all_Given_anyNotMatch_Then_returnsFalse() {
        assertThat(List.of(A, B, C).all(IS_LETTER_A)).isFalse();
        assertThat(new String[]{A, B, A}.all(IS_LETTER_A)).isFalse();
    }

    @Test
    void test_all_Given_empty_Then_returnsTrue() {
        assertThat(Collections.emptyList().all(contradiction())).isTrue();
        assertThat(new String[]{}.all(contradiction())).isTrue();
    }

    @Test
    void test_any_Given_anyMatch_Then_returnsTrue() {
        assertThat(List.of(A, B, C).any(IS_LETTER_A)).isTrue();
        assertThat(new String[]{A, B, C}.any(IS_LETTER_A)).isTrue();
    }

    @Test
    void test_any_Given_empty_Then_returnsFalse() {
        assertThat(Collections.emptyList().any(contradiction())).isFalse();
        assertThat(new String[]{}.any(contradiction())).isFalse();
    }

    @Test
    void test_any_Given_noMatch_Then_returnsFalse() {
        assertThat(List.of(B, C, D).any(IS_LETTER_A)).isFalse();
    }

    @Test
    void test_asContravariant_list_Given_null_Then_returnsNull() {
        assertThat(((List<String>) null).asContravariant(Object.class)).isNull();
    }

    @Test
    void test_asContravariant_list_Given_values_Then_returnsListOfSpecifiedSuperType() {
        var contravariant = List.of(A, B, C).asContravariant(Object.class);
        assertThat(contravariant).containsExactly(A, B, C);

        List<? extends String> widened = List.of(A, B, C);
        assertThat(SequenceExtension.<String>asContravariant(widened)).containsExactly(A, B, C);
    }

    @Test
    void test_asContravariant_stream_Given_null_Then_returnsNull() {
        assertThat(((Stream<? extends Number>) null).asContravariant()).isNull();
    }

    @Test
    void test_asContravariant_stream_Given_values_Then_returnsStreamOfSpecifiedSuperType() {
        var contravariant = Stream.of(A, B, C).asContravariant(Object.class);
        assertThat(contravariant).containsExactly(A, B, C);

        Stream<? extends String> widened = Stream.of(A, B, C);
        assertThat(SequenceExtension.<String>asContravariant(widened)).containsExactly(A, B, C);
    }

    @Test
    void test_associate_collection_Given_empty_Then_returnsEmpty() {
        assertThat(Collections.<String>emptyList().associate(List.of("x"), String::length, String::length)).isEmpty();
    }

    @Test
    void test_associate_collection_Given_matchingKeys_Then_returnsEntriesWithMatchingKeys() {
        var associated = List.of("a", "ab", "abc")
            .associate(List.of("1", "12", "1234"), String::length, String::length);
        assertThat(associated).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, "a", "1"))
            .anySatisfy(e -> assertThatEntryEquals(e, "ab", "12"))
            .anySatisfy(e -> assertThatEntryEquals(e, "abc", null));
    }

    @Test
    void test_associate_stream_Given_empty_Then_returnsEmpty() {
        var associated = Stream.<String>empty().associate(Stream.of("x"), String::length, String::length);
        assertThat(associated).isEmpty();
    }

    @Test
    void test_associate_stream_Given_matchingKeys_Then_returnsEntriesWithMatchingKeys() {
        var associated = Stream.of("a", "ab", "abc")
            .associate(Stream.of("1", "12", "1234"), String::length, String::length);
        assertThat(associated).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, "a", "1"))
            .anySatisfy(e -> assertThatEntryEquals(e, "ab", "12"))
            .anySatisfy(e -> assertThatEntryEquals(e, "abc", null));
    }

    @Test
    void test_chunked_collection_Given_empty_Then_returnsEmpty() {
        assertThat(Collections.emptyList().chunked(2)).isEmpty();
    }

    @Test
    void test_chunked_collection_Given_longerThanSize_Then_returnsListsContainingGroups() {
        assertThat(List.of(A, B, C, D, E).chunked(2)).containsExactly(List.of(A, B), List.of(C, D), List.of(E));
        assertThat(List.of(A, B, C, D, E).chunked(3)).containsExactly(List.of(A, B, C), List.of(D, E));
    }

    @Test
    void test_chunked_collection_Given_shorterThanSize_Then_returnsListsContainingItems() {
        assertThat(List.of(A).chunked(2)).containsExactly(List.of(A));
        assertThat(List.of(A, B).chunked(3)).containsExactly(List.of(A, B));
    }

    @Test
    void test_chunked_stream_Given_empty_Then_returnsEmpty() {
        assertThat(Stream.empty().chunked(2)).isEmpty();
    }

    @Test
    void test_chunked_stream_Given_longerThanSize_Then_returnsListsContainingGroups() {
        assertThat(List.of(A, B, C, D, E).chunked(2)).containsExactly(List.of(A, B), List.of(C, D), List.of(E));
        assertThat(List.of(A, B, C, D, E).chunked(3)).containsExactly(List.of(A, B, C), List.of(D, E));
    }

    @Test
    void test_chunked_stream_Given_shorterThanSize_Then_returnsStreamsContainingItems() {
        assertThat(Stream.of(A).chunked(2).map(Stream::toList).toList()).containsExactly(List.of(A));
        assertThat(Stream.of(A, B).chunked(3).map(Stream::toList).toList()).containsExactly(List.of(A, B));
    }

    @Test
    void test_concatWith_streamCollection_Given_Then_returnsListPairedElementsWithSameIndex() {
        assertThat(Stream.of(A, B, C).concatWith(List.of(1, 2, 3))).containsExactly(A, B, C, 1, 2, 3);
        assertThat(Stream.of(A, B, C).concatWith(1, 2, 3)).containsExactly(A, B, C, 1, 2, 3);
        assertThat(Stream.of(A, B, C).concatWith(Stream.of(1, 2, 3))).containsExactly(A, B, C, 1, 2, 3);
    }

    @Test
    void test_concat_Given_streams_Then_returnsStreamContainingAllValues() {
        assertThat(SequenceExtension.concat(Stream.of(A, B, C), Stream.of(D, E), Stream.of(A, B))).containsExactly(A, B, C, D, E, A, B);
    }

    @Test
    void test_difference_Given_sameElements_Then_returnsEmpty() {
        assertThat(List.of().difference(List.of())).isEmpty();
        assertThat(List.of(A, B, C).difference(List.of(A, B, C))).isEmpty();
    }

    @Test
    void test_difference_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of().difference(List.of(A, B))).isEmpty();
        assertThat(List.of(A, B, C).difference(List.of(A, B))).containsExactly(C);
        assertThat(List.of(A, B, C).difference(List.of())).containsExactly(A, B, C);
    }

    @Test
    void test_difference_extractor_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of("A", "AA", "AAA").difference(List.of("B", "BB", "BBB"), String::length)).isEmpty();
        assertThat(List.of().difference(List.of(A, B), String::length)).isEmpty();
        assertThat(List.of("AA", "BBB", C, C).difference(List.of("01", "012"), String::length)).containsExactly(C, C);
        assertThat(List.of(A, B, C).difference(List.of(), String::length)).containsExactly(A, B, C);
    }

    @Test
    void test_difference_extractors_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of("AA", "BB", "CC")
            .difference(List.of("XA", "YB", "ZC"), s -> s.charAt(0), s -> s.charAt(1))).isEmpty();
        assertThat(List.<String>of().difference(List.of("AA", "BB"), s -> s.charAt(0), s -> s.charAt(1))).isEmpty();
        assertThat(List.of("A_", "B_", "C_", "C_")
            .difference(List.of("_A", "_B"), s -> s.charAt(0), s -> s.charAt(1))).containsExactly("C_", "C_");
        assertThat(List.of("A_", "B_", "C_")
            .difference(List.<String>of("A_", "B_", "C_"), s -> s.charAt(0), s -> s.charAt(1))).containsExactly("A_", "B_", "C_");
    }

    @Test
    void test_distinctByKey_collection_Given_values_Then_returnsFilteredStreamRemovingValuesWithSameExtractedValue() {
        assertThat(List.of(A, B, C + C, "d").distinctByKey(String::length)).containsExactly(A, C + C);
    }

    @Test
    void test_distinctByKey_stream_Given_values_Then_returnsFilteredStreamRemovingValuesWithSameExtractedValue() {
        assertThat(Stream.of(A, B, C + C, "d").distinctByKey(String::length)).containsExactly(A, C + C);
    }

    @Test
    void test_distinctBy_collection_Given_values_Then_returnsFilteredStreamRemovingValuesWithSameExtractedValue() {
        assertThat(List.of(A, B, C + C, "d")
            .distinctBy(Distinctors.extracting(String::length))).containsExactly(A, C + C);
    }

    @Test
    void test_distinctBy_stream_Given_Values_Then_returnsFilteredStreamRemovingValuesWithSameExtractedValue() {
        assertThat(Stream.of(A, B, C + C, "d")
            .distinctBy(Distinctors.extracting(String::length))).containsExactly(A, C + C);
    }

    @Test
    void test_filterEntry_map_Given_entryMatchesPredicate_Then_returnsEntries() {
        assertThat(Map.of(A, 1, B, 2).filterEntry((a, b) -> a.equals(A) && b == 1)).hasSize(1).containsEntry(A, 1);
    }

    @Test
    void test_filterEntry_optional_Given_valueDoesNotMatchPredicate_Then_returnsEmpty() {
        assertThat(Optional.of(Pair.of(A, "1")).filterEntry((a, b) -> a.equals("z") && b.equals("1"))).isEmpty();
    }

    @Test
    void test_filterEntry_optional_Given_valueMatchesPredicate_Then_returnsOptionWithValue() {
        assertThat(Optional.of(Pair.of(A, "1"))
            .filterEntry((a, b) -> a.equals(A) && b.equals("1"))).hasValue(Pair.of(A, "1"));
    }

    @Test
    void test_filterEntry_stream_Given_entryMatchesPredicate_Then_returnsEntries() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2"))
            .filterEntry((a, b) -> a.equals(A) && b.equals("1"))).containsExactly(Pair.of(A, "1"));
    }

    @Test
    void test_filterIndexed_Given_predicate_Then_predicateIsIndexAware() {
        assertThat(Stream.empty().filterIndexed((v, i) -> i % 2 == 0)).isEmpty();
        assertThat(Stream.of(A, B, C).filterIndexed((v, i) -> i % 2 == 0)).containsExactly(A, C);
    }

    @Test
    void test_filterInstanceOf_Given_sequence_Then_returnsElements() {
        assertThat(Stream.of(A, B, BigDecimal.valueOf(1), 2.0).filterInstanceOf(String.class)).containsExactly(A, B);
        assertThat(Stream.of(A, B, BigDecimal.valueOf(1), 2.0)
            .filterInstanceOf(Number.class)).containsExactly(BigDecimal.valueOf(1), 2.0);
    }

    @Test
    void test_filterInstanceOf_optional_Given_sequence_Then_returnsElements() {
        assertThat(Optional.of(BigDecimal.valueOf(1)).filterInstanceOf(String.class)).isEmpty();
        assertThat(Optional.of(BigDecimal.valueOf(1)).filterInstanceOf(Number.class)).hasValue(BigDecimal.valueOf(1));
    }

    @Test
    void test_filterKeyInstanceOf_stream_Given_sequence_and_value_instanceOfType_Then_returnsValue() {
        assertThat(Stream.of(Pair.of(A, A), Pair.of(A, B), Pair.of(BigDecimal.valueOf(1), A), Pair.of(2.0, A))
            .filterKeyInstanceOf(String.class)).hasSize(2)
            .anySatisfy(p -> assertThatEntryEquals(p, A, A))
            .anySatisfy(p -> assertThatEntryEquals(p, A, B));
        assertThat(Stream.of(Pair.of(A, A), Pair.of(A, B), Pair.of(BigDecimal.valueOf(1), A), Pair.of(2.0, A))
            .filterKeyInstanceOf(Number.class)).hasSize(2)
            .anySatisfy(p -> assertThatEntryEquals(p, BigDecimal.valueOf(1), A))
            .anySatisfy(p -> assertThatEntryEquals(p, 2.0, A));
    }

    @Test
    void test_filterKeyOptional_Given_sequence_and_itemKeyValueDoesNotMatchPredicate_Then_returnsItem() {
        assertThat(Optional.of(Pair.of(A, "1")).filterKey(s -> s.equals("z"))).isEmpty();
    }

    @Test
    void test_filterKeyOptional_Given_sequence_and_itemKeyValueMatchesPredicate_Then_returnsItem() {
        assertThat(Optional.of(Pair.of(A, "1")).filterKey(IS_LETTER_A)).hasValue(Pair.of(A, "1"));
    }

    @Test
    void test_filterKey_Given_sequence_and_itemKeyValueMatchesPredicate_Then_returnsItem() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2")).filterKey(IS_LETTER_A)).containsExactly(Pair.of(A, "1"));
    }

    @Test
    void test_filterKey_map_Given_mapKeyMatchesPredicate_Then_returnsMapWithMatchingKeyEntries() {
        assertThat(Map.of(A, 1, B, 2, C, 1).filterKey(k -> !k.equals(B))).hasSize(2)
            .containsEntry(A, 1)
            .containsEntry(C, 1);
    }

    @Test
    void test_filterNonNullWithExtractor_Given_optionEmpty_Then_returnsEmpty() {
        assertThat(Optional.<Pair<String, String>>empty().filterNonNull(Pair::getValue)).isEmpty();
    }

    @Test
    void test_filterNonNullWithExtractor_Given_sequenceWithNullValues_Then_returnsSequenceWithoutNullValues() {
        assertThat(Stream.<Pair<String, String>>of(Pair.of(A, A), Pair.of(B, A), Pair.of(null, C), Pair.of(C, null))
            .filterNonNull(Pair::getValue)).containsExactly(Pair.of(A, A), Pair.of(B, A), Pair.of(null, C));
    }

    @Test
    void test_filterNonNull_Given_sequenceWithNullValues_Then_returnsSequenceWithoutNullValues() {
        assertThat(Stream.of(null, A, B, null, C, null).filterNonNull()).containsExactly(A, B, C);
        assertThat(Stream.of(null, null).filterNonNull()).isEmpty();
    }

    @Test
    void test_filterTry_optional_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Optional.of(A)
            .filterTry(a -> {
                throw EXCEPTION;
            })).withCause(EXCEPTION);
    }

    @Test
    void test_filterTry_optional_Given_noException_Then_retunsValue() {
        var actual = Optional.of(A).filterTry(a -> true, (v, e) -> new IllegalArgumentException(e));
        assertThat(actual).hasValue(A);
    }

    @Test
    void test_filterTry_optional_exceptionMapper_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> Optional.of(A)
            .filterTry(a -> {
                throw EXCEPTION;
            }, (v, e) -> new IllegalArgumentException(e)));
    }

    @Test
    void test_filterTry_stream_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Stream.of(A)
            .filterTry(a -> {
                throw EXCEPTION;
            })
            .toList()).withCause(EXCEPTION);
    }

    @Test
    void test_filterTry_stream_Given_noException_Then_returnsValues() {
        var actual = Stream.of(A, B).filterTry(a -> true, (v, e) -> new IllegalArgumentException(e));
        assertThat(actual).containsExactly(A, B);
    }

    @Test
    void test_filterUnlessWhen_Given_extractedValueDoesNotMatchPredicate_Then_returnsEmpty() {
        assertThat(Optional.of(Pair.of(A, "1")).filterUnlessWhen(Pair::getLeft, IS_LETTER_A)).isEmpty();
    }

    @Test
    void test_filterUnlessWhen_Given_sequence_and_extractedValueDoesNotMatchesPredicate_Then_returnsItem() {
        assertThat(Optional.of(Pair.of(A, "1")).filterUnlessWhen(Pair::getLeft, IS_LETTER_B)).hasValue(Pair.of(A, "1"));
    }

    @Test
    void test_filterUnlessWhen_Given_sequence_and_itemExtractedValueMatchesPredicate_Then_returnsItem() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2"))
            .filterUnlessWhen(Pair::getLeft, IS_LETTER_A)).containsExactly(Pair.of(B, "2"));
    }

    @Test
    void test_filterUnless_Given_empty_Then_returnsEmpty() {
        assertThat(Optional.empty().filterUnless(contradiction())).isEmpty();
        assertThat(Stream.empty().filterUnless(contradiction())).isEmpty();
    }

    @Test
    void test_filterUnless_Given_predicateFalse_Then_returnsValues() {
        assertThat(Optional.of(A).filterUnless(contradiction())).hasValue(A);
        assertThat(Stream.of(A, B).filterUnless(contradiction())).containsExactly(A, B);
    }

    @Test
    void test_filterUnless_Given_predicateTrue_Then_returnsEmpty() {
        assertThat(Optional.of(A).filterUnless(tautology())).isEmpty();
        assertThat(Stream.of(A, B).filterUnless(tautology())).isEmpty();
    }

    @Test
    void test_filterValueInstanceOf_stream_Given_sequence_and_value_instanceOfType_Then_returnsValue() {
        assertThat(Stream.of(Pair.of(A, A), Pair.of(A, B), Pair.of(A, BigDecimal.valueOf(1)), Pair.of(A, 2.0))
            .filterValueInstanceOf(String.class)).hasSize(2)
            .anySatisfy(p -> assertThatEntryEquals(p, A, A))
            .anySatisfy(p -> assertThatEntryEquals(p, A, B));
        assertThat(Stream.of(Pair.of(A, A), Pair.of(A, B), Pair.of(A, BigDecimal.valueOf(1)), Pair.of(A, 2.0))
            .filterValueInstanceOf(Number.class)).hasSize(2).anySatisfy(p -> {
                assertThat(p.getKey()).isEqualTo(A);
                assertThat(p.getValue()).isEqualTo(BigDecimal.valueOf(1));
            }).anySatisfy(p -> {
                assertThat(p.getKey()).isEqualTo(A);
                assertThat(p.getValue()).isEqualTo(2.0);
            });
    }

    @Test
    void test_filterValueUnless_map_Given_mapValueMatchesPredicate_Then_returnsMapWithNotMatchingValuesEntries() {
        assertThat(Map.of(A, 1, B, 2, C, 1).filterValueUnless(v -> v == 1)).hasSize(1).containsEntry(B, 2);
    }

    @Test
    void test_filterValueUnless_optional_Given_sequence_and_itemKeyValueDoesNotMatchPredicate_Then_returnsItem() {
        assertThat(Optional.of(Pair.of(A, "1")).filterValueUnless(s -> s.equals("9"))).hasValue(Pair.of(A, "1"));
    }

    @Test
    void test_filterValueUnless_optional_Given_sequence_and_itemKeyValueMatchesPredicate_Then_doesNotReturnItem() {
        assertThat(Optional.of(Pair.of(A, "1")).filterValueUnless(s -> s.equals("1"))).isEmpty();
    }

    @Test
    void test_filterValueUnless_stream_Given_sequence_and_entryValueMatchesPredicate_Then_doesNotReturnItem() {
        assertThat(Stream.of(Pair.of(A, "1")).filterValueUnless(s -> s.equals("1"))).isEmpty();
    }

    @Test
    void test_filterValueUnless_stream_Given_sequence_and_itemKeyValueDoesNotMatchPredicate_Then_returnsItem() {
        assertThat(Stream.of(Pair.of(A, "1")).filterValueUnless(s -> s.equals("9"))).containsExactly(Pair.of(A, "1"));
    }

    @Test
    void test_filterValue_map_Given_mapValueMatchesPredicate_Then_returnsMapWithMatchingValuesEntries() {
        assertThat(Map.of(A, 1, B, 2, C, 1).filterValue(v -> v == 1)).hasSize(2)
            .containsEntry(A, 1)
            .containsEntry(C, 1);
    }

    @Test
    void test_filterValue_optional_Given_sequence_and_itemKeyValueMatchesPredicate_Then_returnsItem() {
        assertThat(Optional.of(Pair.of(A, "1")).filterValue(s -> s.equals("1"))).hasValue(Pair.of(A, "1"));
    }

    @Test
    void test_filterValue_optional_Given_value_entryValueDoesNotMatchPredicate_Then_doesNotReturnItem() {
        assertThat(Optional.of(Pair.of(A, "1")).filterValue(s -> s.equals("9"))).isEmpty();
    }

    @Test
    void test_filterValue_sequence_Given_values_and_entryValueMatchesPredicate_Then_returnsItem() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2"))
            .filterValue(s -> s.equals("1"))).containsExactly(Pair.of(A, "1"));
    }

    @Test
    void test_filterWhen_Given_sequence_and_extractedValueDoesNotMatchesPredicate_Then_returnsEmpty() {
        assertThat(Optional.of(Pair.of(A, "1")).filterWhen(Pair::getLeft, IS_LETTER_A)).hasValue(Pair.of(A, "1"));
    }

    @Test
    void test_filterWhen_Given_sequence_and_extractedValueMatchesPredicate_Then_returnsItem() {
        assertThat(Optional.of(Pair.of(A, "1")).filterWhen(Pair::getLeft, IS_LETTER_A)).hasValue(Pair.of(A, "1"));
    }

    @Test
    void test_filterWhen_Given_sequence_and_itemExtractedValueMatchesPredicate_Then_returnsItem() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2")).filterWhen(Pair::getLeft, IS_LETTER_A)).hasSize(1)
            .containsExactly(Pair.of(A, "1"));
    }

    @Test
    void test_findFirstNonNull_stream_Given_allNulls_Then_returnsEmpty() {
        assertThat(Stream.of(null, null, null).findFirstNonNull()).isEmpty();
    }

    @Test
    void test_findFirstNonNull_stream_Given_empty_Then_returnsEmpty() {
        assertThat(Stream.empty().findFirstNonNull()).isEmpty();
    }

    @Test
    void test_findFirstNonNull_stream_Given_someNulls_Then_returnsFirstNonNullValue() {
        assertThat(Stream.of(null, null, C, null).findFirstNonNull()).hasValue(C);
    }

    @Test
    void test_findFirstPresent_stream_Given_allNulls_Then_returnsEmpty() {
        assertThat(Stream.of(Optional.empty(), Optional.empty()).findFirstPresent()).isEmpty();
    }

    @Test
    void test_findFirstPresent_stream_Given_empty_Then_returnsEmpty() {
        assertThat(Stream.empty().findFirstPresent()).isEmpty();
    }

    @Test
    void test_findFirstPresent_stream_Given_someNulls_Then_returnsFirstPresentValue() {
        assertThat(Stream.of(Optional.empty(), Optional.empty(), Optional.of(C), Optional.empty())
            .findFirstPresent()).hasValue(C);
    }

    @Test
    void test_findFirst_Given_elements_Then_returnsIndexZeroElement() {
        assertThat(new String[]{A, B, C}.findFirst()).hasValue(A);
        assertThat(new String[]{A}.findFirst()).hasValue(A);

        assertThat(List.of(A, B, C).findFirst()).hasValue(A);
        assertThat(List.of(A).findFirst()).hasValue(A);
    }

    @Test
    void test_findFirst_Given_empty_Then_returnsEmpty() {
        assertThat(new String[]{}.findFirst()).isEmpty();
        assertThat(Collections.emptyList().findFirst()).isEmpty();
    }

    @Test
    void test_findFirst_Given_valueMatchingPredicate_Then_returnsValue() {
        assertThat(List.of(A, B, C).findFirst(IS_LETTER_B)).hasValue(B);
        assertThat(Stream.of(A, B, C).findFirst(IS_LETTER_B)).hasValue(B);
        assertThat(new String[]{A, B, C}.findFirst(IS_LETTER_B)).hasValue(B);
    }

    @Test
    void test_findFirst_map_Given_matchingBiPredicate_Then_returnsMapEntry() {
        assertThat(Map.of(A, 1, B, 1).findFirst((k, v) -> k.equals(A) && v == 1)).hasValueSatisfying(e -> {
            assertThat(e.getKey()).isEqualTo(A);
            assertThat(e.getValue()).isEqualTo(1);
        });
    }

    @Test
    void test_findFirst_map_Given_matchingPredicate_Then_returnsMapEntry() {
        assertThat(Map.of(A, 1, B, 1).findFirst(v -> v == 1)).isPresent();
        assertThat(Map.of(A, 1, B, 1).findFirst((k, v) -> v == 1)).isPresent();
    }

    @Test
    void test_findFirst_map_Given_notMatchingPredicate_Then_returnsMapEntry() {
        assertThat(Map.of(A, 1, B, 1).findFirst(v -> v == 2)).isEmpty();
    }

    @Test
    void test_findLastWithStream_Given_emptyStream_Then_returnsEmpty() {
        assertThat(Stream.empty().findLast()).isEmpty();
    }

    @Test
    void test_findLastWithStream_Given_last_Then_returnsLastElement() {
        assertThat(Stream.of(A, B, C).findLast()).hasValue(C);
        assertThat(Stream.of(A).findLast()).hasValue(A);
    }

    @Test
    void test_findLast_Given_emptyList_Then_returnsEmpty() {
        assertThat(Collections.emptyList().findLast()).isEmpty();
    }

    @Test
    void test_findLast_Given_emptyList_Then_returnsLastElement() {
        assertThat(Collections.emptyList().findLast()).isEmpty();
    }

    @Test
    void test_findLast_Given_last_Then_returnsLastElement() {
        assertThat(List.of(A, B, C).findLast()).hasValue(C);
        assertThat(List.of(A).findLast()).hasValue(A);
    }

    @Test
    void test_first_Given_array_Then_returnsZeroIndexElement() {
        assertThat(new Integer[]{1, 2, 3}.first()).isEqualTo(1);
    }

    @Test
    void test_first_Given_list_Then_returnsZeroIndexElement() {
        assertThat(List.of(1, 2, 3).first()).isEqualTo(1);
        assertThat(List.of(1).first()).isEqualTo(1);
    }

    @Test
    void test_first_array_Given_empty_Then_throwsException() {
        assertThatException().isThrownBy(() -> new String[]{}.first());
    }

    @Test
    void test_first_list_Given_empty_Then_throwsException() {
        assertThatException().isThrownBy(() -> Collections.emptyList().first());
    }

    @Test
    void test_flatMapComprehension_Given_values_Then_returnsMapFlattenAndFlatmappingApplicationThatIsAwareOfFlattenedValue() {
        assertThat(Stream.of("AAA", "BB", "C")
            .flatMapComprehension(s -> IntStream.range(0, s.length())
                .boxed(), (s, n) -> Stream.of(s + ":" + n, "dup_" + s + ":" + n)))
            .containsExactly("AAA:0", "dup_AAA:0", "AAA:1", "dup_AAA:1", "AAA:2", "dup_AAA:2", "BB:0", "dup_BB:0", "BB:1", "dup_BB:1", "C:0", "dup_C:0");
    }

    @Test
    void test_flatMapMap_Given_values_Then_returnsMapFlattenAndMappingApplicationThatIsAwareOfFlattenedValue() {
        assertThat(Stream.of("AAA", "BB", "C")
            .flatMapMap(s -> IntStream.range(0, s.length())
                .boxed(), (s, n) -> s + ":" + n))
            .containsExactly("AAA:0", "AAA:1", "AAA:2", "BB:0", "BB:1", "C:0");
    }

    @Test
    void test_flatMapTry_OptionalAndMapper_Given_exception_Then_throwsMappedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> Optional.of(A)
            .flatMapTry(s -> {
                throw new IOException("testIOException");
            }, (a, x) -> new IllegalStateException("testException", x))).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_flatMapTry_Optional_Given_exception_Then_throwsMappedException() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Optional.of(A)
            .flatMapTry(s -> {
                throw new IOException("testIOException");
            })).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_flatMapTry_Optional_Given_noException_Then_returnsFlatMap() {
        assertThat(Optional.of(A)
            .flatMapTry(s -> Optional.of(s + "1"), (a, x) -> new IllegalStateException("testException"))).hasValue("a1");
    }

    @Test
    void test_flatMapTry_Stream_Given_exception_Then_throwsMappedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> Stream.of(A)
            .flatMapTry(s -> {
                throw new IOException("testIOException");
            }, (a, x) -> new IllegalStateException("testException", x))
            .toList()).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_flatMapTry_Stream_Given_noException_Then_returnsFlatMap() {
        assertThat(Stream.of(A)
            .flatMapTry(s -> Stream.of(s + "1"), (a, x) -> new IllegalStateException("testException"))).containsExactly("a1");
    }

    @Test
    void test_flatMapTry_optional_Given_exception_Then_throwsWrappingRuntimeException() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Optional.of(A)
            .flatMapTry(s -> {
                throw new IOException("testIOException");
            })).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_flatMapTry_stream_Given_exception_Then_throwsWrappingRuntimeException() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Stream.of(A)
            .flatMapTry(s -> {
                throw new IOException("testIOException");
            })
            .toList()).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_flatMapTry_stream_Given_noException_Then_returnsValue() {
        assertThat(Stream.of(A).flatMapTry(Stream::of)).containsExactly(A);
    }

    @Test
    void test_flattenAll_streamAndElementToArray_Given_stream_Then_returnsMappedArrayValuesStream() {
        assertThat(Stream.of(A, B, C).flattenAll(s -> new String[]{s, s})).containsExactly(A, A, B, B, C, C);
    }

    @Test
    void test_flattenValues_Given_streamWithMaps_Then_returnsAllMapValues() {
        assertThat(Stream.of(Map.of(A, "a1", B, "b2"), Map.of(C, "c3", "d", "d4"), Map.of("d", "d4b"))
            .flattenValues()).containsExactlyInAnyOrder("a1", "b2", "c3", "d4", "d4b");
    }

    @Test
    void test_flattenValues_mapMapper_Given_streamWithMaps_Then_returnsAllMapValues() {
        assertThat(Stream.of(A, B, C)
            .flattenValues(v -> Map.of(v + "a", v + "1", v + "b", v + "2"))).containsExactlyInAnyOrder("a1", "a2", "b1", "b2", "c1", "c2");
    }

    @Test
    void test_flatten_collection_Given_collectionOfCollections_Then_returnsValuesInSubCollections() {
        assertThat(List.of(List.of(A), List.of(B, C), Collections.emptyList()).flatten()).containsExactly(A, B, C);
    }

    @Test
    void test_flatten_mapper_Given_collection_Then_returnsFlattenedMappedCollectionItems() {
        assertThat(List.of(List.of(A, A), List.of(B, B), List.of(C, C))
            .flatten(l -> l.stream()
                .map(s -> "*" + s)
                .toList()))
            .containsExactly("*" + A, "*" + A, "*" + B, "*" + B, "*" + C, "*" + C);
    }

    @Test
    void test_fold_collection_Given_empty_Then_returnIdentity() {
        assertThat(Collections.emptyList().fold("X", (a, b) -> a + b)).isEqualTo("X");
    }

    @Test
    void test_fold_collection_Given_values_Then_returnLeftFoldedValue() {
        assertThat(List.of(A, B, C).fold("", (a, b) -> a + b)).isEqualTo("abc");
    }

    @Test
    void test_fold_stream_Given_empty_Then_returnIdentity() {
        assertThat(Stream.empty().fold("X", (a, b) -> a + b)).isEqualTo("X");
    }

    @Test
    void test_fold_stream_Given_values_Then_returnLeftFoldedValue() {
        assertThat(Stream.of(A, B, C).fold("", (a, b) -> a + b)).isEqualTo("abc");
    }

    @Test
    void test_forEachComprehension_collection_Given_values_Then_returnsMappingApplicationThatIsAwareOfMappedValue() {
        var result = new ArrayList<String>();
        List.of("AAA", "BB", "C").forEachComprehension(String::length, (t, u) -> result.add("%s:%d".formatted(t, u)));

        assertThat(result).containsExactly("AAA:3", "BB:2", "C:1");
    }

    @Test
    void test_forEachComprehension_collection_three_Given_values_Then_returnsMappingApplicationThatIsAwareOfMappedValue() {
        var result = new ArrayList<String>();
        List.of("A", "B", "C")
            .forEachComprehension(t -> t + "t", "%s(%s)"::formatted, (t, u, v) -> result.add("%s[%s|%s]".formatted(t, u, v)));

        assertThat(result).containsExactly("A[At|A(At)]", "B[Bt|B(Bt)]", "C[Ct|C(Ct)]");
    }

    @Test
    void test_forEachComprehension_stream_Given_values_Then_returnsMappingApplicationThatIsAwareOfMappedValue() {
        var result = new ArrayList<String>();
        Stream.of("AAA", "BB", "C").forEachComprehension(String::length, (t, u) -> result.add("%s:%d".formatted(t, u)));

        assertThat(result).containsExactly("AAA:3", "BB:2", "C:1");
    }

    @Test
    void test_forEachComprehension_stream_three_Given_values_Then_returnsMappingApplicationThatIsAwareOfMappedValue() {
        var result = new ArrayList<String>();
        Stream.of("A", "B", "C")
            .forEachComprehension(t -> t + "t", "%s(%s)"::formatted, (t, u, v) -> result.add("%s[%s|%s]".formatted(t, u, v)));

        assertThat(result).containsExactly("A[At|A(At)]", "B[Bt|B(Bt)]", "C[Ct|C(Ct)]");
    }

    @Test
    void test_forEachEntry_collection_Given_values_Then_consumerAcceptsValues() {
        var result = new ArrayList<String>();
        List.of(Pair.of(A, 1), Pair.of(B, 2), Pair.of(C, 3)).forEachEntry((a, b) -> result.add(a + b));
        assertThat(result).containsExactly("a1", "b2", "c3");
    }

    @Test
    void test_forEachEntry_optional_Given_values_Then_consumerAcceptsValues() {
        var result = new ArrayList<String>();
        Optional.of(Map.of(A, 1, B, 2, C, 3)).forEachEntry((a, b) -> result.add(a + b));
        assertThat(result).containsExactlyInAnyOrder("a1", "b2", "c3");
    }

    @Test
    void test_forEachEntry_stream_Given_values_Then_consumerAcceptsValues() {
        var result = new ArrayList<String>();
        Stream.of(Pair.of(A, "1"), Pair.of(B, "2"), Pair.of(C, "3")).forEachEntry((a, b) -> result.add(a + b));
        assertThat(result).containsExactly("a1", "b2", "c3");
    }

    @Test
    void test_foreEachIndexed_list_Given_values_Then_consumesEachValueWithItsIndex() {
        var result = new ArrayList<String>();
        List.of(A, B, C).forEachIndexed((v, i) -> result.add(v + i));
        assertThat(result).containsExactly(A + "0", B + "1", C + "2");
    }

    @Test
    void test_foreEachIndexed_list_Given_values_and_parallel_Then_consumesEachValueWithItsIndexInParallel() {
        var result = new ArrayList<String>();
        var countDownLatch = new CountDownLatch(1);
        List.of(A, B).forEachIndexed(true, (v, i) -> {
            if (i == 0) {
                try {
                    var timedOut = countDownLatch.await(1L, TimeUnit.SECONDS);
                    assertThat(timedOut).isTrue();
                } catch (Exception e) {
                    //
                }
            } else {
                countDownLatch.countDown();
            }
            result.add(i + v);
        });
        assertThat(result).containsExactly("1" + B, "0" + A);
    }

    @Test
    void test_foreEachIndexed_stream_Given_values_Then_consumesEachValueWithItsIndex() {
        var result = new ArrayList<String>();
        Stream.of(A, B, C).forEachIndexed((v, i) -> result.add(v + i));
        assertThat(result).containsExactly(A + "0", B + "1", C + "2");
    }

    @Test
    void test_groupByKey_Given_values_Then_returnsMapOfListsOfRepeatedValues() {
        var actual = List.of(Pair.of(A, "1"), Pair.of(B, "1"), Pair.of(A, "2"), Pair.of(C, "1")).stream().groupByKey();

        assertThat(actual).hasSize(3)
            .containsEntry(A, List.of("1", "2"))
            .containsEntry(B, List.of("1"))
            .containsEntry(C, List.of("1"));
    }

    @Test
    void test_groupBy_array_Given_repeatedValues_Then_returnsMapOfListsGroupedBySelector() {
        assertThat(new String[]{A, A, B, C}.groupBy(s -> s)).hasSize(3)
            .containsEntry(A, List.of(A, A))
            .containsEntry(B, List.of(B))
            .containsEntry(C, List.of(C));
    }

    @Test
    void test_groupBy_collectionAndMapper_Given_values_Then_returnsMapOfListsOfRepeatedValues() {
        assertThat(List.of(Pair.of(A, "1"), Pair.of(B, "1"), Pair.of(A, "2"), Pair.of(C, "1"))
            .groupBy(Pair::getKey, Pair::getValue)).hasSize(3)
            .containsEntry(A, List.of("1", "2"))
            .containsEntry(B, List.of("1"))
            .containsEntry(C, List.of("1"));
    }

    @Test
    void test_groupBy_collection_Given_repeatedValues_Then_returnsMapOfListsGroupedBySelector() {
        assertThat(List.of(A, A, B, C).groupBy(s -> s)).hasSize(3)
            .containsEntry(A, List.of(A, A))
            .containsEntry(B, List.of(B))
            .containsEntry(C, List.of(C));
    }

    @Test
    void test_groupBy_iterable_Given_repeatedValues_Then_returnsMapOfListsGroupedBySelector() {
        assertThat(createIterable(A, A, B, C).groupBy(s -> s)).hasSize(3)
            .containsEntry(A, List.of(A, A))
            .containsEntry(B, List.of(B))
            .containsEntry(C, List.of(C));
    }

    @Test
    void test_groupBy_streamAndMapper_Given_values_Then_returnsMapOfListsOfRepeatedValues() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "1"), Pair.of(A, "2"), Pair.of(C, "1"))
            .groupBy(Pair::getKey, Pair::getValue)).hasSize(3)
            .containsEntry(A, List.of("1", "2"))
            .containsEntry(B, List.of("1"))
            .containsEntry(C, List.of("1"));
    }

    @Test
    void test_groupBy_stream_Given_repeatedValues_Then_returnsMapOfListsGroupedBySelector() {
        assertThat(Stream.of(A, A, B, C).groupBy(s -> s)).hasSize(3)
            .containsEntry(A, List.of(A, A))
            .containsEntry(B, List.of(B))
            .containsEntry(C, List.of(C));
    }

    @Test
    void test_head_Given_values_Then_returnsFirstElement() {
        assertThat(List.of(1, 2, 3).head()).containsExactly(1);
        assertThat(List.of(1).head()).containsExactly(1);
    }

    @Test
    void test_ifEmpty_Given_empty_Then_callsRunnable() {
        Optional.<String>empty().ifEmpty(() -> boolean1Called.set(true));
        assertThat(boolean1Called.get()).isTrue();
    }

    @Test
    void test_ifEmpty_Given_notEmpty_Then_doesNotCallRunnable() {
        Optional.of("xxx").ifEmpty(() -> boolean1Called.set(true));
        assertThat(boolean1Called.get()).isFalse();
    }

    @Test
    void test_isPresent_Given_empty_Then_returnsFalse() {
        assertThat(Optional.<String>empty().isPresent()).isFalse();
    }

    @Test
    void test_isPresent_Given_predicateDoesNotMatch_Then_returnsFalse() {
        assertThat(Optional.of("xxxx").isPresent("zzzz"::equals)).isFalse();
    }

    @Test
    void test_isPresent_Given_predicateMatches_Then_returnsTrue() {
        assertThat(Optional.of("xxxx").isPresent("xxxx"::equals)).isTrue();
    }

    @Test
    void test_joinWith_Given_null_Then_returnsNull() {
        assertThat(((String[]) null).joinWith(",")).isNull();
        assertThat(((List<CharSequence>) null).joinWith(",")).isNull();
    }

    @Test
    void test_joinWith_Given_values_Then_returnsStringsJoined() {
        // assertThat(new String[]{A,B,C}.joinWith()).isEqualTo("a,b,c");
        assertThat(List.of(A, B, C).joinWith()).isEqualTo("abc");
        assertThat(Stream.of(A, B, C).joinWith()).isEqualTo("abc");
    }

    @Test
    void test_joinWith_delimiter_Given_values_Then_returnsStringsJoinedByDelimiter() {
        assertThat(new String[]{A, B, C}.joinWith(",")).isEqualTo("a,b,c");
        assertThat(List.of(A, B, C).joinWith(",")).isEqualTo("a,b,c");
        assertThat(Stream.of(A, B, C).joinWith(",")).isEqualTo("a,b,c");
    }

    @Test
    void test_joinWith_delimiters_Given_values_Then_returnsStringsJoinedByDelimiterAndPrefixedAndSuffixed() {
        // assertThat(new String[]{A,B,C}.joinWith(",", "[", "]")).isEqualTo("[a,b,c]");
        assertThat(Stream.of(A, B, C).joinWith(",", "[", "]")).isEqualTo("[a,b,c]");
        assertThat(List.of(A, B, C).joinWith(",", "[", "]")).isEqualTo("[a,b,c]");
    }

    @Test
    void test_last_Given_array_Then_returnsLastIndexElement() {
        assertThat(new Integer[]{1, 2, 3}.last()).isEqualTo(3);
    }

    @Test
    void test_last_Given_list_Then_returnsLastIndexedElement() {
        assertThat(List.of(1, 2, 3).last()).isEqualTo(3);
        assertThat(List.of(3).last()).isEqualTo(3);
    }

    @Test
    void test_last_array_Given_empty_Then_throwsException() {
        assertThatException().isThrownBy(() -> new String[]{}.last());
    }

    @Test
    void test_last_list_Given_empty_Then_throwsException() {
        assertThatException().isThrownBy(() -> Collections.emptyList().last());
    }

    @Test
    void test_mapComprehension_Given_values_Then_returnsMappingApplicationThatIsAwareOfMappedValue() {
        assertThat(Stream.of("AAA", "BB", "C")
            .mapComprehension(String::length, "%s:%d"::formatted)).containsExactly("AAA:3", "BB:2", "C:1");
    }

    @Test
    void test_mapComprehension_three_Given_values_Then_returnsMappingApplicationThatIsAwareOfMappedValue() {
        assertThat(Stream.of("A", "B", "C")
            .mapComprehension(t -> t + "t", "%s(%s)"::formatted, "%s[%s|%s]"::formatted)).containsExactly("A[At|A(At)]", "B[Bt|B(Bt)]", "C[Ct|C(Ct)]");
    }

    @Test
    void test_mapEntryToPairWithEither_Given_left_Then_returnsEither() {
        assertThat(Either.<String, Pair<String, String>>ofLeft("").mapEntryToPair(k -> k, v -> v).isLeft()).isTrue();
    }

    @Test
    void test_mapEntryToPairWithEither_Given_right_Then_returnsEitherWithMappedRight() {
        assertThat(Either.<String, Pair<String, String>>ofRight(Pair.of("a", "b"))
            .mapEntryToPair(k -> "x", v -> "y")
            .getRight()).isEqualTo(Pair.of("x", "y"));
    }

    @Test
    void test_mapEntry_Either_Given_left_Then_returnsEither() {
        assertThat(Either.<String, Pair<String, String>>ofLeft("").mapEntry((a, b) -> a + b).isLeft()).isTrue();
    }

    @Test
    void test_mapEntry_Either_Given_right_Then_returnsFunctionAppliedToValue() {
        assertThat(Either.ofRight(Pair.of(A, B)).mapEntry((a, b) -> a + b).getRight()).isEqualTo("ab");
    }

    @Test
    void test_mapEntry_Optional_Given_empty_Then_returnsEmpty() {
        assertThat(Optional.<Pair<String, String>>empty().mapEntry((a, b) -> a + b)).isEmpty();
    }

    @Test
    void test_mapEntry_Optional_Given_value_Then_returnsFunctionAppliedToValue() {
        assertThat(Optional.of(Pair.of(A, B)).mapEntry((a, b) -> a + b)).hasValue("ab");
    }

    @Test
    void test_mapIndexed_Given_mapper_Then_mappingIsIndexAware() {
        assertThat(Stream.empty().mapIndexed((v, i) -> i % 2 == 0)).isEmpty();
        assertThat(Stream.of(A, B, C).mapIndexed((v, i) -> v + i)).containsExactly("a0", "b1", "c2");
    }

    @Test
    void test_mapKey_Given_emptyOptional_Then_returnsEmpty() {
        assertThat(Optional.<Pair<String, String>>empty().mapKey()).isEmpty();
    }

    @Test
    void test_mapKey_Given_emptyStream_Then_returnsEmpty() {
        assertThat(Stream.<Pair<String, String>>empty().mapKey()).isEmpty();
    }

    @Test
    void test_mapKey_Given_optional_Then_returnsKey() {
        assertThat(Optional.of(Pair.of(A, "1")).mapKey()).hasValue(A);
    }

    @Test
    void test_mapKey_Given_stream_Then_returnsSteamOfKeys() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2")).mapKey()).containsExactly(A, B);
    }

    @Test
    void test_mapKey_function_Given_values_Then_returnsSteamOfValuesWithFunctionAppliedToKey() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2")).mapKey(v -> "*" + v)).hasSize(2).satisfies(s -> {
            assertThat(s).element(0).isEqualTo(Pair.of("*" + A, "1"));
            assertThat(s).element(1).isEqualTo(Pair.of("*" + B, "2"));
        });
    }

    @Test
    void test_mapMatcherBuilder_stream_Given_values_Then_mapsMatches() {
        var actual = Stream.of(A, B, C)
            .mapMatcher(String.class)
            .when(IS_LETTER_A, ADD_SQUARE_BRACKETS)
            .when(IS_LETTER_B, ADD_PARENTHESES)
            .otherwise(s -> s)
            .build()
            .map(s -> "**" + s);

        assertThat(actual).containsExactly("**[a]", "**(b)", "**c");
    }

    @Test
    void test_mapMatcher_option_Given_value_Then_mapsMatch() {
        var actual = Optional.of(A)
            .mapMatcher(Matcher.<String, String>applying()
                .when(IS_LETTER_A, ADD_SQUARE_BRACKETS)
                .when(IS_LETTER_B, ADD_PARENTHESES)
                .otherwise(s -> s));

        assertThat(actual).hasValue("[a]");
    }

    @Test
    void test_mapMatcher_stream_Given_value_Then_mapsMatches() {
        var actual = Stream.of(A, B, C)
            .mapMatcher(Matcher.<String, String>applying()
                .when(IS_LETTER_A, ADD_SQUARE_BRACKETS)
                .when(IS_LETTER_B, ADD_PARENTHESES)
                .otherwise(s -> s));

        assertThat(actual).containsExactly("[a]", "(b)", C);
    }

    @Test
    void test_mapToPairWithEither_Given_left_Then_returnsEither() {
        assertThat(Either.<String, Pair<String, String>>ofLeft("").mapToPair(k -> k, v -> v).isLeft()).isTrue();
    }

    @Test
    void test_mapToPairWithEither_Given_right_Then_returnsEitherWithMappedRight() {
        assertThat(Either.<String, String>ofRight("a")
            .mapToPair(k -> "x", v -> "y")
            .getRight()).isEqualTo(Pair.of("x", "y"));
    }

    @Test
    void test_mapTry_Either_Given_exception_Then_returnsLeftWithException() {
        var actual = Either.ofRight(A)
            .mapTry(s -> {
                throw new IOException("testIOException");
            }, (v, x) -> new IllegalStateException("testException", x));
        assertThat(actual.isLeft()).isTrue();
        assertThat(actual.getLeft()).isInstanceOf(IllegalStateException.class);
    }

    @Test
    void test_mapTry_Either_Given_noException_Then_returnsRight() {
        var actual = Either.ofRight(A).mapTry(s -> "*" + s, (v, x) -> new IllegalStateException("testException", x));
        assertThat(actual.getRight()).isEqualTo("*a");
    }

    @Test
    void test_mapTry_optionalAndMapper_Given_exception_Then_throwsExceptionRemapped() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> Optional.of(A)
            .mapTry(s -> {
                throw new IOException("testIOException");
            }, (a, x) -> new IllegalStateException("testException", x))).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_mapTry_optional_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Optional.of(A)
            .mapTry(a -> {
                throw EXCEPTION;
            })).withCause(EXCEPTION);
    }

    @Test
    void test_mapTry_optional_Given_exception_Then_throwsException() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Optional.of(A)
            .mapTry(s -> {
                throw new IOException("testIOException");
            })).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_mapTry_optional_Given_noException_Then_executesConsumer() {
        var actual = Optional.of(A).mapTry(a -> B, (v, e) -> new IllegalArgumentException(e));
        assertThat(actual).hasValue(B);
    }

    @Test
    void test_mapTry_optional_Given_noException_Then_returnsFunctionApplicationResult() {
        assertThat(Optional.of(A)
            .mapTry(s -> s + "1", (a, x) -> new IllegalStateException("testException"))).hasValue("a1");
    }

    @Test
    void test_mapTry_optional_exceptionMapper_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> Optional.of(A)
            .mapTry(a -> {
                throw EXCEPTION;
            }, (v, e) -> new IllegalArgumentException(e)));
    }

    @Test
    void test_mapTry_streamAndMapper_Given_exception_Then_throwsException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> Stream.of(A)
            .mapTry(s -> {
                throw new IOException("testIOException");
            }, (a, x) -> new IllegalStateException("testException", x))
            .toList()).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_mapTry_streamAndMapper_Given_noException_Then_returnsFlatMap() {
        assertThat(Stream.of(A)
            .mapTry(s -> s + "1", (a, x) -> new IllegalStateException("testException"))).containsExactly("a1");
    }

    @Test
    void test_mapTry_stream_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Stream.of(A)
            .mapTry(a -> {
                throw EXCEPTION;
            })
            .toList()).withCause(EXCEPTION);
    }

    @Test
    void test_mapTry_stream_Given_exception_Then_returnsFlatMap() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Stream.of(A)
            .mapTry(s -> {
                throw new IOException("testIOException");
            })
            .toList()).withCauseInstanceOf(IOException.class);
    }

    @Test
    void test_mapTry_stream_Given_noException_Then_executesConsumer() {
        var actual = Stream.of(A).mapTry(a -> B, (v, e) -> new IllegalArgumentException(e));
        assertThat(actual).containsExactly(B);
    }

    @Test
    void test_mapTry_stream_Given_noException_Then_returnsFlatMap() {
        assertThat(Stream.of(A).mapTry(s -> s + "1")).containsExactly("a1");
    }

    @Test
    void test_mapTry_stream_exceptionMapper_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> Stream.of(A)
            .mapTry(a -> {
                throw EXCEPTION;
            }, (v, e) -> new IllegalArgumentException(e))
            .toList());
    }

    @Test
    void test_mapValue_Given_values_Then_returnsSteamOfValues() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2")).mapValue()).containsExactly("1", "2");
    }

    @Test
    void test_mapValue_Given_values_Then_returnsSteamOfValuesWithFunctionAppliedToValue() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2")).mapValue(v -> "*" + v)).hasSize(2).satisfies(s -> {
            assertThat(s).element(0).isEqualTo(Pair.of(A, "*1"));
            assertThat(s).element(1).isEqualTo(Pair.of(B, "*2"));
        });
    }

    @Test
    void test_maxAsScalar_Given_collection_Then_returnsMaxIntMapping() {
        assertThat(List.of("A", A + A, "AAA", "B").maxAsScalar(String::length)).isEqualTo(3);
    }

    @Test
    void test_maxAsScalar_Given_empty_Then_returnsZero() {
        assertThat(Collections.emptyList().maxAsScalar(String::length)).isZero();
    }

    @Test
    void test_maxBy_Given_collection_Then_returnsMaxValueByMapping() {
        assertThat(List.of("A", A + A, "AAA", "B").maxBy(String::length)).hasValue("AAA");
    }

    @Test
    void test_maxBy_Given_emptyCollection_Then_returnsOther() {
        assertThat(Collections.<String>emptyList().maxBy(String::length)).isEmpty();
    }

    @Test
    void test_maxBy_comparator_Given_collection_Then_returnsMaxValueByMapping() {
        assertThat(List.of("A", "AA", "AAA", "B").maxBy(Comparator.naturalOrder())).hasValue("B");
    }

    @Test
    void test_mergeEach_maps_Given_values_Then_returnsMapWithElementsOfSecondMapAndNotDuplicatedKeyElementsFromFirstMap() {
        assertThat(Map.of(A, "1A", B, "1B").mergeEach(Map.of(A, "2A"))).hasSize(2)
            .containsEntry(A, "2A")
            .containsEntry(B, "1B");
    }

    @Test
    void test_mergeEach_stream_Given_values_Then_returnsStreamOfElementsOfFirstStreamMatchingPredicateAndElementsOfSecondStream() {
        assertThat(Stream.of(A, B, C).mergeEach(tautology(), Stream.of(A, C))).hasSize(5)
            .containsExactly(A, B, C, A, C);
        assertThat(Stream.of(A, B, C).mergeEach(A::equals, Stream.of(C, D))).hasSize(3).containsExactly(A, C, D);
    }

    @Test
    void test_minBy_Given_collection_Then_returnsMinValueByMapping() {
        assertThat(List.of("A", A + A, "AAA", "B").minBy(String::length)).hasValue("A");
    }

    @Test
    void test_minBy_Given_emptyCollection_Then_returnsOther() {
        assertThat(Collections.<String>emptyList().minBy(String::length)).isEmpty();
    }

    @Test
    void test_minBy_comparator_Given_collection_Then_returnsFirstMinValueByMapping() {
        assertThat(List.of("A", "AA", "AAA", "B").minBy(Comparator.naturalOrder())).hasValue("A");
    }

    @Test
    void test_minBy_comparator_Given_collection_Then_returnsMinValueByMapping() {
        assertThat(List.of("A", "AA", "AAA", "BB").minBy(Comparator.naturalOrder())).hasValue("A");
    }

    @Test
    void test_none_Given_anyMatch_Then_returnsFalse() {
        assertThat(List.of(A, B, C).none(IS_LETTER_A)).isFalse();
        assertThat(new String[]{A, B, C}.none(IS_LETTER_A)).isFalse();
    }

    @Test
    void test_none_Given_empty_Then_returnsTrue() {
        assertThat(Collections.emptyList().none(contradiction())).isTrue();
        assertThat(new String[]{}.none(IS_LETTER_A)).isTrue();
    }

    @Test
    void test_none_Given_noMatch_Then_returnsTrue() {
        assertThat(List.of(B, C, D).none(IS_LETTER_A)).isTrue();
        assertThat(new String[]{B, B, D}.none(IS_LETTER_A)).isTrue();
    }

    @Test
    void test_partition_Given_emptyList_Then_returnsPairOfEmptyLists() {
        assertThat(Collections.emptyList()
            .partition(tautology())).isEqualTo(Pair.of(Collections.emptyList(), Collections.emptyList()));
        assertThat(Collections.emptyList()
            .partition(contradiction())).isEqualTo(Pair.of(Collections.emptyList(), Collections.emptyList()));
    }

    @Test
    void test_partition_Given_match_Then_returnsPairOfMatchingElementsInFirstList() {
        assertThat(List.of(A, B, C)
            .partition(tautology())).satisfies(p -> assertThat(p.getKey()).containsExactly(A, B, C));
        assertThat(List.of(A, B, C).partition(contradiction())).satisfies(p -> assertThat(p.getKey()).isEmpty());
        assertThat(List.of(A, B, C).partition(A::equals)).satisfies(p -> assertThat(p.getKey()).containsExactly(A));
    }

    @Test
    void test_partition_Given_noMatch_Then_returnsPairOfNonMatchingElementsInSecondList() {
        assertThat(List.of(A, B, C).partition(tautology())).satisfies(p -> assertThat(p.getValue()).isEmpty());
        assertThat(List.of(A, B, C)
            .partition(contradiction())).satisfies(p -> assertThat(p.getValue()).containsExactly(A, B, C));
        assertThat(List.of(A, B, C)
            .partition(A::equals)).satisfies(p -> assertThat(p.getValue()).containsExactly(B, C));
    }

    @Test
    void test_peekIfWithBoolean_Given_empty_Then_doesNotCallConsumer() {
        Optional.empty().peekIf(true, booleanConsumer1);
        assertThat(boolean1Called.get()).isFalse();
    }

    @Test
    void test_peekIfWithPredicateAndOtherwise_Given_value_and_predicateFalse_Then_callsOtherwiseConsumer() {
        Optional.of("a").peekIf(contradiction(), booleanConsumer1, stringConsumer1);
        assertThat(boolean1Called.get()).isFalse();
        assertThat(stringConsumer1CalledValue).hasValue("a");
    }

    @Test
    void test_peekIfWithPredicateAndOtherwise_Given_value_and_predicateTrue_Then_callsConsumer() {
        Optional.of("a").peekIf(tautology(), stringConsumer1, booleanConsumer1);
        assertThat(stringConsumer1CalledValue).hasValue("a");
        assertThat(boolean1Called.get()).isFalse();
    }

    @Test
    void test_peekIfWithPredicate_Given_empty_predicateTrue_Then_doesNotCallConsumer() {
        Optional.empty().peekIf(tautology(), booleanConsumer1);
        assertThat(boolean1Called.get()).isFalse();
    }

    @Test
    void test_peekIfWithPredicate_Given_value_and_predicateFalse_Then_doesNotCallConsumer() {
        Optional.of("a").peekIf(contradiction(), booleanConsumer1);
        assertThat(boolean1Called.get()).isFalse();
    }

    @Test
    void test_peekIfWithPredicate_Given_value_and_predicateTrue_Then_callsConsumer() {
        Optional.of("a").peekIf(tautology(), stringConsumer1);
        assertThat(stringConsumer1CalledValue).hasValue("a");
    }

    @Test
    void test_plus_Given_emptyValues_Then_returnsListWithValuesFromList() {
        assertThat(List.of(A, B, C).plus()).containsExactly(A, B, C);
    }

    @Test
    void test_plus_Given_values_Then_returnsListWithValuesFromBothLists() {
        assertThat(List.of(A, B, C).plus("1", "2", "3")).containsExactly(A, B, C, "1", "2", "3");
        assertThat(List.of(A, B, C).plus(List.of("1", "2", "3"))).containsExactly(A, B, C, "1", "2", "3");
        assertThat(List.of(A, B, C).plus(Stream.of("1", "2", "3"))).containsExactly(A, B, C, "1", "2", "3");

        assertThat(List.of(A, B, C).plus()).containsExactly(A, B, C);
        assertThat(List.of(A, B, C).plus(Collections.emptyList())).containsExactly(A, B, C);
        assertThat(List.of(A, B, C).plus(Stream.empty())).containsExactly(A, B, C);
    }

    @Test
    void test_plus_map_Given_emptyMaps_Then_returnsMapsWithValuesFromNonEmptyMaps() {
        assertThat(Collections.emptyMap().plus(Collections.emptyMap())).isEmpty();

        assertThat(Map.of(A, 1, B, 2).plus(Collections.emptyMap())).hasSize(2).containsEntry(A, 1).containsEntry(B, 2);

        assertThat(Map.of(A, 1, B, 2).plus(null)).hasSize(2).containsEntry(A, 1).containsEntry(B, 2);

        assertThat(Collections.emptyMap().plus(Map.of(A, 1, B, 2))).hasSize(2).containsEntry(A, 1).containsEntry(B, 2);
    }

    @Test
    void test_plus_map_Given_values_Then_returnsMapsWithKeysAndValuesFromBothMaps() {
        assertThat(Map.of(A, 1, B, 2).plus(Map.of(C, 3, D, 4))).hasSize(4)
            .containsEntry(A, 1)
            .containsEntry(B, 2)
            .containsEntry(C, 3)
            .containsEntry(D, 4);
    }

    @Test
    void test_plus_map_Given_values_and_key_inBothMaps_Then_returnsMapsWithKeysAndValueFromSecondMap() {
        assertThat(Map.of(A, 1, B, 2).plus(Map.of(A, 3, D, 4))).hasSize(3)
            .containsEntry(A, 3)
            .containsEntry(B, 2)
            .containsEntry(D, 4);
    }

    @Test
    void test_prepend_Given_emptyValues_Then_returnsListWithValuesFromList() {
        assertThat(List.of(A, B, C).prepend()).containsExactly(A, B, C);
    }

    @Test
    void test_prepend_Given_values_Then_returnsListWithValuesFromSecondSequenceBeforeElementsFromFistSequence() {
        assertThat(List.of(A, B, C).prepend("1", "2", "3")).containsExactly("1", "2", "3", A, B, C);
        assertThat(List.of(A, B, C).prepend(List.of("1", "2", "3"))).containsExactly("1", "2", "3", A, B, C);
        assertThat(List.of(A, B, C).prepend(Stream.of("1", "2", "3"))).containsExactly("1", "2", "3", A, B, C);

        assertThat(List.of(A, B, C).prepend()).containsExactly(A, B, C);
        assertThat(List.of(A, B, C).prepend(Collections.emptyList())).containsExactly(A, B, C);
        assertThat(List.of(A, B, C).prepend(Stream.empty())).containsExactly(A, B, C);
    }

    @Test
    void test_sortBy_collection_Given_empty_Then_returnsEmpty() {
        assertThat(Collections.<String>emptyList().sortBy(String::length)).isEmpty();
    }

    @Test
    void test_sortBy_collection_Given_values_Then_returnsSortedValues() {
        assertThat(List.of(C + C, A + A + A, B).sortBy(String::length)).containsExactly(B, C + C, A + A + A);
    }

    @Test
    void test_sortBy_stream_Given_values_Then_returnsSortedValues() {
        assertThat(Stream.of(C + C, A + A + A, B).sortBy(String::length)).containsExactly(B, C + C, A + A + A);
    }

    @Test
    void test_sortWith_collection_Given_empty_Then_returnsEmpty() {
        assertThat(Collections.<String>emptyList().sortWith(Comparator.naturalOrder())).isEmpty();
    }

    @Test
    void test_sortWith_collection_Given_values_Then_returnsSortedValues() {
        assertThat(List.of(C, A, B).sortWith(Comparator.naturalOrder())).containsExactly(A, B, C);
    }

    @Test
    void test_sortWith_stream_Given_empty_Then_returnsEmpty() {
        assertThat(Stream.<String>empty().sortWith(Comparator.naturalOrder())).isEmpty();
    }

    @Test
    void test_sortWith_stream_Given_values_Then_returnsSortedValues() {
        assertThat(Stream.of(C, A, B).sortWith(Comparator.naturalOrder())).containsExactly(A, B, C);
    }

    @Test
    void test_streamEachIf_Given_null_Then_returnsEmpty() {
        assertThat(NULL_COLLECTION.streamEachIf(tautology())).isEmpty();
    }

    @Test
    void test_streamEachIf_Given_values_Then_returnsStreamOfValuesMatchingPredicate() {
        assertThat(List.of(A, B, C).streamEachIf(IS_LETTER_A.negate())).containsExactly(B, C);
    }

    @Test
    void test_streamEachUnless_Given_null_Then_returnsEmpty() {
        assertThat(NULL_COLLECTION.streamEachUnless(tautology())).isEmpty();
    }

    @Test
    void test_streamEachUnless_Given_values_Then_returnsStreamOfValuesNotMatchingPredicate() {
        assertThat(List.of(A, B, C).streamEachUnless(IS_LETTER_A)).containsExactly(B, C);
    }

    @Test
    void test_streamEach_Given_null_Then_returnsEmpty() {
        assertThat(NULL_COLLECTION.streamEach()).isEmpty();
        assertThat(NULL_COLLECTION.streamEach(a -> a)).isEmpty();
        Iterable<String> iterable = null;
        assertThat(iterable.streamEach()).isEmpty();
        assertThat(NULL_ARRAY.streamEach()).isEmpty();
        assertThat(NULL_ARRAY.streamEach(a -> a)).isEmpty();
    }

    @Test
    void test_streamEach_Given_values_Then_returnsStreamOfValues() {
        assertThat(List.of(A, B, C).streamEach()).containsExactly(A, B, C);
        assertThat(List.of(A, B, C).streamEach(v -> v + v)).containsExactly(A + A, B + B, C + C);
        Iterable<String> iterable = List.of(A, B, C);
        assertThat(iterable.streamEach()).containsExactly(A, B, C);
        assertThat(new String[]{A, B, C}.streamEach()).containsExactly(A, B, C);
        assertThat(new String[]{A, B, C}.streamEach(a -> a)).containsExactly(A, B, C);
    }

    @Test
    void test_streamEntries_Given_empty_Then_returnsEmptyStream() {
        assertThat(Collections.emptyMap()).isEmpty();
    }

    @Test
    void test_streamEntries_Given_values_Then_returnsStreamOfMapEntries() {
        assertThat(Map.of(A, 1, B, 2, C, 3).streamEntries()).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, A, 1))
            .anySatisfy(e -> assertThatEntryEquals(e, B, 2))
            .anySatisfy(e -> assertThatEntryEquals(e, C, 3));
    }

    @Test
    void test_streamEntries_mapper_Given_values_Then_returnsStreamOfMapEntries() {
        assertThat(Map.of(A, 1, B, 2, C, 3)
            .streamEntries((k, v) -> k + v)).containsExactlyInAnyOrder(A + 1, B + 2, C + 3);
    }

    @Test
    void test_streamEntries_optionalMap_Given_empty_Then_returnsEmptyStream() {
        assertThat(Optional.of(Collections.emptyMap()).streamEntries()).isEmpty();
        assertThat(Optional.<Map<String, String>>empty().streamEntries()).isEmpty();
    }

    @Test
    void test_streamEntries_optionalMap_Given_values_Then_returnsStreamOfMapEntries() {
        assertThat(Optional.of(Map.of(A, 1, B, 2, C, 3)).streamEntries()).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, A, 1))
            .anySatisfy(e -> assertThatEntryEquals(e, B, 2))
            .anySatisfy(e -> assertThatEntryEquals(e, C, 3));
    }

    @Test
    void test_streamEntries_optionalMap_mapper_Given_values_Then_returnsStreamOfMapEntries() {
        assertThat(Optional.of(Map.of(A, 1, B, 2, C, 3))
            .streamEntries((k, v) -> k + v)).containsExactlyInAnyOrder(A + 1, B + 2, C + 3);
    }

    @Test
    void test_streamIndexes_Given_emptyList_Then_returnsEmpty() {
        assertThat(Collections.emptyList().streamIndexes()).isEmpty();
    }

    @Test
    void test_streamIndexes_Given_list_Then_returnsListIndexes() {
        assertThat(List.of("A", "B", "C").streamIndexes()).containsExactly(0, 1, 2);
    }

    @Test
    void test_streamIndexes_Given_nullList_Then_returnsEmpty() {
        assertThat(((List) null).streamIndexes()).isEmpty();
    }

    @Test
    void test_streamKeys_Given_empty_Then_returnsEmpty() {
        assertThat(Collections.emptyMap().streamKeys()).isEmpty();
    }

    @Test
    void test_streamKeys_Given_nullList_Then_returnsEmpty() {
        assertThat(((Map) null).streamKeys()).isEmpty();
    }

    @Test
    void test_streamKeys_Given_values_Then_returnsKeys() {
        assertThat(Map.of(A, 1, B, 2, C, 3).streamKeys()).containsExactlyInAnyOrder(A, B, C);
    }

    @Test
    void test_streamValues_Given_empty_Then_returnsEmpty() {
        assertThat(Collections.emptyMap().streamValues()).isEmpty();
    }

    @Test
    void test_streamValues_Given_null_Then_returnsEmpty() {
        assertThat(((Map) null).streamValues()).isEmpty();
    }

    @Test
    void test_streamValues_Given_values_Then_returnsValues() {
        assertThat(Map.of(A, 1, B, 2, C, 3).streamValues()).containsExactlyInAnyOrder(1, 2, 3);
    }

    @Test
    void test_streamValues_enum_Given_enum_Then_returnsValues() {
        assertThat(TestEnum.class.streamValues()).containsExactly(TestEnum.VA, TestEnum.VB, TestEnum.VC);
    }

    @Test
    void test_streamValues_optionalMap_Given_emptyMap_Then_returnsEmpty() {
        assertThat(Optional.of(Collections.emptyMap()).streamValues()).isEmpty();
    }

    @Test
    void test_streamValues_optionalMap_Given_empty_Then_returnsEmpty() {
        assertThat(Optional.empty().streamValues()).isEmpty();
    }

    @Test
    void test_streamValues_optionalMap_Given_values_Then_returnsValues() {
        assertThat(Optional.of(Map.of(A, 1, B, 2, C, 3)).streamValues()).containsExactlyInAnyOrder(1, 2, 3);
    }

    @Test
    void test_streamWithIndex_collection_Given_list_Then_returnsEntriesWithIndexAndListValue() {
        assertThat(List.of(A, B, C).streamWithIndex()).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, A, 0))
            .anySatisfy(e -> assertThatEntryEquals(e, B, 1))
            .anySatisfy(e -> assertThatEntryEquals(e, C, 2));
    }

    @Test
    void test_symmetricDifference_Given_sameElements_Then_returnsEmpty() {
        assertThat(List.of().symmetricDifference(List.of())).satisfies(p -> {
            assertThat(p.getLeft()).isEmpty();
            assertThat(p.getRight()).isEmpty();
        });
        assertThat(List.of(A, B, C).symmetricDifference(List.of(A, B, C))).satisfies(p -> {
            assertThat(p.getLeft()).isEmpty();
            assertThat(p.getRight()).isEmpty();
        });
    }

    @Test
    void test_symmetricDifference_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of().symmetricDifference(List.of(A, B))).satisfies(p -> {
            assertThat(p.getLeft()).isEmpty();
            assertThat(p.getRight()).containsExactly(A, B);
        });
        assertThat(List.of(A, B, C).symmetricDifference(List.of(A, B))).satisfies(p -> {
            assertThat(p.getLeft()).containsExactly(C);
            assertThat(p.getRight()).isEmpty();
        });
        assertThat(List.of(A, B, C).symmetricDifference(List.of())).satisfies(p -> {
            assertThat(p.getLeft()).containsExactly(A, B, C);
            assertThat(p.getRight()).isEmpty();
        });
    }

    @Test
    void test_symmetricDifference_extractor_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of("A", "AA", "AAA")
            .symmetricDifference(List.of("B", "BB", "BBB"), String::length)).satisfies(p -> {
                assertThat(p.getLeft()).isEmpty();
                assertThat(p.getRight()).isEmpty();
            });
        assertThat(List.of().symmetricDifference(List.of(A, B), String::length)).satisfies(p -> {
            assertThat(p.getLeft()).isEmpty();
            assertThat(p.getRight()).containsExactly(A, B);
        });
        assertThat(List.of("AA", "BBB", C, C)
            .symmetricDifference(List.of("01", "012"), String::length)).satisfies(p -> {
                assertThat(p.getLeft()).containsExactly(C, C);
                assertThat(p.getRight()).isEmpty();
            });
        assertThat(List.of(A, B, C).symmetricDifference(List.of(), String::length)).satisfies(p -> {
            assertThat(p.getLeft()).containsExactly(A, B, C);
            assertThat(p.getRight()).isEmpty();
        });
    }

    @Test
    void test_symmetricDifference_extractors_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of("AA", "BB", "CC")
            .symmetricDifference(List.of("XA", "YB", "ZC"), s -> s.charAt(0), s -> s.charAt(1))).satisfies(p -> {
                assertThat(p.getLeft()).isEmpty();
                assertThat(p.getRight()).isEmpty();
            });
        assertThat(List.<String>of()
            .symmetricDifference(List.of("AA", "BB"), s -> s.charAt(0), s -> s.charAt(1))).satisfies(p -> {
                assertThat(p.getLeft()).isEmpty();
                assertThat(p.getRight()).containsExactly("AA", "BB");
            });
        assertThat(List.of("A_", "B_", "C_", "C_")
            .symmetricDifference(List.of("_A", "_B", "_B", "_D", "_D"), s -> s.charAt(0), s -> s.charAt(1))).satisfies(p -> {
                assertThat(p.getLeft()).containsExactly("C_", "C_");
                assertThat(p.getRight()).containsExactly("_D", "_D");
            });
        assertThat(List.of("A_", "B_", "C_")
            .symmetricDifference(List.<String>of("A_", "B_", "C_"), s -> s.charAt(0), s -> s.charAt(1))).satisfies(p -> {
                assertThat(p.getLeft()).containsExactly("A_", "B_", "C_");
                assertThat(p.getRight()).containsExactly("A_", "B_", "C_");
            });
    }

    @Test
    void test_tail_Given_list_Then_returnsAllElementsExceptFirst() {
        assertThat(List.of(1, 2, 3).tail()).containsExactly(2, 3);
    }

    @Test
    void test_toListMutable_Given_values_Then_returnsMutableList() {
        var listMutable = Stream.of(A, B, C).toMutableList();
        assertThat(listMutable).containsExactly(A, B, C);

        assertThatNoException().isThrownBy(() -> listMutable.add("x"));
    }

    @Test
    void test_toListReverse_Given_empty_Then_returnsEmpty() {
        assertThat(Stream.empty().toListReverse()).isEmpty();
        assertThat(new String[]{}.toListReverse()).isEmpty();
        assertThat(new int[]{}.toListReverse()).isEmpty();
    }

    @Test
    void test_toListReverse_Given_null_Then_returnsEmpty() {
        assertThat(NULL_ARRAY.toListReverse()).isEmpty();
    }

    @Test
    void test_toListReverse_Given_values_Then_returnsListOfValues_and_orderReversed() {
        assertThat(Stream.of(A, B, C).toListReverse()).containsExactly(C, B, A);
        assertThat(new String[]{A, B, C}.toListReverse()).containsExactly(C, B, A);
        assertThat(new int[]{1, 2, 3}.toListReverse()).containsExactly(3, 2, 1);
    }

    @Test
    void test_toList_Given_empty_Then_returnsEmpty() {
        assertThat(new String[]{}.toList()).isEmpty();
        assertThat(new int[]{}.toList()).isEmpty();
    }

    @Test
    void test_toList_Given_null_Then_returnsEmpty() {
        assertThat(NULL_ARRAY.toList()).isEmpty();
    }

    @Test
    void test_toList_Given_values_Then_returnsListOfValues() {
        assertThat(Stream.of(A, B, C).toList()).containsExactly(A, B, C);
        assertThat(new String[]{A, B, C}.toList()).containsExactly(A, B, C);
        assertThat(new int[]{1, 2, 3}.toList()).containsExactly(1, 2, 3);
    }

    @Test
    void test_toList_array_Given_values_Then_returnsListWithMappedValues() {
        assertThat(new String[]{A, B, C}.toList()).containsExactly(A, B, C);
    }

    @Test
    void test_toList_array_and_mapper_Given_values_Then_returnsListWithMappedValues() {
        assertThat(new String[]{A, B, C}.toList(a -> a + a)).containsExactly(A + A, B + B, C + C);
    }

    @Test
    void test_toList_stream_and_mapper_Given_values_Then_returnsListWithMappedValues() {
        assertThat(Stream.of(A, B, C).toList(a -> a + a)).containsExactly(A + A, B + B, C + C);
    }

    @Test
    void test_toList_stream_contravariant_Given_values_Then_returnsListOfSpecifiedSuperType() {
        Stream<? extends String> a = Stream.of(A, B, C);
        List<String> contravariant = a.toList().asContravariant();
        assertThat(contravariant).containsExactly(A, B, C);
    }

    @Test
    void test_toMapPairing_Given_values_Then_returnsMapOfValuesPaired() {
        assertThat(new String[]{A, B, C, D}.toMapPairing()).hasSize(2).containsEntry(A, B).containsEntry(C, D);
    }

    @Test
    void test_toMapPairing_mapping_Given_values_Then_returnsMapOfValuesPaired() {
        assertThat(new String[]{A, B, C, D}.toMapPairing(k -> "1" + k, v -> "2" + v)).hasSize(2)
            .containsEntry("1" + A, "2" + B)
            .containsEntry("1" + C, "2" + D);
    }

    @Test
    void test_toMap_Given_values_Then_returnsMap() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2"), Pair.of(C, "3")).toMap()).hasSize(3)
            .containsEntry(A, "1")
            .containsEntry(B, "2")
            .containsEntry(C, "3");
    }

    @Test
    void test_toMap_entries_Given_values_Then_returnsMap() {
        assertThat(new Pair[]{Pair.of(A, "1"), Pair.of(B, "2"), Pair.of(C, "3")}.toMap()).hasSize(3)
            .containsEntry(A, "1")
            .containsEntry(B, "2")
            .containsEntry(C, "3");
    }

    @Test
    void test_toMap_mapper_Given_empty_Then_returnsEmpty() {
        assertThat(Stream.<Pair<String, String>>empty().toMap(Pair::getKey, Pair::getValue)).isEmpty();
        assertThat(Collections.<Pair<String, String>>emptyList().toMap(Pair::getKey, Pair::getValue)).isEmpty();
    }

    @Test
    void test_toMap_mapper_Given_values_Then_returnsMapMappingKeyAndValue() {
        assertThat(Stream.of(Pair.of(A, 1), Pair.of(B, 2), Pair.of(C, 3))
            .toMap(Pair::getKey, Pair::getValue)).hasSize(3)
            .containsEntry(A, 1)
            .containsEntry(B, 2)
            .containsEntry(C, 3);

        assertThat(List.of(Pair.of(A, 1), Pair.of(B, 2), Pair.of(C, 3)).toMap(Pair::getKey, Pair::getValue)).hasSize(3)
            .containsEntry(A, 1)
            .containsEntry(B, 2)
            .containsEntry(C, 3);
    }

    @Test
    void test_toMap_mapping_Given_values_Then_returnsMapOfValuesPaired() {
        assertThat(new String[]{A, B, C, D}.toMap(k -> "1" + k, v -> "2" + v)).hasSize(4)
            .containsEntry("1" + A, "2" + A)
            .containsEntry("1" + B, "2" + B)
            .containsEntry("1" + C, "2" + C)
            .containsEntry("1" + D, "2" + D);
    }

    @Test
    void test_toMutableMapOf_Given_2Pairs_Then_constructsMapWithKeys() {
        assertThat(SequenceExtension.toMutableMapOf("a", "1", "b", "2")).containsExactly(new AbstractMap.SimpleEntry<>("a", "1"), new AbstractMap.SimpleEntry<>("b", "2"));
    }

    @Test
    void test_toMutableMapOf_Given_3Pairs_Then_constructsMapWithKeys() {
        assertThat(SequenceExtension.toMutableMapOf("a", "1", "b", "2", "c", "3")).containsExactly(new AbstractMap.SimpleEntry<>("a", "1"), new AbstractMap.SimpleEntry<>("b", "2"), new AbstractMap.SimpleEntry<>("c", "3"));
    }

    @Test
    void test_toMutableMapOf_Given_4Pairs_Then_constructsMapWithKeys() {
        assertThat(SequenceExtension.toMutableMapOf("a", "1", "b", "2", "c", "3", "d", "4"))
            .containsExactly(new AbstractMap.SimpleEntry<>("a", "1"), new AbstractMap.SimpleEntry<>("b", "2"), new AbstractMap.SimpleEntry<>("c", "3"), new AbstractMap.SimpleEntry<>("d", "4"));
    }

    @Test
    void test_toMutableMapOf_Given_5Pairs_Then_constructsMapWithKeys() {
        assertThat(SequenceExtension.toMutableMapOf("a", "1", "b", "2", "c", "3", "d", "4", "e", "5"))
            .containsExactly(new AbstractMap.SimpleEntry<>("a", "1"), new AbstractMap.SimpleEntry<>("b", "2"), new AbstractMap.SimpleEntry<>("c", "3"), new AbstractMap.SimpleEntry<>("d", "4"), new AbstractMap.SimpleEntry<>("e", "5"));
    }

    @Test
    void test_toMutableMapOf_Given_6Pairs_Then_constructsMapWithKeys() {
        assertThat(SequenceExtension.toMutableMapOf("a", "1", "b", "2", "c", "3", "d", "4", "e", "5", "f", "6")).containsExactly(new AbstractMap.SimpleEntry<>("a",
            "1"), new AbstractMap.SimpleEntry<>("b", "2"), new AbstractMap.SimpleEntry<>("c", "3"), new AbstractMap.SimpleEntry<>("d", "4"), new AbstractMap.SimpleEntry<>("e", "5"), new AbstractMap.SimpleEntry<>("f", "6"));
    }

    @Test
    void test_toMutableMapOf_Given_7Pairs_Then_constructsMapWithKeys() {
        assertThat(SequenceExtension.toMutableMapOf("a", "1", "b", "2", "c", "3", "d", "4", "e", "5", "f", "6", "g", "7")).containsExactly(new AbstractMap.SimpleEntry<>("a", "1"),
            new AbstractMap.SimpleEntry<>("b", "2"),
            new AbstractMap.SimpleEntry<>("c", "3"),
            new AbstractMap.SimpleEntry<>("d", "4"),
            new AbstractMap.SimpleEntry<>("e", "5"),
            new AbstractMap.SimpleEntry<>("f", "6"),
            new AbstractMap.SimpleEntry<>("g", "7"));
    }

    @Test
    void test_toMutableMapOf_Given_8Pairs_Then_constructsMapWithKeys() {
        assertThat(SequenceExtension.toMutableMapOf("a", "1", "b", "2", "c", "3", "d", "4", "e", "5", "f", "6", "g", "7", "h", "8")).containsExactly(new AbstractMap.SimpleEntry<>("a", "1"),
            new AbstractMap.SimpleEntry<>("b", "2"),
            new AbstractMap.SimpleEntry<>("c", "3"),
            new AbstractMap.SimpleEntry<>("d", "4"),
            new AbstractMap.SimpleEntry<>("e", "5"),
            new AbstractMap.SimpleEntry<>("f", "6"),
            new AbstractMap.SimpleEntry<>("g", "7"),
            new AbstractMap.SimpleEntry<>("h", "8"));
    }

    @Test
    void test_toMutableMapOf_Given_pair_Then_constructsMapWithKeyEqualsFirstElementAndValueEqualsSecondElement() {
        assertThat(SequenceExtension.toMutableMapOf("a", "1")).containsExactly(new AbstractMap.SimpleEntry<>("a", "1"));
    }

    @Test
    void test_toMutableMapPairing_Given_20Pairs_Then_constructsMapWithKeys() {
        assertThat(SequenceExtension
            .toMapPairing(new String[]{
                "a",
                "1",
                "b",
                "2",
                "c",
                "3",
                "d",
                "4",
                "e",
                "5",
                "f",
                "6",
                "g",
                "7",
                "h",
                "8",
                "i",
                "9",
                "j",
                "10",
                "k",
                "11",
                "l",
                "12",
                "m",
                "13",
                "n",
                "14",
                "o",
                "15",
                "p",
                "16",
                "q",
                "17",
                "r",
                "18",
                "s",
                "19",
                "t",
                "20"}))
            .containsExactly(new AbstractMap.SimpleEntry<>("a", "1"),
                new AbstractMap.SimpleEntry<>("b", "2"),
                new AbstractMap.SimpleEntry<>("c", "3"),
                new AbstractMap.SimpleEntry<>("d", "4"),
                new AbstractMap.SimpleEntry<>("e", "5"),
                new AbstractMap.SimpleEntry<>("f", "6"),
                new AbstractMap.SimpleEntry<>("g", "7"),
                new AbstractMap.SimpleEntry<>("h", "8"),
                new AbstractMap.SimpleEntry<>("i", "9"),
                new AbstractMap.SimpleEntry<>("j", "10"),
                new AbstractMap.SimpleEntry<>("k", "11"),
                new AbstractMap.SimpleEntry<>("l", "12"),
                new AbstractMap.SimpleEntry<>("m", "13"),
                new AbstractMap.SimpleEntry<>("n", "14"),
                new AbstractMap.SimpleEntry<>("o", "15"),
                new AbstractMap.SimpleEntry<>("p", "16"),
                new AbstractMap.SimpleEntry<>("q", "17"),
                new AbstractMap.SimpleEntry<>("r", "18"),
                new AbstractMap.SimpleEntry<>("s", "19"),
                new AbstractMap.SimpleEntry<>("t", "20"));
    }

    @Test
    void test_toMutableMapPairing_Given_values_Then_returnsMapOfValuesPaired() {
        assertThat(new String[]{A, B, C, D}.toMutableMapPairing()).hasSize(2).containsEntry(A, B).containsEntry(C, D);
    }

    @Test
    void test_toMutableMapPairing_mapping_Given_values_Then_returnsMapOfValuesPaired() {
        assertThat(new String[]{A, B, C, D}.toMutableMapPairing(k -> "1" + k, v -> "2" + v)).hasSize(2)
            .containsEntry("1" + A, "2" + B)
            .containsEntry("1" + C, "2" + D);
    }

    @Test
    void test_toMutableMap_Given_values_Then_returnsMap() {
        assertThat(Stream.of(Pair.of(A, "1"), Pair.of(B, "2"), Pair.of(C, "3")).toMutableMap()).hasSize(3)
            .containsEntry(A, "1")
            .containsEntry(B, "2")
            .containsEntry(C, "3");
    }

    @Test
    void test_toMutableMap_entries_Given_values_Then_returnsMap() {
        assertThat(SequenceExtension.toMutableMap(Pair.of(A, "1"), Pair.of(B, "2"), Pair.of(C, "3"))).hasSize(3)
            .containsEntry(A, "1")
            .containsEntry(B, "2")
            .containsEntry(C, "3");
    }

    @Test
    void test_toMutableMap_mapper_Given_empty_Then_returnsEmpty() {
        assertThat(Stream.<Pair<String, String>>empty().toMutableMap(Pair::getKey, Pair::getValue)).isEmpty();
        assertThat(Collections.<Pair<String, String>>emptyList().toMutableMap(Pair::getKey, Pair::getValue)).isEmpty();
    }

    @Test
    void test_toMutableMap_mapper_Given_values_Then_returnsMapMappingKeyAndValue() {
        assertThat(Stream.of(Pair.of(A, 1), Pair.of(B, 2), Pair.of(C, 3))
            .toMutableMap(Pair::getKey, Pair::getValue)).hasSize(3)
            .containsEntry(A, 1)
            .containsEntry(B, 2)
            .containsEntry(C, 3);

        assertThat(List.of(Pair.of(A, 1), Pair.of(B, 2), Pair.of(C, 3))
            .toMutableMap(Pair::getKey, Pair::getValue)).hasSize(3)
            .containsEntry(A, 1)
            .containsEntry(B, 2)
            .containsEntry(C, 3);
    }

    @Test
    void test_toMutableMap_mapping_Given_values_Then_returnsMapOfValuesPaired() {
        assertThat(new String[]{A, B, C, D}.toMutableMap(k -> "1" + k, v -> "2" + v)).hasSize(4)
            .containsEntry("1" + A, "2" + A)
            .containsEntry("1" + B, "2" + B)
            .containsEntry("1" + C, "2" + C)
            .containsEntry("1" + D, "2" + D);
    }

    @Test
    void test_toSet_collection_Given_values_Then_returnsSetWithMappedValues() {
        assertThat(List.of(A, B, C).toSet()).containsExactlyInAnyOrder(A, B, C);
    }

    @Test
    void test_toSet_collection_and_mapper_Given_values_Then_returnsSetWithMappedValues() {
        assertThat(List.of(A, B, C).toSet(a -> a + a)).containsExactlyInAnyOrder(A + A, B + B, C + C);
    }

    @Test
    void test_toSet_stream_and_mapper_Given_values_Then_returnsSetWithMappedValues() {
        assertThat(Stream.of(A, B, C).toSet(a -> a + a)).containsExactlyInAnyOrder(A + A, B + B, C + C);
    }

    @Test
    void test_toSortedMap_Given_values_Then_returnsSortedMap() {
        Map<String, Integer> mapSorted = Stream.of(Pair.of(B, 2), Pair.of(C, 3), Pair.of(A, 1)).toSortedMap();

        assertThat(mapSorted).hasSize(3).containsEntry(A, 1).containsEntry(B, 2).containsEntry(C, 3);
    }

    @Test
    void test_toSortedMap_Given_values_Then_returnsSortedMapWithMappedValues() {
        assertThat(toSortedMap(new String[]{"Z", "A", "C", "B"}, s -> s, s -> s)).containsEntry("A", "A")
            .containsEntry("B", "B")
            .containsEntry("C", "C")
            .containsEntry("Z", "Z");

        assertThat(toSortedMap(List.of("Z", "A", "C", "B"), s -> s, s -> s)).containsEntry("A", "A")
            .containsEntry("B", "B")
            .containsEntry("C", "C")
            .containsEntry("Z", "Z");

    }

    @Test
    void test_unfold_Given_noExpansion_Then_returnsCollectionWithInitialValue() {
        assertThat(Long.valueOf(1).unfold(z -> Collections.emptyList())).containsExactly(1L);
        assertThat(Long.valueOf(1).unfold(z -> null)).containsExactly(1L);
    }

    @Test
    void test_unfold_Given_value_Then_returnsCollectionOfMappedValueExpandedWhileExpansionDoesNotReturnEmpty() {
        assertThat(Long.valueOf(1)
            .unfold(z -> z < 5 ? List.of(z + 1) : Collections.emptyList())).containsExactly(1L, 2L, 3L, 4L, 5L);
        assertThat(Long.valueOf(1).unfold(z -> z < 5 ? List.of(z + 1) : null)).containsExactly(1L, 2L, 3L, 4L, 5L);
    }

    @Test
    void test_unfold_mapper_Given_value_Then_returnsCollectionOfMappedValueExpandedWhileExpansionDoesNotReturnEmpty() {
        var actual = Long.valueOf(1).unfold(z -> z < 5 ? List.of(z + 1) : Collections.emptyList(), (v, p) -> "v:" + v);

        assertThat(actual).containsExactly("v:1", "v:2", "v:3", "v:4", "v:5");
    }

    @Test
    void test_windowed_collection_Given_empty_Then_returnsEmpty() {
        assertThat(Collections.emptyList().windowed(2)).isEmpty();
    }

    @Test
    void test_windowed_collection_Given_longerThanSize_Then_returnsListsContainingGroups() {
        assertThat(List.of(A, B, C, D, E)
            .windowed(2)).containsExactly(List.of(A, B), List.of(B, C), List.of(C, D), List.of(D, E));
        assertThat(List.of(A, B, C, D, E)
            .windowed(3)).containsExactly(List.of(A, B, C), List.of(B, C, D), List.of(C, D, E));
    }

    @Test
    void test_windowed_collection_Given_sameSizeAsList_Then_returnsListWithSingleListWithValues() {
        assertThat(List.of(A, B, C).windowed(3)).containsExactly(List.of(A, B, C));
    }

    @Test
    void test_windowed_collection_Given_shorterThanSize_Then_returnsEmpty() {
        assertThat(List.of(A).windowed(2)).isEmpty();
        assertThat(List.of(A, B, C, D).windowed(5)).isEmpty();
    }

    @Test
    void test_windowed_collection_Given_shorterThanSize_and_partialWindows_Then_returnsListWithRemaining() {
        assertThat(List.of(A).windowed(2, true)).containsExactly(List.of(A));
        assertThat(List.of(A, B, C, D)
            .windowed(5, true)).containsExactly(List.of(A, B, C, D), List.of(B, C, D), List.of(C, D), List.of(D));
    }

    @Test
    void test_windowed_collection_Given_windowSizeEqualsStep_Then_returnsListOfElementsWithoutElementDuplication() {
        assertThat(List.of(A, B, C, D, E)
            .windowed(2, 2, true)).containsExactly(List.of(A, B), List.of(C, D), List.of(E));

    }

    @Test
    void test_windowed_collection_step_Given_longerThanSize_Then_returnsListsContainingGroups() {
        assertThat(List.of(A, B, C, D, E).windowed(3, 2)).containsExactly(List.of(A, B, C), List.of(C, D, E));
        assertThat(List.of(A, B, C, D, E)
            .windowed(3, 2, true)).containsExactly(List.of(A, B, C), List.of(C, D, E), List.of(E));
        assertThat(List.of(A, B, C, D, E, F, G)
            .windowed(2, 2)).containsExactly(List.of(A, B), List.of(C, D), List.of(E, F));
        assertThat(List.of(A, B, C, D, E, F, G)
            .windowed(2, 2, true)).containsExactly(List.of(A, B), List.of(C, D), List.of(E, F), List.of(G));
    }

    @Test
    void test_windowed_collection_step_Given_stepLargerThatnWindowSize_Then_returnsListsContainingSparseGroups() {
        assertThat(List.of(A, B, C, D, E, F, G, H)
            .windowed(2, 3)).containsExactly(List.of(A, B), List.of(D, E), List.of(G, H));
    }

    @Test
    void test_windowed_stream_Given_elements_Then_returnsStreamsOfSlidingWindowElements() {
        assertThat(Stream.of(A, B, C, D, E).windowed(2)).hasSize(4).satisfies(c -> {
            assertThat(c).element(0).satisfies(e -> assertThat(e).containsExactly(A, B));
            assertThat(c).element(1).satisfies(e -> assertThat(e).containsExactly(B, C));
            assertThat(c).element(2).satisfies(e -> assertThat(e).containsExactly(C, D));
            assertThat(c).element(3).satisfies(e -> assertThat(e).containsExactly(D, E));
        });
    }

    @Test
    void test_windowed_stream_step_Given_windowSizeEqualsStep_Then_returnsListOfElementsWithoutElementDuplication() {
        assertThat(Stream.of(A, B, C, D, E, F, G, H).windowed(2, 3)).hasSize(3).satisfies(c -> {
            assertThat(c).element(0).satisfies(e -> assertThat(e).containsExactly(A, B));
            assertThat(c).element(1).satisfies(e -> assertThat(e).containsExactly(D, E));
            assertThat(c).element(2).satisfies(e -> assertThat(e).containsExactly(G, H));
        });
    }

    @Test
    void test_zipAll_Given_differentLengthLists_Then_returnsListPairedElementsWithSameIndexLongestListAndFillingMissingValues() {
        assertThat(List.of(A, B).zip(List.of(1, 2, 3), "z", 0)).hasSize(3).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
            assertThatValueSatisfies(l, 2, "z", 3);
        });
        assertThat(List.of(A, B, C).zip(List.of(1, 2), "z", 0)).hasSize(3).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
            assertThatValueSatisfies(l, 2, C, 0);
        });

        assertThat(List.of(A, B).zip(new ArrayList<>(), "z", 0)).hasSize(2).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 0);
            assertThatValueSatisfies(l, 1, B, 0);
        });

        assertThat(new ArrayList<>().zip(List.of(1), "z", 0)).hasSize(1)
            .satisfies(l -> assertThatValueSatisfies(l, 0, "z", 1));
    }

    @Test
    void test_zipAll_Given_differentLengthStreams_Then_returnsStreamPairedElementsWithSameIndexLongestListAndFillingMissingValues() {
        assertThat(Stream.of(A, B, C, "d")
            .zip(Stream.of(1, 2, 3), "X", 9, (a, b) -> a + b)).containsExactly("a1", "b2", "c3", "d9");

        assertThat(Stream.of(A, B, C)
            .zip(Stream.of(1, 2, 3, 4), "X", 9, (a, b) -> a + b)).containsExactly("a1", "b2", "c3", "X4");
    }

    @Test
    void test_zipAll_Given_sameLengthLists_Then_returnsListPairedElementsWithSameIndex() {
        assertThat(List.of(A, B, C).zip(List.of(1, 2, 3), "z", 0)).hasSize(3).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
            assertThatValueSatisfies(l, 2, C, 3);
        });
    }

    @Test
    void test_zipAll_stream_Given_sameLengthStreams_Then_returnsStreamPairedElementsWithSameIndex() {
        assertThat(Stream.of(A, B, C)
            .zip(Stream.of(1, 2, 3), "X", "Y", (a, b) -> a + b)).containsExactly("a1", "b2", "c3");
    }

    @Test
    void test_zipWithIndex_collection_Given_list_Then_returnsListOfEntriesWithIndexAndListValue() {
        assertThat(List.of(A, B, C).zipWithIndex()).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, A, 0))
            .anySatisfy(e -> assertThatEntryEquals(e, B, 1))
            .anySatisfy(e -> assertThatEntryEquals(e, C, 2));
    }

    @Test
    void test_zipWithIndex_listZipper_Given_list_Then_returnsListOfEntriesWithIndexAndListValue() {
        assertThat(List.of(A, B, C).zipWithIndex((v, i) -> v + i)).containsExactly("a0", "b1", "c2");
    }

    @Test
    void test_zipWithIndex_streamZipper_Given_stream_Then_returnsStreamOfEntriesWithIndexAndStreamValue() {
        assertThat(Stream.of(A, B, C).zipWithIndex((v, i) -> v + i)).containsExactly("a0", "b1", "c2");
    }

    @Test
    void test_zipWithNext_array_Given_empty_Then_returnsEmpty() {
        assertThat(new String[]{}.zipWithNext()).isEmpty();
    }

    @Test
    void test_zipWithNext_array_Given_oddValuesCount_Then_returnsStreamOfPairsWithLastValueEqualsNull() {
        assertThat(new String[]{A}.zipWithNext()).singleElement().satisfies(e -> assertThatEntryEquals(e, A, null));

        assertThat(new String[]{A, B, C, D, E}.zipWithNext()).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, A, B))
            .anySatisfy(e -> assertThatEntryEquals(e, C, D))
            .anySatisfy(e -> assertThatEntryEquals(e, E, null));
    }

    @Test
    void test_zipWithNext_array_Given_values_Then_returnsStreamOfPairsOfConsecutiveValues() {
        assertThat(new String[]{A, B}.zipWithNext()).singleElement().satisfies(e -> assertThatEntryEquals(e, A, B));
        assertThat(new String[]{A, B, C, D}.zipWithNext()).hasSize(2)
            .anySatisfy(e -> assertThatEntryEquals(e, A, B))
            .anySatisfy(e -> assertThatEntryEquals(e, C, D));
    }

    @Test
    void test_zipWithNext_collection_Given_empty_Then_returnsEmpty() {
        assertThat(Collections.emptyList().zipWithNext()).isEmpty();
    }

    @Test
    void test_zipWithNext_collection_Given_oddValuesCount_Then_returnsStreamOfPairsWithLastValueEqualsNull() {
        assertThat(List.of(A).zipWithNext()).singleElement().satisfies(e -> assertThatEntryEquals(e, A, null));

        assertThat(List.of(A, B, C, D, E).zipWithNext()).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, A, B))
            .anySatisfy(e -> assertThatEntryEquals(e, C, D))
            .anySatisfy(e -> assertThatEntryEquals(e, E, null));
    }

    @Test
    void test_zipWithNext_collection_Given_values_Then_returnsStreamOfPairsOfConsecutiveValues() {
        assertThat(List.of(A, B).zipWithNext()).singleElement().satisfies(e -> assertThatEntryEquals(e, A, B));
        assertThat(List.of(A, B, C, D).zipWithNext()).hasSize(2)
            .anySatisfy(e -> assertThatEntryEquals(e, A, B))
            .anySatisfy(e -> assertThatEntryEquals(e, C, D));
    }

    @Test
    void test_zipWithNext_stream_Given_empty_Then_returnsEmpty() {
        assertThat(Stream.empty().zipWithNext()).isEmpty();
    }

    @Test
    void test_zipWithNext_stream_Given_oddValuesCount_Then_returnsStreamOfPairsWithLastValueEqualsNull() {
        assertThat(Stream.of(A).zipWithNext()).singleElement().satisfies(e -> assertThatEntryEquals(e, A, null));

        assertThat(Stream.of(A, B, C, D, E).zipWithNext()).hasSize(3)
            .anySatisfy(e -> assertThatEntryEquals(e, A, B))
            .anySatisfy(e -> assertThatEntryEquals(e, C, D))
            .anySatisfy(e -> assertThatEntryEquals(e, E, null));
    }

    @Test
    void test_zipWithNext_stream_Given_values_Then_returnsStreamOfPairsOfConsecutiveValues() {
        assertThat(Stream.of(A, B).zipWithNext()).singleElement().satisfies(e -> assertThatEntryEquals(e, A, B));
        assertThat(Stream.of(A, B, C, D).zipWithNext()).hasSize(2)
            .anySatisfy(e -> assertThatEntryEquals(e, A, B))
            .anySatisfy(e -> assertThatEntryEquals(e, C, D));
    }

    @Test
    void test_zip_Given_differentLengthLists_Then_returnsListPairedElementsWithSameIndexShortestList() {
        assertThat(List.of(A, B).zip(List.of(1, 2, 3))).hasSize(2).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
        });
        assertThat(List.of(A, B, C).zip(List.of(1, 2))).hasSize(2).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
        });

        assertThat(List.of(A, B).zip(List.of())).isEmpty();

        assertThat(List.of().zip(List.of(1))).isEmpty();
    }

    @Test
    void test_zip_Given_differentLength_Then_returnsListPairedElementsWithSameIndexShortestInput() {
        assertThat(Stream.of(A, B).zip(Stream.of(1, 2, 3))).hasSize(2).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
        });
        assertThat(Stream.of(A, B, C).zip(Stream.of(1, 2))).hasSize(2).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
        });

        assertThat(Stream.of(A, B).zip(Stream.of())).isEmpty();

        assertThat(Stream.of().zip(Stream.of(1))).isEmpty();
    }

    @Test
    void test_zip_Given_sameLength_Then_returnsStreamOfPairedElementsWithSameIndex() {
        assertThat(Stream.of(A, B, C).zip(Stream.of(1, 2, 3))).hasSize(3).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
            assertThatValueSatisfies(l, 2, C, 3);
        });
    }

    @Test
    void test_zip_collections_Given_differentLength_Then_returnsListPairedElementsWithSameIndexShortestInput() {
        assertThat(List.of(A, B).zip(List.of(1, 2, 3))).hasSize(2).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
        });

        assertThat(List.of(A, B, C).zip(List.of(1, 2))).hasSize(2).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
        });

        assertThat(List.of(A, B, C)
            .zip(List.of(1, 2))
            .stream()
            .asContravariantEntry()).containsExactly(Pair.of(A, 1), Pair.of(B, 2));

        assertThat(List.of(A, B).zip(List.of())).isEmpty();

        assertThat(List.of().zip(List.of(1))).isEmpty();
    }

    @Test
    void test_zip_collections_Given_sameLength_Then_returnsListOfPairedElementsWithSameIndex() {
        assertThat(List.of(A, B, C).zip(List.of(1, 2, 3))).hasSize(3).satisfies(l -> {
            assertThatValueSatisfies(l, 0, A, 1);
            assertThatValueSatisfies(l, 1, B, 2);
            assertThatValueSatisfies(l, 2, C, 3);
        });
    }

    @Test
    void test_zip_stream_Given_differentLengthStreams_Then_returnsShorterStreamPairedElementsWithSameIndex() {
        assertThat(Stream.of(A, B, C, "d").zip(Stream.of(1, 2, 3), (a, b) -> a + b)).containsExactly("a1", "b2", "c3");
    }

    @Test
    void test_zip_stream_Given_sameLengthStreams_Then_returnsStreamPairedElementsWithSameIndex() {
        assertThat(Stream.of(A, B, C).zip(Stream.of(1, 2, 3), (a, b) -> a + b)).containsExactly("a1", "b2", "c3");
    }

    private enum TestEnum {
        VA, VB, VC
    }

}
