package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.*;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.UnaryOperator;

import static org.assertj.core.api.Assertions.assertThat;

class TraversalTest {
    public static final Lens<TypeA, String> LENS_TYPE_A_VALUE_A = Lens.of(TypeA::getData, TypeA::withData);
    public static final Lens<Type, TypeA> LENS_TYPE_TYPE_A = Lens.of(Type::getTypeA, Type::withTypeA);
    public static final UnaryOperator<String> PREPEND_PREFIX = s -> "pr_" + s;
    TypeA aA = new TypeA("a_a");
    TypeA bA = new TypeA("b_a");
    TypeA cA = new TypeA("c_a");

    @Test
    void test_traversal_Given_list_Then_returnsLensThat_modifiesListValues() {
        var traversal = Traversal.<TypeA>ofList()
            .andThenEach(Lens.of(TypeA::getData, (s, v) -> s.toBuilder().data(v).build()));

        var list = List.of(new TypeA("a"), new TypeA("b"), new TypeA("c"));

        assertThat(traversal.modifyEach(list, PREPEND_PREFIX)).containsExactly(new TypeA("pr_a"),
            new TypeA("pr_b"),
            new TypeA("pr_c"));
    }

    @Test
    void test_traversal_ofArray_Given_values_Then_returnsLensThat_modifiesListValuesIfPredicateMatches() {
        var traversal = Traversal.ofArray(TypeA.class)
            .andThenEach(Lens.of(TypeA::getData, (s, v) -> s.toBuilder().data(v).build()));

        assertThat(traversal.modifyEachIf(new TypeA[]{new TypeA("a"), new TypeA("b"), new TypeA("c")},
            x -> x.contains("a"),
            PREPEND_PREFIX)).containsExactly(new TypeA("pr_a"), new TypeA("b"), new TypeA("c"));
    }

    @Test
    void test_traversal_ofList_Given_listWithNestedElements_Then_returnsLensThat_getsNestedValues() {
        var traversal = Traversal.<Type>ofList()
            .andThenEach(Lens.of(Type::getTypeA, (s, v) -> s.toBuilder().typeA(v).build()));

        assertThat(traversal.getValue(List.of(new Type("a", aA, new TypeB("a_b")),
            new Type("b", bA, new TypeB("b_b")),
            new Type("c", cA, new TypeB("c_b"))))).containsExactly(aA, bA, cA);
    }

    @Test
    void test_traversal_ofList_Given_list_Then_returnsLensThat_getsListValues() {
        var traversal = Traversal.<TypeA>ofList()
            .andThenEach(Lens.of(TypeA::getData, (s, v) -> s.toBuilder().data(v).build()));

        assertThat(traversal.getValue(List.of(new TypeA("a"), new TypeA("b"), new TypeA("c")))).containsExactly("a",
            "b",
            "c");
    }

    @Test
    void test_traversal_ofList_Given_list_Then_returnsLensThat_modifiesListValuesIfPredicateMatches() {
        var traversal = Traversal.<TypeA>ofList()
            .andThenEach(Lens.of(TypeA::getData, (s, v) -> s.toBuilder().data(v).build()));

        assertThat(traversal.modifyEachIf(List.of(new TypeA("a"), new TypeA("b"), new TypeA("c")),
            x -> x.contains("a"),
            PREPEND_PREFIX)).containsExactly(new TypeA("pr_a"), new TypeA("b"), new TypeA("c"));
    }

    @Test
    void test_traversal_ofList_Given_list_Then_returnsLensThat_modifiesListValuesUnlessPredicateMatches() {
        var traversal = Traversal.<TypeA>ofList()
            .andThenEach(Lens.of(TypeA::getData, (s, v) -> s.toBuilder().data(v).build()));

        assertThat(traversal.modifyEachUnless(List.of(new TypeA("a"), new TypeA("b"), new TypeA("c")),
            x -> x.contains("a"),
            PREPEND_PREFIX)).containsExactly(new TypeA("a"), new TypeA("pr_b"), new TypeA("pr_c"));
    }

    @Test
    void test_traversal_ofList_Given_list_Then_returnsLensThat_setsListValues() {
        var traversal = Traversal.<TypeA>ofList()
            .andThenEach(Lens.of(TypeA::getData, (s, v) -> s.toBuilder().data(v).build()));

        assertThat(traversal.setValue(List.of(new TypeA("a"), new TypeA("b"), new TypeA("c")),
            List.of("1", "2", "3"))).containsExactly(new TypeA("1"), new TypeA("2"), new TypeA("3"));
    }

    @Test
    void test_traversal_ofList_Given_nestedLists_Then_returnsLensThat_getsNestedValues() {
        var subA1 = new SubType(new TypeA("a1"), List.of(new TypeB("a11")));
        var subA2 = new SubType(new TypeA("a2"), List.of(new TypeB("a21"), new TypeB("a22")));
        var rootA = new Type("a", List.of(subA1, subA2), new String[]{"as1", "as2"});

        var traversal = Traversal.ofList(Type::getList, Type::withList);
        var modifiedTraversal = traversal.modifyEach(rootA, l -> {
            var typeA = l.getTypeA();
            return l.withTypeA(typeA.withData("changed_" + typeA.getData()));
        });
        assertThat(modifiedTraversal.getList()).allSatisfy(s -> assertThat(s.getTypeA()
            .getData()).contains("changed_"));

        var traversalComposed = Traversal.ofList(Type::getList, Type::withList) //
            .andThenEach(Lens.of(SubType::getTypeA, SubType::withTypeA))
            .andThenEach(Lens.of(TypeA::getData, TypeA::withData));

        var traversalComposedValues = traversalComposed.getValue(rootA);
        assertThat(traversalComposedValues).containsExactly("a1", "a2");

        var modifiedTraversalComposed = traversalComposed.modifyEach(rootA, PREPEND_PREFIX);
        assertThat(modifiedTraversalComposed.getList()).allSatisfy(s -> assertThat(s.getTypeA().getData()).startsWith(
            "pr_"));

        var newValues = traversalComposed.setValue(rootA, List.of("nv1", "nv2"));
        assertThat(newValues.getList()).allSatisfy(s -> assertThat(s.getTypeA().getData()).contains("nv"));
    }

    @Test
    void test_traversal_ofList_Given_nestedLists_Then_returnsLensThat_getsNestedValues2() {
        var subA1 = new SubType(new TypeA("a1"), List.of(new TypeB("a11")));
        var subA2 = new SubType(new TypeA("a2"), List.of(new TypeB("a21"), new TypeB("a22")));
        var rootA = new Type("a", List.of(subA1, subA2), null);

        var traversalDoubleComposed = Traversal.ofList(Type::getList, Type::withList) //
            .andThenEach(Traversal.ofList(SubType::getList, SubType::withList))
            .andThenEach(Lens.of(TypeB::getData, TypeB::withData));

        var values = traversalDoubleComposed.getValue(rootA);
        assertThat(values).containsExactly("a11", "a21", "a22");

        var modified = traversalDoubleComposed.modifyEach(rootA, PREPEND_PREFIX);
        assertThat(modified.getList()).allSatisfy(s -> assertThat(s.getList()).allSatisfy(ss -> assertThat(ss.getData()).startsWith(
            "pr_")));

        var newValues = traversalDoubleComposed.setValue(rootA, List.of("new_a11", "new_a21", "new_a22"));
        assertThat(newValues.getList()).allSatisfy(s -> assertThat(s.getList()).allSatisfy(ss -> assertThat(ss.getData()).startsWith(
            "new_")));

        var newValuesWithMoreThanNeeded = traversalDoubleComposed.setValue(rootA,
            List.of("new_a11", "new_a21", "new_a22", "new_a23"));
        assertThat(newValuesWithMoreThanNeeded.getList()).allSatisfy(s -> assertThat(s.getList()).allSatisfy(ss -> assertThat(
            ss.getData()).startsWith("new_")));
    }

    @Test
    void test_traversal_ofList_andThenTraversal_Given_listWithNestedElements_Then_returnsLensThat_getsNestedValues() {
        var traversal = Traversal.ListTraversal.<Type>ofList()
            .andThenEach(LENS_TYPE_TYPE_A)
            .andThenEach(LENS_TYPE_A_VALUE_A);

        var list = List.of(new Type("a", aA, new TypeB("a_b")),
            new Type("b", bA, new TypeB("b_b")),
            new Type("c", cA, new TypeB("c_b")));

        assertThat(traversal.getValue(list)).containsExactly(aA.getData(), bA.getData(), cA.getData());

        var actual = traversal.setValue(list, List.of("a_1", "b_1", "c_1"));
        assertThat(actual).containsExactly(new Type("a", new TypeA("a_1"), new TypeB("a_b")),
            new Type("b", new TypeA("b_1"), new TypeB("b_b")),
            new Type("c", new TypeA("c_1"), new TypeB("c_b")));
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class TypeA {
        private String data;
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class TypeB {
        private String data;
    }

    @Data
    @With
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    private static class Type {
        private String data;
        private List<SubType> list;
        private String[] array;
        private TypeA typeA;
        private TypeB typeB;

        Type(String data, List<SubType> list, String[] array) {
            this.data = data;
            this.list = list;
            this.array = array;
        }

        Type(String data, TypeA typeA, TypeB typeB) {
            this.data = data;
            this.typeA = typeA;
            this.typeB = typeB;
        }

        Type(String data) {
            this.data = data;
        }
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class SubType {
        private TypeA typeA;
        private List<TypeB> list;
    }

}
