package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

class QuadriConsumerTest {

    private static final Function<String, Integer> PARSE_INT = Integer::parseInt;

    private final AtomicInteger result = new AtomicInteger();
    private final QuadriConsumer<Integer, Integer, Integer, Integer> SUM = (a, b, c, d) -> result.set(a + b + c + d);

    @Test
    void test_composeLeft_Given_function_Then_returns_consumerThatAcceptsComposedType() {
        SUM.composeLeft(PARSE_INT).accept("1", 2, 4, 8);
        assertThat(result).hasValue(15);
    }

    @Test
    void test_composeMiddleLeft_Given_function_Then_returns_consumerThatAcceptsComposedType() {
        SUM.composeMiddleLeft(PARSE_INT).accept(1, "2", 4, 8);
        assertThat(result).hasValue(15);
    }

    @Test
    void test_composeMiddleRight_Given_function_Then_returns_consumerThatAcceptsComposedType() {
        SUM.composeMiddleRight(PARSE_INT).accept(1, 2, "4", 8);
        assertThat(result).hasValue(15);
    }

    @Test
    void test_composeRight_Given_function_Then_returns_consumerThatAcceptsComposedType() {
        SUM.composeRight(PARSE_INT).accept(1, 2, 4, "8");
        assertThat(result).hasValue(15);
    }

    @Test
    void test_compose_Given_function_Then_returns_consumerThatAcceptsComposedType() {
        SUM.compose(PARSE_INT, PARSE_INT, PARSE_INT, PARSE_INT).accept("1", "2", "4", "8");
        assertThat(result).hasValue(15);
    }
}
