package com.tincore.util.lang.tuple;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UnitTest {

    @Test
    void test_compareTo_Given_differentElements_Then_returnsGreaterThanByComparingElements() {
        assertThat(Unit.of("a")).isLessThan(Unit.of("x"));
        assertThat(Unit.of("x")).isGreaterThan(Unit.of("a"));

        assertThat(Unit.of("a")).isLessThan(Unit.ofMutable("x"));
        assertThat(Unit.of("x")).isGreaterThan(Unit.of("a"));

        assertThat(Unit.of("a")).isLessThan(Unit.ofMutable("x"));
        assertThat(Unit.of("x")).isGreaterThan(Unit.ofMutable("a"));
    }

    @Test
    void test_compareTo_Given_sameElements_Then_returnsZero() {
        assertThat(Unit.of("a")).isEqualByComparingTo(Unit.of("a"));
        assertThat(Unit.ofMutable("a")).isEqualByComparingTo(Unit.ofMutable("a"));
        assertThat(Unit.of("a")).isEqualByComparingTo(Unit.ofMutable("a"));
    }

    @Test
    void test_equals_Given_differentElements_Then_returnsFalse() {
        assertThat(Unit.of("a")).isNotEqualTo(Unit.of("x"));
        assertThat(Unit.of("x")).isNotEqualTo(Unit.of("a"));
    }

    @Test
    void test_equals_Given_sameElements_Then_returnsTrue() {
        assertThat(Unit.of("a")).isEqualTo(Unit.of("a"));
        assertThat(Unit.ofMutable("a")).isEqualTo(Unit.ofMutable("a"));
        assertThat(Unit.of("a")).isEqualTo(Unit.ofMutable("a"));
        assertThat(Unit.ofMutable("a")).isEqualTo(Unit.of("a"));
    }

    @Test
    void test_getHead_Given_instance_Then_returnsLeftmostValue() {
        assertThat(Unit.of("a").getHead()).isEqualTo("a");
    }

    @Test
    void test_getValue_Given_instance_Then_returnsRightValues() {
        assertThat(Unit.of("a").getValue()).isEqualTo("a");
    }

    @Test
    void test_hashCode_Given_differentElements_Then_returnsDifferent() {
        assertThat(Unit.of("a")).doesNotHaveSameHashCodeAs(Unit.of("x"));
        assertThat(Unit.of("x")).doesNotHaveSameHashCodeAs(Unit.of("a"));
    }

    @Test
    void test_hashCode_Given_sameElements_Then_returnsSame() {
        assertThat(Unit.of("a")).hasSameHashCodeAs(Unit.of("a"));
        assertThat(Unit.ofMutable("a")).hasSameHashCodeAs(Unit.ofMutable("a"));
        assertThat(Unit.of("a")).hasSameHashCodeAs(Unit.ofMutable("a"));
        assertThat(Unit.ofMutable("a")).hasSameHashCodeAs(Unit.of("a"));
    }

    @Test
    void test_map_Given_mappers_Then_returnsWithMappedRight() {
        assertThat(Unit.of("a").map(s -> s + "1")).isEqualTo(Unit.of("a1"));
    }

    @Test
    void test_stream_Given_elements_Then_returnsStreamOfElements() {
        assertThat(Unit.of("a").stream()).containsExactly("a");
    }

    @Test
    void test_withHead_Given_instance_Then_returnsInstanceWithFirstElementSet() {
        assertThat(Unit.of("a").withHead("x")).isEqualTo(Unit.of("x"));
    }

    @Test
    void test_withTail_Given_instance_Then_returnsInstanceWithNonFirstElementsSet() {
        assertThat(Unit.of("a").withTail(null)).isEqualTo(Unit.of("a"));
    }

}
