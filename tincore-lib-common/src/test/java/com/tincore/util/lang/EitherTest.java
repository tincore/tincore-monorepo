package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;

import static com.tincore.util.lang.Either.*;
import static com.tincore.util.lang.function.FunctionExtension.contradiction;
import static com.tincore.util.lang.function.FunctionExtension.tautology;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class EitherTest {

    private static final Either<String, String> RIGHT_A = ofRight("A", String.class);
    private static final Either<String, String> RIGHT_B = ofRight("B");
    private static final Either<String, String> RIGHT_C = ofRight("C");
    private static final Either<String, String> LEFT_1 = ofLeft("1", String.class);
    private static final Either<String, String> LEFT_2 = ofLeft("2");

    private final Consumer<String> consumerR = Mockito.mock(Consumer.class);
    private final Consumer<String> consumerL = Mockito.mock(Consumer.class);

    @Test
    public void new_test_tryEither_function_Given_throwsRuntimeException_and_mapAllExceptions_Then_returnsLeftWithException() {
        assertThat(tryEither(a -> {
            throw new RuntimeException("test");
        }, true).apply("x"))
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(l -> assertThat(l.getMessage()).isEqualTo("test"));
            });

        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> tryEither(a -> {
            throw new RuntimeException("test");
        }, false).apply("x"));

        assertThat(tryEither(a -> {
            throw new IOException("iotest");
        }, true).apply("x"))
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(l -> assertThat(l.getMessage()).isEqualTo("iotest"));
            });

        assertThat(tryEither(a -> {
            throw new IOException("iotest");
        }, false).apply("x"))
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(l -> assertThat(l.getMessage()).isEqualTo("iotest"));
            });
    }

    @Test
    public void test_either_optional_Given_empty_Then_returnsLeft() {
        assertThat(of(Optional.empty(), "B")).satisfies(e -> {
            assertThat(e.isRight()).isFalse();
            assertThat(e.getLeft()).isEqualTo("B");
        });
    }

    @Test
    public void test_either_optional_Given_notEmpty_Then_returnsRight() {
        assertThat(of(Optional.of("A"), "B")).satisfies(e -> {
            assertThat(e.isRight()).isTrue();
            assertThat(e.getRight()).isEqualTo("A");
        });
    }

    @Test
    public void test_either_suppliers_Given_rightSupplierNull_Then_returnsLeft() {
        assertThat(of(() -> "L", () -> null)).satisfies(e -> {
            assertThat(e.isRight()).isFalse();
            assertThat(e.getLeft()).isEqualTo("L");
        });
    }

    @Test
    public void test_either_suppliers_Given_rightSupplier_Then_returnsRight() {
        assertThat(of(() -> "L", () -> "R")).satisfies(e -> {
            assertThat(e.isRight()).isTrue();
            assertThat(e.getRight()).isEqualTo("R");
        });
    }

    @Test
    public void test_filterOrElse() {
        assertThat(RIGHT_A.filterOrElse(tautology(), "O1").getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(RIGHT_A.filterOrElse(contradiction(), "O2").getRight()).isEqualTo("O2");
        assertThat(LEFT_1.filterOrElse(tautology(), "O3").getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.filterOrElse(contradiction(), "O4").getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_filterOrElseGet() {
        assertThat(RIGHT_A.filterOrElseGet(tautology(), () -> "O1").getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(RIGHT_A.filterOrElseGet(contradiction(), () -> "O2").getRight()).isEqualTo("O2");
        assertThat(LEFT_1.filterOrElseGet(tautology(), () -> "O3").getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.filterOrElseGet(contradiction(), () -> "O4").getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_filterOrElseLeft() {
        assertThat(RIGHT_A.filterOrElseLeft(tautology(), "O1").getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(RIGHT_A.filterOrElseLeft(contradiction(), "O2").getLeft()).isEqualTo("O2");
        assertThat(LEFT_1.filterOrElseLeft(tautology(), "O3").getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.filterOrElseLeft(contradiction(), "O4").getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_filterOrElseLeftWithMapper() {
        assertThat(RIGHT_A.filterOrElseLeft(tautology(), v -> "O1").getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(RIGHT_A.filterOrElseLeft(contradiction(), v -> v + "O2").getLeft()).isEqualTo("AO2");
        assertThat(LEFT_1.filterOrElseLeft(tautology(), v -> "O3").getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.filterOrElseLeft(contradiction(), v -> "O4").getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_findIf() {
        assertThat(RIGHT_A.findIf(tautology()).get().getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(RIGHT_A.findIf(contradiction())).isEmpty();
        assertThat(LEFT_1.findIf(tautology())).isEmpty();
        assertThat(LEFT_1.findIf(contradiction())).isEmpty();
    }

    @Test
    public void test_findIfLeft() {
        assertThat(RIGHT_A.findIfLeft(tautology())).isEmpty();
        assertThat(RIGHT_A.findIfLeft(contradiction())).isEmpty();
        assertThat(LEFT_1.findIfLeft(tautology()).get().getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.findIfLeft(contradiction())).isEmpty();
    }

    @Test
    public void test_findIfNot() {
        assertThat(RIGHT_A.findIfNot(tautology())).isEmpty();
        assertThat(RIGHT_A.findIfNot(contradiction()).get().getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(LEFT_1.findIfNot(tautology())).isEmpty();
        assertThat(LEFT_1.findIfNot(contradiction())).isEmpty();
    }

    @Test
    public void test_findIfRight() {
        assertThat(RIGHT_A.findIfRight(tautology()).get().getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(RIGHT_A.findIfRight(contradiction())).isEmpty();
        assertThat(LEFT_1.findIfRight(tautology())).isEmpty();
        assertThat(LEFT_1.findIfRight(contradiction())).isEmpty();
    }

    @Test
    public void test_flatMap() {
        assertThat(RIGHT_A.flatMap(r -> RIGHT_B).getRight()).isEqualTo(RIGHT_B.getRight());
        assertThat(RIGHT_A.flatMap(l -> LEFT_1).getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.flatMap(l -> LEFT_2).getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.flatMap(l -> RIGHT_A).getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_flatMapLeft() {
        assertThat(RIGHT_A.flatMapLeft(r -> RIGHT_B).getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(RIGHT_A.flatMapLeft(l -> LEFT_1).getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(LEFT_1.flatMapLeft(l -> LEFT_2).getLeft()).isEqualTo(LEFT_2.getLeft());
        assertThat(LEFT_1.flatMapLeft(l -> RIGHT_A).getRight()).isEqualTo(RIGHT_A.getRight());
    }

    @Test
    public void test_flatMapRight() {
        assertThat(RIGHT_A.flatMapRight(r -> RIGHT_B).getRight()).isEqualTo(RIGHT_B.getRight());
        assertThat(RIGHT_A.flatMapRight(l -> LEFT_1).getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.flatMapRight(l -> LEFT_2).getLeft()).isEqualTo(LEFT_1.getLeft());
        assertThat(LEFT_1.flatMapRight(l -> RIGHT_A).getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_flatMap_Given_chainingWithAllRights_Then_returnsLastRight() {
        assertThat(RIGHT_A
            .flatMap(r -> ofRight(r + "1"))
            .flatMap(r -> ofRight(r + "2"))
            .flatMap(r -> ofRight(r + "3"))
            .getRight()).isEqualTo("A123");
    }

    @Test
    public void test_flatMap_Given_chainingWithOneLeft_Then_returnsFirstLeft() {
        assertThat(RIGHT_A
            .flatMap(r -> ofRight(r + "1"))
            .flatMap(r -> LEFT_1)
            .flatMap(r -> LEFT_2)
            .flatMap(r -> ofRight(r + "3"))
            .getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_flatMap_leftAndRight() {
        assertThat(RIGHT_A.flatMap(l -> LEFT_2, r -> RIGHT_B).getRight()).isEqualTo(RIGHT_B.getRight());
        assertThat(RIGHT_A.flatMap(l -> LEFT_1, r -> LEFT_2).getLeft()).isEqualTo(LEFT_2.getLeft());
        assertThat(LEFT_1.flatMap(l -> LEFT_2, r -> RIGHT_B).getLeft()).isEqualTo(LEFT_2.getLeft());
        assertThat(LEFT_1.flatMap(l -> RIGHT_A, r -> RIGHT_B).getRight()).isEqualTo(RIGHT_A.getRight());
    }

    @Test
    public void test_fold() {
        assertThat(RIGHT_A.<String>fold(l1 -> "LEFT", r1 -> "RIGHT")).isEqualTo("RIGHT");

        assertThat(LEFT_1.<String>fold(l2 -> "LEFT", r2 -> "RIGHT")).isEqualTo("LEFT");

        var foldRMix = RIGHT_A.fold(l -> 2, r -> "RIGHT");
        assertThat(foldRMix).isEqualTo("RIGHT");
        var foldLMix = LEFT_1.fold(l -> 2, r -> "RIGHT");
        assertThat(foldLMix).isEqualTo(2);

    }

    @Test
    public void test_get_Given_left_Then_throwsNoSuchElementException() {
        assertThatExceptionOfType(NoSuchElementException.class).isThrownBy(LEFT_1::get);
    }

    @Test
    public void test_get_Given_right_Then_returnsValue() {
        assertThat(RIGHT_A.get()).isEqualTo("A");
    }

    @Test
    public void test_ifLeft_Given_left_Then_doesNotCallConsumer() {
        LEFT_1.ifLeft(consumerL);
        verify(consumerL, times(1)).accept(LEFT_1.getLeft());
    }

    @Test
    public void test_ifLeft_Given_right_Then_callsConsumer() {
        RIGHT_A.ifLeft(consumerL);
        verifyNoInteractions(consumerL);
    }

    @Test
    public void test_ifPresent_Given_left_Then_doesNotCallConsumer() {
        LEFT_1.ifPresent(consumerR);
        verifyNoInteractions(consumerR);
    }

    @Test
    public void test_ifPresent_Given_right_Then_callsConsumer() {
        RIGHT_A.ifPresent(consumerR);
        verify(consumerR, times(1)).accept(RIGHT_A.get());
    }

    @Test
    public void test_ifRightOrElseThrow_Given_left_Then_throwsException() {
        assertThatIllegalStateException().isThrownBy(() -> LEFT_1.ifRightOrElseThrow(consumerR, x -> new IllegalStateException("failed")));
        verifyNoInteractions(consumerR);
    }

    @Test
    public void test_ifRightOrElseThrow_Given_right_Then_callsRightConsumer() {
        RIGHT_A.ifRightOrElseThrow(consumerR, x -> new RuntimeException("failed"));
        verify(consumerR, times(1)).accept(RIGHT_A.get());
    }

    @Test
    public void test_ifRightOrElse_Given_left_Then_callsLeftConsumer() {
        LEFT_1.ifRightOrElse(consumerR, consumerL);
        verify(consumerL, times(1)).accept(LEFT_1.getLeft());
        verifyNoInteractions(consumerR);
    }

    @Test
    public void test_ifRightOrElse_Given_right_Then_callsRightConsumer() {
        RIGHT_A.ifRightOrElse(consumerR, consumerL);
        verifyNoInteractions(consumerL);
        verify(consumerR, times(1)).accept(RIGHT_A.get());
    }

    @Test
    public void test_ifRight_Given_left_Then_doesNotCallConsumer() {
        LEFT_1.ifRight(consumerR);
        verifyNoInteractions(consumerR);
    }

    @Test
    public void test_ifRight_Given_right_Then_callsConsumer() {
        RIGHT_A.ifRight(consumerR);
        verify(consumerR, times(1)).accept(RIGHT_A.get());
    }

    @Test
    public void test_map() {
        assertThat(RIGHT_A.map(l -> 1, r -> 2).getRight()).isEqualTo(2);
        assertThat(LEFT_1.map(l -> 1, r -> 2).getLeft()).isEqualTo(1);
    }

    @Test
    public void test_mapLeft() {
        assertThat(RIGHT_A.mapLeft(l -> "L" + l).getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(LEFT_1.mapLeft(l -> "L" + l).getLeft()).isEqualTo("L1");
    }

    @Test
    public void test_mapRight() {
        assertThat(RIGHT_A.mapRight(r -> "R" + r).getRight()).isEqualTo("RA");
        assertThat(LEFT_1.mapRight(r -> "R" + r).getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_ofMerge_Given_anyLeftEithers_Then_ReturnsLeft() {
        assertThat(ofMerge(RIGHT_A, LEFT_1, RIGHT_C)).satisfies(e -> {
            assertThat(e.isLeft()).isTrue();
            assertThat(e.getLeft()).isEqualTo(LEFT_1.getLeft());
        });
    }

    @Test
    public void test_ofMerge_Given_empty_Then_ReturnsRightAndEmpty() {
        Either<String, ? extends List<String>> merge2 = ofMerge();
        assertThat(merge2).satisfies(e -> {
            assertThat(e.isRight()).isTrue();
            assertThat(e.getRight()).isEmpty();
        });
    }

    @Test
    public void test_ofMerge_Given_multipleRightEithers_Then_ReturnsRight() {
        var merge = ofMerge(RIGHT_A, RIGHT_B, RIGHT_C);
        assertThat(merge).satisfies(e -> {
            assertThat(e.isRight()).isTrue();
            assertThat(e.getRight()).containsExactly("A", "B", "C");
        });
    }

    @Test
    public void test_option_Given_either_Then_returnsEitherWrappedInOption() {
        assertThat(RIGHT_A.option()).hasValue(RIGHT_A);
    }

    @Test
    public void test_or() {
        assertThat(RIGHT_A.or(() -> LEFT_1).getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(RIGHT_A.or(() -> RIGHT_B).getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(LEFT_1.or(() -> RIGHT_A).getRight()).isEqualTo(RIGHT_A.getRight());
        assertThat(LEFT_1.or(() -> LEFT_2).getLeft()).isEqualTo(LEFT_1.getLeft());
    }

    @Test
    public void test_orElseGetValue_Given_left_Then_returnsLeftSupplierValue() {
        assertThat(LEFT_1.orElseGetValue(() -> "B")).isEqualTo("B");
    }

    @Test
    public void test_orElseGetValue_Given_right_Then_returnsSelf() {
        assertThat(RIGHT_A.orElseGetValue(() -> "B")).isEqualTo("A");
    }

    @Test
    public void test_orElseGet_Given_left_Then_returnsLeftSupplierValue() {
        assertThat(LEFT_1.orElseGet(() -> RIGHT_B)).isEqualTo(RIGHT_B);
    }

    @Test
    public void test_orElseGet_Given_right_Then_returnsSelf() {
        assertThat(RIGHT_A.orElseGet(() -> RIGHT_B)).isEqualTo(RIGHT_A);
    }

    @Test
    public void test_peek2_Given_left_Then_callsLeftConsumer() {
        assertThat(LEFT_1.peek(consumerL, consumerR)).isEqualTo(LEFT_1);
        verify(consumerL, times(1)).accept(LEFT_1.getLeft());
        verifyNoInteractions(consumerR);
    }

    @Test
    public void test_peek2_Given_right_Then_callsRightConsumer() {
        assertThat(RIGHT_A.peek(consumerL, consumerR)).isEqualTo(RIGHT_A);
        verifyNoInteractions(consumerL);
        verify(consumerR, times(1)).accept(RIGHT_A.get());
    }

    @Test
    public void test_peekLeft_Given_left_Then_doesNotCallConsumer() {
        assertThat(LEFT_1.peekLeft(consumerL)).isEqualTo(LEFT_1);
        verify(consumerL, times(1)).accept(LEFT_1.getLeft());
    }

    @Test
    public void test_peekLeft_Given_right_Then_callsConsumer() {
        assertThat(RIGHT_A.peekLeft(consumerL)).isEqualTo(RIGHT_A);
        verifyNoInteractions(consumerL);
    }

    @Test
    public void test_peekRight_Given_left_Then_doesNotCallConsumer() {
        assertThat(LEFT_1.peekRight(consumerR)).isEqualTo(LEFT_1);
        verifyNoInteractions(consumerR);
    }

    @Test
    public void test_peekRight_Given_right_Then_callsConsumer() {
        assertThat(RIGHT_A.peekRight(consumerR)).isEqualTo(RIGHT_A);
        verify(consumerR, times(1)).accept(RIGHT_A.get());
    }

    @Test
    public void test_peek_Given_left_Then_doesNotCallConsumer() {
        assertThat(LEFT_1.peek(consumerR)).isEqualTo(LEFT_1);
        verifyNoInteractions(consumerR);
    }

    @Test
    public void test_peek_Given_right_Then_callsConsumer() {
        assertThat(RIGHT_A.peek(consumerR)).isEqualTo(RIGHT_A);
        verify(consumerR, times(1)).accept(RIGHT_A.get());
    }

    @Test
    public void test_recover_Given_left_Then_returnsLeftSupplierValue() {
        assertThat(Either.ofLeft("1").recover(Either::ofRight))
            .satisfies(e -> assertThat(e.getRightOption()).hasValue("1"));
    }

    @Test
    public void test_recover_Given_right_Then_returnsSelf() {
        assertThat(RIGHT_A.recover(x -> RIGHT_B)).isEqualTo(RIGHT_A);
    }

    @Test
    public void test_stream_Given_left_Then_returnsEmptyStream() {
        assertThat(LEFT_1.stream()).isEmpty();
    }

    @Test
    public void test_stream_Given_right_Then_returnsStreamWithRight() {
        assertThat(RIGHT_A.stream()).containsExactly("A");
    }

    @Test
    public void test_swap_Given_left_Then_returnsReversedEither() {
        assertThat(ofRight("A").swap()).isEqualTo(ofLeft("A"));
    }

    @Test
    public void test_swap_Given_right_Then_returnsReversedEither() {
        assertThat(ofLeft("A").swap()).isEqualTo(ofRight("A"));
    }

    // @Test
    // public void test_tryEitherGet_runnableAndValue_Given_executesSuccessfully_Then_returnsRightWithValue() {
    // assertThat(Either.tryEitherGet(() -> {}, "val", false))
    // .satisfies(e -> {
    // assertThat(e.isRight()).isTrue();
    // assertThat(e.getRightOption()).hasValueSatisfying(l -> assertThat(l).isEqualTo("val"));
    // assertThat(e.isLeft()).isFalse();
    // });
    // }
    //
    // @Test
    // public void test_tryEitherGet_runnable_Given_executesSuccessfully_Then_returnsRightWithValue() {
    // assertThat(Either.tryEitherGet(() -> {}, false))
    // .satisfies(e -> {
    // assertThat(e.isRight()).isTrue();
    // assertThat(e.getRightOption()).hasValueSatisfying(l -> assertThat(l).isEqualTo(EITHER_RUNNABLE_SUCCESS));
    // assertThat(e.isLeft()).isFalse();
    // });
    // }
    //
    // @Test
    // public void test_tryEitherGet_supplier_Given_returnsValue_Then_returnsRightWithValue() {
    // assertThat(Either.tryEitherGet(() -> "val", (Function<Exception, ?>) e1 -> null, false))
    // .satisfies(e -> {
    // assertThat(e.isRight()).isTrue();
    // assertThat(e.getRightOption()).hasValueSatisfying(l -> assertThat(l).isEqualTo("val"));
    // assertThat(e.isLeft()).isFalse();
    // });
    // }

    @Test
    public void test_tryEither_biFunctionAndMapper_Given_noException_Then_returnsRight() {
        assertThat(tryEither(a -> a, (t, e) -> t.toString() + "_" + e.getMessage())
            .apply("x"))
            .satisfies(e -> {
                assertThat(e.isRight()).isTrue();
                assertThat(e.isLeft()).isFalse();
                assertThat(e.getRight()).isEqualTo("x");
            });
    }

    @Test
    public void test_tryEither_biFunctionAndMapper_Given_throwsException_Then_returnsLeftWithMappedException() {
        assertThat(tryEither(a -> {
            throw new Exception("test");
        }, (t, e) -> t.toString() + "_" + e.getMessage())
            .apply("x"))
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValue("x_test");
            });
    }

    @Test
    public void test_tryEither_functionAndMapper_Given_throwsException_Then_returnsLeftWithException() {
        assertThat(tryEither(a -> {
            throw new Exception("test");
        }, Throwable::getMessage).apply("x"))
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(s -> assertThat(s).isEqualTo("test"));
            });
    }

    @Test
    public void test_tryEither_function_Given_returnsValue_Then_returnsRightWithValue() {
        assertThat(tryEither(a -> a).apply("val"))
            .satisfies(e -> {
                assertThat(e.isRight()).isTrue();
                assertThat(e.isLeft()).isFalse();
                assertThat(e.getRightOption()).hasValueSatisfying(l -> assertThat(l).isEqualTo("val"));
            });
    }

    @Test
    public void test_tryEither_function_Given_throwsException_Then_returnsLeftWithException() {
        assertThat(tryEither(a -> {
            throw new Exception("test");
        }).apply("x"))
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(l -> assertThat(l.getMessage()).isEqualTo("test"));
            });
    }

    @Test
    public void test_tryEither_function_Given_throwsRuntimeException_and_mapAllExceptions_Then_returnsLeftWithException() {
        assertThat(tryEither(a -> {
            throw new RuntimeException("test");
        }, true).apply("x"))
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(l -> assertThat(l.getMessage()).isEqualTo("test"));
            });
    }

    @Test
    public void test_tryEither_function_Given_throwsRuntimeException_and_notMapAllExceptions_Then_throwsRuntimeException() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> tryEither(a -> {
            throw new RuntimeException("test");
        }).apply("x"));
    }

    @Test
    public void test_tryEither_supplierAndMapper_Given_throwsException_Then_returnsLeftWithException() {
        assertThat(tryEither(() -> {
            throw new Exception("test");
        }, Throwable::getMessage).get())
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(s -> assertThat(s).isEqualTo("test"));
            });
    }

    @Test
    public void test_tryEither_supplier_Given_error_Then_throwsError() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> tryEither(() -> {
            throw new Error("test");
        }).get());
    }

    @Test
    public void test_tryEither_supplier_Given_returnsValue_Then_returnsRightWithValue() {
        assertThat(tryEither(() -> "val").get())
            .satisfies(e -> {
                assertThat(e.isRight()).isTrue();
                assertThat(e.getRightOption()).hasValueSatisfying(l -> assertThat(l).isEqualTo("val"));
                assertThat(e.isLeft()).isFalse();
            });
    }

    @Test
    public void test_tryEither_supplier_Given_throwsException_Then_returnsLeftWithException() {
        assertThat(tryEither(() -> {
            throw new Exception("test");
        }).get())
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(l -> assertThat(l.getMessage()).isEqualTo("test"));
            });
    }

    @Test
    public void test_tryEither_supplier_Given_throwsRuntimeException_and_mapAllExceptions_Then_returnsLeftWithException() {
        assertThat(tryEither(() -> {
            throw new RuntimeException("test");
        }, true).get())
            .satisfies(e -> {
                assertThat(e.isRight()).isFalse();
                assertThat(e.isLeft()).isTrue();
                assertThat(e.getLeftOption()).hasValueSatisfying(l -> assertThat(l.getMessage()).isEqualTo("test"));
            });
    }

    @Test
    public void test_tryEither_supplier_Given_throwsRuntimeException_and_notMapAllExceptions_Then_throwsRuntimeException() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> tryEither(() -> {
            throw new RuntimeException("test");
        }).get());
    }

}
