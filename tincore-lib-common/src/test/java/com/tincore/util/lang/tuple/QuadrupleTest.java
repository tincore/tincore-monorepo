package com.tincore.util.lang.tuple;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

class QuadrupleTest {

    @Test
    void test_compareTo_Given_differentElements_Then_returnsGreaterThanByComparingElements() {
        assertThat(Quadruple.of("a", "b", "c", "d")).isLessThan(Quadruple.of("x", "b", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isLessThan(Quadruple.of("a", "x", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isLessThan(Quadruple.of("a", "b", "x", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isLessThan(Quadruple.of("a", "b", "c", "x"));

        assertThat(Quadruple.of("x", "b", "c", "d")).isGreaterThan(Quadruple.of("a", "b", "c", "d"));
        assertThat(Quadruple.of("a", "x", "c", "d")).isGreaterThan(Quadruple.of("a", "b", "c", "d"));
        assertThat(Quadruple.of("a", "b", "x", "d")).isGreaterThan(Quadruple.of("a", "b", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "x")).isGreaterThan(Quadruple.of("a", "b", "c", "d"));

        assertThat(Quadruple.of("a", "b", "c", "d")).isLessThan(Quadruple.ofMutable("x", "b", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isLessThan(Quadruple.ofMutable("a", "x", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isLessThan(Quadruple.ofMutable("a", "b", "x", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isLessThan(Quadruple.ofMutable("a", "b", "c", "x"));

        assertThat(Quadruple.of("x", "b", "c", "d")).isGreaterThan(Quadruple.ofMutable("a", "b", "c", "d"));
        assertThat(Quadruple.of("a", "x", "c", "d")).isGreaterThan(Quadruple.ofMutable("a", "b", "c", "d"));
        assertThat(Quadruple.of("a", "b", "x", "d")).isGreaterThan(Quadruple.ofMutable("a", "b", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "x")).isGreaterThan(Quadruple.ofMutable("a", "b", "c", "d"));
    }

    @Test
    void test_compareTo_Given_sameElements_Then_returnsZero() {
        assertThat(Quadruple.of("a", "b", 1, 2)).isEqualByComparingTo(Quadruple.of("a", "b", 1, 2));
        assertThat(Quadruple.ofMutable("a", "b", 1, 2)).isEqualByComparingTo(Quadruple.ofMutable("a", "b", 1, 2));
        assertThat(Quadruple.of("a", "b", 1, 2)).isEqualByComparingTo(Quadruple.ofMutable("a", "b", 1, 2));
    }

    @Test
    void test_equals_Given_differentElements_Then_returnsFalse() {
        assertThat(Quadruple.of("a", "b", "c", "d")).isNotEqualTo(Quadruple.of("x", "b", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isNotEqualTo(Quadruple.of("a", "x", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isNotEqualTo(Quadruple.of("a", "b", "x", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).isNotEqualTo(Quadruple.of("a", "b", "c", "x"));
    }

    @Test
    void test_equals_Given_sameElements_Then_returnsTrue() {
        assertThat(Quadruple.of("a", "b", 1, 2)).isEqualTo(Quadruple.of("a", "b", 1, 2));
        assertThat(Quadruple.ofMutable("a", "b", 1, 2)).isEqualTo(Quadruple.ofMutable("a", "b", 1, 2));
        assertThat(Quadruple.of("a", "b", 1, 2)).isEqualTo(Quadruple.ofMutable("a", "b", 1, 2));
    }

    @Test
    void test_getHead_Given_instance_Then_returnsLeftmostValue() {
        assertThat(Quadruple.of("a", "b", "c", "d").getHead()).isEqualTo("a");
    }

    @Test
    void test_getTail_Given_instance_Then_returnsRightValues() {
        assertThat(Quadruple.of("a", "b", "c", "d").getTail()).isEqualTo(Triple.of("b", "c", "d"));
    }

    @Test
    void test_hashCode_Given_differentElements_Then_returnsDifferent() {
        assertThat(Quadruple.of("a", "b", "c", "d")).doesNotHaveSameHashCodeAs(Quadruple.of("x", "b", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).doesNotHaveSameHashCodeAs(Quadruple.of("a", "x", "c", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).doesNotHaveSameHashCodeAs(Quadruple.of("a", "b", "x", "d"));
        assertThat(Quadruple.of("a", "b", "c", "d")).doesNotHaveSameHashCodeAs(Quadruple.of("a", "b", "c", "x"));
    }

    @Test
    void test_hashCode_Given_sameElements_Then_returnsSame() {
        assertThat(Quadruple.of("a", "b", 1, 2)).hasSameHashCodeAs(Quadruple.of("a", "b", 1, 2));
        assertThat(Quadruple.ofMutable("a", "b", 1, 2)).hasSameHashCodeAs(Quadruple.ofMutable("a", "b", 1, 2));
        assertThat(Quadruple.of("a", "b", 1, 2)).hasSameHashCodeAs(Quadruple.ofMutable("a", "b", 1, 2));
    }

    @Test
    void test_mapRight_Given_mapper_Then_returnsWithFunctionAppliedToRight() {
        assertThat(Quadruple.of("a", "b", "c", "d").mapRight(s -> s + "1"))
            .isEqualTo(Quadruple.of("a", "b", "c", "d1"));
    }

    @Test
    void test_map_Given_mappers_Then_returnsQuadrupleWithMappedValues() {
        assertThat(Quadruple.of("a", "b", "c", "d").map(s -> s + "1", s -> s + "2", s -> s + "3", s -> s + "4"))
            .isEqualTo(Quadruple.of("a1", "b2", "c3", "d4"));
    }

    @Test
    void test_ofIterable_Given_autoFill_listLongerThanArity_Then_returnsInstanceWithMissinElementsAsNull() {
        assertThat(Quadruple.ofIterable(List.of("a", "b", "c", "d", "e"), true))
            .isEqualTo(Quadruple.of("a", "b", "c", "d"));
    }

    @Test
    void test_ofIterable_Given_autoFill_listShorterThanArity_Then_returnsInstanceWithMissinElementsAsNull() {
        assertThat(Quadruple.ofIterable(List.of(), true))
            .isEqualTo(Quadruple.of(null, null, null, null));
        assertThat(Quadruple.ofIterable(List.of("a"), true))
            .isEqualTo(Quadruple.of("a", null, null, null));
        assertThat(Quadruple.ofIterable(List.of("a", "b"), true))
            .isEqualTo(Quadruple.of("a", "b", null, null));
        assertThat(Quadruple.ofIterable(List.of("a", "b", "c"), true))
            .isEqualTo(Quadruple.of("a", "b", "c", null));
    }

    @Test
    void test_ofIterable_Given_listLongerThanArity_Then_throwsIllegalArgumentException() {
        assertThatIllegalArgumentException().isThrownBy(() -> Quadruple.ofIterable(List.of("a", "b", "c", "d", "e")));
    }

    @Test
    void test_ofIterable_Given_listOfSameSizeAsArity_Then_returnsInstanceWithListElements() {
        assertThat(Quadruple.ofIterable(List.of("a", "b", "c", "d")))
            .isEqualTo(Quadruple.of("a", "b", "c", "d"));
    }

    @Test
    void test_ofIterable_Given_listOfShorterThanArity_Then_throwsIllegalArgumentException() {
        assertThatIllegalArgumentException().isThrownBy(() -> Quadruple.ofIterable(List.of()));
        assertThatIllegalArgumentException().isThrownBy(() -> Quadruple.ofIterable(List.of("a")));
        assertThatIllegalArgumentException().isThrownBy(() -> Quadruple.ofIterable(List.of("a", "b")));
        assertThatIllegalArgumentException().isThrownBy(() -> Quadruple.ofIterable(List.of("a", "b", "c")));
    }

    @Test
    void test_stream_Given_elements_Then_returnsStreamOfElements() {
        assertThat(Quadruple.of("a", "b", 1, 2).stream()).containsExactly("a", "b", 1, 2);
    }

    @Test
    void test_withHead_Given_instance_Then_returnsInstanceWithFirstElementSet() {
        assertThat(Quadruple.of("a", "b", "c", "d").withHead("x")).isEqualTo(Quadruple.of("x", "b", "c", "d"));
    }

    @Test
    void test_withTail_Given_instance_Then_returnsInstanceWithNonFirstElementsSet() {
        assertThat(Quadruple.of("a", "b", "c", "d").withTail(Triple.of("1", "2", "3")))
            .isEqualTo(Quadruple.of("a", "1", "2", "3"));
    }
}
