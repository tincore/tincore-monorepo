package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static com.tincore.util.lang.function.ThrowingConsumer.uConsumer;
import static org.assertj.core.api.Assertions.*;

class ThrowingConsumerTest {

    @Test
    void test_uConsumerWithMapAll_Given_mapAllEqualsFalse_and_uncheckedException_Then_throwsUncheckedException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> ThrowingConsumer.uConsumer(a -> {
            throw new IllegalArgumentException("test");
        }, ExceptionMapper.wrapChecked((v, x) -> new IllegalStateException(x))).accept("x")).withMessage("test");
    }

    @Test
    void test_uConsumerWithMapAll_Given_mapAllEqualsTrue_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uConsumer(a -> {
            throw new IllegalArgumentException("test");
        }, ExceptionMapper.wrapAll((v, x) -> new IllegalStateException(x))).accept("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uConsumer_Given_noException_Then_executesSuccessfully() {
        var ref = new AtomicReference<String>();
        uConsumer(a -> ref.set(String.valueOf(a))).accept("x");
        assertThat(ref).hasValue("x");
    }

    @Test
    void test_uConsumer_Given_throwingExceptionConsumer_Then_convertsCheckedIntoUncheckedException() {
        assertThatException().isThrownBy(() -> uConsumer(a -> {
            throw new Exception("test");
        }).accept("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uConsumer_Given_throwsError_Then_rethrows() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> uConsumer(a -> {
            throw new Error("unchecked");
        }).accept("x"));
    }

    @Test
    void test_uConsumer_exceptionBiMapper_Given_noException_Then_executesSuccessfully() {
        var ref = new AtomicReference<String>();
        uConsumer(a -> ref.set(String.valueOf(a)), e -> e).accept("x");
        assertThat(ref).hasValue("x");
    }

    @Test
    void test_uConsumer_exceptionMapper_Given_noException_Then_executesSuccessfully() {
        var ref = new AtomicReference<String>();
        uConsumer(a -> ref.set(String.valueOf(a)), e -> e).accept("x");
        assertThat(ref).hasValue("x");
    }
}
