package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "Java Utility")
abstract class AbstractPileTest extends BehaviourAwareTest {

    BiFunction<Integer, Integer, Integer> MULT = (a, b) -> a * b;
    BiFunction<String, String, String> CONCAT = (a, b) -> a + b;

    abstract Pile<?, String> createPile(String... items);

    @Test
    void test_contains_Given_valueExisting_Then_returnsTrue() {
        assertThat(createPile("a", "b", "c").contains("a")).isTrue();
        assertThat(createPile("a", "b", "c").contains("b")).isTrue();
        assertThat(createPile("a", "b", "c").contains("c")).isTrue();
    }

    @Test
    void test_contains_Given_valueNotExisting_Then_returnsFalse() {
        assertThat(createPile().contains("z")).isFalse();
        assertThat(createPile("a", "b", "c").contains("z")).isFalse();
    }

    @Test
    void test_dup2_Given_atLeastPairOfElements_Then_duplicatesPair() {
        assertThat(createPile("a", "b").dup2()).containsExactly("a", "b", "a", "b");
    }

    @Test
    void test_dup2_Given_head_Then_head() {
        assertThat(createPile("a", "b").dup()).containsExactly("a", "a", "b");
    }

    @Test
    void test_isEmpty_Given_empty_Then_returnsTrue() {
        assertThat(createPile().isEmpty()).isTrue();
    }

    @Test
    void test_isEmpty_Given_notEmpty_Then_returnsFalse() {
        assertThat(createPile("a").isEmpty()).isFalse();
    }

    @Test
    void test_nip2_Given_values_Then_removesSecondPairOfValues() {
        assertThat(createPile("a", "b", "c", "d").nip2()).containsExactly("a", "b");
    }

    @Test
    void test_nip_Given_values_Then_removesSecondValue() {
        assertThat(createPile("a", "b").nip()).containsExactly("a");
    }

    @Test
    void test_over2_Given_pileWithTwoPairs_Then_returnsPileWithTheePairs_and_pileSecondBetweenPileTop() {
        assertThat(createPile("a", "b", "c", "d").over2()).containsExactly("a", "b", "c", "d", "a", "b");
    }

    @Test
    void test_over2_Given_pileWithTwoPairs_Then_returnsStackWithTheePairs_and_pileSecondBetweenStackTop() {
        assertThat(createPile("a", "b", "c", "d").over2()).containsExactly("a", "b", "c", "d", "a", "b");
    }

    @Test
    void test_over_Given_pileWithTwoElements_Then_returnsPileWithTheeElements_and_pileSecondBetweenPileTop() {
        assertThat(createPile("a", "b").over()).containsExactly("a", "b", "a");
    }

    @Test
    void test_over_Given_pileWithTwoElements_Then_returnsStackWithTheeElements_and_pileSecondBetweenStackTop() {
        assertThat(createPile("a", "b").over()).containsExactly("a", "b", "a");
    }

    @Test
    void test_pop_Given_multipleElements_Then_removesHead() {
        assertThat(createPile("a", "b").pop()).containsExactly("b");
    }

    @Test
    void test_rot2_Given_values_Then_rotatesFullPileTwoPlaces() {
        assertThat(createPile("a", "b", "c", "d", "f", "g").rot2()).containsExactly("c", "d", "f", "g", "a", "b");
    }

    @Test
    void test_rotN2_Given_values_Then_rotatesFullPileTwoPlacesInReverseOrder() {
        assertThat(createPile("a", "b", "c", "d", "f", "g").rotN2()).containsExactly("f", "g", "a", "b", "c", "d");
    }

    @Test
    void test_rotN_Given_values_ThenRotatesFullPile() {
        assertThat(createPile("a", "b", "c").rot()).containsExactly("b", "c", "a");
    }

    @Test
    void test_rotN_Given_values_ThenRotatesFullPileInReverseOrder() {
        assertThat(createPile("a", "b", "c").rotN()).containsExactly("c", "a", "b");
    }

    @Test
    void test_size_Given_empty_Then_returnsZero() {
        assertThat(createPile()).isEmpty();
    }

    @Test
    void test_stream_Given_contents_Then_returnsListOfContents() {
        assertThat(createPile("a", "b").stream()).containsExactly("a", "b");
    }

    @Test
    void test_stream_Given_empty_Then_returnsEmptyList() {
        assertThat(createPile()).isEmpty();
    }

    @Test
    void test_swap2_Given_values_Then_swapsTopPairWithNextPair() {
        assertThat(createPile("a", "b", "c", "d").swap2()).containsExactly("c", "d", "a", "b");
    }

    @Test
    void test_swap_Given_values_Then_swapsTopWithNext() {
        assertThat(createPile("a", "b", "c").swap()).containsExactly("b", "a", "c");
    }

    @Test
    void test_toArray_Given_contents_Then_returnsArrayOfContents() {
        assertThat(createPile("a", "b").toArray()).containsExactly("a", "b");
    }

    @Test
    void test_toArray_Given_empty_Then_returnsEmpty() {
        assertThat(createPile().toArray()).isEmpty();
    }

    @Test
    void test_toList_Given_contents_Then_returnsListOfContents() {
        assertThat(createPile("a", "b").toList()).containsExactly("a", "b");
    }

    @Test
    void test_toList_Given_empty_Then_returnsEmptyList() {
        assertThat(createPile()).isEmpty();
    }

    @Test
    void test_tuck2_Given_values_Then_places_secondPairOfValuesCopyOnTop() {
        assertThat(createPile("a", "b", "c", "d").tuck2()).containsExactly("c", "d", "a", "b", "c", "d");
    }

    @Test
    void test_tuck_Given_values_Then_places_secondValueCopyOnTop() {
        assertThat(createPile("a", "b").tuck()).containsExactly("b", "a", "b");
    }
}
