package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.tincore.util.lang.function.ThrowingBiPredicate.*;
import static org.assertj.core.api.Assertions.*;

class ThrowingBiPredicateTest {
    @Test
    void test_ThrowingBiPredicate_Given_throwsUncheckedException_Then_throwsException() {
        assertThatException().isThrownBy(
            () -> ((ThrowingBiPredicate<Object, Object, IOException>) (a, b) -> {
                throw new RuntimeException("unchecked");
            }).test("x", "y"))
            .withMessage("unchecked");
    }

    @Test
    @SneakyThrows
    void test_ThrowingPredicate_and_Given_predicate_Then_returnsTrueIfBothConditionsAreMet_falseOtherwise() {
        assertThat(tautology().and(tautology()).test("A", "B")).isTrue();
        assertThat(tautology().and(contradiction()).test("A", "B")).isFalse();
        assertThat(contradiction().and(tautology()).test("A", "B")).isFalse();
        assertThat(contradiction().and(contradiction()).test("A", "B")).isFalse();
    }

    @Test
    @SneakyThrows
    void test_ThrowingPredicate_negate_Given_predicate_Then_returnsPredicateLogicallyInverted() {
        assertThat(tautology().negate().test("A", "B")).isFalse();
        assertThat(contradiction().negate().test("A", "B")).isTrue();
    }

    @Test
    @SneakyThrows
    void test_ThrowingPredicate_or_Given_predicate_Then_returnsTrueIfAnyConditionIsMet_falseOtherwise() {
        assertThat(tautology().or(tautology()).test("A", "B")).isTrue();
        assertThat(tautology().or(contradiction()).test("A", "B")).isTrue();
        assertThat(contradiction().or(tautology()).test("A", "B")).isTrue();
        assertThat(contradiction().or(contradiction()).test("A", "B")).isFalse();
    }

    @Test
    void test_uBiPredicate_Given_noException_Then_executesSuccessfully() {
        assertThat(uBiPredicate(tautology()).test("x", "y")).isTrue();
    }

    @Test
    void test_uBiPredicate_Given_throwingExceptionBiPredicate_Then_convertsCheckedIntoUncheckedException() {
        assertThatException().isThrownBy(() -> uBiPredicate((a, b) -> {
            throw new Exception("test");
        }).test("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiPredicate_Given_throwsError_Then_rethrows() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> uBiPredicate((a, b) -> {
            throw new Error("unchecked");
        }).test("x", "y"));
    }

    @Test
    void test_uBiPredicate_exceptionBiMapper_Given_noException_Then_executesSuccessfully() {
        Assertions.assertThat(ThrowingBiPredicate.uBiPredicate(tautology(), ExceptionMapper.wrapChecked()).test("x", "y")).isTrue();
        assertThat(uBiPredicate(tautology(), ExceptionMapper.wrapChecked(e -> new RuntimeException(e))).test("x", "y")).isTrue();
        assertThat(uBiPredicate(tautology(), ExceptionMapper.wrapChecked((a, x) -> new RuntimeException(x))).test("x", "y")).isTrue();
    }

    @Test
    void test_uBiPredicate_exceptionMapper_Given_mapsAllExceptions_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(
            () -> uBiPredicate((a, b) -> {
                throw new IllegalArgumentException("test");
            }, (v, x) -> new IllegalStateException(x)).test("x", "y"))
            .havingCause()
            .withMessage("test");
    }

    @Test
    void test_uBiPredicate_exceptionMapper_Given_mapsOnlyChecked_and_uncheckedException_Then_throwsUncheckedException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
            () -> uBiPredicate((a, b) -> {
                throw new IllegalArgumentException("test");
            }, ExceptionMapper.wrapChecked((v, x) -> new IllegalStateException(x))).test("x", "y"))
            .withMessage("test");
    }

    @Test
    void test_uBiPredicate_exceptionMapper_Given_noException_Then_executesSuccessfully() {
        assertThat(uBiPredicate(tautology(), ExceptionMapper.wrapChecked()).test("x", "y")).isTrue();
        assertThat(uBiPredicate((a, b) -> true, e -> e).test("x", "y")).isTrue();
    }

}
