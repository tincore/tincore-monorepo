package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "Java Utility")
class LRUCacheTest extends BehaviourAwareTest {

    private static final int MAX_SIZE = 5;

    private final LRUCache<Integer, Integer> cache = new LRUCache<>(MAX_SIZE);

    @Test
    void test_put_Given_moreElementsAddedThanSize_Then_oldestElementsAreRemoved() throws Exception {
        IntStream.range(0, MAX_SIZE + 1).forEach(i -> cache.put(i, i));

        assertThat(cache).doesNotContainKeys(0);
    }

    @Test
    void test_put_Given_moreElementsAddedThanSize_Then_sizeIsEqualsToMaxSize() throws Exception {
        IntStream.range(0, MAX_SIZE + 1).forEach(i -> cache.put(i, i));

        assertThat(cache).hasSize(MAX_SIZE);
    }
}
