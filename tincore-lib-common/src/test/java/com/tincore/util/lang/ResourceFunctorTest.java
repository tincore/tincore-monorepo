package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.AutoCloseableFunction;
import lombok.experimental.ExtensionMethod;
import org.apache.commons.io.input.ObservableInputStream;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.tincore.util.lang.function.ThrowingFunction.uFunction;
import static org.assertj.core.api.Assertions.*;

@ExtensionMethod({SequenceExtension.class})
class ResourceFunctorTest {

    private final AtomicBoolean closed = new AtomicBoolean();
    private final ObservableInputStream.Observer observer = new ObservableInputStream.Observer() {
        @Override
        public void closed() throws IOException {
            super.closed();
            closed.set(true);
        }
    };

    @Test
    void test_applyEither_Given_actionException_Then_throwsException_and_closesResource() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer))
            .map(uFunction(InputStream::readAllBytes))
            .map(s -> {
                throw new IllegalArgumentException("Action error");
            });

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> resourceFunctor.applyEither("ABC")).withMessage("Action error");

        assertThat(closed).isTrue();
    }

    @Test
    void test_applyEither_Given_throwableActionException_Then_throwsException_and_closesResource() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer))
            .map(uFunction(InputStream::readAllBytes))
            .mapTry(s -> {
                throw new IOException("Action error");
            });

        var either = resourceFunctor.applyEither("ABC");
        assertThat(either.getLeftOption()).hasValueSatisfying(l -> assertThat(l)
            .isInstanceOf(IOException.class)
            .hasMessage("Action error"));

        assertThat(closed).isTrue();
    }

    @Test
    void test_applyEither_Given_throwableActionUncheckedException_Then_throwsException_and_closesResource() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer))
            .map(uFunction(InputStream::readAllBytes))
            .mapTry(s -> {
                throw new IllegalArgumentException("Action error");
            });

        assertThatIllegalArgumentException().isThrownBy(() -> resourceFunctor.applyEither("ABC"))
            .withMessage("Action error");
        assertThat(closed).isTrue();
    }

    @Test
    void test_applyEither_Given_throwableActionUncheckedException_Then_throwsException_and_closesResource2() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException, IllegalStateException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer),
            (o, x) -> new IllegalStateException(x))
            .map(uFunction(InputStream::readAllBytes))
            .mapTry(s -> {
                throw new IllegalArgumentException("Action error");
            });

        assertThatIllegalArgumentException().isThrownBy(() -> resourceFunctor.applyEither("ABC"))
            .withMessage("Action error");
        assertThat(closed).isTrue();
    }

    @Test
    void test_applyEither_Given_value_Then_returnsResultValue_and_closesResource() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer))
            .map(uFunction(InputStream::readAllBytes))
            .map(String::new);

        var either = resourceFunctor.applyEither("ABC");

        assertThat(either.getRightOption()).hasValue("ABC");

        assertThat(closed).isTrue();
    }

    @Test
    void test_apply_Given_actionException_Then_throwsException_and_closesResource() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer))
            .map(uFunction(InputStream::readAllBytes))
            .map(s -> {
                throw new IllegalArgumentException("Action error");
            });

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> resourceFunctor.apply("ABC")).withMessage("Action error");
        assertThat(closed).isTrue();
    }

    @Test
    void test_apply_Given_throwableActionException_Then_throwsException_and_closesResource() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer))
            .map(uFunction(InputStream::readAllBytes))
            .mapTry(s -> {
                throw new IOException("Action error");
            });

        assertThatExceptionOfType(IOException.class).isThrownBy(() -> resourceFunctor.apply("ABC"))
            .withMessage("Action error");
        assertThat(closed).isTrue();
    }

    @Test
    void test_apply_Given_throwableActionUncheckedException_Then_throwsException_and_closesResource() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer))
            .map(uFunction(InputStream::readAllBytes))
            .mapTry(s -> {
                throw new IllegalArgumentException("Action error");
            });

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> resourceFunctor.apply("ABC"));
        assertThat(closed).isTrue();
    }

    @Test
    void test_apply_Given_value_Then_returnsResultValue_and_closesResource() {
        var resourceFunctor = ResourceFunctor.<String, InputStream, IOException>of(
            o -> new ObservableInputStream(new ByteArrayInputStream(o.getBytes()), observer))
            .mapException(RuntimeException::new)
            .map(uFunction(InputStream::readAllBytes))
            .map(String::new);

        assertThat(resourceFunctor.apply("ABC")).isEqualTo("ABC");
        assertThat(closed).isTrue();
    }

    @Test
    void test_functor_Given_applied_Then_returnsResultValue() {
        Optional<String> actual = Optional.of("ABC")
            .map(ResourceFunctor.<String, InputStream, IOException>of(o -> new ByteArrayInputStream(o.getBytes()))
                .map(uFunction(InputStream::readAllBytes))
                .map(String::new)
                .uncheck());

        assertThat(actual).hasValue("ABC");
    }

    @Test
    void test_mapException_Given_error_Then_throwsMappedException() {
        AutoCloseableFunction<String, InputStream, IOException> autoCloseableFunction = o -> {
            throw new IOException("Creation error");
        };
        var resourceFunctor = ResourceFunctor.of(autoCloseableFunction)
            .map(uFunction(InputStream::readAllBytes))
            .mapException(IllegalStateException::new)
            .map(s -> {
                throw new IllegalArgumentException("Action error");
            });

        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> resourceFunctor.apply("ABC"));
    }

}
