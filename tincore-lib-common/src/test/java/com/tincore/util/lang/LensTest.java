package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.Lens.MatcherCompositeLens;
import lombok.*;
import org.assertj.core.api.ThrowingConsumer;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.UnaryOperator;

import static com.tincore.util.lang.function.FunctionExtension.contradiction;
import static com.tincore.util.lang.function.FunctionExtension.tautology;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.of;
import static org.assertj.core.api.Assertions.assertThat;

class LensTest {

    public static final Lens<TypeA, String> LENS_TYPE_A_VALUE_A_MUTABLE = Lens.ofMutable(TypeA::getData,
        TypeA::setData);
    public static final Lens<TypeA, String> LENS_TYPE_A_VALUE_A = Lens.of(TypeA::getData, TypeA::withData);
    public static final Lens<Type, TypeA> LENS_TYPE_TYPE_A = Lens.of(Type::getTypeA, Type::withTypeA);

    public static final UnaryOperator<String> PREPEND_PREFIX = s -> "pr_" + s;

    @Test
    void test_andThen_Given_lenses_Then_returnsLens_and_lensManipulatesZoomedValue() {
        var lens = LENS_TYPE_TYPE_A.andThen(LENS_TYPE_A_VALUE_A);

        assertThat(lens.getValue(new Type("c", new TypeA("x"), null))).isEqualTo("x");
        assertThat(lens.setValue(new Type("c", new TypeA("x"), null), "z")).satisfies(d -> assertThat(d.getTypeA()
            .getData()).isEqualTo("z"));
    }

    @Test
    void test_applied_Given_Lens_and_instance_Then_returnsPartialLensForInstance() {
        var lens = LENS_TYPE_A_VALUE_A.toApplied(new TypeA("A"));

        assertThat(lens.getValue()).isEqualTo("A");
        assertThat(lens.setValue("B")).satisfies(d -> assertThat(d.getData()).isEqualTo("B"));
        assertThat(lens.modifyValue(v -> "x")).satisfies(d -> assertThat(d.getData()).isEqualTo("x"));

        assertThat(lens.modifyValueIf(tautology(), v -> "x1")).satisfies(d -> assertThat(d.getData()).isEqualTo("x1"));
        assertThat(lens.modifyValueIf(contradiction(),
            v -> "x1")).satisfies(d -> assertThat(d.getData()).isEqualTo("A"));
        assertThat(lens.modifyValueUnless(tautology(),
            v -> "x1")).satisfies(d -> assertThat(d.getData()).isEqualTo("A"));
        assertThat(lens.modifyValueUnless(contradiction(), v -> "x1")).satisfies(d -> assertThat(d.getData()).isEqualTo(
            "x1"));
    }

    @Test
    void test_compose_Given_lenses_Then_returnsLens_and_lensManipulatesZoomedValue() {
        var lens = LENS_TYPE_A_VALUE_A.compose(LENS_TYPE_TYPE_A);
        assertThat(lens.getValue(new Type("c", new TypeA("x"), null))).isEqualTo("x");
        assertThat(lens.setValue(new Type("c", new TypeA("x"), null), "z")).satisfies(d -> assertThat(d.getTypeA()
            .getData()).isEqualTo("z"));
    }

    @Test
    void test_filterSet_Given_predicateDoesNotMatch_Then_doesNotSetValue() {
        var lens = LENS_TYPE_A_VALUE_A.filterSet("zzz"::equals);

        assertThat(lens.getValue(new TypeA("x"))).isEqualTo("x");
        assertThat(lens.getValue(new TypeA("a"))).isEqualTo("a");
        assertThat(lens.setValue(new TypeA("x"), "a")).satisfies(d -> assertThat(d.getData()).isEqualTo("x"));
        assertThat(lens.modifyValue(new TypeA("x"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo(
            "x"));
    }

    @Test
    void test_filterSet_Given_predicateMatch_Then_setsValue() {
        var lens = LENS_TYPE_A_VALUE_A.filterSet(v -> v.contains("a"));

        assertThat(lens.getValue(new TypeA("x"))).isEqualTo("x");
        assertThat(lens.getValue(new TypeA("a"))).isEqualTo("a");
        assertThat(lens.setValue(new TypeA("x"), "a")).satisfies(d -> assertThat(d.getData()).isEqualTo("a"));
        assertThat(lens.modifyValue(new TypeA("a"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo(
            "pr_a"));
    }

    @Test
    void test_getValue_Given_mutableLens_Then_returnsZoomedValue() {
        assertThat(LENS_TYPE_A_VALUE_A_MUTABLE.getValue(new TypeA("x"))).isEqualTo("x");
    }

    @Test
    void test_getValue_Given_type_Then_returnsZoomedValue() {
        assertThat(LENS_TYPE_A_VALUE_A.getValue(new TypeA("x"))).isEqualTo("x");
    }

    @Test
    void test_mapGet_Given_type_Then_appliesMappingFunctionToValueGet() {
        var lens = LENS_TYPE_A_VALUE_A.mapGet(String::toUpperCase);

        assertThat(lens.getValue(new TypeA("x"))).isEqualTo("X");
        assertThat(lens.setValue(new TypeA("x"), "a")).satisfies(d -> assertThat(d.getData()).isEqualTo("a"));
        assertThat(lens.modifyValue(new TypeA("x"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo(
            "pr_X"));
    }

    @Test
    void test_mapSet_Given_type_Then_appliesMappingFunctionToValueSet() {
        var lens = LENS_TYPE_A_VALUE_A.mapSet(String::toUpperCase);

        assertThat(lens.getValue(new TypeA("x"))).isEqualTo("x");
        assertThat(lens.setValue(new TypeA("x"), "a")).satisfies(d -> assertThat(d.getData()).isEqualTo("A"));
        assertThat(lens.modifyValue(new TypeA("x"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo(
            "PR_X"));
    }

    @Test
    void test_map_Given_type_Then_appliesMappingFunctionToValues() {
        var lens = LENS_TYPE_A_VALUE_A.map(String::toUpperCase);

        assertThat(lens.getValue(new TypeA("x"))).isEqualTo("X");
        assertThat(lens.setValue(new TypeA("x"), "a")).satisfies(d -> assertThat(d.getData()).isEqualTo("A"));
        assertThat(lens.modifyValue(new TypeA("x"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo(
            "PR_X"));
    }

    @Test
    void test_matcherCompositeLens_getValue_Given_multipleTypes_Then_returnsZoomedValue() {
        var matcher = new MatcherCompositeLens<Object, String>().when(TypeA.class, LENS_TYPE_A_VALUE_A)
            .when(TypeB.class, Lens.of(TypeB::getData, (s, v) -> s.toBuilder().data(v).build()));

        assertThat(matcher.getValue(new TypeA("x"))).isEqualTo("x");
        assertThat(matcher.getValue(new TypeB("x"))).isEqualTo("x");
    }

    @Test
    void test_modifyValueIf_Given_mutableLens_and_predicateDoesNotMatch_Then_doesNotManipulateExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A_MUTABLE.modifyValueIf(new TypeA("x"),
            contradiction(),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("x"));
    }

    @Test
    void test_modifyValueIf_Given_mutableLens_and_predicateMatches_Then_manipulatesExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A_MUTABLE.modifyValueIf(new TypeA("x"),
            tautology(),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("pr_x"));
    }

    @Test
    void test_modifyValueIf_Given_predicateDoesNotMatch_Then_doesNotManipulateExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A.modifyValueIf(new TypeA("x"),
            contradiction(),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("x"));
    }

    @Test
    void test_modifyValueIf_Given_predicateMatches_Then_manipulatesExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A.modifyValueIf(new TypeA("x"),
            tautology(),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("pr_x"));
    }

    @Test
    void test_modifyValueUnless_Given_mutableLens_and_predicateDoesNotMatch_Then_manipulatesExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A_MUTABLE.modifyValueUnless(new TypeA("x"),
            contradiction(),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("pr_x"));
    }

    @Test
    void test_modifyValueUnless_Given_mutableLens_and_predicateMatches_Then_doesNotManipulateExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A_MUTABLE.modifyValueUnless(new TypeA("x"),
            tautology(),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("x"));
    }

    @Test
    void test_modifyValueUnless_Given_predicateDoesNotMatch_Then_manipulatesExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A.modifyValueUnless(new TypeA("x"),
            contradiction(),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("pr_x"));
    }

    @Test
    void test_modifyValueUnless_Given_predicateMatches_Then_doesNotManipulatesExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A.modifyValueUnless(new TypeA("x"),
            tautology(),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("x"));
    }

    @Test
    void test_modifyValue_Given_mutableLens_Then_manipulatesExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A_MUTABLE.modifyValue(new TypeA("x"),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("pr_x"));
    }

    @Test
    void test_modifyValue_Given_type_Then_manipulatesExistingValue() {
        assertThat(LENS_TYPE_A_VALUE_A.modifyValue(new TypeA("x"),
            PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo("pr_x"));
    }

    @Test
    void test_ofListFirst_Given_index_Then_returnsLens_and_lensManipulatesZoomedValue() {
        var lens = Lens.<String>ofListFirst();

        assertThat(lens.getValue(Collections.emptyList())).isNull();
        assertThat(lens.setValue(Collections.emptyList(), "x")).isEmpty();
        assertThat(lens.getValue(of("a", "b", "c").collect(toList()))).isEqualTo("a");
        assertThat(lens.setValue(of("a", "b", "c").collect(toList()), "x")).containsExactly("x", "b", "c");
        assertThat(lens.modifyValue(of("a", "b", "c").collect(toList()), PREPEND_PREFIX)).containsExactly("pr_a",
            "b",
            "c");
    }

    @Test
    void test_ofListLast_Given_index_Then_returnsLens_and_lensManipulatesZoomedValue() {
        var lens = Lens.<String>ofListLast();

        assertThat(lens.getValue(Collections.emptyList())).isNull();
        assertThat(lens.setValue(Collections.emptyList(), "x")).isEmpty();
        assertThat(lens.getValue(of("a", "b", "c").collect(toList()))).isEqualTo("c");
        assertThat(lens.setValue(of("a", "b", "c").collect(toList()), "x")).containsExactly("a", "b", "x");
        assertThat(lens.modifyValue(of("a", "b", "c").collect(toList()), PREPEND_PREFIX)).containsExactly("a",
            "b",
            "pr_c");
    }

    @Test
    void test_ofList_Given_indexOutOfRange_Then_returnsLens_and_getReturnsNull() {
        var lens = Lens.<String>ofList(3);

        assertThat(lens.getValue(Collections.emptyList())).isNull();
        assertThat(lens.setValue(Collections.emptyList(), "x")).isEmpty();
        assertThat(lens.getValue(of("a", "b").collect(toList()))).isNull();
        assertThat(lens.setValue(of("a", "b").collect(toList()), "x")).containsExactly("a", "b");
        assertThat(lens.modifyValue(of("a", "b").collect(toList()), PREPEND_PREFIX)).containsExactly("a", "b");
    }

    @Test
    void test_ofList_Given_index_Then_returnsLens_and_lensManipulatesZoomedValue() {
        var lens = Lens.<String>ofList(1);

        assertThat(lens.getValue(Collections.emptyList())).isNull();
        assertThat(lens.setValue(Collections.emptyList(), "x")).isEmpty();
        assertThat(lens.getValue(of("a", "b", "c").collect(toList()))).isEqualTo("b");
        assertThat(lens.setValue(of("a", "b", "c").collect(toList()), "x")).containsExactly("a", "x", "c");
        assertThat(lens.modifyValue(of("a", "b", "c").collect(toList()), PREPEND_PREFIX)).containsExactly("a",
            "pr_b",
            "c");
    }

    @Test
    void test_ofMap_Given_mapKey_Then_returnsLens_and_lensManipulatesZoomedValue() {
        var lens = Lens.<String, String>ofMap("k");

        assertThat(lens.getValue(new HashMap<>() {
            {
                put("k", "v");
                put("z", "z");
            }
        })).isEqualTo("v");
        assertThat(lens.setValue(new HashMap<>() {
            {
                put("k", "v");
                put("z", "z");
            }
        }, "x")).containsEntry("k", "x");
        assertThat(lens.modifyValue(new HashMap<>() {
            {
                put("k", "v");
                put("z", "z");
            }
        }, PREPEND_PREFIX)).containsEntry("k", "pr_v");
    }

    @Test
    void test_ofNullableAndThen_Given_nullableFirstLevel_Then_getReturnsNull_and_setIsIgnored() {
        var lens = LENS_TYPE_TYPE_A.toNullable().andThen(LENS_TYPE_A_VALUE_A);

        assertThat(lens.getValue(null)).isNull();
        assertThat(lens.setValue(null, "z")).isNull();
        assertThat(lens.modifyValue(null, PREPEND_PREFIX)).isNull();
    }

    @Test
    void test_ofNullableAndThen_Given_nullableLenses_Then_getReturnsNull_and_setIsIgnored() {
        var lens = LENS_TYPE_TYPE_A.toNullable().andThen(LENS_TYPE_A_VALUE_A.toNullable());

        assertThat(lens.getValue(null)).isNull();
        assertThat(lens.setValue(null, "z")).isNull();
        assertThat(lens.modifyValue(null, PREPEND_PREFIX)).isNull();
        assertThat(lens.getValue(new Type("c"))).isNull();
        assertThat(lens.setValue(new Type("c"), "z")).satisfies(d -> assertThat(d.getTypeA()).isNull());
        assertThat(lens.modifyValue(new Type("c"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getTypeA()).isNull());
    }

    @Test
    void test_ofNullableAndThen_Given_nullableSecondLevel_Then_getReturnsNull_and_setIsIgnored() {
        var lens = LENS_TYPE_TYPE_A.andThen(LENS_TYPE_A_VALUE_A.toNullable());

        assertThat(lens.getValue(new Type("c"))).isNull();
        assertThat(lens.setValue(new Type("c"), "z")).satisfies(d -> assertThat(d.getTypeA()).isNull());
        assertThat(lens.modifyValue(new Type("c"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getTypeA()).isNull());
    }

    @Test
    void test_ofNullableComposed_Given_nullableFirstLevel_Then_getReturnsNull_and_setIsIgnored() {
        var lens = LENS_TYPE_A_VALUE_A.compose(LENS_TYPE_TYPE_A.toNullable());

        assertThat(lens.getValue(null)).isNull();
        assertThat(lens.setValue(null, "z")).isNull();
        assertThat(lens.modifyValue(null, PREPEND_PREFIX)).isNull();
    }

    @Test
    void test_ofNullableComposed_Given_nullableLenses_Then_getReturnsNull_and_setIsIgnored() {
        var lens = LENS_TYPE_A_VALUE_A.toNullable().compose(LENS_TYPE_TYPE_A.toNullable());

        assertThat(lens.getValue(null)).isNull();
        assertThat(lens.setValue(null, "z")).isNull();
        assertThat(lens.modifyValue(null, PREPEND_PREFIX)).isNull();
        assertThat(lens.getValue(new Type("c"))).isNull();
        assertThat(lens.setValue(new Type("c"), "z")).satisfies(d -> assertThat(d.getTypeA()).isNull());
        assertThat(lens.modifyValue(new Type("c"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getTypeA()).isNull());
    }

    @Test
    void test_ofNullableComposed_Given_nullableSecondLevel_Then_getReturnsNull_and_setIsIgnored() {
        var lens = LENS_TYPE_A_VALUE_A.toNullable().compose(LENS_TYPE_TYPE_A);

        assertThat(lens.getValue(new Type("c"))).isNull();
        assertThat(lens.setValue(new Type("c"), "z")).satisfies(d -> assertThat(d.getTypeA()).isNull());
        assertThat(lens.modifyValue(new Type("c"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getTypeA()).isNull());
    }

    @Test
    void test_ofNullable_Given_null_Then_getReturnsNull_and_setIsIgnored() {
        var lens = LENS_TYPE_A_VALUE_A.toNullable();

        assertThat(lens.getValue(null)).isNull();
        assertThat(lens.setValue(null, "x")).isNull();
        assertThat(lens.modifyValue(null, PREPEND_PREFIX)).isNull();

        assertThat(lens.getValue(new TypeA("x"))).isEqualTo("x");
        assertThat(lens.setValue(new TypeA("x"), "z")).satisfies(d -> assertThat(d.getData()).isEqualTo("z"));
        assertThat(lens.modifyValue(new TypeA("x"), PREPEND_PREFIX)).satisfies(d -> assertThat(d.getData()).isEqualTo(
            "pr_x"));
    }

    @Test
    void test_setValue_Given_mutableLensType_Then_manipulatesValue() {
        var data = new TypeA("x");
        assertThat(LENS_TYPE_A_VALUE_A_MUTABLE.setValue(data,

            "z")).satisfies(d -> assertThat(d.getData()).isEqualTo("z"));
        assertThat(data.getData()).isEqualTo("z");
    }

    @Test
    void test_setValue_Given_type_Then_manipulatesValue() {
        assertThat(LENS_TYPE_A_VALUE_A.setValue(new TypeA("x"), "z")).satisfies(d -> assertThat(d.getData()).isEqualTo(
            "z"));
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class TypeA {
        private String data;
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class TypeB {
        private String data;
    }

    @Data
    @With
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    private static class Type {
        private String data;
        private List<SubType> list;
        private String[] array;
        private TypeA typeA;
        private TypeB typeB;

        Type(String data, List<SubType> list, String[] array) {
            this.data = data;
            this.list = list;
            this.array = array;
        }

        Type(String data, TypeA typeA, TypeB typeB) {
            this.data = data;
            this.typeA = typeA;
            this.typeB = typeB;
        }

        Type(String data) {
            this.data = data;
        }
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class SubType {
        private TypeA typeA;
        private List<TypeB> list;
    }

}
