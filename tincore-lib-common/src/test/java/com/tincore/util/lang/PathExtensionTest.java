package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.TestFixtureTrait;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import lombok.experimental.ExtensionMethod;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.util.lang.function.ThrowingConsumer.uConsumer;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@BehaviourGroup(scenario = "Java Utility")
@TestMethodOrder(MethodOrderer.MethodName.class)
@ExtensionMethod(PathExtension.class)
class PathExtensionTest extends BehaviourAwareTest implements TestFixtureTrait {

    @TempDir
    public File tempDir;
    private Path rootPath;

    public void assertThatSizeEquals(Path path, int expectedSize) throws IOException {
        assertThat(path).exists().isRegularFile();
        assertThat(Files.size(path)).isEqualTo(expectedSize);
    }

    @BeforeEach
    public void setUp() {
        rootPath = tempDir.toPath();
    }

    @Test
    void test_createDirectoryContainer_Given_pathAndPathParentIsRoot_Then_executesSuccessfullyAndDoesNotCreatesPathParentDirectory() throws Exception {
        assertThat(PathExtension.createParentDirectories(FileSystems.getDefault().getPath("/"))).isNull();
    }

    @Test
    void test_createDirectoryContainer_Given_pathAndPathParentNotExisting_Then_createsPathParentDirectory() throws Exception {
        Path parent = rootPath.resolve("parent");
        Path file = parent.resolve("someFile.xxx");
        assertThat(PathExtension.createParentDirectories(file)).exists();
        assertThat(file).doesNotExist();
    }

    @Test
    void test_existsAndNotEmpty_Given_pathFile_Then_returnsFalse() throws Exception {
        List<Path> filePaths = createFiles(rootPath, "file.txt");
        Path path = filePaths.get(0);
        assertThat(Files.exists(path) && PathExtension.isEmpty(path)).isFalse();
    }

    @Test
    void test_existsAndNotEmpty_Given_pathFolderAndEmpty_Then_returnsFalse() throws Exception {
        assertThat(!PathExtension.isEmpty(rootPath)).isFalse();
    }

    @Test
    void test_existsAndNotEmpty_Given_pathFolderAndNotEmpty_Then_returnsTrue() throws Exception {
        List<Path> filePaths = createFiles(rootPath, "file.txt");
        assertThat(!PathExtension.isEmpty(rootPath)).isTrue();
    }

    @Test
    void test_existsAndNotEmpty_Given_pathFolderAndNotExists_Then_returnsFalse() throws Exception {
        Path path = rootPath.resolve("noExisitingSubFolder");
        assertThat(Files.exists(path) && !PathExtension.isEmpty(path)).isFalse();
    }

    @Test
    void test_getCommonAncestor_Given_pathsNotSharingParts_Then_returnsEmpty() {
        assertThat(PathExtension.getCommonAncestor(
            Path.of("a").resolve("b"),
            Path.of("z").resolve("b"))).isEmpty();

        assertThat(PathExtension.getCommonAncestor(
            Path.of("z"),
            Path.of("a").resolve("z"))).isEmpty();

        assertThat(PathExtension.getCommonAncestor(
            Path.of("a").resolve("z"),
            Path.of("z"))).isEmpty();
    }

    @Test
    void test_getCommonAncestor_Given_pathsSharingPart_Then_returnsSharedParentPath() {
        assertThat(PathExtension.getCommonAncestor(
            Path.of("a").resolve("b").resolve("c"),
            Path.of("a").resolve("b").resolve("c"))).hasValue(Path.of("a").resolve("b").resolve("c"));

        assertThat(PathExtension.getCommonAncestor(
            Path.of("a"),
            Path.of("a").resolve("b").resolve("c"))).hasValue(Path.of("a"));

        assertThat(PathExtension.getCommonAncestor(
            Path.of("a").resolve("b").resolve("c"),
            Path.of("a"))).hasValue(Path.of("a"));
    }

    @Test
    void test_getCommonAncestor_collection_Given_pathsSharingPart_Then_returnsSharedParentPath() {
        assertThat(PathExtension.getCommonAncestor(List.of(
            Path.of("a").resolve("b"),
            Path.of("a").resolve("b").resolve("c").resolve("d"),
            Path.of("a").resolve("b").resolve("c1").resolve("d"),
            Path.of("a").resolve("b").resolve("c"),
            Path.of("a").resolve("b").resolve("c")))).hasValue(Path.of("a").resolve("b"));
    }

    @Test
    void test_getDirectoriesByEmpty_Given_existingPathAndPathContainsEmptySubdirectories_Then_returnsPathAndAllEmptySubdirectories() throws Exception {
        Path sub0 = rootPath.resolve("sub0");
        Files.createDirectories(sub0);
        Path sub1 = rootPath.resolve("sub1");
        Path sub11 = sub1.resolve("sub11");
        Files.createDirectories(sub11);

        assertThat(PathExtension.getDirectoriesByEmpty(rootPath)).containsExactlyInAnyOrder(rootPath, sub0, sub1, sub11);
    }

    @Test
    void test_getDirectoriesByEmpty_Given_existingPathAndPathContainsFile_Then_returnsEmpty() throws Exception {
        Files.createFile(rootPath.resolve("f1.txt"));

        assertThat(PathExtension.getDirectoriesByEmpty(rootPath)).isEmpty();
    }

    @Test
    void test_getDirectoriesByEmpty_Given_existingPathAndPathContainsSubdirectoryWithFile_Then_returnsEmpty() throws Exception {
        Path sub1 = rootPath.resolve("sub1");
        Files.createDirectories(sub1);
        Files.createFile(sub1.resolve("f1.txt"));

        assertThat(PathExtension.getDirectoriesByEmpty(rootPath)).isEmpty();
    }

    @Test
    void test_getDirectoriesByEmpty_Given_existingPathAndPathEmpty_Then_returnsPath() throws Exception {
        assertThat(PathExtension.getDirectoriesByEmpty(rootPath)).containsExactlyInAnyOrder(rootPath);
    }

    @Test
    void test_getDirectoriesByEmpty_Given_existingPathAndPathIsFile_Then_returnsEmpty() throws Exception {
        assertThat(PathExtension.getDirectoriesByEmpty(Files.createFile(rootPath.resolve("f1.txt")))).isEmpty();
    }

    @Test
    void test_getDirectoriesByEmpty_Given_existingPath_Then_returnsEmptySubdirectories() throws Exception {
        Path sub0 = rootPath.resolve("sub0");
        Files.createDirectories(sub0);

        Path sub1 = rootPath.resolve("sub1");
        Path sub11 = sub1.resolve("sub11");
        Files.createDirectories(sub11);

        Path sub2 = rootPath.resolve("sub2");
        Path sub21 = sub2.resolve("sub21");
        Files.createDirectories(sub21);
        Path sub22 = sub2.resolve("sub22");
        Files.createDirectories(sub22);

        Files.createFile(sub21.resolve("f1.txt"));

        assertThat(PathExtension.getDirectoriesByEmpty(rootPath)).containsExactlyInAnyOrder(sub0, sub1, sub11, sub22);
    }

    @Test
    void test_getDirectoriesByEmpty_Given_pathDirectoryAndLinkDirectory_Then_doesNotReturnLink() throws Exception {
        Path sub1 = Files.createDirectories(rootPath.resolve("sub1"));
        Path sub11 = Files.createDirectories(sub1.resolve("sub11"));
        Path sub2 = Files.createDirectories(rootPath.resolve("sub2"));
        Path sub21 = Files.createDirectories(sub2.resolve("sub21"));
        Files.createSymbolicLink(sub1.resolve("sub12"), sub21);

        List<Path> directoriesByEmpty = PathExtension.getDirectoriesByEmpty(sub1);
        assertThat(directoriesByEmpty).containsExactlyInAnyOrder(sub11);
    }

    @Test
    void test_getDirectoriesByEmpty_Given_pathDirectoryAndNestedDirectoriesAndBranchDirectoryWithFileAndLeafDirectoryEmpty_Then_returnsLeafDirectory() throws Exception {
        Path sub1 = Files.createDirectories(rootPath.resolve("sub1"));
        Path sub11 = Files.createDirectories(sub1.resolve("sub11"));
        createFile(sub11, "file.txt", "somecontent");
        Path sub12 = Files.createDirectories(sub1.resolve("sub12"));
        Path sub111 = Files.createDirectories(sub11.resolve("sub111"));

        List<Path> directoriesByEmpty = PathExtension.getDirectoriesByEmpty(rootPath);
        assertThat(directoriesByEmpty).containsExactlyInAnyOrder(sub12, sub111);
    }

    @Test
    void test_getDirectoriesByEmpty_Given_pathDirectoryAndNestedDirectoriesAndLeafDirectoryWithFileAndBranchDirectoryEmpty_Then_returnsEmpty() throws Exception {
        Path sub1 = Files.createDirectories(rootPath.resolve("sub1"));
        Path sub11 = Files.createDirectories(sub1.resolve("sub11"));
        Path sub111 = Files.createDirectories(sub11.resolve("sub111"));
        createFile(sub111, "file.txt", "somecontent");

        List<Path> directoriesByEmpty = PathExtension.getDirectoriesByEmpty(rootPath);
        assertThat(directoriesByEmpty).isEmpty();
    }

    @Test
    void test_getDirectoriesByEmpty_Given_pathDirectoryAndNestedDirectoriesAndLeafDirectoryWithFileAndSiblingBranchDirectoryEmpty_Then_returnsSiblingBranch() throws Exception {
        Path sub1 = Files.createDirectories(rootPath.resolve("sub1"));
        Path sub11 = Files.createDirectories(sub1.resolve("sub11"));
        Path sub2 = Files.createDirectories(rootPath.resolve("sub2"));
        Path sub21 = Files.createDirectories(sub2.resolve("sub21"));
        Path sub111 = Files.createDirectories(sub11.resolve("sub111"));
        createFile(sub111, "file.txt", "somecontent");

        List<Path> directoriesByEmpty = PathExtension.getDirectoriesByEmpty(rootPath);
        assertThat(directoriesByEmpty).containsExactlyInAnyOrder(sub2, sub21);
    }

    @Test
    void test_getDirectoriesByEmpty_Given_pathDirectoryAndNestedEmptyDirectories_Then_returnsAllEmptyDirectories() throws Exception {
        Path sub1 = Files.createDirectories(rootPath.resolve("sub1"));
        Path sub11 = Files.createDirectories(sub1.resolve("sub11"));
        Path sub12 = Files.createDirectories(sub1.resolve("sub12"));
        Path sub111 = Files.createDirectories(sub11.resolve("sub111"));

        List<Path> directoriesByEmpty = PathExtension.getDirectoriesByEmpty(rootPath);
        assertThat(directoriesByEmpty).containsExactlyInAnyOrder(rootPath, sub1, sub11, sub12, sub111);
    }

    @Test
    void test_getDirectoriesByEmpty_Given_pathDirectory_Then_returnsDirectory() throws Exception {
        List<Path> directoriesByEmpty = PathExtension.getDirectoriesByEmpty(rootPath);
        assertThat(directoriesByEmpty).containsExactlyInAnyOrder(rootPath);
    }

    @Test
    void test_getDirectoriesByEmpty_Given_pathFile_Then_returnsEmpty() throws Exception {
        List<Path> filePaths = createFiles(rootPath, "file");

        List<Path> directoriesByEmpty = PathExtension.getDirectoriesByEmpty(filePaths.get(0));
        assertThat(directoriesByEmpty).isEmpty();
    }

    @Test
    void test_getFileTimeNow_Given_invoked_Then_returnsMostRecentFileTimestampRoundedToSeconds() throws Exception {
        FileTime fileTimeNow = PathExtension.getFileTimeNow();
        assertThat(fileTimeNow.toInstant()).isAfter(Instant.now().minusSeconds(2));
        assertThat(fileTimeNow.toMillis()).isEqualTo(1000 * (fileTimeNow.toMillis() / 1000));
    }

    @Test
    void test_getLastModifiedTime_Given_pathFile_Then_returnsFalse() throws Exception {
        // Set in stone. Not good
        assertThat(PathExtension.getLastModifiedLocalDateTime(rootPath)).isEqualTo(LocalDateTime.ofInstant(Files.getLastModifiedTime(rootPath).toInstant(), ZoneId.systemDefault()));
    }

    @Test
    void test_getPathUniqueFile_Given_pathPointingToExistingFileAndFileHasDoubleExtension_Then_returnsFileSamePathWithSameExtensionAndFileNameStartsWithParameterFileName() throws Exception {
        List<Path> filePaths = createFiles(rootPath, "file.tar.lzma");
        Path filePath = filePaths.get(0);
        Path uniquePath = PathExtension.findUniqueFilePath(filePath);
        assertThat(uniquePath.getParent()).isEqualTo(filePath.getParent());
        assertThat(uniquePath.getFileName().toString()).isNotEqualToIgnoringCase("file.tar.lzma");
        assertThat(FilenameUtils.getBaseName(uniquePath.getFileName().toString())).startsWith("file");
        assertThat(uniquePath.getFileName().toString()).endsWith(".tar.lzma");
    }

    @Test
    void test_getPathUniqueFile_Given_pathPointingToExistingFileAndFileHasNoExtension_Then_returnsFileSamePathWithFileNameStartsWithParameterFileName() throws Exception {
        List<Path> filePaths = createFiles(rootPath, "file");
        Path filePath = filePaths.get(0);
        Path uniquePath = PathExtension.findUniqueFilePath(filePath);
        assertThat(uniquePath.getParent()).isEqualTo(filePath.getParent());
        assertThat(uniquePath.getFileName().toString()).isNotEqualToIgnoringCase("file");
        assertThat(FilenameUtils.getBaseName(uniquePath.getFileName().toString())).startsWith("file");
    }

    @Test
    void test_getPathUniqueFile_Given_pathPointingToExistingFile_Then_returnsFileSamePathWithSameExtensionAndFileNameStartsWithParameterFileName() throws Exception {
        List<Path> filePaths = createFiles(rootPath, "file.txt");
        Path filePath = filePaths.get(0);
        Path uniquePath = PathExtension.findUniqueFilePath(filePath);
        assertThat(uniquePath.getParent()).isEqualTo(filePath.getParent());
        assertThat(uniquePath.getFileName().toString()).isNotEqualToIgnoringCase("file.txt");
        assertThat(FilenameUtils.getBaseName(uniquePath.getFileName().toString())).startsWith("file");
        assertThat(FilenameUtils.getExtension(uniquePath.getFileName().toString())).isEqualTo("txt");
    }

    @Test
    void test_getPathUniqueFile_Given_pathPointingToNonExistingFile_Then_returnsSamePath() throws Exception {
        Path nonExistingFilePath = rootPath.resolve("someFile.txt");
        assertThat(PathExtension.findUniqueFilePath(nonExistingFilePath)).isEqualTo(nonExistingFilePath);
    }

    @Test
    void test_isEmpty_Given_emptyPath_Then_returnsTrue() throws Exception {
        assertThat(PathExtension.isEmpty(rootPath)).isTrue();
    }

    @Test
    void test_isEmpty_Given_pathDirectoryWithFile_Then_returnsFalse() throws Exception {
        Files.createFile(rootPath.resolve("f1.txt"));
        assertThat(PathExtension.isEmpty(rootPath)).isFalse();
    }

    @Test
    void test_isEmpty_Given_pathFile_Then_returnsFalse() throws Exception {
        List<Path> filePaths = createFiles(rootPath, "file.txt");
        Path path = filePaths.get(0);
        assertThat(path).exists();

        assertThat(PathExtension.isEmpty(path)).isFalse();
    }

    @Test
    void test_isEmpty_Given_pathFolderAndEmpty_Then_returnsTrue() throws Exception {
        assertThat(PathExtension.isEmpty(rootPath)).isTrue();
    }

    @Test
    void test_isEmpty_Given_pathFolderAndNotEmpty_Then_returnsFalse() throws Exception {
        createFiles(rootPath, "file.txt");
        assertThat(PathExtension.isEmpty(rootPath)).isFalse();
    }

    @Test
    void test_isEmpty_Given_pathFolderAndNotExists_Then_returnsFalse() throws Exception {
        Path path = rootPath.resolve("noExisitingSubFolder");
        assertThat(PathExtension.isEmpty(path)).isFalse();
    }

    @Test
    void test_relocateTo_Given_targetRootPathAndSourceRootAbsolutePathAndSourceAbsolutePath_Then_returnsPathUnderTargetPathAndTargetSubpathIsSameAsSourcePathRelativeToSourceRootPath() throws Exception {
        Path sourceRoot = rootPath.resolve("sourceRoot");
        Path relocate = sourceRoot.resolve("relocateSub");

        Path targetRoot = rootPath.resolve("targetRoot");

        assertThat(PathExtension.getRelocationPath(targetRoot, sourceRoot, relocate)).isEqualTo(targetRoot.resolve("relocateSub"));
    }

    @Test
    void test_touch_Given_pathAndFileTime_Then_setsFileLastModifiedTimeToFileTime() throws Exception {
        FileTime fileTime = FileTime.from(0, TimeUnit.MILLISECONDS);
        PathExtension.touch(rootPath, fileTime);

        assertThat(Files.getLastModifiedTime(rootPath)).isEqualTo(fileTime);
    }

    @Test
    void test_touch_Given_pathsAndFileTime_Then_setsPathsLastModifiedTimesToFileTime() throws Exception {
        FileTime fileTime = FileTime.from(0, TimeUnit.MILLISECONDS);

        Path sub0 = rootPath.resolve("sub0");

        Path sub1 = rootPath.resolve("sub1");
        Path sub11 = sub1.resolve("sub11");

        Path sub2 = rootPath.resolve("sub2");
        Path sub21 = sub2.resolve("sub21");
        Path sub22 = sub2.resolve("sub22");

        Files.createDirectories(sub0);
        Files.createDirectories(sub11);
        Files.createDirectories(sub21);
        Files.createDirectories(sub22);

        Path file = Files.createFile(sub21.resolve("f1.txt"));

        List<Path> paths = Stream.of(sub0, sub1, sub11, sub2, sub21, sub22, file).collect(Collectors.toList());

        PathExtension.touch(paths, fileTime);

        assertThat(paths).allSatisfy(uConsumer(p -> assertThat(Files.getLastModifiedTime(p)).isEqualTo(fileTime)));
    }

    @Test
    void test_truncate_Given_pathWithFileAndSizeAndFileSizeGreaterThanSize_Then_fileSizeIsTruncatedToSize() throws Exception {
        Path path = rootPath.resolve("f1.txt");
        byte[] bytes = RandomStringUtils.random(100).getBytes(Charset.defaultCharset());
        Files.write(path, bytes);

        assertThatSizeEquals(path, bytes.length);

        int truncateSize = bytes.length / 2;
        PathExtension.truncate(path, truncateSize);

        assertThatSizeEquals(path, truncateSize);
    }

    @Test
    void test_truncate_Given_pathWithFileAndSizeAndFileSizeSmallerThanSize_Then_fileSizeIsNotChanged() throws Exception {
        Path path = rootPath.resolve("f2.txt");
        byte[] bytes = RandomStringUtils.random(100).getBytes(Charset.defaultCharset());
        Files.write(path, bytes);

        PathExtension.truncate(path, bytes.length * 2L);

        assertThatSizeEquals(path, bytes.length);
    }

    @Test
    void test_truncate_Given_pathWithFile_When_fileDoesNotExist_Then_throwsIOException() {
        assertThatExceptionOfType(IOException.class).isThrownBy(() -> PathExtension.truncate(rootPath.resolve("notexistingfile.test"), 100));
    }

    @Test
    void test_walkIfExists_Given_existingPath_Then_returnsRootPathAndSubpaths() throws Exception {
        Path sub1 = rootPath.resolve("sub1");
        Path sub2 = rootPath.resolve("sub2");
        Path sub21 = sub2.resolve("sub21");
        Files.createDirectories(sub1);
        Files.createDirectories(sub21);
        Path f1 = Files.createFile(sub21.resolve("f1.txt"));

        assertThat(PathExtension.walkIfExists(rootPath)).containsExactlyInAnyOrder(rootPath, sub1, sub2, sub21, f1);
    }

    @Test
    void test_walkIfExists_Given_nonExistingPath_Then_returnsEmpty() throws Exception {
        assertThat(PathExtension.walkIfExists(rootPath.resolve("parent"))).isEmpty();
    }

}
