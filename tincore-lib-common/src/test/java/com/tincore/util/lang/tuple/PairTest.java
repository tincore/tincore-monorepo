package com.tincore.util.lang.tuple;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PairTest {
    @Test
    void test_compareTo_Given_differentElements_Then_returnsGreaterThanByComparingElements() {
        assertThat(Pair.of("a", "b")).isLessThan(Pair.of("x", "b"));
        assertThat(Pair.of("a", "b")).isLessThan(Pair.of("a", "x"));

        assertThat(Pair.of("x", "b")).isGreaterThan(Pair.of("a", "b"));
        assertThat(Pair.of("a", "x")).isGreaterThan(Pair.of("a", "b"));

        assertThat(Pair.of("a", "b")).isLessThan(Pair.ofMutable("x", "b"));
        assertThat(Pair.of("a", "b")).isLessThan(Pair.ofMutable("a", "x"));

        assertThat(Pair.of("x", "b")).isGreaterThan(Pair.ofMutable("a", "b"));
        assertThat(Pair.of("a", "x")).isGreaterThan(Pair.ofMutable("a", "b"));
    }

    @Test
    void test_compareTo_Given_sameElements_Then_returnsZero() {
        assertThat(Pair.of("a", 1)).isEqualByComparingTo(Pair.of("a", 1));
        assertThat(Pair.ofMutable("a", 1)).isEqualByComparingTo(Pair.ofMutable("a", 1));
        assertThat(Pair.of("a", 1)).isEqualByComparingTo(Pair.ofMutable("a", 1));
    }

    @Test
    void test_equals_Given_differentElements_Then_returnsFalse() {
        assertThat(Pair.of("a", "b")).isNotEqualTo(Pair.of("x", "b"));
        assertThat(Pair.of("a", "b")).isNotEqualTo(Pair.of("a", "x"));
    }

    @Test
    void test_equals_Given_sameElements_Then_returnsTrue() {
        assertThat(Pair.of("a", 1)).isEqualTo(Pair.of("a", 1));
        assertThat(Pair.ofMutable("a", 1)).isEqualTo(Pair.ofMutable("a", 1));
        assertThat(Pair.of("a", 1)).isEqualTo(Pair.ofMutable("a", 1));
    }

    @Test
    void test_getHead_Given_instance_Then_returnsLeftmostValue() {
        assertThat(Pair.of("a", "b").getHead()).isEqualTo("a");
    }

    @Test
    void test_getValue_Given_instance_Then_returnsRightValues() {
        assertThat(Pair.of("a", "b").getValue()).isEqualTo("b");
    }

    @Test
    void test_hashCode_Given_differentElements_Then_returnsDifferent() {
        assertThat(Pair.of("a", "b")).doesNotHaveSameHashCodeAs(Pair.of("x", "b"));
        assertThat(Pair.of("a", "b")).doesNotHaveSameHashCodeAs(Pair.of("a", "x"));
    }

    @Test
    void test_hashCode_Given_sameElements_Then_returnsSame() {
        assertThat(Pair.of("a", 1)).hasSameHashCodeAs(Pair.of("a", 1));
        assertThat(Pair.ofMutable("a", 1)).hasSameHashCodeAs(Pair.ofMutable("a", 1));
    }

    @Test
    void test_mapLeft_Given_mappers_Then_returnsWithFunctionAppliedToLeft() {
        assertThat(Pair.of("a", "b").mapLeft(s -> s + "1")).isEqualTo(Pair.of("a1", "b"));
    }

    @Test
    void test_mapRight_Given_mappers_Then_returnsWithFunctionAppliedToRight() {
        assertThat(Pair.of("a", "b").mapRight(s -> s + "1")).isEqualTo(Pair.of("a", "b1"));
    }

    @Test
    void test_map_Given_mappers_Then_returnsMappedValues() {
        assertThat(Pair.of("a", "b").map(s -> s + "1", s -> s + "2")).isEqualTo(Pair.of("a1", "b2"));
    }

    @Test
    void test_map_Given_mappers_Then_returnsPairWithMappedValues() {
        assertThat(Pair.of("a", "b").map(s -> s + "1", s -> s + "2")).isEqualTo(Pair.of("a1", "b2"));
    }

    @Test
    void test_stream_Given_elements_Then_returnsStreamOfElements() {
        assertThat(Pair.of("a", 1).stream()).containsExactly("a", 1);
    }

    @Test
    void test_swap_Given_value_Then_returnsPairSwitchingKeyWithValue() {
        assertThat(Pair.of("A", 1).swap()).isEqualTo(Pair.of(1, "A"));
    }

    @Test
    void test_withHead_Given_instance_Then_returnsInstanceWithFirstElementSet() {
        assertThat(Pair.of("a", "b").withHead("x")).isEqualTo(Pair.of("x", "b"));
    }

    @Test
    void test_withTail_Given_instance_Then_returnsInstanceWithNonFirstElementsSet() {
        assertThat(Pair.of("a", "b").withTail(Unit.of("1"))).isEqualTo(Pair.of("a", "1"));
    }
}
