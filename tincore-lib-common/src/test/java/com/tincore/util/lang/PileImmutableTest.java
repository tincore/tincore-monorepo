package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class PileImmutableTest extends AbstractPileTest {

    @Override
    Pile<?, String> createPile(String... items) {
        return Pile.of(items);
    }

    @Test
    void test_experiment_Given_operations_Then_modifiesPile() {
        assertThat(Pile.of("a", "b").map(CONCAT)).containsExactly("ab");
        assertThat(Pile.of("a").dup().map(CONCAT)).containsExactly("aa");
        assertThat(Pile.of("a").apply(p -> p.dup().map(CONCAT))).containsExactly("aa");
        assertThat(Pile.of("a").apply(((Function<Pile.Immutable<String>, Pile.Immutable<String>>) p -> p.map(CONCAT)).compose(Pile::dup))).containsExactly("aa");
    }

    @Test
    void test_remove_Given_invoked_Then_throwsUnsupportedOperationException() {
        assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(() -> createPile("a", "b").remove());
    }

}
