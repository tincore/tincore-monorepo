package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.tincore.util.lang.function.ThrowingSupplier.uGet;
import static com.tincore.util.lang.function.ThrowingSupplier.uSupplier;
import static org.assertj.core.api.Assertions.*;

class ThrowingSupplierTest {

    public static final ThrowingSupplier<String, RuntimeException> THROWS_ILLEGAL_ARGUMENT_EXCEPTION = () -> {
        throw new IllegalArgumentException("test");
    };

    @Test
    void test_uGet_Given_error_Then_rethrowsError() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> uGet(() -> {
            throw new Error("test");
        }, ExceptionMapper.wrapChecked(e -> new IllegalStateException(e))));
    }

    @Test
    void test_uSupplierWithMapAll_Given_mapAllEqualsFalse_and_uncheckedException_Then_throwsUncheckedException() {
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(
                () -> uSupplier(THROWS_ILLEGAL_ARGUMENT_EXCEPTION, ExceptionMapper.wrapChecked()).get())
            .withMessage("test");
    }

    @Test
    void test_uSupplierWithMapAll_Given_mapAllEqualsTrue_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(
                () -> uSupplier(THROWS_ILLEGAL_ARGUMENT_EXCEPTION, ExceptionMapper.wrapAll(e -> new IllegalStateException(e))).get())
            .havingCause()
            .withMessage("test");
    }

    @Test
    void test_uSupplier_Given_error_Then_rethrowsError() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> uSupplier(() -> {
            throw new Error("test");
        }).get());
    }

    @Test
    void test_uSupplier_Given_noException_Then_executesSuccessfully() {
        assertThat(uSupplier(() -> "x").get()).isEqualTo("x");
    }

    @Test
    void test_uSupplier_Given_throwingExceptionSupplier_Then_convertsCheckedIntoUncheckedException() {
        assertThatException().isThrownBy(() -> uSupplier(() -> {
            throw new Exception("test");
        }).get()).havingCause().withMessage("test");
    }

    @Test
    void test_uSupplier_Given_throwsUncheckedException_Then_throwsException() {
        assertThatException().isThrownBy(((ThrowingSupplier<String, IOException>) () -> {
            throw new RuntimeException("unchecked");
        })::get).withMessage("unchecked");
    }

    @Test
    void test_uSupplier_exceptionBiMapper_Given_noException_Then_executesSuccessfully() {
        assertThat(uSupplier(() -> "x", e -> e).get()).isEqualTo("x");
    }

}
