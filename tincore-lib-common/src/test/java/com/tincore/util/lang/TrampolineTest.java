package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import com.tincore.util.lang.tuple.Pair;
import com.tincore.util.lang.tuple.Triple;
import lombok.Data;
import lombok.With;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static com.tincore.util.lang.Trampoline.*;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@BehaviourGroup(scenario = "Java Utility")
class TrampolineTest extends BehaviourAwareTest {
    private final BiFunction<Integer, String, Trampoline<Integer>> digitSum = Trampoline.builder((v, c) -> Pair.of(v + Integer.parseInt(c.substring(0, 1)), c.substring(1)), (v1, c1) -> c1.isEmpty());
    private final BiFunction<Integer, Pair<Integer, Integer>, Trampoline<Integer>> fibonacci = Trampoline.builder((v, c) -> Pair.of(v + c.getKey(), Pair.of(v, c.getValue() - 1)), (v, c) -> c.getValue() <= 0);

    private final Node<String> tree = new Node<>(new Node<>(null, "n1r", null), "n0", new Node<>(null, "n1l", null));

    private static Trampoline<BigInteger> createFactorial(int number) {
        return Trampoline.recurse(BigInteger.ONE, number, (v, n) -> Pair.of(v.multiply(BigInteger.valueOf(n)), n - 1), (v, n) -> n <= 1);
    }

    private Trampoline<Integer> countValuesClosed(Integer initialValue, int count) {
        return Trampoline.recurse(initialValue, count, (v, c) -> Pair.of(v + 1, c - 1), (v, c) -> c <= 0);
    }

    private Trampoline<Integer> countValuesClosedWithSupplier(Integer value, int n) {
        if (n <= 0) {
            return complete(value);
        }
        return next(value, () -> countValuesClosedWithSupplier(value + 1, n - 1));
    }

    private Trampoline<Integer> fibonacci(int count) {
        return fibonacci.apply(0, Pair.of(1, count));
    }

    @Test
    void test_createControlled_Given_recursiveDeclarativeProblem_Then_problemExecutesSuccessfully() {
        assertThat(digitSum.apply(0, "111222111").reduce()).isEqualTo(12);
    }

    @Test
    void test_reduce_Given_buildDefinedTrampolineWithBiFunction_Then_returnsResult() {
        var u = new LinkedList<>(Collections.singletonList(tree));
        assertThat(Trampoline.<String, Queue<Node<String>>>builder((r, q) -> {
            var node = q.poll();
            q.addAll(Stream.of(node.getLeft(), node.getRight()).filter(Objects::nonNull).toList());
            return Pair.of(r + node.getMiddle(), q);
        }, (r, q) -> q.isEmpty()).apply("", u).reduce()).isEqualTo("n0n1rn1l");
    }

    @Test
    void test_reduce_Given_buildDefinedTrampolineWithFunction_Then_returnsResult() {
        var u = new LinkedList<>(Collections.singletonList(tree));

        assertThat(Trampoline.<Pair<Queue<Node<String>>, String>>builder(p -> {
            var q = p.getLeft();
            var node = q.poll();
            q.addAll(Stream.of(node.getLeft(), node.getRight()).filter(Objects::nonNull).toList());
            return Pair.of(q, p.getRight() + node.getMiddle());
        }, p -> p.getLeft().isEmpty()).apply(Pair.of(u, "")).reduce().getRight()).isEqualTo("n0n1rn1l");
    }

    @Test
    void test_reduce_Given_buildDefinedTrampoline_Then_returnsResult() {
        var build = Trampoline.<Pair<String, Integer>>builder(v -> Pair.of(v.getKey().substring(1), v.getValue() + 1), v -> v.getKey().isEmpty());
        assertThat(build.apply(Pair.of("abcd", 0)).reduce()).isEqualTo(Pair.of("", 4));
    }

    @Test
    void test_reduce_Given_problemThatShouldThrowStackOverflow_Then_problemExecutesSuccessfully() {
        createFactorial(100_000).reduce();
    }

    @Test
    void test_reduce_Given_recurseDefinedTrampolineBivalued_Then_returnsResult() {
        assertThat(createFactorial(2).runningReduce()).containsExactly(BigInteger.valueOf(1), BigInteger.valueOf(2));
        assertThat(createFactorial(2).reduce()).isEqualTo(BigInteger.valueOf(2));
        assertThat(createFactorial(5).runningReduce()).containsExactly(BigInteger.valueOf(1), BigInteger.valueOf(5), BigInteger.valueOf(20), BigInteger.valueOf(60), BigInteger.valueOf(120));
        assertThat(createFactorial(5).reduce()).isEqualTo(BigInteger.valueOf(120));
    }

    @Test
    void test_reduce_Given_recurseDefinedTrampoline_Then_returnsResult() {
        assertThat(recurse(Pair.of("abcd", 0), v -> Pair.of(v.getKey().substring(1), v.getValue() + 1), v -> v.getKey().isEmpty()).reduce()).isEqualTo(Pair.of("", 4));
    }

    @Test
    void test_runningReduce_Given_buildDefinedTrampoline_Then_returnsResult() {
        var build = Trampoline.<Pair<String, Integer>>builder(v -> Pair.of(v.getKey().substring(1), v.getValue() + 1), v -> v.getKey().isEmpty());
        assertThat(build.apply(Pair.of("abcd", 0)).runningReduce())
            .containsExactly(
                Pair.of("abcd", 0),
                Pair.of("bcd", 1),
                Pair.of("cd", 2),
                Pair.of("d", 3),
                Pair.of("", 4));
    }

    @Test
    void test_runningReduce_Given_recurseDefinedTrampoline_Then_returnsResult() {
        assertThat(recurse(Pair.of("a", 0), v -> Pair.of(v.getKey().substring(1), v.getValue() + 1), v -> v.getKey().isEmpty()).runningReduce())
            .containsExactly(
                Pair.of("a", 0),
                Pair.of("", 1));

        assertThat(recurse(Pair.of("abcd", 0), v -> Pair.of(v.getKey().substring(1), v.getValue() + 1), v -> v.getKey().isEmpty()).runningReduce())
            .containsExactly(
                Pair.of("abcd", 0),
                Pair.of("bcd", 1),
                Pair.of("cd", 2),
                Pair.of("d", 3),
                Pair.of("", 4));
    }

    @Test
    void test_runningReduce_Given_trampolineWithIntermediateValuesAndMapper_Then_streamsValues_and_lastValueIsSameAsFold() {

        assertThat(fibonacci(10).runningReduce()).containsExactly(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55);

        var integerTrampoline = countValuesClosed(0, 1);
        assertThat(integerTrampoline.runningReduce()).containsExactly(0, 1);

        assertThat(countValuesClosed(0, 1).runningReduce()).containsExactly(0, 1);
        assertThat(countValuesClosed(0, 1).reduce()).isEqualTo(1);

        assertThat(countValuesClosed(0, 5).runningReduce()).containsExactly(0, 1, 2, 3, 4, 5);
        assertThat(countValuesClosed(0, 10).runningReduce().limit(5)).containsExactly(0, 1, 2, 3, 4);
        assertThat(countValuesClosed(0, 5).reduce()).isEqualTo(5);

        assertThat(countValuesClosed(0, 0).runningReduce()).containsExactly(0);
        assertThat(countValuesClosed(0, 0).reduce()).isEqualTo(0);

    }

    @Test
    void test_runningReduce_Given_trampolineWithIntermediateValuesAndMapper_Then_streamsValues_and_lastValueIsSameAsFold2() {
        assertThat(countValuesClosed(0, 1).runningReduce()).containsExactly(0, 1);
        assertThat(countValuesClosed(5, 3).runningReduce()).containsExactly(5, 6, 7, 8);

    }

    @Test
    void test_runningReduce_Given_trampolineWithIntermediateValues_Then_streamsValues() {
        assertThat(countValuesClosedWithSupplier(0, 1).runningReduce()).containsExactly(0, 1);
        assertThat(countValuesClosedWithSupplier(0, 5).runningReduce()).containsExactly(0, 1, 2, 3, 4, 5);
        assertThat(countValuesClosedWithSupplier(0, 0).runningReduce()).containsExactly(0);
    }

    @Test
    void test_runningReduce_Given_unseededTrampolineWithIntermediateValuesAndMapper_Then_streamsValues_and_lastValueIsSameAsFold() {
        assertThat(Trampoline.recurseUnseeded(0, c -> Pair.of(c, c + 1), c -> c == 5).runningReduce()).containsExactly(0, 1, 2, 3, 4);
        assertThat(Trampoline.recurseUnseeded(0, (v, c) -> Pair.of(c, c + 1), (v, c) -> c == 5).runningReduce()).containsExactly(0, 1, 2, 3, 4);

    }

    @Data
    @With
    private static class Node<T> implements Triple<Node<T>, T, Node<T>> {
        private final Node<T> left;
        private final T middle;
        private final Node<T> right;

        @Override
        public Triple<Node<T>, T, Node<T>> withTail(Pair<T, Node<T>> value) {
            return new Node<>(this.left, value.getLeft(), value.getRight());
        }
    }
}
