package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static com.tincore.util.lang.ExceptionMapper.wrapAll;
import static com.tincore.util.lang.ExceptionMapper.wrapChecked;
import static com.tincore.util.lang.function.ThrowingFunction.uFunction;
import static org.assertj.core.api.Assertions.*;

class ThrowingFunctionTest {

    @Test
    void test_ThrowingFunction_Given_throwsError_Then_rethrows() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> ((ThrowingFunction<Object, Object, IOException>) a -> {
            throw new Error("unchecked");
        }).apply("x"));
    }

    @Test
    void test_ThrowingFunction_Given_throwsUncheckedException_Then_throwsException() {
        assertThatException().isThrownBy(() -> ((ThrowingFunction<Object, Object, IOException>) a -> {
            throw new RuntimeException("unchecked");
        }).apply("x")).withMessage("unchecked");
    }

    @Test
    void test_applyUnchecked_Given_successfulExecution_Then_returnsValue() {
        ThrowingFunction<String, String, Exception> throwingFunction = a -> "*" + a;

        var uncheckedThrowingFunctionA = ThrowingFunction.uFunction(throwingFunction, ExceptionMapper.wrapChecked());

        assertThat(throwingFunction.applyUnchecked("x", r -> new RuntimeException(r))).isEqualTo("*x");
        assertThat(uncheckedThrowingFunctionA.apply("x")).isEqualTo("*x");

        ThrowingFunction<? super String, ? extends String, Exception> throwingFunctionGenerics = a -> "*" + a;
        var uncheckedThrowingFunctionGenerics = ThrowingFunction.uFunction(throwingFunctionGenerics, ExceptionMapper.wrapChecked());

        assertThat(uncheckedThrowingFunctionGenerics.apply("x")).isEqualTo("*x");
        assertThat(Optional.of("x").map(uncheckedThrowingFunctionGenerics)).hasValueSatisfying(s -> assertThat(s).isEqualTo("*x"));

    }

    @Test
    void test_uFunctionWithMapAll_Given_mapAllEqualsFalse_and_uncheckedException_Then_throwsUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IllegalArgumentException("test");
        }, (v, x) -> new IllegalStateException(x)).apply("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uFunctionWithMapAll_Given_mapAllEqualsTrue_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IllegalArgumentException("test");
        }, ExceptionMapper.wrapAll((v, x) -> new IllegalStateException(x))).apply("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uFunction_Given_noException_Then_executesSuccessfully() {
        assertThat(uFunction(a -> "*" + a).apply("x")).isEqualTo("*x");
    }

    @Test
    void test_uFunction_Given_throwingExceptionFunction_Then_convertsCheckedIntoUncheckedException() {
        assertThatException().isThrownBy(() -> uFunction(a -> {
            throw new Exception("test");
        }).apply("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uFunction_exceptionBiMapper_Given_exception_Then_throwsWrapped() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(
            () -> uFunction(a -> {
                throw new IOException("some error");
            }, e -> new RuntimeException(e)).apply("x")).havingCause().withMessage("some error");
    }

    @Test
    void test_uFunction_exceptionBiMapper_Given_noException_Then_executesSuccessfully() {
        assertThat(uFunction(a -> "*" + a, e -> e).apply("x")).isEqualTo("*x");
    }

    @Test
    void test_uFunction_exceptionMapper_Given_noException_Then_executesSuccessfully() {
        assertThat(ThrowingFunction.uFunction(a -> "*" + a, ExceptionMapper.wrapChecked()).apply("x")).isEqualTo("*x");
    }

    @Test
    void test_uFunction_exceptionMapper_Given_toIllegalStateException_and_checkedException_Then_mapsCheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IOException("test");
        }, wrapAll(e -> new IllegalStateException(e))).apply("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uFunction_exceptionMapper_Given_toIllegalStateException_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IllegalArgumentException("test");
        }, wrapAll(e -> new IllegalStateException(e))).apply("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uFunction_exceptionMapper_Given_toUncheckedAll_and_uncheckedException_Then_doesNotRemapUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IllegalArgumentException("test");
        }, wrapAll((v, e) -> new IllegalStateException(e))).apply("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uFunction_exceptionMapper_Given_toUnchecked_and_uncheckedException_Then_doesNotRemapUncheckedException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IllegalArgumentException("test");
        }, wrapChecked()).apply("x")).withMessage("test");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IllegalArgumentException("test");
        }, ExceptionMapper.wrapChecked(e -> new IllegalStateException(e))).apply("x")).withMessage("test");
    }

    @Test
    void test_uFunction_exceptionMapper_Given_toUnchecked_and_uncheckedException_Then_mapsCheckedExceptionIntoUncheckedException2() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IOException("io error");
        }, wrapChecked()).apply("x")).havingCause().withMessage("io error");

        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> ThrowingFunction.uFunction(a -> {
            throw new IOException("io error");
        }, ExceptionMapper.wrapChecked(e -> new IllegalStateException(e))).apply("x")).havingCause().withMessage("io error");
    }

}
