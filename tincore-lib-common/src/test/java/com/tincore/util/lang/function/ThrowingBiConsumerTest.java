package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static com.tincore.util.lang.function.ThrowingBiConsumer.uBiConsumer;
import static org.assertj.core.api.Assertions.*;

class ThrowingBiConsumerTest {

    @Test
    void test_uBiConsumerWithMapAll_Given_mapAllEqualsFalse_and_uncheckedException_Then_throwsUncheckedException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> ThrowingBiConsumer.uBiConsumer((a, b) -> {
            throw new IllegalArgumentException("test");
        }, ExceptionMapper.wrapChecked(e -> new IllegalStateException(e))).accept("x", "y")).withMessage("test");
    }

    @Test
    void test_uBiConsumerWithMapAll_Given_mapAllEqualsTrue_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uBiConsumer((a, b) -> {
            throw new IllegalArgumentException("test");
        }, ExceptionMapper.wrapAll(e -> new IllegalStateException(e))).accept("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiConsumer_Given_noException_Then_executesSuccessfully() {
        var ref = new AtomicReference<String>();
        uBiConsumer((a, b) -> ref.set(String.valueOf(a) + b)).accept("x", "y");
        assertThat(ref).hasValue("xy");
    }

    @Test
    void test_uBiConsumer_Given_throwingExceptionConsumer_Then_convertsCheckedIntoUncheckedException() {
        assertThatException().isThrownBy(() -> uBiConsumer((a, b) -> {
            throw new Exception("test");
        }).accept("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiConsumer_Given_throwsError_Then_rethrows() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> uBiConsumer((a, b) -> {
            throw new Error("unchecked");
        }).accept("x", "y"));
    }

    @Test
    void test_uBiConsumer_exceptionMapper_Given_noException_Then_executesSuccessfully() {
        var ref = new AtomicReference<String>();
        uBiConsumer((a, b) -> ref.set(String.valueOf(a) + b), e -> e).accept("x", "y");
        assertThat(ref).hasValue("xy");
    }

}
