package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class DistinctorsTest {

    @Test
    void test_extracting_Given_extractingProperty_When_testItemsMultipleTimes_Then_returnsTrueFirstTimeItemWithPropertyIsAdded_and_returnsFalseSubsequentTimes() {
        var distinctor = Distinctors.<Optional>extracting(Optional::get);
        var itemA = Optional.of("A");
        var itemB = Optional.of("B");
        assertThat(distinctor.test(itemA)).isTrue();
        assertThat(distinctor.test(itemA)).isFalse();
        assertThat(distinctor.test(itemB)).isTrue();
        assertThat(distinctor.test(itemA)).isFalse();
        assertThat(distinctor.test(itemB)).isFalse();
    }

    @Test
    void test_extracting_Given_items_When_testItemsMultipleTimes_Then_returnsTrueFirstTimeItemIsAdded_and_returnsFalseSubsequentItemsEqualsItemPreviouslyAdded() {
        var distinctor = Distinctors.extracting(v -> v);
        var itemA = Optional.of("A");
        var itemB = Optional.of("A");
        assertThat(distinctor.test(itemA)).isTrue();
        assertThat(distinctor.test(itemA)).isFalse();
        assertThat(distinctor.test(itemB)).isFalse();
        assertThat(distinctor.test(itemA)).isFalse();
        assertThat(distinctor.test(itemB)).isFalse();
    }

    @Test
    void test_reference_Given_items_When_testItemsMultipleTimes_Then_returnsTrueFirstTimeItemReferenceIsTested_and_returnsFalseSubsequentTimes() {
        var distinctor = Distinctors.reference();
        var itemA = Optional.of("A");
        var itemB = Optional.of("A");
        assertThat(distinctor.test(itemA)).isTrue();
        assertThat(distinctor.test(itemA)).isFalse();
        assertThat(distinctor.test(itemB)).isTrue();
        assertThat(distinctor.test(itemA)).isFalse();
        assertThat(distinctor.test(itemB)).isFalse();
    }
}
