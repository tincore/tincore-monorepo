package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@BehaviourGroup(scenario = "Java Utility")
@TestMethodOrder(MethodOrderer.MethodName.class)
class PartitionTest extends BehaviourAwareTest {

    private final List<String> list = Stream.of("a", "b", "c").collect(Collectors.toList());

    @Test
    void test_get_Given_chunkSize_Then_returnsElementsInPageOfChunkSize() {
        Partition<String> partition = Partition.ofSize(list, 2);
        assertThat(partition.get(0)).containsExactly("a", "b");
        assertThat(partition.get(1)).containsExactly("c");
    }

    @Test
    void test_get_Given_indexMoreThanSize_Then_throwsIndexOutOfBoundsException() {
        Partition<String> partition = Partition.ofSize(list, 2);
        assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> partition.get(100));
    }

    @Test
    void test_ofSize_Given_chunkSizeZero_Then_throwsAssertionError() {
        assertThatExceptionOfType(AssertionError.class).isThrownBy(() -> Partition.ofSize(list, 0));
    }

    @Test
    void test_size_Given_chunkMoreThanOne_Then_returnsCountOfPartitionsWithSizeAtMostEqualsToChunkSize() {
        assertThat(Partition.ofSize(list, 2).size()).isEqualTo(2);
    }

    @Test
    void test_size_Given_chunkSizeOne_Then_returnsListSize() {
        assertThat(Partition.ofSize(list, 1).size()).isEqualTo(list.size());
    }

    @Test
    void test_size_Given_emptyList_Then_returnsZero() {
        assertThat(Partition.ofSize(Collections.emptyList(), 1).size()).isEqualTo(0);
    }

    @Test
    void test_streamGet_Given_chunkSize_Then_returnsElementsInPageOfChunkSize() {
        var partition = Partition.ofSize(list, 2).stream().toList();
        assertThat(partition.get(0)).containsExactly("a", "b");
        assertThat(partition.get(1)).containsExactly("c");
    }
}
