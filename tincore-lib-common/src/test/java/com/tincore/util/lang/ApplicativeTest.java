package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.FunctionExtension;
import lombok.experimental.ExtensionMethod;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.tincore.util.lang.Applicative.*;
import static com.tincore.util.lang.Either.ofRight;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

@ExtensionMethod({FunctionExtension.class, LangExtension.class, SequenceExtension.class})
public class ApplicativeTest {
    private final List<String> values = List.of("A", "B", "C");
    private final BiFunction<String, Integer, String> concatStringIntegerFunction = (a, b) -> a + b;
    private final ApplyOne<Integer, String> integerToStringApplicative = Applicative.ofFunction(String::valueOf);
    private final Function<String, String> lengthFunction = s -> "length:" + s.length();
    private final Function<String, String> contentFunction = s -> "content:" + s;
    private final ApplyOne<String, String> lengthApplicative = Applicative.ofFunction(lengthFunction);
    private final ApplyOne<String, String> contentApplicative = Applicative.ofFunction(contentFunction);
    private final List<Applicative<Function<? super String, ? extends String>>> testApplicatives = List.of(lengthApplicative, contentApplicative);

    @Test
    void test_andThenAp_Given_applyMultipleApplicativesRepeatedly_Then_returnsFunctionCompositionApplicativeAndAppliesAllApplicatives() {
        assertThat(Applicative.of(1000)
            .ap(integerToStringApplicative
                .andThenAp(ofEach(lengthApplicative, contentApplicative))
                .andThenAp(ofEach(lengthApplicative, contentApplicative))
                .andThenAp(ofEach(lengthApplicative, contentApplicative)))
            .stream()).containsExactly("length:8", "content:length:8", "length:16", "content:content:length:4", "length:9", "content:length:12", "length:20", "content:content:content:1000");
    }

    @Test
    void test_andThenAp_Given_applyMultipleApplicatives_Then_returnsFunctionCompositionApplicativeAndAppliesAllApplicatives_and_sameResultAsApChaining() {
        Applicative<Function<String, String>> afterApplicative = ofEach(lengthApplicative, contentApplicative);
        assertThat(Applicative.of(1000).ap(integerToStringApplicative.andThenAp(afterApplicative)).stream()).containsExactly("length:4", "content:1000");
        assertThat(Applicative.of(1000).ap(integerToStringApplicative).ap(afterApplicative).stream()).containsExactly("length:4", "content:1000");
    }

    @Test
    void test_andThenAp_Given_apply_Then_returnsFunctionCompositionApplicative() {
        assertThat(Applicative.of(1000).ap(integerToStringApplicative.andThenAp(lengthApplicative)).stream()).containsExactly("length:4");
    }

    @Test
    void test_apReverse_Given_multipleFunctions_Then_returnsApplicativeWithAppliedFunctions_and_resultsGroupedByApplicativeFunction() {
        assertThat(ofFunctions(lengthApplicative, contentApplicative).ap(Applicative.of("A")).stream()).containsExactly("length:1", "content:A");
        assertThat(Applicative.of("A").apReverse(testApplicatives).stream()).containsExactly("length:1", "content:A");

        assertThat(ofFunctions(lengthApplicative, contentApplicative).ap(Applicative.ofEach("A", "AB")).stream())
            .containsExactly(
                "length:1",
                "length:2",
                "content:A",
                "content:AB");
        assertThat(ofEach("A", "AB").apReverse(testApplicatives).stream())
            .containsExactly(
                "length:1",
                "length:2",
                "content:A",
                "content:AB");
    }

    @Test
    void test_apValue_Given_emptyFunctionApplicative_and_singleValueApplicative_Then_returnsEmpty() {
        assertThat(ofFunctions().ap(of("A")).stream()).isEmpty();
    }

    @Test
    void test_apValue_Given_multiFunctionApplicative_and_valueApplicative_Then_returnsApplicativeApplication() {
        assertThat(ofFunctions(lengthApplicative, contentApplicative).ap(of("A")).stream()).containsExactly("length:1", "content:A");
        assertThat(ofFunctions(lengthApplicative, contentApplicative).ap(ofEach(values)).stream()).containsExactly("length:1", "length:1", "length:1", "content:A", "content:B", "content:C");
    }

    @Test
    void test_apValue_Given_singleFunctionApplicative_and_valueApplicative_Then_returnsApplicativeApplication() {
        assertThat(lengthApplicative.ap(of("A")).stream()).containsExactly("length:1");
        assertThat(lengthApplicative.ap(ofEach(values)).stream()).containsExactly("length:1", "length:1", "length:1");
    }

    @Test
    void test_ap_Given_applicativeProduct_Then_returnsListOfEachValueWithEachFunctionApplied() {
        var actual = ofEach(values).ap(ofFunctions(s -> "1-" + s, s -> "2-" + s));
        assertThat(actual.stream())
            .containsExactlyInAnyOrder("1-A", "2-A", "1-B", "2-B", "1-C", "2-C");

        var actual2 = ofEach(values).ap(ofFunctions(List.of(s -> "1-" + s, s -> "2-" + s)));
        assertThat(actual2.stream())
            .containsExactlyInAnyOrder("1-A", "2-A", "1-B", "2-B", "1-C", "2-C");
    }

    @Test
    void test_ap_Given_applicative_Then_returnsListOfEachValueWithEachFunctionApplied() {
        var actual = Applicative.of(values).ap(ofFunctions(s -> "1-" + s, s -> "2-" + s));
        assertThat(actual.stream()).containsExactly("1-[A, B, C]", "2-[A, B, C]");
    }

    @Test
    void test_ap_Given_collectionApplicative_Then_returnsListOfEachValueWithEachFunctionApplied() {
        var actual = ofEach(values).ap(List.of(ofFunction(s -> "1-" + s), ofFunction(s -> "2-" + s)));

        assertThat(actual.stream())
            .containsExactlyInAnyOrder("1-A", "2-A", "1-B", "2-B", "1-C", "2-C");
    }

    @Test
    void test_ap_Given_function_Then_returnsApplicativeWithAppliedFunction() {
        var actual = Applicative.of("A").ap(lengthApplicative);

        assertThat(actual.stream())
            .singleElement()
            .isEqualTo("length:1");
    }

    @Test
    void test_ap_Given_identity_Then_returnsSame() {
        assertThat(ofEach(values).ap(ofFunction(Function.identity())).stream()).containsExactlyElementsOf(values);
    }

    @Test
    void test_ap_Given_multipleFunctions_Then_returnsApplicativeWithAppliedFunctions() {
        assertThat(Applicative.of("A").ap(testApplicatives).stream()).containsExactly("length:1", "content:A");
        assertThat(ofEach("A", "AB").ap(testApplicatives).stream())
            .containsExactly(
                "length:1",
                "content:A",
                "length:2",
                "content:AB");
    }

    @Test
    void test_map_Given_applyApplicative_Then_returnsApplyApplicativeComposingFunction() {
        assertThat(lengthApplicative.map(s -> "prefix" + s).ap(of("01234567890")).stream()).containsExactly("prefixlength:11");
    }

    @Test
    void test_map_Given_empty_Then_returnsEmptyApplicative() {
        assertThat(Applicative.ofNullable(null).map(s -> "x" + s).isEmpty()).isTrue();
        assertThat(Applicative.empty().map(s -> "x" + s).isEmpty()).isTrue();

    }

    @Test
    void test_map_Given_value_Then_returnsApplicativeWithMappedValue() {
        var applicative = Applicative.of("A");
        assertThat(applicative.map(s -> "x" + s).stream())
            .singleElement()
            .isEqualTo("xA");
    }

    @Test
    void test_ofEach_array_Given_empty_Then_returnsEmpty() {
        assertThat(ofEach().isEmpty()).isTrue();
    }

    @Test
    void test_ofEach_collection_Given_null_Then_throwsNullPointerException() {
        assertThatNullPointerException().isThrownBy(() -> ofEach((Collection<String>) null));
    }

    @Test
    void test_validation_POC() {
        BiFunction<String, Integer, Either<String, String>> lengthGt = (s, l) -> ofRight(s, String.class).filterOrElseLeft(s2 -> s2.length() > l, "Length less or equals to " + l);
        var lengthGt2 = lengthGt.applyPartialRight(2);
        Function<String, Either<String, String>> containsLetterX = s -> ofRight(s, String.class).filterOrElseLeft(s2 -> s2.contains("X"), "String does not contain X");

        assertThat(Applicative.of("ABC")
            .ap(ofFunctions(lengthGt2, containsLetterX))
            .stream()
            .map(String::valueOf)
            .toList())
            .hasSize(2);
    }

    @Test
    void test_zip_Given_applicatives_Then_apReturnsComposedApplication() {
        var actual = ofEach("A", "B", "C")
            .zip(ofEach(1, 2, 3))
            .ap(ofFunction(concatStringIntegerFunction.tupled()));

        assertThat(actual.stream()).containsExactly("A1", "B2", "C3");

    }

    @Test
    void test_zip_Given_twoApplicatives_and_curriedFunctionApplicative_and_mappingToFunctionApplication_Then_returnsFunctionAppliedToEachApplicativeContentElements() {
        var actual = ofEach("A", "B", "C")
            .ap(ofFunction(concatStringIntegerFunction.curry()))
            .zip(ofEach(1, 2, 3))
            .mapEntry(Function::apply);

        assertThat(actual.stream()).containsExactly("A1", "B2", "C3");
    }

}
