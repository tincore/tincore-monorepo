package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.FunctionExtension;
import lombok.experimental.ExtensionMethod;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.tincore.util.lang.Kleisli.*;
import static com.tincore.util.lang.SequenceExtensionTest.A;
import static org.assertj.core.api.Assertions.assertThat;

@ExtensionMethod(FunctionExtension.class)
class KleisliTest {

    @Test
    public void test_POC() {
        var k1 = Kleisli.<Integer>ofApplicative()
            .andThen(ofApplicativeAp(String::valueOf, i -> i + 1, i -> i + 100))
            .andThen(ofCollection(List::of));
        assertThat(k1.apply(1)).containsExactly("1", 2, 101);

        var k2 = Kleisli.<Integer, Integer>ofCollection(i -> IntStream.range(0, i).boxed().toList())
            .andThen(ofApplicativeAp(String::valueOf, i -> i + 1, i -> i + 100))
            .andThen(ofCollection(List::of));
        assertThat(k2.apply(3)).hasSize(9);

        var k3 = Kleisli.<Integer, Integer>ofStream(i -> IntStream.range(0, i).boxed())
            .andThen(ofApplicativeAp(String::valueOf, i -> i + 1, i -> i + 100))
            .andThen(ofCollection(List::of));
        var apply3 = k3.apply(3);
        assertThat(apply3).hasSize(9);

        Function<Integer, Stream<Integer>> generateRange = i -> IntStream.rangeClosed(0, i).boxed();

        var k4 = Kleisli.ofStream(generateRange)
            .andThen(ofStream(generateRange))
            .andThen(ofCollection(List::of))
            .andThen(ofOptionalAggregation(s -> s.filter(v -> v > 0).max(Comparator.naturalOrder())));
        assertThat(k4.apply(5)).hasValue(5);

        var k5 = Kleisli.ofStream(generateRange)
            .andThen(ofStream(generateRange))
            .andThen(ofCollection(List::of));
        assertThat(k5.apply(5)).containsExactly(0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 5);

        var k6 = Kleisli.ofStream(generateRange)
            .andThen(ofStream(generateRange))
            .andThen(ofCollection(List::of))
            .andThen(ofOptional(Optional::of, s -> s.filter(v -> v > 0).max(Comparator.naturalOrder())));
        var apply6 = k6.apply(5);
        assertThat(apply6).isNotNull();

        var k7 = Kleisli.ofStream(generateRange)
            .andThen(ofStream(generateRange))
            .andThen(ofCollection(List::of))
            .andThen(ofOptionalMerging(s -> Optional.of(s.map(i -> (Integer) i).toList())));
        var apply7 = k7.apply(5);
        assertThat(apply7).hasValueSatisfying(v -> assertThat(v).containsExactly(0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 5));

    }

    @Test
    public void test_andThen_Given_composableKleislis_Then_returnsComposedKleisli_and_runWith_returnsFunctionComposition() {
        var kleisli1 = Kleisli.<String, String>ofOptional(s -> Optional.of(s).map(v -> v + 1));
        var kleisli2 = Kleisli.<String, String>ofOptional(s -> Optional.of(s).map(v -> v + 2));

        var of = kleisli1.andThen(kleisli2);

        assertThat(of.apply(A)).hasValue(A + 1 + 2);
    }

    @Test
    public void test_andThen_Given_composableStreamKleislis_Then_returnsComposedKleisli_and_runWith_returnsFunctionComposition() {
        var kleisli1 = Kleisli.<String, String>ofStream(s -> Stream.of(s, s, s));
        var kleisli2 = Kleisli.<String, String>ofOptional(s -> Optional.of(s).map(v -> v + 2));

        var of = kleisli1.andThen(kleisli2);

        assertThat(of.apply(A)).hasValue(A + 2);
    }

    @Test
    public void test_andThen_applicativeKleisli_and_createdWithDefault_Given_changeOfType_Then_returnsKleisliThatModifiesResult() {
        var actual = Kleisli.<Integer>ofApplicative()
            .andThen(ofApplicativeAp(String::valueOf))
            .apply(1);
        assertThat(actual.stream()).containsExactly("1");
    }

    @Test
    public void test_andThen_applicativeKleisli_and_createdWithInitialApplicative_Given_changeOfType_Then_returnsKleisliThatModifiesResult() {

        var actual = Kleisli.<Integer, Integer>ofApplicative(s -> Applicative.of(s + 1))
            .andThen(ofApplicativeAp(String::valueOf))
            .apply(1);
        assertThat(actual.stream()).containsExactly("2");
    }

    @Test
    public void test_andThen_applicativeKleisli_and_multipleApplicativeAp_Given_changeOfType_Then_returnsKleisliThatModifiesResult() {
        var actual = Kleisli.<Integer>ofApplicative()
            .andThen(ofApplicativeAp(String::valueOf, i -> i + 1, i -> i + 100))
            .apply(1);
        assertThat(actual.stream()).containsExactly("1", 2, 101);
    }

    @Test
    public void test_apply_Given_nullValue_and_noModifications_Then_returnsEmpty() {
        assertThat(Kleisli.of((String) null).apply("a"))
            .isEmpty();
    }

    @Test
    public void test_apply_Given_ofCollectionKleisli_Then_returnsMappedValue() {
        assertThat(Kleisli.<String, String>ofCollection(s -> List.of(s, s)).apply("x"))
            .containsExactly("x", "x");
    }

    @Test
    public void test_apply_Given_ofFirstKleisli_Then_returnsMappedValue() {
        assertThat(Kleisli.<String, String>ofOptional(s -> Optional.of(s).map(v -> v + "1")).apply("x"))
            .hasValue("x1");
    }

    @Test
    public void test_apply_Given_ofOptionalKleisli_and_noModifications_Then_returnsValue() {
        assertThat(Kleisli.ofOptional()
            .apply("a"))
            .hasValue("a");
    }

    @Test
    public void test_apply_Given_ofStreamKleisli_Then_returnsMappedValue() {
        assertThat(Kleisli.<String, String>ofStream(s -> Stream.of(s, s).map(v -> v + "1")).apply("x"))
            .containsExactly("x1", "x1");
    }

    @Test
    public void test_apply_Given_value_and_noModifications_Then_returnsValue() {
        assertThat(Kleisli.of("x").apply("a"))
            .hasValue("x");
    }

    @Test
    public void test_compose_Given_composableKleislis_Then_returnsComposedKleisli_and_runWith_returnsFunctionComposition() {
        var kleisli1 = Kleisli.<String, String>ofOptional(s -> Optional.of(s).map(v -> v + 1));
        var kleisli2 = Kleisli.<String, String>ofOptional(s -> Optional.of(s).map(v -> v + 2));

        var of = kleisli1.compose(kleisli2);

        assertThat(of.apply(A)).hasValue(A + 2 + 1);
    }

    @Test
    public void test_compose_Given_composableStreamKleislis_Then_returnsComposedKleisli_and_runWith_returnsFunctionComposition() {
        var kleisli1 = Kleisli.<String, String>ofStream(s -> Stream.of(s, s, s));
        var kleisli2 = Kleisli.<String, String>ofOptional(s -> Optional.of(s).map(v -> v + 2));

        var of = kleisli1.compose(kleisli2);

        assertThat(of.apply(A)).containsExactly(A + 2, A + 2, A + 2);
    }

    @Test
    public void test_map_applicativeApKleisli_Given_changeOfType_Then_returnsKleisliThatModifiesResult() {
        var mapped = Kleisli.<Integer, Integer>ofApplicativeAp(s -> s + 1)
            .map(String::valueOf);
        assertThat(mapped.apply(1).stream()).containsExactly("2");
    }

    @Test
    public void test_map_applicativeKleisli_Given_changeOfType_Then_returnsKleisliThatModifiesResult() {
        var mapped = Kleisli.<Integer, Integer>ofApplicative(s -> Applicative.of(s + 1))
            .map(String::valueOf);

        assertThat(mapped.apply(1).stream()).containsExactly("2");
    }

    @Test
    public void test_map_collectionKleisli_Given_mapper_Then_returnsKleisliThatMapsResult() {
        var mapped = Kleisli.<Integer, Integer>ofCollection(s -> List.of(s, s + 1))
            .map(String::valueOf);
        assertThat(mapped.apply(1)).containsExactly("1", "2");
    }

    @Test
    public void test_map_firstKleisli_Given_changeOfType_Then_returnsKleisliThatModifiesResult() {
        var mapped = Kleisli.<Integer, Integer>ofOptional(s -> Optional.of(s + 1))
            .map(String::valueOf);
        assertThat(mapped.apply(1)).hasValue("2");
    }

    @Test
    public void test_map_streamKleisli_Given_mapper_Then_returnsKleisliThatMapsResult() {
        var mapped = Kleisli.<Integer, Integer>ofStream(s -> Stream.of(s, s + 1))
            .map(String::valueOf);
        assertThat(mapped.apply(1)).containsExactly("1", "2");
    }
}
