package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.TestFixtureTrait;
import lombok.experimental.ExtensionMethod;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import static com.tincore.test.support.TestFixtureTrait.EXCEPTION;
import static com.tincore.util.lang.SequenceExtensionTest.*;
import static com.tincore.util.lang.function.FunctionExtension.contradiction;
import static com.tincore.util.lang.function.FunctionExtension.tautology;
import static com.tincore.util.lang.function.ThrowingConsumer.uConsumer;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtensionMethod(MonadicExtension.class)
class MonadicExtensionAlsoTest {

    private final Consumer<String> consumerA = Mockito.mock(Consumer.class, "consumerA");
    private final Consumer<String> consumerB = Mockito.mock(Consumer.class, "consumerB");
    private final Consumer<String> consumerC = Mockito.mock(Consumer.class, "consumerC");
    private final Consumer<String> consumerD = Mockito.mock(Consumer.class, "consumerD");
    private final Consumer<String> consumerE = Mockito.mock(Consumer.class, "consumerE");

    private static List<String> createListWithNull(String... values) {
        return Arrays.asList(values);
    }

    private static List<String> createListWithNull() {
        return createListWithNull(new String[]{null});
    }

    @Test
    void test_alsoEachIf_consumer2_Given_nullValue_Then_doesNotExecuteConsumer() {
        var list = createListWithNull();
        assertThat(list.alsoEachIf(true, consumerA, consumerB)).isEqualTo(list);
        Mockito.verifyNoInteractions(consumerA, consumerB);
    }

    @Test
    void test_alsoEachIf_consumer2_Given_null_Then_doesNotExecuteConsumer() {
        assertThat(((List<String>) null).alsoEachIf(true, consumerA, consumerB)).isNull();
        Mockito.verifyNoInteractions(consumerA, consumerB);
    }

    @Test
    void test_alsoEachIf_consumer2_Given_predicate_Then_executesFirstConsumerForValuesMatchingPredicate_and_executesSecondConsumerForValuesNotMatchingPredicate() {
        var list = createListWithNull(A, null, B, C, null);
        assertThat(list.alsoEachIf(B::equals, consumerA, consumerB)).isEqualTo(list);
        Mockito.verify(consumerB, Mockito.times(1)).accept(A);
        Mockito.verify(consumerA, Mockito.times(1)).accept(B);
        Mockito.verify(consumerB, Mockito.times(1)).accept(C);
        Mockito.verifyNoMoreInteractions(consumerA, consumerB);
    }

    @Test
    void test_alsoEachIf_consumer2_Given_valueFalse_Then_executesSecondConsumer() {
        var list = createListWithNull(A, null, B, C, null);
        assertThat(list.alsoEachIf(false, consumerA, consumerB)).isEqualTo(list);
        Mockito.verify(consumerB, Mockito.times(1)).accept(A);
        Mockito.verify(consumerB, Mockito.times(1)).accept(B);
        Mockito.verify(consumerB, Mockito.times(1)).accept(C);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoEachIf_consumer2_Given_valueTrue_Then_executesFirstConsumer() {
        var list = createListWithNull(A, null, B, C, null);
        assertThat(list.alsoEachIf(true, consumerA, consumerB)).isEqualTo(list);
        Mockito.verify(consumerA, Mockito.times(1)).accept(A);
        Mockito.verify(consumerA, Mockito.times(1)).accept(B);
        Mockito.verify(consumerA, Mockito.times(1)).accept(C);
        Mockito.verifyNoInteractions(consumerB);
    }

    @Test
    void test_alsoEachIf_consumer_Given_nullValue_Then_doesNotExecuteConsumer() {
        var list = createListWithNull();
        assertThat(list.alsoEachIf(true, consumerA)).isEqualTo(list);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoEachIf_consumer_Given_null_Then_doesNotExecuteConsumer() {
        assertThat(((List<String>) null).alsoEachIf(true, consumerA)).isNull();
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoEachIf_consumer_Given_predicate_Then_executesConsumerForValuesMatchingPredicate() {
        var list = createListWithNull(A, null, B, C, null);
        assertThat(list.alsoEachIf(B::equals, consumerA)).isEqualTo(list);
        Mockito.verify(consumerA, Mockito.times(1)).accept(B);
        Mockito.verifyNoMoreInteractions(consumerA);
    }

    @Test
    void test_alsoEachIf_consumer_Given_valueFalse_Then_doesNotexecutesConsumer() {
        var list = createListWithNull(A, null, B, C, null);
        assertThat(list.alsoEachIf(false, consumerA)).isEqualTo(list);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoEachIf_consumer_Given_valueTrue_Then_executesConsumer() {
        var list = createListWithNull(A, null, B, C, null);
        assertThat(list.alsoEachIf(true, consumerA)).isEqualTo(list);
        Mockito.verify(consumerA, Mockito.times(1)).accept(A);
        Mockito.verify(consumerA, Mockito.times(1)).accept(B);
        Mockito.verify(consumerA, Mockito.times(1)).accept(C);
    }

    @Test
    void test_alsoEach_Given_nullValue_Then_doesNotExecuteConsumer() {
        var list = createListWithNull();
        assertThat(list.alsoEach(consumerA)).isEqualTo(list);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoEach_Given_null_Then_doesNotExecuteConsumer() {
        assertThat(((List<String>) null).alsoEach(consumerA)).isNull();
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoIf_fourPredicatesFourConsumers_Given_matchFirstPredicate_Then_executesFirstConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(tautology(), consumerA, tautology(), consumerB, tautology(), consumerC, tautology(), consumerD, consumerE)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verifyNoInteractions(consumerC);
        Mockito.verifyNoInteractions(consumerD);
        Mockito.verifyNoInteractions(consumerE);

    }

    @Test
    void test_alsoIf_fourPredicatesFourConsumers_Given_matchSecondPredicate_Then_executesSecondConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, tautology(), consumerB, tautology(), consumerC, tautology(), consumerD, consumerE)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verify(consumerB).accept(A);
        Mockito.verifyNoInteractions(consumerC);
        Mockito.verifyNoInteractions(consumerD);
        Mockito.verifyNoInteractions(consumerE);
    }

    @Test
    void test_alsoIf_fourPredicatesFourConsumers_Given_noMatchFourthPredicate_Then_executesLastConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, contradiction(), consumerB, contradiction(), consumerC, contradiction(), consumerD, consumerE)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verifyNoInteractions(consumerC);
        Mockito.verifyNoInteractions(consumerD);
        Mockito.verify(consumerE).accept(A);
    }

    @Test
    void test_alsoIf_fourPredicatesFourConsumers_Given_noMatchSecondPredicate_Then_executesThirdConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, contradiction(), consumerB, tautology(), consumerC, tautology(), consumerD, consumerE)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verify(consumerC).accept(A);
        Mockito.verifyNoInteractions(consumerD);
        Mockito.verifyNoInteractions(consumerE);
    }

    @Test
    void test_alsoIf_fourPredicatesFourConsumers_Given_noMatchThirdPredicate_Then_executesLastConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, contradiction(), consumerB, contradiction(), consumerC, contradiction(), consumerD, consumerE)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verifyNoInteractions(consumerC);
        Mockito.verifyNoInteractions(consumerD);
        Mockito.verify(consumerE).accept(A);
    }

    @Test
    void test_alsoIf_matcher_threePredicatesFourConsumers_Given_matchSecondPredicate_Then_executesSecondConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(Matcher.<String>accepting()
            .when(contradiction(), consumerA)
            .when(tautology(), consumerB)
            .when(tautology(), consumerC)
            .when(tautology(), consumerD))).isEqualTo(A);

        Mockito.verifyNoInteractions(consumerA);
        Mockito.verify(consumerB).accept(A);
        Mockito.verifyNoInteractions(consumerC);
        Mockito.verifyNoInteractions(consumerD);
    }

    @Test
    void test_alsoIf_onePredicateOneConsumer_Given_matchFirstPredicate_Then_executesConsumer() {
        assertThat(A.alsoIf(tautology(), consumerA)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_alsoIf_onePredicateOneConsumer_Given_noMatchFirstPredicate_Then_doesNotExecuteConsumer() {
        assertThat(A.alsoIf(contradiction(), consumerA)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoIf_onePredicateTwoConsumers_Given_matchFirstPredicate_Then_executesFirstConsumer_and_doesNotExecuteSecondConsumer() {
        assertThat(A.alsoIf(tautology(), consumerA, consumerB)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
        Mockito.verifyNoInteractions(consumerB);
    }

    @Test
    void test_alsoIf_onePredicateTwoConsumers_Given_noMatchFirstPredicate_Then_doesNotExecutesFirstConsumer_and_executesSecondConsumer() {
        assertThat(A.alsoIf(contradiction(), consumerA, consumerB)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verify(consumerB).accept(A);
    }

    @Test
    void test_alsoIf_oneValueOneConsumer_Given_matchFirstPredicate_Then_executesConsumer() {
        assertThat(A.alsoIf(true, consumerA)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_alsoIf_oneValueOneConsumer_Given_noMatchFirstPredicate_Then_doesNotExecuteConsumer() {
        assertThat(A.alsoIf(false, consumerA)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoIf_oneValueTwoConsumers_Given_matchFirstPredicate_Then_executesFirstConsumer_and_doesNotExecuteSecondConsumer() {
        assertThat(A.alsoIf(true, consumerA, consumerB)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
        Mockito.verifyNoInteractions(consumerB);
    }

    @Test
    void test_alsoIf_threePredicatesFourConsumers_Given_matchFirstPredicate_Then_executesFirstConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(tautology(), consumerA, tautology(), consumerB, tautology(), consumerC, consumerD)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verifyNoInteractions(consumerC);
        Mockito.verifyNoInteractions(consumerC);
    }

    @Test
    void test_alsoIf_threePredicatesFourConsumers_Given_matchSecondPredicate_Then_executesSecondConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, tautology(), consumerB, tautology(), consumerC, consumerD)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verify(consumerB).accept(A);
        Mockito.verifyNoInteractions(consumerC);
        Mockito.verifyNoInteractions(consumerD);
    }

    @Test
    void test_alsoIf_threePredicatesFourConsumers_Given_noMatchSecondPredicate_Then_executesThirdConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, contradiction(), consumerB, tautology(), consumerC, consumerD)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verify(consumerC).accept(A);
        Mockito.verifyNoInteractions(consumerD);
    }

    @Test
    void test_alsoIf_threePredicatesFourConsumers_Given_noMatchThirdPredicate_Then_executesLastConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, contradiction(), consumerB, tautology(), consumerC, consumerD)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verify(consumerC).accept(A);
        Mockito.verifyNoInteractions(consumerD);
    }

    @Test
    void test_alsoIf_twoPredicatesThreeConsumers_Given_matchFirstPredicate_Then_executesFirstConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(tautology(), consumerA, tautology(), consumerB, consumerC)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verifyNoInteractions(consumerC);
    }

    @Test
    void test_alsoIf_twoPredicatesThreeConsumers_Given_matchSecondPredicate_Then_executesSecondConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, tautology(), consumerB, consumerC)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verify(consumerB).accept(A);
        Mockito.verifyNoInteractions(consumerC);
    }

    @Test
    void test_alsoIf_twoPredicatesThreeConsumers_Given_noMatchSecondPredicate_Then_executeslastConsumer_and_doesNotExecuteOtherConsumers() {
        assertThat(A.alsoIf(contradiction(), consumerA, contradiction(), consumerB, consumerC)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verify(consumerC).accept(A);
    }

    @Test
    void test_alsoResource_Given_exception_Then_constructsResource_and_throwsException_and_closesResource() {
        var reference = new AtomicReference<TestFixtureTrait.TestAutoCloseable>();
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> A.alsoResource(s -> {
            var testAutoCloseable = new TestFixtureTrait.TestAutoCloseable();
            testAutoCloseable.setValue(s);
            reference.set(testAutoCloseable);
            return testAutoCloseable;
        },
            ExceptionMapper.wrapChecked(),
            uConsumer(t -> {
                throw EXCEPTION;
            })))
            .withCause(EXCEPTION);

        assertThat(reference).hasValueSatisfying(r -> {
            assertThat(r.isClosed()).isTrue();
            assertThat(r.getValue()).isEqualTo(A);
        });
    }

    @Test
    void test_alsoResource_Given_noException_Then_constructsResource_and_appliesValue_and_closesResource() {
        var reference = new AtomicReference<TestFixtureTrait.TestAutoCloseable>();
        A.alsoResource(s -> {
            var testAutoCloseable = new TestFixtureTrait.TestAutoCloseable();
            reference.set(testAutoCloseable);
            return testAutoCloseable;
        },
            ExceptionMapper.wrapChecked(),
            t -> t.setValue(B));

        assertThat(reference).hasValueSatisfying(r -> {
            assertThat(r.isClosed()).isTrue();
            assertThat(r.getValue()).isEqualTo(B);
        });
    }

    @Test
    void test_alsoTry_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> A.alsoTry(a -> {
            throw EXCEPTION;
        })).withCause(EXCEPTION);
    }

    @Test
    void test_alsoTry_Given_noException_Then_executesConsumer() {
        assertThat(A.alsoTry(consumerA::accept, (v, e) -> new IllegalArgumentException(e))).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_alsoTry_exceptionMapper_Given_exception_Then_rethrowsWrapped() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> A.alsoTry(a -> {
            throw EXCEPTION;
        }, (v, e) -> new IllegalArgumentException(e)));
    }

    @Test
    void test_alsoUnlessPresent_Given_nonNullValue_Then_doesNotExecuteConsumer() {
        assertThat(A.alsoUnlessPresent(() -> consumerA.accept(A))).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoUnlessPresent_Given_nullValue_Then_doesExecutesConsumer() {
        assertThat(NULL.alsoUnlessPresent(() -> consumerA.accept(A))).isNull();
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_alsoUnless_onePredicateOneConsumers_Given_matchFirstPredicate_Then_doesNotExecutesConsumer() {
        assertThat(A.alsoUnless(tautology(), consumerA)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoUnless_onePredicateOneConsumers_Given_noMatchFirstPredicate_Then_executesConsumer() {
        assertThat(A.alsoUnless(contradiction(), consumerA)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_alsoUnless_valueOneConsumers_Given_matchFirstPredicate_Then_doesNotExecutesConsumer() {
        assertThat(A.alsoUnless(true, consumerA)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoUnless_valueOneConsumers_Given_noMatchFirstPredicate_Then_executesConsumer() {
        assertThat(A.alsoUnless(false, consumerA)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_alsoWhen_Given_nullValue_Then_returnsNull_and_doesNotCallAction() {
        assertThat(NULL.alsoWhen(a -> "x", tautology(), consumerA)).isNull();
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoWhen_Given_value_and_predicateDoesNotMatch_Then_doesNotCallAction() {
        assertThat(A.alsoWhen(a -> a, contradiction(), consumerA)).isEqualTo(A);
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_alsoWhen_Given_value_and_predicateMatches_Then_callsAction() {
        assertThat(A.alsoWhen(String::toUpperCase, s -> A.toUpperCase().equals(s), consumerA)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_alsoWith_Given_nonNullValues_Then_returnsValue_and_callsActionWithBothValues() {
        assertThat(A.alsoWith(B, (a, b) -> consumerA.accept(a + b))).isEqualTo(A);
        Mockito.verify(consumerA).accept(A + B);
    }

    @Test
    void test_alsoWith_Given_nullValues_Then_returnsNull_and_doesNotCallAction() {
        assertThat(NULL.alsoWith(NULL, (a, b) -> consumerA.accept(a + b))).isNull();
        assertThat(NULL.alsoWith(B, (a, b) -> consumerA.accept(a + b))).isNull();
        assertThat(A.alsoWith(NULL, (a, b) -> consumerA.accept(a + b))).isEqualTo(A);
    }

    @Test
    void test_also_Given_nonNullValue_Then_executesConsumer() {
        assertThat(A.also(consumerA)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_also_Given_nonNullValue_Then_executesConsumer_and_doesNotExecuteOtherwise() {
        assertThat(A.also(consumerA, consumerB)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
    }

    @Test
    void test_also_Given_nullValue_Then_doesNotExecuteConsumer() {
        assertThat(NULL.also(consumerA)).isNull();
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_also_Given_nullValue_Then_executesOtherwise_and_doesNotExecuteConsumer() {
        assertThat(NULL.also(consumerA, consumerB)).isNull();
        Mockito.verifyNoInteractions(consumerA);
    }

    @Test
    void test_also_withConsumers_Given_nonNullValue_Then_executesConsumers() {
        assertThat(A.also(consumerA, consumerB, consumerC)).isEqualTo(A);
        Mockito.verify(consumerA).accept(A);
        Mockito.verify(consumerB).accept(A);
        Mockito.verify(consumerC).accept(A);
    }

    @Test
    void test_also_withConsumers_Given_nullValue_Then_doesNotExecuteConsumer() {
        assertThat(NULL.also(consumerA, consumerB, consumerC)).isNull();
        Mockito.verifyNoInteractions(consumerA);
        Mockito.verifyNoInteractions(consumerB);
        Mockito.verifyNoInteractions(consumerC);
    }

}
