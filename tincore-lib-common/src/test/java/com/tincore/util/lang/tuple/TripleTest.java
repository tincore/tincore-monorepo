package com.tincore.util.lang.tuple;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TripleTest {

    @Test
    void test_compareTo_Given_differentElements_Then_returnsGreaterThanByComparingElements() {
        assertThat(Triple.of("a", "b", "c")).isLessThan(Triple.of("x", "b", "c"));
        assertThat(Triple.of("a", "b", "c")).isLessThan(Triple.of("a", "x", "c"));
        assertThat(Triple.of("a", "b", "c")).isLessThan(Triple.of("a", "b", "x"));

        assertThat(Triple.of("x", "b", "c")).isGreaterThan(Triple.of("a", "b", "c"));
        assertThat(Triple.of("a", "x", "c")).isGreaterThan(Triple.of("a", "b", "c"));
        assertThat(Triple.of("a", "b", "x")).isGreaterThan(Triple.of("a", "b", "c"));

        assertThat(Triple.of("a", "b", "c")).isLessThan(Triple.ofMutable("x", "b", "c"));
        assertThat(Triple.of("a", "b", "c")).isLessThan(Triple.ofMutable("a", "x", "c"));
        assertThat(Triple.of("a", "b", "c")).isLessThan(Triple.ofMutable("a", "b", "x"));

        assertThat(Triple.of("x", "b", "c")).isGreaterThan(Triple.ofMutable("a", "b", "c"));
        assertThat(Triple.of("a", "x", "c")).isGreaterThan(Triple.ofMutable("a", "b", "c"));
        assertThat(Triple.of("a", "b", "x")).isGreaterThan(Triple.ofMutable("a", "b", "c"));
    }

    @Test
    void test_compareTo_Given_sameElements_Then_returnsZero() {
        assertThat(Triple.of("a", "b", 1)).isEqualByComparingTo(Triple.of("a", "b", 1));
        assertThat(Triple.ofMutable("a", "b", 1)).isEqualByComparingTo(Triple.ofMutable("a", "b", 1));
        assertThat(Triple.of("a", "b", 1)).isEqualByComparingTo(Triple.ofMutable("a", "b", 1));
    }

    @Test
    void test_equals_Given_differentElements_Then_returnsFalse() {
        assertThat(Triple.of("a", "b", "c")).isNotEqualTo(Triple.of("x", "b", "c"));
        assertThat(Triple.of("a", "b", "c")).isNotEqualTo(Triple.of("a", "x", "c"));
        assertThat(Triple.of("a", "b", "c")).isNotEqualTo(Triple.of("a", "b", "x"));
    }

    @Test
    void test_equals_Given_sameElements_Then_returnsTrue() {
        assertThat(Triple.of("a", "b", 1)).isEqualTo(Triple.of("a", "b", 1));
        assertThat(Triple.ofMutable("a", "b", 1)).isEqualTo(Triple.ofMutable("a", "b", 1));
        assertThat(Triple.of("a", "b", 1)).isEqualTo(Triple.ofMutable("a", "b", 1));
    }

    @Test
    void test_getHead_Given_instance_Then_returnsLeftmostValue() {
        assertThat(Triple.of("a", "b", "c").getHead()).isEqualTo("a");
    }

    @Test
    void test_getTail_Given_instance_Then_returnsRightValues() {
        assertThat(Triple.of("a", "b", "c").getTail()).isEqualTo(Pair.of("b", "c"));
    }

    @Test
    void test_hashCode_Given_differentElements_Then_returnsDifferent() {
        assertThat(Triple.of("a", "b", "c")).doesNotHaveSameHashCodeAs(Triple.of("x", "b", "c"));
        assertThat(Triple.of("a", "b", "c")).doesNotHaveSameHashCodeAs(Triple.of("a", "x", "c"));
        assertThat(Triple.of("a", "b", "c")).doesNotHaveSameHashCodeAs(Triple.of("a", "b", "x"));
    }

    @Test
    void test_hashCode_Given_sameElements_Then_returnsSame() {
        assertThat(Triple.of("a", "b", 1)).hasSameHashCodeAs(Triple.of("a", "b", 1));
        assertThat(Triple.ofMutable("a", "b", 1)).hasSameHashCodeAs(Triple.ofMutable("a", "b", 1));
        assertThat(Triple.of("a", "b", 1)).hasSameHashCodeAs(Triple.ofMutable("a", "b", 1));
    }

    @Test
    void test_mapLeft_Given_mappers_Then_returnsWithFunctionAppliedToLeft() {
        assertThat(Triple.of("a", "b", "c").mapLeft(s -> s + "1")).isEqualTo(Triple.of("a1", "b", "c"));
    }

    @Test
    void test_mapMiddle_Given_mappers_Then_returnsWithFunctionAppliedToMiddle() {
        assertThat(Triple.of("a", "b", "c").mapMiddle(s -> s + "1")).isEqualTo(Triple.of("a", "b1", "c"));
    }

    @Test
    void test_mapRight_Given_mappers_Then_returnsWithFunctionAppliedToRight() {
        assertThat(Triple.of("a", "b", "c").mapRight(s -> s + "1")).isEqualTo(Triple.of("a", "b", "c1"));
    }

    @Test
    void test_map_Given_mappers_Then_returnsTripleWithMappedValues() {
        assertThat(Triple.of("a", "b", "c").map(s -> s + "1", s -> s + "2", s -> s + "3"))
            .isEqualTo(Triple.of("a1", "b2", "c3"));
    }

    @Test
    void test_stream_Given_elements_Then_returnsStreamOfElements() {
        assertThat(Triple.of("a", "b", 1).stream()).containsExactly("a", "b", 1);
    }

    @Test
    void test_withHead_Given_instance_Then_returnsInstanceWithFirstElementSet() {
        assertThat(Triple.of("a", "b", "c").withHead("x")).isEqualTo(Triple.of("x", "b", "c"));
    }

    @Test
    void test_withTail_Given_instance_Then_returnsInstanceWithNonFirstElementsSet() {
        assertThat(Triple.of("a", "b", "c").withTail(Pair.of("1", "2"))).isEqualTo(Triple.of("a", "1", "2"));
    }
}
