package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.experimental.ExtensionMethod;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static com.tincore.util.lang.SequenceExtensionTest.*;
import static com.tincore.util.lang.function.FunctionExtension.*;
import static org.assertj.core.api.Assertions.*;

@ExtensionMethod(MonadicExtension.class)
class MonadicExtensionTakeTest {

    @Test
    void test_takeEachAlsoIn_Given_noMatchingElementsInSecondCollection_Then_returnsEmpty() {
        assertThat(List.of(A, B, C).takeEachAlsoIn(List.of())).isEmpty();
        assertThat(List.of(A, B, C).takeEachAlsoIn(List.of("X"))).isEmpty();
        assertThat(List.of().takeEachAlsoIn(List.of(A, B, C))).isEmpty();
        assertThat(List.of().takeEachAlsoIn(List.of())).isEmpty();
    }

    @Test
    void test_takeEachAlsoIn_Given_values_Then_returnsValuesInFirstCollectionAndInSecondCollection() {
        assertThat(List.of(A, B, C).takeEachAlsoIn(List.of(A, B))).containsExactly(A, B);
    }

    @Test
    void test_takeEachAlsoIn_extractor_Given_values_Then_returnsValuesInFirstCollectionAndInSecondCollection() {
        assertThat(List.of().takeEachAlsoIn(List.of(A, B), String::length)).isEmpty();
        assertThat(List.of(A, B).takeEachAlsoIn(List.of(), String::length)).isEmpty();

        assertThat(List.of("A", "AA", "AAA").takeEachAlsoIn(List.of("B", "BB", "BBB"), String::length)).containsExactly("A", "AA", "AAA");
        assertThat(List.of("AA", "BBB", C, C).takeEachAlsoIn(List.of("01", "012"), String::length)).containsExactly("AA", "BBB");
        assertThat(List.of(A, B, C).takeEachAlsoIn(List.of("X"), String::length)).containsExactly(A, B, C);
    }

    @Test
    void test_takeEachAlsoIn_extractors_Given_values_Then_returnsValuesInFirstCollectionAndInSecondCollection() {
        assertThat(List.<String>of().takeEachAlsoIn(List.of("AA", "BB"), s -> s.charAt(0), s -> s.charAt(1))).isEmpty();
        assertThat(List.of("AA", "BB").takeEachAlsoIn(List.<String>of(), s -> s.charAt(0), s -> s.charAt(1))).isEmpty();

        assertThat(List.of("AA", "BB", "CC").takeEachAlsoIn(List.of("XA", "YB", "ZC"), s -> s.charAt(0), s -> s.charAt(1))).containsExactly("AA", "BB", "CC");
        assertThat(List.of("A_", "B_", "A_", "C_").takeEachAlsoIn(List.of("_A", "_B"), s -> s.charAt(0), s -> s.charAt(1))).containsExactly("A_", "B_", "A_");
        assertThat(List.of("A_", "B_", "C_").takeEachAlsoIn(List.<String>of("A_", "B_", "C_"), s -> s.charAt(0), s -> s.charAt(1))).isEmpty();
    }

    @Test
    void test_takeEachIfValue_Given_predicateSatisfied_Then_returnsEntries() {
        assertThat(Map.of(
            A,
            "x",
            B,
            "y",
            C,
            "z",
            D,
            "x").takeEachIfValue("x"::equals))
            .hasSize(2)
            .containsEntry(A, "x")
            .containsEntry(D, "x");
    }

    @Test
    void test_takeEachIf_Given_collection_Then_returnsListWithMatchingValues() {
        assertThat(List.of(A, B, C, A).takeEachIf(A::equals)).containsExactly(A, A);
    }

    @Test
    void test_takeEachUnlessIn_Given_sameElements_Then_returnsEmpty() {
        assertThat(List.of().takeEachUnlessIn(List.of())).isEmpty();
        assertThat(List.of(A, B, C).takeEachUnlessIn(List.of(A, B, C))).isEmpty();
    }

    @Test
    void test_takeEachUnlessIn_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of().takeEachUnlessIn(List.of(A, B))).isEmpty();
        assertThat(List.of(A, B, C).takeEachUnlessIn(List.of(A, B))).containsExactly(C);
        assertThat(List.of(A, B, C).takeEachUnlessIn(List.of())).containsExactly(A, B, C);
    }

    @Test
    void test_takeEachUnlessIn_extractor_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of("A", "AA", "AAA").takeEachUnlessIn(List.of("B", "BB", "BBB"), String::length)).isEmpty();
        assertThat(List.of().takeEachUnlessIn(List.of(A, B), String::length)).isEmpty();
        assertThat(List.of("AA", "BBB", C, C).takeEachUnlessIn(List.of("01", "012"), String::length)).containsExactly(C, C);
        assertThat(List.of(A, B, C).takeEachUnlessIn(List.of(), String::length)).containsExactly(A, B, C);
    }

    @Test
    void test_takeEachUnlessIn_extractors_Given_values_Then_returnsValuesInFirstCollectionAndNotInSecondCollection() {
        assertThat(List.of("AA", "BB", "CC").takeEachUnlessIn(List.of("XA", "YB", "ZC"), s -> s.charAt(0), s -> s.charAt(1))).isEmpty();
        assertThat(List.<String>of().takeEachUnlessIn(List.of("AA", "BB"), s -> s.charAt(0), s -> s.charAt(1))).isEmpty();
        assertThat(List.of("A_", "B_", "C_", "C_").takeEachUnlessIn(List.of("_A", "_B"), s -> s.charAt(0), s -> s.charAt(1))).containsExactly("C_", "C_");
        assertThat(List.of("A_", "B_", "C_").takeEachUnlessIn(List.<String>of("A_", "B_", "C_"), s -> s.charAt(0), s -> s.charAt(1))).containsExactly("A_", "B_", "C_");
    }

    @Test
    void test_takeEachUnlessValue_Given_predicateNotSatisfied_Then_returnsEntries() {
        assertThat(Map.of(
            A,
            "x",
            B,
            "y",
            C,
            "z",
            D,
            "x")
            .takeEachUnlessValue("x"::equals))
            .hasSize(2)
            .containsEntry(B, "y")
            .containsEntry(C, "z");
    }

    @Test
    void test_takeEachUnless_Given_collection_Then_returnsListWithMatchingValues() {
        assertThat(List.of(A, B, C, A).takeEachUnless(A::equals)).containsExactly(B, C);
    }

    @Test
    void test_takeEachWhen_Given_predicateSatisfiedOnExtractedValue_Then_returnsEntries() {
        assertThat(List.of(
            Pair.of(A, "x"),
            Pair.of(B, "y"),
            Pair.of(C, "z"),
            Pair.of(D, "x"))
            .takeEachWhen(Pair::getValue, "x"::equals))
            .hasSize(2)
            .contains(Pair.of(A, "x"))
            .contains(Pair.of(D, "x"));
    }

    @Test
    void test_takeIfOrThrow_Given_predicateNotSatisfied_Then_throwsException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> A.takeIfOrThrow(B::equals, x -> new IllegalArgumentException("bad" + x)));
    }

    @Test
    void test_takeIfOrThrow_Given_predicateSatisfied_Then_returnsValue() {
        assertThat(A.takeIfOrThrow(A::equals, x -> new IllegalArgumentException("bad" + x))).isEqualTo(A);
    }

    @Test
    void test_takeIfOr_Given_nullValue_Then_returnsElse() {
        assertThat(NULL.takeIfOr(contradiction(), A)).isEqualTo(A);
        assertThat(NULL.takeIfOrGet(contradiction(), () -> A)).isEqualTo(A);
        assertThat(NULL.takeIfOrApply(contradiction(), x -> A)).isEqualTo(A);
    }

    @Test
    void test_takeIfOr_Given_value_and_predicateNotSatisfiedThen_returnsOther() {
        assertThat(A.takeIfOr(contradiction(), B)).isEqualTo(B);
        assertThat(A.takeIfOrGet(contradiction(), () -> B)).isEqualTo(B);
        assertThat(A.takeIfOrApply(contradiction(), x -> B)).isEqualTo(B);
    }

    @Test
    void test_takeIfOr_Given_value_and_predicateSatisfiedThen_returnsValue() {
        assertThat(A.takeIfOr(tautology(), B)).isEqualTo(A);
        assertThat(A.takeIfOrGet(tautology(), () -> B)).isEqualTo(A);
        assertThat(A.takeIfOrApply(tautology(), x -> B)).isEqualTo(A);
    }

    @Test
    void test_takeIf_Given_nullValue_Then_returnsNull() {
        assertThat(NULL.takeIf(false)).isNull();
        assertThat(NULL.takeIf(contradiction())).isNull();
    }

    @Test
    void test_takeIf_Given_predicateNotSatisfied_Then_returnsNull() {
        assertThat(A.takeIf(false)).isNull();
        assertThat(A.takeIf(contradiction())).isNull();
        assertThat(A.takeIf(p(A::equals).negate())).isNull();
    }

    @Test
    void test_takeIf_Given_predicateSatisfied_Then_returnsValue() {
        assertThat(A.takeIf(true)).isEqualTo(A);
        assertThat(A.takeIf(tautology())).isEqualTo(A);
        assertThat(A.takeIf(A::equals)).isEqualTo(A);
    }

    @Test
    void test_takeOrThrow_Given_notNull_Then_returnsValue() {
        assertThat(A.takeOrThrow(RuntimeException::new)).isEqualTo(A);

    }

    @Test
    void test_takeOrThrow_Given_null_Then_throwsExcepton() {
        assertThatException().isThrownBy(() -> NULL.takeOrThrow(RuntimeException::new));
    }

    @Test
    void test_takeOr_Given_notNull_Then_returnsValue() {
        assertThat(A.takeOr(B)).isEqualTo(A);
        assertThat(A.takeOrGet(() -> B)).isEqualTo(A);

    }

    @Test
    void test_takeOr_Given_null_Then_returnsOther() {
        assertThat(NULL.takeOr(B)).isEqualTo(B);
        assertThat(NULL.takeOrGet(() -> B)).isEqualTo(B);
    }

    @Test
    void test_takeUnlessOrThrow_Given_predicateNotSatisfied_Then_returnsValue() {
        assertThat(A.takeUnlessOrThrow(B::equals, x -> new IllegalArgumentException("bad" + x))).isEqualTo(A);
    }

    @Test
    void test_takeUnlessOrThrow_Given_predicateSatisfied_Then_throwsException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> A.takeUnlessOrThrow(A::equals, x -> new IllegalArgumentException("bad" + x)));
    }

    @Test
    void test_takeUnlessOr_Given_nullValue_Then_returnsElse() {
        assertThat(NULL.takeUnlessOr(contradiction(), B)).isEqualTo(B);
        assertThat(NULL.takeUnlessOrGet(tautology(), () -> A)).isEqualTo(A);
        assertThat(NULL.takeUnlessOrApply(tautology(), x -> A)).isEqualTo(A);
    }

    @Test
    void test_takeUnlessOr_Given_predicateNotSatisfied_Then_returnsValue() {
        assertThat(B.takeUnlessOr(A::equals, A)).isEqualTo(B);
        assertThat(B.takeUnlessOrGet(contradiction(), () -> B)).isEqualTo(B);
        assertThat(B.takeUnlessOrApply(contradiction(), x -> B)).isEqualTo(B);
    }

    @Test
    void test_takeUnlessOr_Given_predicateSatisfied_Then_returnsElse() {
        assertThat(A.takeUnlessOr(A::equals, B)).isEqualTo(B);
        assertThat(A.takeUnlessOrGet(tautology(), () -> B)).isEqualTo(B);
        assertThat(A.takeUnlessOrApply(tautology(), x -> B)).isEqualTo(B);
    }

    @Test
    void test_takeUnless_Given_nullValue_Then_returnsNull() {
        assertThat(NULL.takeUnless(contradiction())).isNull();
    }

    @Test
    void test_takeUnless_Given_predicateNotSatisfied_Then_returnsValue() {
        assertThat(B.takeUnless(A::equals)).isEqualTo(B);
    }

    @Test
    void test_takeUnless_Given_predicateSatisfied_Then_returnsNull() {
        assertThat(A.takeUnless(A::equals)).isNull();
    }

}
