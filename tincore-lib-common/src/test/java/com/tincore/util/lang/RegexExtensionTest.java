package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import lombok.experimental.ExtensionMethod;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.regex.Pattern;

import static com.tincore.util.lang.RegexExtension.*;
import static org.assertj.core.api.Assertions.assertThat;

@ExtensionMethod(RegexExtension.class)
class RegexExtensionTest extends BehaviourAwareTest {

    @Test
    void test_isFound_Given_regexContainsAnyMatche_Then_returnsTrue() {
        var pattern = Pattern.compile("a");
        assertThat(isFound("a", pattern)).isTrue();
        assertThat(isFound("bab", pattern)).isTrue();
    }

    @Test
    void test_isFound_Given_regexDoesNotContainAnyMatch_Then_returnsFalse() {
        assertThat(isFound("b", Pattern.compile("a"))).isFalse();
    }

    @Test
    void test_isMatchWildcard_Given_caseSensitive_and_matching_Then_returnsTrue() {
        assertThat(isMatchWildcard("abc", "abc", true)).isTrue();
        assertThat(isMatchWildcard("abc", "?bc", true)).isTrue();
        assertThat(isMatchWildcard("abc", "a?c", true)).isTrue();
        assertThat(isMatchWildcard("abc", "ab?", true)).isTrue();
        assertThat(isMatchWildcard("abc", "???", true)).isTrue();

        assertThat(isMatchWildcard("abc", "*bc", true)).isTrue();
        assertThat(isMatchWildcard("abc", "a*c", true)).isTrue();
        assertThat(isMatchWildcard("abc", "ab*", true)).isTrue();

        assertThat(isMatchWildcard("abc", "*", true)).isTrue();

        assertThat(isMatchWildcard("abc/cde", "abc/cde", true)).isTrue();
        assertThat(isMatchWildcard("abc/cde", "abc/*", true)).isTrue();
        assertThat(isMatchWildcard("abc/cde", "ab*", true)).isTrue();

        assertThat(isMatchWildcard("abc/cde", "*bc*", true)).isTrue();
        assertThat(isMatchWildcard("abc/cde", "*c/c*", true)).isTrue();
    }

    @Test
    void test_isMatchWildcard_Given_caseSensitive_and_notMatching_Then_returnsFalse() {
        assertThat(isMatchWildcard("abc", "aBc", true)).isFalse();
        assertThat(isMatchWildcard("abc", "*B*", true)).isFalse();

        assertThat(isMatchWildcard("abc", "?", true)).isFalse();
        assertThat(isMatchWildcard("abc", "??", true)).isFalse();
        assertThat(isMatchWildcard("abc", "????", true)).isFalse();
    }

    @Test
    void test_isMatchWildcard_Given_notCaseSensitive_and_matching_Then_returnsTrue() {
        assertThat(isMatchWildcard("abc", "aBc", false)).isTrue();
        assertThat(isMatchWildcard("aBc", "abc", false)).isTrue();
        assertThat(isMatchWildcard("abC", "?Bc", false)).isTrue();
        assertThat(isMatchWildcard("ABc", "a?C", false)).isTrue();
        assertThat(isMatchWildcard("aBc", "Ab?", false)).isTrue();
        assertThat(isMatchWildcard("abc", "???", false)).isTrue();

        assertThat(isMatchWildcard("Abc", "*BC", false)).isTrue();
        assertThat(isMatchWildcard("aBc", "A*c", false)).isTrue();
        assertThat(isMatchWildcard("AbC", "aB*", false)).isTrue();

        assertThat(isMatchWildcard("AbC", "aBc", false)).isTrue();
        assertThat(isMatchWildcard("abc", "*", false)).isTrue();

        assertThat(isMatchWildcard("AbC/cde", "aBc/cdE", false)).isTrue();
        assertThat(isMatchWildcard("abC/cde", "abC/*", false)).isTrue();
        assertThat(isMatchWildcard("abc/cde", "aB*", false)).isTrue();

        assertThat(isMatchWildcard("aBc/cde", "*bC*", false)).isTrue();
        assertThat(isMatchWildcard("abC/cde", "*c/C*", false)).isTrue();
    }

    @Test
    void test_isMatchWildcard_Given_notCaseSensitive_and_notMatching_Then_returnsFalse() {
        assertThat(isMatchWildcard("abc", "", false)).isFalse();
        assertThat(isMatchWildcard("abc", "z", false)).isFalse();
        assertThat(isMatchWildcard("abc", "*z*", false)).isFalse();

        assertThat(isMatchWildcard("abc", "?", true)).isFalse();
        assertThat(isMatchWildcard("abc", "??", true)).isFalse();
        assertThat(isMatchWildcard("abc", "????", true)).isFalse();
    }

    @Test
    void test_isMatch_Given_regexDoesNotMatch_Then_returnsFalse() {
        assertThat(isMatch("b", Pattern.compile("a"))).isFalse();
    }

    @Test
    void test_isMatch_Given_regexMatches_Then_returnsTrue() {
        assertThat(isMatch("a", Pattern.compile("a"))).isTrue();
    }

    @Test
    void test_replaceAll_Given_matchingStringWithNoMatches_Then_retunsString() {
        assertThat(replaceAll("", VALUES_BETWEEN_BRACKETS_PATTERN, VALUES_BETWEEN_BRACKETS_MAPPING.andThen(n -> "!" + n + "!"))).isEqualTo("");
        assertThat(replaceAll("(a},{b),[c]", VALUES_BETWEEN_BRACKETS_PATTERN, VALUES_BETWEEN_BRACKETS_MAPPING.andThen(n -> "!" + n + "!"))).isEqualTo("(a},{b),[c]");
    }

    @Test
    void test_replaceAll_Given_matchingString_Then_replacesMatchesWithMappingFunction() {
        assertThat(replaceAll("{a},{b},{c}", VALUES_BETWEEN_BRACKETS_PATTERN, VALUES_BETWEEN_BRACKETS_MAPPING.andThen(n -> "!" + n + "!"))).isEqualTo("!a!,!b!,!c!");
    }

    @Test
    void test_replaceNamedGroupMatches_Given_matchingStringWithNamedGroups_Then_replacesGroupsWithMappingFunction() {
        assertThat(replaceNamedGroupMatches(
            "a,b,c",
            Pattern.compile("a,b,c"),
            Map.of("g1", "s1", "g2", "s2", "g3", "s3")))
            .containsExactly("a,b,c");

        assertThat(replaceNamedGroupMatches(
            "a,b,c",
            Pattern.compile("(?<g2>a),(?:b),(?<g1>c).*"),
            Map.of("g1", "s1", "g2", "s2", "g3", "s3")))
            .containsExactly("s2,b,s1");
    }

    @Test
    void test_replaceNamedGroupMatches_Given_multipleMatchingStringWithNamedGroups_Then_returnsEachMatch() {
        assertThat(replaceNamedGroupMatches(
            "a,a,b,a",
            Pattern.compile("(?<g2>a)"),
            Map.of("g1", "s1", "g2", "s2", "g3", "s3")))
            .containsExactlyInAnyOrder("s2,a,b,a", "a,s2,b,a", "a,a,b,s2");
    }

    @Test
    void test_replaceNamedGroupMatches_Given_notMatchingStringWithNamedGroups_Then_returnsEmpty() {
        assertThat(replaceNamedGroupMatches(
            "a,b,c",
            Pattern.compile("x"),
            Map.of("g1", "s1", "g2", "s2", "g3", "s3"))).isEmpty();
    }

    @Test
    void test_splitIncludingRight_Given_noRegexMatches_Then_returnsEmpty() {
        assertThat(splitIncludingRight("zzzx:ax:b", Pattern.compile("aa:"))).isEmpty();
    }

    @Test
    void test_splitIncludingRight_Given_regexMatches_Then_returnsAllMatchesWithRemainingCharactersToTheRightOfThem() {
        assertThat(splitIncludingRight("zzzx:ax:bcd", Pattern.compile("x:"))).containsExactly("x:a", "x:bcd");
    }

    @Test
    void test_withSingleSpaceOrNone_Given_pattern_Then_returnsPatternThatMatchesArgumentAsWordWithOneSpaceIfPresent() {
        var pattern = Pattern.compile("a").withSingleSpaceOrNone();
        assertThat(pattern.matcher("a xxx xxx").replaceAll("")).isEqualTo("xxx xxx");
        assertThat(pattern.matcher("xxx a xxx").replaceAll("")).isEqualTo("xxx xxx");
        assertThat(pattern.matcher("xxx xxx a").replaceAll("")).isEqualTo("xxx xxx");
        assertThat(pattern.matcher("a").replaceAll("")).isEqualTo("");
        assertThat(pattern.matcher("x").replaceAll("")).isEqualTo("x");
        assertThat(pattern.matcher("xax").replaceAll("")).isEqualTo("xax");
        assertThat(pattern.matcher("xxa").replaceAll("")).isEqualTo("xxa");
        assertThat(pattern.matcher("axx").replaceAll("")).isEqualTo("axx");
        assertThat(pattern.matcher("xxa xxx").replaceAll("")).isEqualTo("xxa xxx");
        assertThat(pattern.matcher("xxx axx").replaceAll("")).isEqualTo("xxx axx");
    }
}
