package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.experimental.ExtensionMethod;
import org.junit.jupiter.api.Test;

import static com.tincore.util.lang.SequenceExtensionTest.*;
import static org.assertj.core.api.Assertions.assertThat;

@ExtensionMethod(LangExtension.class)
class LangExtensionTest {
    public static final String B = "b";

    @Test
    void test_optionIf_Given_valueDoesNotMatchPredicate_Then_returnsEmpty() {
        assertThat(A.optionIf("b"::equals)).isEmpty();
    }

    @Test
    void test_optionIf_Given_valueMatchesPredicate_Then_returnsOptional() {
        assertThat(A.optionIf(IS_LETTER_A)).hasValue(A);
    }

    @Test
    void test_optionUnless_Given_valueDoesNotMatchPredicate_Then_returnsOptional() {
        assertThat(A.optionUnless("b"::equals)).hasValue(A);
    }

    @Test
    void test_optionUnless_Given_valueMatchesPredicate_Then_returnsEmpty() {
        assertThat(A.optionUnless(IS_LETTER_A)).isEmpty();
    }

    @Test
    void test_option_Given_nullValue_Then_returnsEmpty() {
        assertThat(NULL.option()).isEmpty();
    }

    @Test
    void test_option_Given_null_Then_returnsEmpty() {
        assertThat(NULL.option()).isEmpty();
    }

    @Test
    void test_option_Given_value_Then_returnsOptional() {
        assertThat(A.option()).hasValue(A);
    }

    @Test
    void test_option_Given_value_Then_returnsOptionalOfValue() {
        assertThat(A.option()).hasValue(A);
    }

    @Test
    void test_option_mapper_Given_empty_Then_returnsEmpty() {
        assertThat(NULL.option(x -> x)).isEmpty();
    }

    @Test
    void test_option_mapper_Given_valueMatchesPredicate_Then_returnsOptional() {
        assertThat(A.option(v -> v + v)).hasValue(A + A);
    }

    @Test
    void test_streamNullable_Given_null_Then_returnsEmptyStream() {
        assertThat(NULL.streamNullable()).isEmpty();
    }

    @Test
    void test_streamNullable_Given_value_Then_returnsStreamWithValue() {
        assertThat(A.streamNullable()).containsExactly(A);
    }

}
