package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Builder;
import lombok.Data;
import lombok.With;
import org.assertj.core.api.ThrowingConsumer;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class PrismTest {
    @Test
    void test_prism_getValue_Given_composed_and_noValuePresent_Then_returnsEmpty() {
        var composedPrism = Lens.of(Type::getTypeA, Type::withTypeA)
            .toPrism()
            .andThenValue(Lens.of(TypeA::getData, TypeA::withData));

        assertThat(composedPrism.getValue(null)).isEmpty();
        assertThat(composedPrism.getValue(new Type(new TypeA(null), List.of()))).isEmpty();
        assertThat(composedPrism.getValue(new Type(null, List.of()))).isEmpty();
    }

    @Test
    void test_prism_getValue_Given_composed_and_valuePresent_Then_returnsOptionalWithValue() {
        var composedPrism = Lens.of(Type::getTypeA, Type::withTypeA)
            .toPrism()
            .andThenValue(Lens.of(TypeA::getData, TypeA::withData));

        assertThat(composedPrism.getValue(new Type(new TypeA("b1"), List.of()))).hasValue("b1");
    }

    @Test
    void test_prism_getValue_Given_valueNull_Then_returnsEmpty() {
        var prism = new Prism<>(Type::getTypeA, Type::withTypeA);

        assertThat(prism.getValue(new Type(null, List.of()))).isEmpty();
    }

    @Test
    void test_prism_getValue_Given_valuePresent_Then_returnsOptionalWithValue() {
        var prism = Lens.of(Type::getTypeA, Type::withTypeA).toPrism();

        assertThat(prism.getValue(new Type(new TypeA("a1"), List.of()))).hasValue(new TypeA("a1"));
    }

    @Test
    void test_prism_setValue_Given_composed_and_valuePresent_Then_returnsOptionalWithValue() {
        var TypeAStringLens = Lens.of(TypeA::getData, TypeA::withData);

        var composedWithLensPrism = Lens.of(Type::getTypeA, Type::withTypeA)
            .toPrism()
            .andThenValue(TypeAStringLens);

        assertThat(composedWithLensPrism.setValueNullable(new Type(new TypeA("initial"), List.of()),
            "changed")).satisfies(v -> assertThat(v.typeA.data).isEqualTo("changed"));

        assertThat(composedWithLensPrism.setValueNullable(new Type(new TypeA(null), List.of()),
            "changed")).satisfies(v -> assertThat(v.typeA.data).isEqualTo("changed"));

        assertThat(composedWithLensPrism.setValueNullable(new Type(null, List.of()),
            "changed")).satisfies(v -> assertThat(v.typeA).isNull());

        var composedWithPrismPrism = Lens.of(Type::getTypeA, Type::withTypeA)
            .toPrism()
            .andThenValue(TypeAStringLens.toPrism());

        assertThat(composedWithPrismPrism.setValueNullable(new Type(new TypeA("initial"), List.of()),
            "changed")).satisfies(v -> assertThat(v.typeA.data).isEqualTo("changed"));

        assertThat(composedWithPrismPrism.setValueNullable(new Type(new TypeA(null), List.of()),
            "changed")).satisfies(v -> assertThat(v.typeA.data).isEqualTo("changed"));

        assertThat(composedWithPrismPrism.setValueNullable(new Type(null, List.of()),
            "changed")).satisfies(v -> assertThat(v.typeA).isNull());
    }

    @Test
    void test_prism_setValue_Given_valuePresent_Then_setValueInProperty() {
        var prism = new Prism<>(Type::getTypeA, Type::withTypeA);

        var newValue = new TypeA("change");
        ThrowingConsumer<Type> containsNewValue = v -> assertThat(v.typeA).isEqualTo(newValue);

        assertThat(prism.setValueNullable(new Type(new TypeA("a1"), List.of()),
            newValue)).satisfies(containsNewValue);

        assertThat(prism.setValueNullable(new Type(null, List.of()), newValue)).satisfies(containsNewValue);
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class TypeA {
        private String data;
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class TypeB {
        private String data;
    }

    @Data
    @With
    @Builder(toBuilder = true)
    private static class Type {
        private TypeA typeA;
        private List<TypeB> list;
    }

}
