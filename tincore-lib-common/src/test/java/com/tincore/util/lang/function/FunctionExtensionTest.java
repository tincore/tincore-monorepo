package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.tuple.Pair;
import com.tincore.util.lang.tuple.Triple;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static com.tincore.util.lang.function.FunctionExtension.*;
import static org.assertj.core.api.Assertions.assertThat;

@ExtensionMethod(FunctionExtension.class)
public class FunctionExtensionTest {

    private static final Function<String, Integer> PARSE_INT = Integer::parseInt;

    private static final Function<Integer, String> TO_STRING = String::valueOf;
    private static final Function<Integer, Integer> TIMES_TWO = a -> a + a;
    private static final BiFunction<Integer, Integer, Integer> BISUM = Integer::sum;
    private static final TriFunction<Integer, Integer, Integer, Integer> TRISUM = (a, b, c) -> a + b + c;

    private static final BiPredicate<Integer, Integer> SUM_GT_ZERO = (a, b) -> a + b > 0;

    private static final TriFunction<String, String, String, String> CONCAT3 = (a, b, c) -> a + b + c;
    private static final BiFunction<String, String, String> CONCAT2 = CONCAT3.applyPartial("");

    private static final TriFunction<Integer, Integer, Integer, Integer> DIVIDE_DIVIDE = (a, b, c) -> a / b / c;
    private static final BiFunction<Integer, Integer, Integer> DIVIDE = DIVIDE_DIVIDE.applyPartialRight(1);
    private static final Function<Integer, Integer> FRACTION_OF_10 = DIVIDE.applyPartial(10);

    private static final AutoCloseableFunction<String, InputStream, IOException> STRING_TO_INPUT_STREAM = s -> new ByteArrayInputStream(s.getBytes());

    private static <T> List<T> evaluateConcurrently(Supplier<T> supplier, int threadCount) throws InterruptedException {
        List<T> results = Collections.synchronizedList(new ArrayList<>());
        var waitToStart = new CountDownLatch(threadCount);
        var waitForCompleted = new CountDownLatch(threadCount);
        var mainThreadWait = new CountDownLatch(1);

        var threads = IntStream.range(0, threadCount)
            .mapToObj(i -> new Thread(() -> {
                waitToStart.countDown();
                try {
                    mainThreadWait.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                results.add(supplier.get());
                waitForCompleted.countDown();
            }))
            .toList();

        threads.forEach(Thread::start);
        waitToStart.await();
        mainThreadWait.countDown();
        waitForCompleted.await();
        return results;
    }

    private int sumNothingMethod() {
        return 4;
    }

    private int sumSelfMethod(int value) {
        return value + value;
    }

    private int sumThreeMethod(int value, int value2, int value3) {
        return value + value2 + value3;
    }

    private int sumTwoMethod(int value, int value2) {
        return value + value2;
    }

    @Test
    void test_applyPartialLeft_biFunction_Given_value_Then_returns_functionWithFixedParameterEqualsValue() {
        assertThat(CONCAT2.applyPartialLeft("A").apply("B")).isEqualTo("AB");
    }

    @Test
    void test_applyPartialLeft_triFunction_Given_value_Then_returns_functionWithFixedParameterEqualsValue() {
        assertThat(CONCAT3.applyPartialLeft("A").apply("B", "C")).isEqualTo("ABC");
    }

    @Test
    void test_applyPartialMiddle_triFunction_Given_value_Then_returns_functionWithFixedParameterEqualsValue() {
        assertThat(CONCAT3.applyPartialMiddle("A").apply("B", "C")).isEqualTo("BAC");
    }

    @Test
    @SneakyThrows
    void test_applyPartialResourceFunction_Given_value_Then_returns_functionWithFixedParameterEqualsValue() {
        var supplier = STRING_TO_INPUT_STREAM.applyPartial("A");

        InputStream inputStream = supplier.get();
        assertThat(new String(inputStream.readAllBytes())).isEqualTo("A");
        assertThat(inputStream.read()).isEqualTo(-1);

    }

    @Test
    void test_applyPartialRight_biFunction_Given_value_Then_returns_functionWithFixedParameterEqualsValue() {
        assertThat(CONCAT2.applyPartialRight("A").apply("B")).isEqualTo("BA");
    }

    @Test
    void test_applyPartialRight_triFunction_Given_value_Then_returns_functionWithFixedParameterEqualsValue() {
        assertThat(CONCAT3.applyPartialRight("A").apply("B", "C")).isEqualTo("BCA");
    }

    @Test
    void test_applyPartial_biFunction_Given_value_Then_returns_functionWithFixedParameterEqualsValue() {
        assertThat(CONCAT2.applyPartial("A").apply("B")).isEqualTo("AB");
    }

    @Test
    void test_applyPartial_function_Given_and_value_Then_returns_supplierWithFixedParameterEqualsValue() {
        var sum4 = TIMES_TWO.applyPartial(2);
        assertThat(sum4.get()).isEqualTo(4);
    }

    @Test
    void test_applyPartial_triFunction_Given_value_Then_returns_functionWithFixedParameterEqualsValue() {
        assertThat(CONCAT3.applyPartial("A").apply("B", "C")).isEqualTo("ABC");
    }

    @Test
    void test_biContradiction_Given_apply_Then_returnsFalse() {
        assertThat(biContradiction().test(true, true)).isFalse();
        assertThat(biContradiction().test(true, false)).isFalse();
        assertThat(biContradiction().test(false, true)).isFalse();
        assertThat(biContradiction().test(false, false)).isFalse();
    }

    @Test
    void test_biTautology_Given_apply_Then_returnsTrue() {
        assertThat(biTautology().test(true, true)).isTrue();
        assertThat(biTautology().test(true, false)).isTrue();
        assertThat(biTautology().test(false, true)).isTrue();
        assertThat(biTautology().test(false, false)).isTrue();
    }

    @Test
    void test_combine_threeFunctions_Given_functions_Then_returnsTriFunctionThatAppliesFunctionsToLeftAndMiddleAndRightReturningApplicationAsTriple() {
        assertThat(TO_STRING.combine(TIMES_TWO, FRACTION_OF_10).apply(9, 1, 100)).isEqualTo(Triple.of("9", 2, 0));
    }

    @Test
    void test_combine_twoFunctions_Given_functions_Then_returnsBiFunctionThatAppliesFunctionsToLeftAndRightReturningApplicationAsPair() {
        assertThat(TO_STRING.combine(TIMES_TWO).apply(9, 1)).isEqualTo(Pair.of("9", 2));
    }

    @Test
    void test_composeLeft_biFunction_Given_function_Then_returns_functionThatAcceptsComposedType() {
        assertThat(BISUM.composeLeft(PARSE_INT).apply("1", 2)).isEqualTo(3);
    }

    @Test
    void test_composeLeft_biPredicate_Given_function_Then_returns_functionThatAcceptsComposedType() {
        assertThat(SUM_GT_ZERO.composeLeft(PARSE_INT).test("1", 2)).isTrue();
        assertThat(SUM_GT_ZERO.composeLeft(PARSE_INT).test("-3", 2)).isFalse();
    }

    @Test
    void test_composeRight_biFunction_Given_function_Then_returns_functionThatAcceptsComposedType() {
        assertThat(BISUM.composeRight(PARSE_INT).apply(1, "2")).isEqualTo(3);
    }

    @Test
    void test_composeRight_biPredicate_Given_function_Then_returns_functionThatAcceptsComposedType() {
        assertThat(SUM_GT_ZERO.composeRight(PARSE_INT).test(1, "2")).isTrue();
        assertThat(SUM_GT_ZERO.composeRight(PARSE_INT).test(1, "-2")).isFalse();
    }

    @Test
    void test_compose_biFunction_Given_function_Then_returns_functionThatAcceptsComposedType() {
        assertThat(BISUM.compose(PARSE_INT, PARSE_INT).apply("1", "2")).isEqualTo(3);
    }

    @Test
    void test_compose_biPredicate_Given_function_Then_returns_functionThatAcceptsComposedType() {
        assertThat(SUM_GT_ZERO.compose(PARSE_INT, PARSE_INT).test("1", "2")).isTrue();
        assertThat(SUM_GT_ZERO.compose(PARSE_INT, PARSE_INT).test("-3", "-1")).isFalse();
    }

    @Test
    void test_contradiction_Given_apply_Then_returnsFalse() {
        assertThat(contradiction().test(true)).isFalse();
        assertThat(contradiction().test(false)).isFalse();
    }

    @Test
    void test_curry_Given_biFunction_Then_returns_curried() {
        Function<Integer, Function<Integer, Integer>> curried = curry(Integer::sum);
        assertThat(curried.apply(0).apply(4)).isEqualTo(4);
        assertThat(curried.apply(-1).apply(4)).isEqualTo(3);
        assertThat(curried.apply(100).apply(4)).isEqualTo(104);
    }

    @Test
    void test_curry_Given_triFunction_Then_returns_curried() {
        Function<Integer, Function<Integer, Function<Integer, Integer>>> curried = TRISUM.curry();
        assertThat(curried.apply(0).apply(4).apply(5)).isEqualTo(9);
        assertThat(curried.apply(-1).apply(4).apply(-3)).isEqualTo(0);
        assertThat(curried.apply(100).apply(4).apply(1000)).isEqualTo(1104);
    }

    @Test
    void test_expandTriple_Given_triFunction_Then_returnsFunctionWithTripleAsParameter() {
        var tupled = TRISUM.tupled();
        var expandedFunction = tupled.expandTriple();

        assertThat(expandedFunction.apply(0, 0, 0)).isEqualTo(tupled.apply(Triple.of(0, 0, 0)));
        assertThat(expandedFunction.apply(1, 2, 3)).isEqualTo(tupled.apply(Triple.of(1, 2, 3)));
    }

    @Test
    void test_expand_Given_biFunction_Then_returnsFunctionWithPairAsParameter() {
        var tupled = BISUM.tupled();
        var expandedFunction = expand(tupled);

        assertThat(expandedFunction.apply(0, 0)).isEqualTo(tupled.apply(Pair.of(0, 0)));
        assertThat(expandedFunction.apply(1, 2)).isEqualTo(tupled.apply(Pair.of(1, 2)));
    }

    @Test
    void test_f_Given_function_Then_facilitatesConciseMethodLifting() {
        assertThat(f(this::sumNothingMethod).get()).isEqualTo(4);
        assertThat(f(this::sumSelfMethod).apply(2)).isEqualTo(4);
        assertThat(f(this::sumTwoMethod).apply(2, 2)).isEqualTo(4);
        assertThat(f(this::sumThreeMethod).apply(1, 1, 2)).isEqualTo(4);
    }

    @Test
    void test_lazy_Given_supplier_Then_only_executes_once() throws Exception {
        var lazySupplier = ((Supplier<Long>) RandomUtils::nextLong).lazy();
        var threadCount = 1000;
        List<Long> results = evaluateConcurrently(lazySupplier, threadCount);
        assertThat(results).hasSize(threadCount);
        assertThat(results.stream().distinct().toList()).hasSize(1);
    }

    @Test
    void test_lift_Given_biFunctionDefinedAtValue_Then_returnsValue() {
        var lifted = DIVIDE.lift((a, b) -> b != 0);
        assertThat(lifted.apply(0, 1)).hasValue(0);
        assertThat(lifted.apply(2, 1)).hasValue(2);
        assertThat(lifted.apply(100, 20)).hasValue(5);
    }

    @Test
    void test_lift_Given_biFunctionUndefinedAtValue_Then_returnsEmpty() {
        assertThat(DIVIDE.lift((a, b) -> b != 0).apply(1, 0)).isEmpty();
    }

    @Test
    void test_lift_Given_functionDefinedAtValue_Then_returnsValue() {
        var fractionOf10 = FRACTION_OF_10.lift(n -> n != 0);
        assertThat(fractionOf10.apply(1)).hasValue(10);
        assertThat(fractionOf10.apply(2)).hasValue(5);
    }

    @Test
    void test_lift_Given_functionUndefinedAtValue_Then_returnsEmpty() {
        assertThat(FRACTION_OF_10.lift(n -> n != 0).apply(0)).isEmpty();
    }

    @Test
    void test_lift_Given_triFunctionDefinedAtValue_Then_returnsValue() {
        var lifted = DIVIDE_DIVIDE.lift((a, b, c) -> b != 0 && c != 0);
        assertThat(lifted.apply(0, 1, 1)).hasValue(0);
        assertThat(lifted.apply(1, 1, 1)).hasValue(1);
        assertThat(lifted.apply(10, 5, 1)).hasValue(2);
    }

    @Test
    void test_lift_Given_triFunctionUndefinedAtValue_Then_returnsEmpty() {
        var lifted = DIVIDE_DIVIDE.lift((a, b, c) -> b != 0 && c != 0);
        assertThat(lifted.apply(1, 0, 0)).isEmpty();
        assertThat(DIVIDE_DIVIDE.lift((a, b, c) -> b != 0 && c != 0).apply(1, 1, 0)).isEmpty();
        assertThat(DIVIDE_DIVIDE.lift((a, b, c) -> b != 0 && c != 0).apply(1, 0, 1)).isEmpty();
    }

    @Test
    void test_lift_function_Given_successfulApply_Then_returnsValue() {
        var lifted = FunctionExtension.<String, Long>lift(Long::parseLong);
        assertThat(lifted.apply("0")).hasValue(0L);
        assertThat(lifted.apply("-1")).hasValue(-1L);
        assertThat(lifted.apply("123")).hasValue(123L);
    }

    @Test
    void test_lift_function_Given_unsuccessfulApply_Then_returnsEmpty() {
        var lifted = FunctionExtension.<String, Long>lift(Long::parseLong);
        assertThat(lifted.apply(null)).isEmpty();
        assertThat(lifted.apply("notLong")).isEmpty();
    }

    @Test
    void test_lift_partialBiFunction_Given_functionUndefinedAtValue_Then_returnsEmpty() {
        var partial = DIVIDE.partial((a, b) -> b != 0);
        assertThat(partial.lift().apply(1, 0)).isEmpty();
    }

    @Test
    void test_lift_partialFunction_Given_functionUndefinedAtValue_Then_returnsEmpty() {
        var partial = FRACTION_OF_10.partial(n -> n != 0);
        assertThat(partial.lift().apply(0)).isEmpty();
    }

    @Test
    void test_lift_partialTriFunction_Given_functionUndefinedAtValue_Then_returnsEmpty() {
        var partial = DIVIDE_DIVIDE.partial((a, b, c) -> b != 0 && c != 0);
        assertThat(partial.lift().apply(1, 1, 0)).isEmpty();
    }

    @Test
    void test_lift_throwingFunction_Given_successfulApply_Then_returnsValue() {
        var lifted = FunctionExtension.lift((ThrowingFunction<String, Long, NumberFormatException>) Long::parseLong);
        assertThat(lifted.apply("0")).hasValue(0L);
        assertThat(lifted.apply("-1")).hasValue(-1L);
        assertThat(lifted.apply("123")).hasValue(123L);
    }

    @Test
    void test_lift_throwingFunction_Given_unsuccessfulApply_Then_returnsEmpty() {
        var lifted = FunctionExtension.lift((ThrowingFunction<String, Long, NumberFormatException>) Long::parseLong);
        assertThat(lifted.apply(null)).isEmpty();
        assertThat(lifted.apply("notLong")).isEmpty();
    }

    @Test
    void test_memoized_Given_biFunction_Then_only_executes_each_value_once() throws Exception {
        var memoized = FunctionExtension.<Integer, String, String>f((t, u) -> t + "_" + u + "_" + RandomUtils.nextLong()).memoized();
        var threadCount = 1000;
        var atomicInteger = new AtomicInteger(0);
        var results = evaluateConcurrently(() -> memoized.apply(atomicInteger.incrementAndGet() % 5, "aa"), threadCount);
        assertThat(results).hasSize(threadCount);
        assertThat(results.stream().distinct().toList()).hasSize(5);
    }

    @Test
    void test_memoized_Given_function_Then_only_executes_each_value_once() throws Exception {
        var memoized = FunctionExtension.<Integer, String>f(t -> t + "_" + RandomUtils.nextLong()).memoized();
        var threadCount = 1000;
        var atomicInteger = new AtomicInteger(0);
        var results = evaluateConcurrently(() -> memoized.apply(atomicInteger.incrementAndGet() % 5), threadCount);
        assertThat(results).hasSize(threadCount);
        assertThat(results.stream().distinct().toList()).hasSize(5);
    }

    @Test
    void test_memoized_Given_supplier_Then_only_executes_once() throws Exception {
        var memoized = ((Supplier<Long>) RandomUtils::nextLong).memoized();
        var threadCount = 1000;
        List<Long> results = evaluateConcurrently(memoized, threadCount);
        assertThat(results).hasSize(threadCount);
        assertThat(results.stream().distinct().toList()).hasSize(1);
    }

    @Test
    void test_memoized_Given_triFunction_Then_only_executes_each_value_once() throws Exception {
        var memoized = FunctionExtension.<Integer, String, String, String>f((t, u, v) -> t + "_" + u + "_" + v + "_" + RandomUtils.nextLong()).memoized();
        var threadCount = 1000;
        var atomicInteger = new AtomicInteger(0);
        var results = evaluateConcurrently(() -> memoized.apply(atomicInteger.incrementAndGet() % 5, "aa", "bb"), threadCount);
        assertThat(results).hasSize(threadCount);
        assertThat(results.stream().distinct().toList()).hasSize(5);
    }

    @Test
    void test_partial_Given_biFunction_Then_isDefinedAtReturnsPredicateEvaluation() {
        var partial = DIVIDE.partial((a, b) -> b != 0);
        assertThat(partial.isDefinedAt(0, 0)).isFalse();
        assertThat(partial.isDefinedAt(1, 0)).isFalse();
        assertThat(partial.isDefinedAt(0, 1)).isTrue();
        assertThat(partial.isDefinedAt(1, 1)).isTrue();
    }

    @Test
    void test_partial_Given_function_Then_isDefinedAtReturnsPredicateEvaluation() {
        var partial = FRACTION_OF_10.partial(n -> n != 0);
        assertThat(partial.isDefinedAt(0)).isFalse();
        assertThat(partial.isDefinedAt(1)).isTrue();
    }

    @Test
    void test_partial_Given_triFunction_Then_isDefinedAtReturnsPredicateEvaluation() {
        var partial = DIVIDE_DIVIDE.partial((a, b, c) -> b != 0 && c != 0);
        assertThat(partial.isDefinedAt(0, 0, 0)).isFalse();
        assertThat(partial.isDefinedAt(1, 0, 0)).isFalse();
        assertThat(partial.isDefinedAt(0, 1, 0)).isFalse();
        assertThat(partial.isDefinedAt(1, 1, 0)).isFalse();
        assertThat(partial.isDefinedAt(0, 0, 1)).isFalse();
        assertThat(partial.isDefinedAt(1, 0, 1)).isFalse();
        assertThat(partial.isDefinedAt(0, 1, 1)).isTrue();
        assertThat(partial.isDefinedAt(1, 1, 1)).isTrue();
    }

    @Test
    void test_product_Given_twoFunctions_Then_returnsFunctionThatAppliesBothFunctionsReturningApplicationAsPair() {
        var product = TO_STRING.product(TIMES_TWO);
        assertThat(product.apply(1)).isEqualTo(Pair.of("1", 2));
    }

    @Test
    void test_product_threeFunctions_Given_functions_Then_returnsFunctionThatAppliesFunctionsReturningApplicationAsTriple() {
        var product = TO_STRING.product(TIMES_TWO, FRACTION_OF_10);
        assertThat(product.apply(100)).isEqualTo(Triple.of("100", 200, 0));
    }

    @Test
    void test_tautology_Given_apply_Then_returnsTrue() {
        assertThat(tautology().test(true)).isTrue();
        assertThat(tautology().test(false)).isTrue();
    }

    @Test
    void test_triContradiction_Given_apply_Then_returnsFalse() {
        assertThat(triContradiction().test(true, true, true)).isFalse();
        assertThat(triContradiction().test(true, false, true)).isFalse();
        assertThat(triContradiction().test(false, true, true)).isFalse();
        assertThat(triContradiction().test(false, false, true)).isFalse();
        assertThat(triContradiction().test(true, true, false)).isFalse();
        assertThat(triContradiction().test(true, false, false)).isFalse();
        assertThat(triContradiction().test(false, true, false)).isFalse();
        assertThat(triContradiction().test(false, false, false)).isFalse();
    }

    @Test
    void test_triTautology_Given_apply_Then_returnsTrue() {
        assertThat(triTautology().test(true, true, true)).isTrue();
        assertThat(triTautology().test(true, false, true)).isTrue();
        assertThat(triTautology().test(false, true, true)).isTrue();
        assertThat(triTautology().test(false, false, true)).isTrue();
        assertThat(triTautology().test(true, true, false)).isTrue();
        assertThat(triTautology().test(true, false, false)).isTrue();
        assertThat(triTautology().test(false, true, false)).isTrue();
        assertThat(triTautology().test(false, false, false)).isTrue();
    }

    @Test
    void test_tupled_Given_biFunction_Then_returnsFunctionWithPairAsParameter() {
        var function = BISUM;
        var tupled = function.tupled();

        assertThat(tupled.apply(Pair.of(0, 0))).isEqualTo(function.apply(0, 0));
        assertThat(tupled.apply(Pair.of(1, 2))).isEqualTo(function.apply(1, 2));
    }

    @Test
    void test_tupled_Given_triFunction_Then_returnsFunctionWithTripleAsParameter() {
        var function = TRISUM;
        var tupled = function.tupled();

        assertThat(tupled.apply(Triple.of(0, 0, 0))).isEqualTo(function.apply(0, 0, 0));
        assertThat(tupled.apply(Triple.of(1, 2, 3))).isEqualTo(function.apply(1, 2, 3));
    }

    @Test
    void test_uncurry_Given_biFunction_Then_returnsFunctionWithPairAsParameter() {
        var curried = BISUM.curry();
        var uncurried = curried.uncurry();

        assertThat(uncurried.apply(0, 0)).isEqualTo(curried.apply(0).apply(0));
        assertThat(uncurried.apply(1, 2)).isEqualTo(curried.apply(1).apply(2));
    }

    @Test
    void test_uncurry_Given_triFunction_Then_returnsFunctionWithTripleAsParameter() {
        var curried = TRISUM.curry();
        var uncurried = curried.uncurryTriple();

        assertThat(uncurried.apply(0, 0, 0)).isEqualTo(curried.apply(0).apply(0).apply(0));
        assertThat(uncurried.apply(1, 2, 3)).isEqualTo(curried.apply(1).apply(2).apply(3));
    }
}
