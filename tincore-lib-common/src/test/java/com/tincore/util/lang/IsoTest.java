package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2024 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class IsoTest {

    @Test
    void test_ofArrayToStream_Given_instance_Then_obeysIsoLaws() {
        var iso = Iso.ofArrayToStream(Integer.class);

        var empty = new Integer[]{};
        assertThat(iso.reverseGet(iso.get(empty))).isEqualTo(empty);

        var array = new Integer[]{1, 2, 3};
        assertThat(iso.reverseGet(iso.get(array))).isEqualTo(array);
    }

    @Test
    void test_ofArrayToStream_reverse_Given_instance_Then_obeysIsoLaws() {
        var reverseIso = Iso.ofArrayToStream(Integer.class).reverse();

        assertThat(reverseIso.reverseGet(reverseIso.get(Stream.empty()))).isEmpty();
        assertThat(reverseIso.reverseGet(reverseIso.get(Stream.of(1, 2, 3)))).containsExactly(1, 2, 3);
    }

    @Test
    void test_ofListToStream_Given_instance_Then_obeysIsoLaws() {
        var iso = Iso.<Integer>ofListToStream();

        var empty = Collections.<Integer>emptyList();
        assertThat(iso.reverseGet(iso.get(empty))).isEqualTo(empty);

        var list = List.of(1, 2, 3);
        assertThat(iso.reverseGet(iso.get(list))).isEqualTo(list);
    }

    @Test
    void test_ofListToStream_reverse_Given_instance_Then_obeysIsoLaws() {
        var reverseIso = Iso.ofListToStream().reverse();

        assertThat(reverseIso.reverseGet(reverseIso.get(Stream.empty()))).isEmpty();
        assertThat(reverseIso.reverseGet(reverseIso.get(Stream.of(1, 2, 3)))).containsExactly(1, 2, 3);
    }

}
