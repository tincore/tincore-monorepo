package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import com.tincore.util.lang.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static com.tincore.util.lang.ExceptionMapper.wrapAll;
import static com.tincore.util.lang.ExceptionMapper.wrapChecked;
import static com.tincore.util.lang.function.ThrowingBiFunction.uBiFunction;
import static org.assertj.core.api.Assertions.*;

class ThrowingBiFunctionTest {

    @Test
    void test_ThrowingBiFunction_Given_throwsError_Then_rethrows() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> ((ThrowingBiFunction<Object, Object, Object, IOException>) (a, b) -> {
            throw new Error("unchecked");
        }).apply("x", "y"));
    }

    @Test
    void test_ThrowingBiFunction_Given_throwsUncheckedException_Then_throwsException() {
        assertThatException().isThrownBy(() -> ((ThrowingBiFunction<Object, Object, Object, IOException>) (a, b) -> {
            throw new RuntimeException("unchecked");
        }).apply("x", "y")).withMessage("unchecked");
    }

    @Test
    void test_applyUnchecked_Given_successfulExecution_Then_returnsValue() {
        ThrowingBiFunction<String, String, String, Exception> throwingBiFunction = (a, b) -> "*" + a + b;

        var uncheckedThrowingBiFunctionA = uBiFunction(throwingBiFunction, ExceptionMapper.wrapChecked());

        assertThat(throwingBiFunction.applyUnchecked("x", "y", r -> new RuntimeException(r))).isEqualTo("*xy");
        assertThat(uncheckedThrowingBiFunctionA.apply("x", "y")).isEqualTo("*xy");

        ThrowingBiFunction<? super String, ? super String, ? extends String, Exception> throwingBiFunctionGenerics = (a, b) -> "*" + a + b;
        var uncheckedThrowingBiFunctionGenerics = uBiFunction(throwingBiFunctionGenerics, ExceptionMapper.wrapChecked());

        assertThat(uncheckedThrowingBiFunctionGenerics.apply("x", "y")).isEqualTo("*xy");
        assertThat(Optional.of(Pair.of("x", "y")).map(p -> uncheckedThrowingBiFunctionGenerics.apply(p.getLeft(), p.getRight()))).hasValueSatisfying(s -> assertThat(s).isEqualTo("*xy"));

    }

    @Test
    void test_uBiFunctionWithMapAll_Given_mapAllEqualsFalse_and_uncheckedException_Then_throwsUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IllegalArgumentException("test");
        }, (v, x) -> new IllegalStateException(x)).apply("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiFunctionWithMapAll_Given_mapAllEqualsTrue_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IllegalArgumentException("test");
        }, ExceptionMapper.wrapAll((v, x) -> new IllegalStateException(x))).apply("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiFunction_Given_noException_Then_executesSuccessfully() {
        assertThat(uBiFunction((a, b) -> "*" + a).apply("x", "y")).isEqualTo("*x");
    }

    @Test
    void test_uBiFunction_Given_throwingExceptionFunction_Then_convertsCheckedIntoUncheckedException() {
        assertThatException().isThrownBy(() -> uBiFunction((a, b) -> {
            throw new Exception("test");
        }).apply("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiFunction_exceptionBiMapper_Given_exception_Then_throwsWrapped() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(
            () -> uBiFunction((a, b) -> {
                throw new IOException("some error");
            }, e -> new RuntimeException(e)).apply("x", "y")).havingCause().withMessage("some error");
    }

    @Test
    void test_uBiFunction_exceptionBiMapper_Given_noException_Then_executesSuccessfully() {
        assertThat(uBiFunction((a, b) -> "*" + a, e -> e).apply("x", "y")).isEqualTo("*x");
    }

    @Test
    void test_uBiFunction_exceptionMapper_Given_noException_Then_executesSuccessfully() {
        assertThat(uBiFunction((a, b) -> "*" + a, ExceptionMapper.wrapChecked()).apply("x", "y")).isEqualTo("*x");
    }

    @Test
    void test_uBiFunction_exceptionMapper_Given_toIllegalStateException_and_checkedException_Then_mapsCheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IOException("test");
        }, wrapAll(e -> new IllegalStateException(e))).apply("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiFunction_exceptionMapper_Given_toIllegalStateException_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IllegalArgumentException("test");
        }, wrapAll(e -> new IllegalStateException(e))).apply("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiFunction_exceptionMapper_Given_toUncheckedAll_and_uncheckedException_Then_doesNotRemapUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IllegalArgumentException("test");
        }, wrapAll((v, e) -> new IllegalStateException(e))).apply("x", "y")).havingCause().withMessage("test");
    }

    @Test
    void test_uBiFunction_exceptionMapper_Given_toUnchecked_and_uncheckedException_Then_doesNotRemapUncheckedException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IllegalArgumentException("test");
        }, wrapChecked()).apply("x", "y")).withMessage("test");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IllegalArgumentException("test");
        }, ExceptionMapper.wrapChecked(e -> new IllegalStateException(e))).apply("x", "y")).withMessage("test");
    }

    @Test
    void test_uBiFunction_exceptionMapper_Given_toUnchecked_and_uncheckedException_Then_mapsCheckedExceptionIntoUncheckedException2() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IOException("io error");
        }, wrapChecked()).apply("x", "y")).havingCause().withMessage("io error");

        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> uBiFunction((a, b) -> {
            throw new IOException("io error");
        }, ExceptionMapper.wrapChecked(e -> new IllegalStateException(e))).apply("x", "y")).havingCause().withMessage("io error");
    }

}
