package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.tincore.util.lang.function.ThrowingPredicate.*;
import static org.assertj.core.api.Assertions.*;

class ThrowingPredicateTest {

    @Test
    void test_ThrowingPredicate_Given_throwsUncheckedException_Then_throwsException() {
        assertThatException().isThrownBy(
            () -> ((ThrowingPredicate<Object, IOException>) a -> {
                throw new RuntimeException("unchecked");
            }).test("x"))
            .withMessage("unchecked");
    }

    @Test
    @SneakyThrows
    void test_ThrowingPredicate_and_Given_predicate_Then_returnsTrueIfBothConditionsAreMet_falseOtherwise() {
        assertThat(tautology().and(tautology()).test("A")).isTrue();
        assertThat(tautology().and(contradiction()).test("A")).isFalse();
        assertThat(contradiction().and(tautology()).test("A")).isFalse();
        assertThat(contradiction().and(contradiction()).test("A")).isFalse();
    }

    @Test
    @SneakyThrows
    void test_ThrowingPredicate_negate_Given_predicate_Then_returnsPredicateLogicallyInverted() {
        assertThat(tautology().negate().test("A")).isFalse();
        assertThat(contradiction().negate().test("A")).isTrue();
    }

    @Test
    @SneakyThrows
    void test_ThrowingPredicate_or_Given_predicate_Then_returnsTrueIfAnyConditionIsMet_falseOtherwise() {
        assertThat(tautology().or(tautology()).test("A")).isTrue();
        assertThat(tautology().or(contradiction()).test("A")).isTrue();
        assertThat(contradiction().or(tautology()).test("A")).isTrue();
        assertThat(contradiction().or(contradiction()).test("A")).isFalse();
    }

    @Test
    void test_uPredicate_Given_noException_Then_executesSuccessfully() {
        assertThat(uPredicate(tautology()).test("x")).isTrue();
    }

    @Test
    void test_uPredicate_Given_throwingExceptionPredicate_Then_convertsCheckedIntoUncheckedException() {
        assertThatException().isThrownBy(() -> uPredicate(a -> {
            throw new Exception("test");
        }).test("x")).havingCause().withMessage("test");
    }

    @Test
    void test_uPredicate_Given_throwsError_Then_rethrows() {
        assertThatExceptionOfType(Error.class).isThrownBy(() -> uPredicate(a -> {
            throw new Error("unchecked");
        }).test("x"));
    }

    @Test
    void test_uPredicate_exceptionBiMapper_Given_noException_Then_executesSuccessfully() {
        Assertions.assertThat(ThrowingPredicate.uPredicate(tautology(), ExceptionMapper.wrapChecked()).test("x")).isTrue();
        assertThat(uPredicate(tautology(), ExceptionMapper.wrapChecked(a -> new RuntimeException(a))).test("x")).isTrue();
        assertThat(uPredicate(tautology(), ExceptionMapper.wrapChecked((a, x) -> new RuntimeException(x))).test("x")).isTrue();
    }

    @Test
    void test_uPredicate_exceptionMapper_Given_mapsAllExceptions_and_uncheckedException_Then_mapsUncheckedExceptionIntoUncheckedException() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(
            () -> uPredicate(a -> {
                throw new IllegalArgumentException("test");
            }, (v, x) -> new IllegalStateException(x)).test("x"))
            .havingCause()
            .withMessage("test");
    }

    @Test
    void test_uPredicate_exceptionMapper_Given_mapsOnlyChecked_and_uncheckedException_Then_throwsUncheckedException() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
            () -> uPredicate(a -> {
                throw new IllegalArgumentException("test");
            }, ExceptionMapper.wrapChecked((v, x) -> new IllegalStateException(x))).test("x"))
            .withMessage("test");
    }

    @Test
    void test_uPredicate_exceptionMapper_Given_noException_Then_executesSuccessfully() {
        assertThat(uPredicate(tautology(), ExceptionMapper.wrapChecked()).test("x")).isTrue();
        assertThat(uPredicate(a -> true, e -> e).test("x")).isTrue();
    }

}
