package com.tincore.util;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

public class NetHelper {

    public static final String IPV4_PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

    public static final String COMMAND_PUBLIC_IPV4_1 = "dig @ns1-1.akamaitech.net ANY whoami.akamai.net +short";
    public static final String COMMAND_PUBLIC_IPV4_2 = "dig @ns1.google.com TXT o-o.myaddr.l.google.com +short";
    public static final String COMMAND_PUBLIC_IPV4_3 = "curl -s icanhazip.com";

    public static String getPublicIpv4Address() throws IOException {
        try {
            StringBuilder sb = new StringBuilder();
            ProcessHelper.exec(COMMAND_PUBLIC_IPV4_1, sb::append);
            String ip = sb.toString();
            if (validateIpV4(ip)) {
                return ip;
            } else {
                throw new IOException("Could not get valid ip. Received=" + ip);
            }
        } catch (InterruptedException e) {
            throw new IOException("Execution failed", e);
        }
    }

    public static boolean validateIpV4(final String ip) {
        return ip.matches(IPV4_PATTERN);
    }
}
