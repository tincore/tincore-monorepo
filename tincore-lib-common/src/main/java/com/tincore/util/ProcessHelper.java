package com.tincore.util;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.SystemUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class ProcessHelper {

    public static final int ON_CLOSE_WAIT_MILLIS = 100;

    public static int exec(String command) throws IOException, InterruptedException {
        return exec(command, null, -1);
    }

    public static int exec(String command, Consumer<String> onConsoleLine) throws IOException, InterruptedException {
        return exec(command, onConsoleLine, -1);
    }

    public static int exec(String command, Consumer<String> onConsoleLine, Duration duration) throws IOException, InterruptedException {
        return exec(command, onConsoleLine, duration.toMillis());
    }

    public static int exec(String command, Consumer<String> onConsoleLine, long timeoutMs) throws IOException, InterruptedException {
        var cmd = getCmd(command);
        var process = new ProcessBuilder().command(cmd)
            .redirectErrorStream(true)
            // .directory(new File(System.getProperty("user.home")));
            .start();

        if (onConsoleLine == null) {
            return execProcess(timeoutMs, process);
        }
        ExecutorService executorService = null; // NOPMD try with resources
        try { // NOPMD try with resources
            executorService = Executors.newSingleThreadExecutor();
            executorService.submit(new StreamGobbler(process.getInputStream(), onConsoleLine));
            return execProcess(timeoutMs, process);
        } finally {
            if (executorService != null) {
                Thread.sleep(ON_CLOSE_WAIT_MILLIS); // Give some time for gobbler to transmit text
                process.getInputStream().close();
                executorService.shutdown();
            }
        }
    }

    private static int execProcess(long timeoutMs, Process process) throws InterruptedException {
        if (timeoutMs > 0) {
            process.waitFor(timeoutMs, TimeUnit.MILLISECONDS);
            return process.exitValue();
        }
        return process.waitFor();
    }

    public static String[] getCmd(String command) {
        if (SystemUtils.IS_OS_WINDOWS) {
            return new String[]{"cmd.exe", "/c", command};
        } else {
            return new String[]{"/bin/sh", "-c", command};
        }
    }

    private static class StreamGobbler implements Runnable {
        private final InputStream inputStream;
        private final Consumer<String> consumer;

        StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            new BufferedReader(new InputStreamReader(inputStream)).lines().forEach(consumer);
        }
    }
}
