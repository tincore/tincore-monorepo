package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface TriConsumer<T, U, V> {

    void accept(T t, U u, V v);

    default TriConsumer<T, U, V> andThen(TriConsumer<? super T, ? super U, ? super V> after) {
        Objects.requireNonNull(after);

        return (t, u, v) -> {
            accept(t, u, v);
            after.accept(t, u, v);
        };
    }

    default <W, X, Y> TriConsumer<W, X, Y> compose(Function<? super W, ? extends T> beforeLeft, Function<? super X, ? extends U> beforeMiddle, Function<? super Y, ? extends V> beforeRight) {
        Objects.requireNonNull(beforeLeft);
        Objects.requireNonNull(beforeMiddle);
        Objects.requireNonNull(beforeRight);
        return (W w, X x, Y y) -> accept(beforeLeft.apply(w), beforeMiddle.apply(x), beforeRight.apply(y));
    }

    default <W> TriConsumer<W, U, V> composeLeft(Function<? super W, ? extends T> before) {
        Objects.requireNonNull(before);
        return (W w, U u, V v) -> accept(before.apply(w), u, v);
    }

    default <W> TriConsumer<T, W, V> composeMiddle(Function<? super W, ? extends U> before) {
        Objects.requireNonNull(before);
        return (T t, W w, V v) -> accept(t, before.apply(w), v);
    }

    default <W> TriConsumer<T, U, W> composeRight(Function<? super W, ? extends V> before) {
        Objects.requireNonNull(before);
        return (T t, U u, W w) -> accept(t, u, before.apply(w));
    }
}
