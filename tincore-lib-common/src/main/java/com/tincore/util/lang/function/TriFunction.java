package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface TriFunction<T, U, V, R> {

    default <W> TriFunction<T, U, V, W> andThen(final Function<? super R, ? extends W> after) {
        Objects.requireNonNull(after);
        return (final T t, final U u, final V v) -> after.apply(apply(t, u, v));
    }

    R apply(T t, U u, V v);

    default <W, X, Y> TriFunction<W, X, Y, R> compose(Function<? super W, ? extends T> beforeLeft, Function<? super X, ? extends U> beforeMiddle, Function<? super Y, ? extends V> beforeRight) {
        Objects.requireNonNull(beforeLeft);
        Objects.requireNonNull(beforeMiddle);
        Objects.requireNonNull(beforeRight);
        return (W w, X x, Y y) -> apply(beforeLeft.apply(w), beforeMiddle.apply(x), beforeRight.apply(y));
    }

    default <W> TriFunction<W, U, V, R> composeLeft(Function<? super W, ? extends T> before) {
        Objects.requireNonNull(before);
        return (W w, U u, V v) -> apply(before.apply(w), u, v);
    }

    default <W> TriFunction<T, W, V, R> composeMiddle(Function<? super W, ? extends U> before) {
        Objects.requireNonNull(before);
        return (T t, W w, V v) -> apply(t, before.apply(w), v);
    }

    default <W> TriFunction<T, U, W, R> composeRight(Function<? super W, ? extends V> before) {
        Objects.requireNonNull(before);
        return (T t, U u, W w) -> apply(t, u, before.apply(w));
    }
}
