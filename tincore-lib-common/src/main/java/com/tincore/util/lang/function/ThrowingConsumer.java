package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import lombok.experimental.ExtensionMethod;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ThrowingConsumer<T, X extends Throwable> {

    static <T, X extends Throwable> Consumer<T> uConsumer(ThrowingConsumer<? super T, ? extends X> consumer) {
        Objects.requireNonNull(consumer);
        return uConsumer(consumer, ExceptionMapper.wrapChecked());
    }

    static <T, X extends Throwable, Y extends RuntimeException> Consumer<T> uConsumer(
        ThrowingConsumer<? super T, ? extends X> consumer,
        Function<? super X, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(consumer);
        Objects.requireNonNull(exceptionMapper);
        return uConsumer(consumer, ExceptionMapper.wrapChecked(exceptionMapper));
    }

    static <T, X extends Throwable, Y extends RuntimeException> Consumer<T> uConsumer(
        ThrowingConsumer<? super T, ? extends X> consumer,
        ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(consumer);
        Objects.requireNonNull(exceptionMapper);
        return t -> consumer.acceptUnchecked(t, exceptionMapper.applyPartial(t));
    }

    void accept(T t) throws X;

    default <Y extends RuntimeException> void acceptUnchecked(T t, ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        acceptUnchecked(t, exceptionMapper.applyPartial(t));
    }

    default <Y extends RuntimeException> void acceptUnchecked(T t, Function<Throwable, ? extends Y> throwableMapper) {
        try {
            accept(t);
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD
            throw throwableMapper.apply(e);
        }
    }

    default ThrowingConsumer<T, X> andThen(final ThrowingConsumer<? super T, X> after) {
        Objects.requireNonNull(after);
        return (final T t) -> {
            accept(t);
            after.accept(t);
        };
    }
}
