package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface TriPredicate<T, U, V> {

    TriPredicate<?, ?, ?> TAUTOLOGY = (t, u, v) -> true;
    TriPredicate<?, ?, ?> CONTRADICTION = TAUTOLOGY.negate();

    default TriPredicate<T, U, V> and(TriPredicate<? super T, ? super U, ? super V> other) { // NOPMD
        Objects.requireNonNull(other);
        return (T t, U u, V v) -> test(t, u, v) && other.test(t, u, v);
    }

    default <W, X, Y> TriPredicate<W, X, Y> compose(Function<? super W, ? extends T> beforeLeft, Function<? super X, ? extends U> beforeMiddle, Function<? super Y, ? extends V> beforeRight) {
        Objects.requireNonNull(beforeLeft);
        Objects.requireNonNull(beforeMiddle);
        Objects.requireNonNull(beforeRight);
        return (W w, X x, Y y) -> test(beforeLeft.apply(w), beforeMiddle.apply(x), beforeRight.apply(y));
    }

    default <W> TriPredicate<W, U, V> composeLeft(Function<? super W, ? extends T> before) {
        Objects.requireNonNull(before);
        return (W w, U u, V v) -> test(before.apply(w), u, v);
    }

    default <W> TriPredicate<T, W, V> composeMiddle(Function<? super W, ? extends U> before) {
        Objects.requireNonNull(before);
        return (T t, W w, V v) -> test(t, before.apply(w), v);
    }

    default <W> TriPredicate<T, U, W> composeRight(Function<? super W, ? extends V> before) {
        Objects.requireNonNull(before);
        return (T t, U u, W w) -> test(t, u, before.apply(w));
    }

    default TriPredicate<T, U, V> negate() {
        return (T t, U u, V v) -> !test(t, u, v);
    }

    default TriPredicate<T, U, V> or(TriPredicate<? super T, ? super U, ? super V> other) { // NOPMD
        Objects.requireNonNull(other);
        return (T t, U u, V v) -> test(t, u, v) || other.test(t, u, v);
    }

    boolean test(T t, U u, V v);

}
