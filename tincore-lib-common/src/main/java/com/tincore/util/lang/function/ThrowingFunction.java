package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.Either;
import com.tincore.util.lang.ExceptionMapper;
import lombok.experimental.ExtensionMethod;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import static com.tincore.util.lang.ExceptionMapper.wrapChecked;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ThrowingFunction<T, R, X extends Throwable> {

    static <T, R, X extends Throwable> Function<T, R> uFunction(
        ThrowingFunction<? super T, ? extends R, ? extends X> function) {
        Objects.requireNonNull(function);
        return uFunction(function, wrapChecked());
    }

    static <T, R, X extends Throwable, Y extends RuntimeException> Function<T, R> uFunction(
        ThrowingFunction<? super T, ? extends R, ? extends X> function,
        Function<? super X, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(function);
        Objects.requireNonNull(exceptionMapper);
        return uFunction(function, wrapChecked(exceptionMapper));
    }

    static <T, R, X extends Throwable, Y extends RuntimeException> Function<T, R> uFunction(
        ThrowingFunction<? super T, ? extends R, ? extends X> function,
        ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(function);
        Objects.requireNonNull(exceptionMapper);
        return t -> function.applyUnchecked(t, exceptionMapper.applyPartial(t));
    }

    default <V> ThrowingFunction<T, V, X> andThen(final ThrowingFunction<? super R, ? extends V, X> after) {
        Objects.requireNonNull(after);
        return (final T t) -> after.apply(apply(t));
    }

    R apply(T t) throws X;

    @SuppressWarnings("unchecked")
    default <L> Either<L, R> applyEither(T t, Function<? super X, Optional<L>> liftedExceptionMapper) {
        try {
            return Either.ofRight(apply(t));
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD Intentional
            return liftedExceptionMapper.apply((X) e)
                .map(Either::<L, R>ofLeft)
                .orElseThrow(() -> wrapChecked(e)); // NOPMD
        }
    }

    default <Y extends RuntimeException> R applyUnchecked(T t, ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        return applyUnchecked(t, exceptionMapper.applyPartial(t));
    }

    default <Y extends RuntimeException> R applyUnchecked(T t, Function<Throwable, ? extends Y> throwableMapper) {
        try {
            return apply(t);
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD Intentional
            throw throwableMapper.apply(e);
        }
    }

    default Function<T, R> uncheck() {
        return uFunction(this);
    }
}
