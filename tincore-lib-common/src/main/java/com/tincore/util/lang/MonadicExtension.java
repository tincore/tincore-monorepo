package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.Matcher.Accepting;
import com.tincore.util.lang.Matcher.Applying;
import com.tincore.util.lang.function.AutoCloseableFunction;
import com.tincore.util.lang.function.ThrowingConsumer;
import com.tincore.util.lang.function.ThrowingFunction;
import com.tincore.util.lang.function.TriFunction;
import com.tincore.util.lang.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.*;

import static com.tincore.util.lang.ResourceFunctor.of;
import static com.tincore.util.lang.SequenceExtension.*;
import static com.tincore.util.lang.function.FunctionExtension.combine;
import static com.tincore.util.lang.function.FunctionExtension.product;
import static com.tincore.util.lang.function.ThrowingFunction.uFunction;

public class MonadicExtension { // NOPMD No god

    public static <T> T also(T object, Consumer<? super T> action) {
        if (object != null) {
            action.accept(object);
        }
        return object;
    }

    public static <T> T also(T object, Consumer<? super T>... actions) {
        if (object != null) {
            streamEach(actions).forEach(a -> a.accept(object));
        }
        return object;
    }

    public static <T> T also(T object, Consumer<? super T> action, Runnable otherwiseAction) {
        if (object != null) {
            action.accept(object);
        } else {
            otherwiseAction.run();
        }
        return object;
    }

    public static <T, I extends Iterable<T>> I alsoEach(I iterable, Consumer<? super T> action) {
        return alsoEach(iterable, action, false);
    }

    public static <T, I extends Iterable<T>> I alsoEach(I iterable, Consumer<? super T> action, boolean parallel) {
        if (iterable != null) {
            streamEach(iterable, parallel).forEach(e -> also(e, action));
        }
        return iterable;
    }

    public static <T, C extends Collection<T>> C alsoEachIf(C collection, Predicate<? super T> predicate, Consumer<? super T> consumer, Consumer<? super T> otherwiseConsumer) {
        if (collection != null) {
            collection.forEach(t -> alsoIf(t, predicate, consumer, otherwiseConsumer));
        }
        return collection;
    }

    public static <T, C extends Collection<T>> C alsoEachIf(C collection, boolean value, Consumer<? super T> consumer, Consumer<? super T> otherwiseConsumer) {
        return alsoEachIf(collection, t -> value, consumer, otherwiseConsumer);
    }

    public static <T, C extends Collection<T>> C alsoEachIf(C collection, Predicate<? super T> predicate, Consumer<? super T> consumer) {
        if (collection != null) {
            collection.forEach(t -> alsoIf(t, predicate, consumer));
        }
        return collection;
    }

    public static <T, C extends Collection<T>> C alsoEachIf(C collection, boolean value, Consumer<? super T> consumer) {
        return alsoEachIf(collection, t -> value, consumer);
    }

    public static <T> T alsoIf(T object, Predicate<? super T> predicate, Consumer<? super T> action) {
        return alsoIf(object, predicate, action, o -> { // Do nothing
        });
    }

    public static <T> T alsoIf(T object, boolean value, Consumer<? super T> action) {
        return alsoIf(object, o -> value, action, o -> { // Do nothing
        });
    }

    public static <T> T alsoIf(T object, boolean value, Consumer<? super T> action, Consumer<? super T> otherwiseAction) {
        return alsoIf(object, o -> value, action, otherwiseAction);
    }

    public static <T> T alsoIf(T object, Predicate<? super T> predicate, Consumer<? super T> action, Consumer<? super T> otherwiseAction) {
        if (object != null) {
            if (predicate.test(object)) {
                action.accept(object);
            } else {
                otherwiseAction.accept(object);
            }
        }
        return object;
    }

    public static <T> T alsoIf(T object, Predicate<? super T> predicate1, Consumer<? super T> action1, Predicate<? super T> predicate2, Consumer<? super T> action2, Consumer<? super T> otherwiseAction) {
        if (object != null) {
            if (predicate1.test(object)) {
                action1.accept(object);
            } else if (predicate2.test(object)) {
                action2.accept(object);
            } else {
                otherwiseAction.accept(object);
            }
        }
        return object;
    }

    public static <T> T alsoIf(
        T object,
        Predicate<? super T> predicate1,
        Consumer<? super T> action1,
        Predicate<? super T> predicate2,
        Consumer<? super T> action2,
        Predicate<? super T> predicate3,
        Consumer<? super T> action3,
        Consumer<? super T> otherwiseAction) {
        if (object != null) {
            if (predicate1.test(object)) {
                action1.accept(object);
            } else if (predicate2.test(object)) {
                action2.accept(object);
            } else if (predicate3.test(object)) {
                action3.accept(object);
            } else {
                otherwiseAction.accept(object);
            }
        }
        return object;
    }

    public static <T> T alsoIf(
        T object,
        Predicate<? super T> predicate1,
        Consumer<? super T> action1,
        Predicate<? super T> predicate2,
        Consumer<? super T> action2,
        Predicate<? super T> predicate3,
        Consumer<? super T> action3,
        Predicate<? super T> predicate4,
        Consumer<? super T> action4,
        Consumer<? super T> otherwiseAction) {
        if (object != null) {
            if (predicate1.test(object)) {
                action1.accept(object);
            } else if (predicate2.test(object)) {
                action2.accept(object);
            } else if (predicate3.test(object)) {
                action3.accept(object);
            } else if (predicate4.test(object)) {
                action4.accept(object);
            } else {
                otherwiseAction.accept(object);
            }
        }
        return object;
    }

    public static <T> T alsoIf(
        T object,
        Accepting<T> matcher) {
        if (object == null) {
            return null;
        }
        matcher.accept(object);
        return object;
    }

    public static <T, U extends AutoCloseable, X extends Throwable, Y extends RuntimeException> T alsoResource(
        T object,
        AutoCloseableFunction<? super T, ? extends U, ? extends X> autoCloseableMapper,
        Consumer<? super U> action) throws Y { // NOPMD
        return alsoResource(object, autoCloseableMapper, ExceptionMapper.wrapChecked(), action);
    }

    @SuppressWarnings({"checkstyle:ParenPad"})
    public static <T, U extends AutoCloseable, X extends Throwable, Y extends RuntimeException> T alsoResource(
        T object,
        AutoCloseableFunction<? super T, ? extends U, ? extends X> autoCloseableMapper,
        BiFunction<? super T, ? super X, ? extends Y> autoCloseableExceptionMapper,
        Consumer<? super U> action) throws Y {   // NOPMD
        if (object == null) {
            return null;
        }

        return letResource(
            object,
            autoCloseableMapper,
            autoCloseableExceptionMapper,
            m -> {
                action.accept(m);
                return object;
            });
    }

    public static <T, X extends Throwable, Y extends Throwable> T alsoTry(
        T object,
        ThrowingConsumer<? super T, ? extends X> throwingConsumer) {
        return alsoTry(object, throwingConsumer, ExceptionMapper.wrapChecked());
    }

    @SuppressWarnings({"checkstyle:ParenPad"})
    public static <T, X extends Throwable, Y extends RuntimeException> T alsoTry( // NOPMD Complex
        T object,
        ThrowingConsumer<? super T, ? extends X> throwingConsumer,
        ExceptionMapper<? super T, ? extends Y> exceptionMapper) throws Y {  // NOPMD
        if (object == null) {
            return null;
        }

        ThrowingConsumer.uConsumer(throwingConsumer, exceptionMapper).accept(object);
        return object;
    }

    public static <T> T alsoUnless(T object, boolean value, Consumer<? super T> action) {
        return alsoIf(object, o -> !value, action, o -> { // Do nothing
        });
    }

    public static <T> T alsoUnless(T object, Predicate<? super T> predicate, Consumer<? super T> action) {
        return alsoIf(object, predicate, o -> { // Do nothing
        }, action);
    }

    public static <T> T alsoUnlessPresent(T object, Runnable action) {
        return also(object, (Consumer<? super T>) o -> { // Do nothing
        }, action);
    }

    public static <T, U> T alsoWhen(T object, Function<? super T, ? extends U> extractor, Predicate<? super U> predicate, Consumer<? super T> action) {
        if (object != null) {
            var extractedObject = extractor.apply(object);
            if (predicate.test(extractedObject)) {
                action.accept(object);
            }
        }
        return object;
    }

    public static <T, U> T alsoWith(T object, U associatedObject, BiConsumer<? super T, ? super U> action) {
        if (object != null && associatedObject != null) {
            action.accept(object, associatedObject);
        }
        return object;
    }

    @SuppressWarnings("unchecked")
    public static <T, R extends T> R asInstanceOf(T object, Class<R> type) {
        return (R) object;
    }

    private static boolean isEmptyOrNull(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    private static boolean isEmptyOrNull(Object[] array) { // NOPMD
        return array == null || array.length == 0;
    }

    public static <T, R> R let(T object, Function<? super T, ? extends R> mapper) {
        return object == null ? null : mapper.apply(object);
    }

    public static <T, U, R> R letComprehension(T object, Function<? super T, ? extends U> mapper1, BiFunction<? super T, ? super U, ? extends R> mapper2) {
        var u = let(object, mapper1);
        return u == null ? null : mapper2.apply(object, u);
    }

    public static <T, U, V, R> R letComprehension(T object, Function<? super T, ? extends U> mapper1, BiFunction<? super T, ? super U, ? extends V> mapper2, TriFunction<? super T, ? super U, ? super V, ? extends R> mapper3) {
        var u = let(object, mapper1);
        if (u == null) {
            return null;
        }
        var v = mapper2.apply(object, u);
        return v == null ? null : mapper3.apply(object, u, v);
    }

    @SuppressWarnings("unchecked")
    public static <T, R> List<R> letEach(Collection<T> collection, Function<? super T, ? extends R> mapper) {
        return collection == null ? null : (List<R>) collection.stream().map(mapper).toList(); // NOPMD on purpose null
    }

    @SuppressWarnings("unchecked")
    public static <K, V, R> List<R> letEachEntry(Collection<? extends Entry<? extends K, ? extends V>> collection, BiFunction<? super K, ? super V, ? extends R> mapper) {
        return collection == null ? null : (List<R>) collection.stream().map(e -> mapper.apply(e.getKey(), e.getValue())).toList(); // NOPMD on purpose null
    }

    @SuppressWarnings("unchecked")
    public static <K, V, R> List<R> letEachEntry(Map<? extends K, ? extends V> map, BiFunction<? super K, ? super V, ? extends R> mapper) {
        return map == null ? null : (List<R>) map.entrySet().stream().map(e -> mapper.apply(e.getKey(), e.getValue())).toList(); // NOPMD on purpose null
    }

    public static <K, V, R> List<? extends Pair<? extends K, ? extends R>> letEachEntryValue(Collection<? extends Entry<? extends K, ? extends V>> collection, Function<? super V, ? extends R> mapper) {
        return collection == null ? null : collection.stream().map(e -> Pair.of(e.getKey(), mapper.apply(e.getValue()))).toList(); // NOPMD on purpose null
    }

    public static <T, R> List<R> letEachIf(Collection<T> collection, Predicate<? super T> predicate, Function<? super T, ? extends R> mapper, Function<? super T, ? extends R> otherwiseMapper) {
        return collection == null ? null : collection.stream().map(t -> letIf(t, predicate, mapper, otherwiseMapper)).toList(); // NOPMD on purpose null
    }

    public static <T, R> List<R> letEachIf(Collection<T> collection, boolean value, Function<? super T, ? extends R> mapper, Function<? super T, ? extends R> otherwiseMapper) {
        return letEachIf(collection, o -> value, mapper, otherwiseMapper);
    }

    public static <T> List<T> letEachIf(Collection<T> collection, Predicate<? super T> predicate, Function<? super T, ? extends T> mapper) {
        return letEachIf(collection, predicate, mapper, Function.identity());
    }

    public static <T> List<T> letEachIf(Collection<T> collection, boolean value, Function<? super T, ? extends T> mapper) {
        return letEachIf(collection, o -> value, mapper, Function.identity());
    }

    public static <T> List<T> letEachPresent(Collection<Optional<T>> collection) {
        return collection == null ? null : mapPresent(collection.stream()).toList(); // NOPMD on purpose null
    }

    public static <T, R> List<R> letEachPresent(Collection<T> collection, Function<? super T, Optional<R>> mapper) {
        return collection == null ? null : mapPresent(collection.stream().map(mapper)).toList(); // NOPMD on purpose null
    }

    public static <T, L, R> List<Pair<L, R>> letEachToPair(Collection<T> collection, Function<? super T, L> keyMapper, Function<? super T, R> valueMapper) {
        return collection == null ? null : collection.stream().map(v -> letToPair(v, keyMapper, valueMapper)).toList(); // NOPMD on purpose null
    }

    public static <K, V, L, R> List<Pair<L, R>> letEachToPair(Map<K, V> map, Function<? super K, L> keyMapper, Function<? super V, R> valueMapper) {
        return map == null ? null : streamEachToPair(map, keyMapper, valueMapper).toList(); // NOPMD on purpose null
    }

    public static <K, V, R> R letEntry(Entry<K, V> entry, BiFunction<? super K, ? super V, ? extends R> mapper) {
        return entry == null ? null : mapper.apply(entry.getKey(), entry.getValue());
    }

    public static <K, V, L, R> Pair<L, R> letEntryToPair(Entry<K, V> entry, Function<? super K, L> keyMapper, Function<? super V, R> valueMapper) {
        return letEntry(entry, combine(keyMapper, valueMapper));
    }

    public static <T, R> R letFirst(List<T> list, Function<? super T, ? extends R> mapper) {
        return isEmptyOrNull(list) ? null : let(first(list), mapper); // NOPMD on purpose null
    }

    public static <T, R> R letFirst(T[] array, Function<? super T, ? extends R> mapper) {
        return isEmptyOrNull(array) ? null : let(first(array), mapper); // NOPMD on purpose null
    }

    public static <T> T letFirst(List<T> list) {
        return isEmptyOrNull(list) ? null : first(list); // NOPMD on purpose null
    }

    public static <T> T letFirst(T[] array) { // NOPMD no varargs
        return isEmptyOrNull(array) ? null : first(array); // NOPMD on purpose null
    }

    public static <T, R> R letIf(T object, Predicate<? super T> predicate, Function<? super T, ? extends R> mapper, Function<? super T, ? extends R> otherwiseMapper) {
        return object == null ? null : predicate.test(object) ? mapper.apply(object) : otherwiseMapper.apply(object);
    }

    public static <T, R> R letIf(
        T object,
        Predicate<? super T> predicate1,
        Function<? super T, ? extends R> mapper1,
        Predicate<? super T> predicate2,
        Function<? super T, ? extends R> mapper2,
        Function<? super T, ? extends R> otherwiseMapper) {
        if (object == null) {
            return null;
        }
        if (predicate1.test(object)) {
            return mapper1.apply(object);
        }
        if (predicate2.test(object)) {
            return mapper2.apply(object);
        }
        return otherwiseMapper.apply(object);
    }

    public static <T, R> R letIf(
        T object,
        Predicate<? super T> predicate1,
        Function<? super T, ? extends R> mapper1,
        Predicate<? super T> predicate2,
        Function<? super T, ? extends R> mapper2,
        Predicate<? super T> predicate3,
        Function<? super T, ? extends R> mapper3,
        Function<? super T, ? extends R> otherwiseMapper) {
        if (object == null) {
            return null;
        }
        if (predicate1.test(object)) {
            return mapper1.apply(object);
        }
        if (predicate2.test(object)) {
            return mapper2.apply(object);
        }
        if (predicate3.test(object)) {
            return mapper3.apply(object);
        }
        return otherwiseMapper.apply(object);
    }

    public static <T, R> R letIf(T object, Applying<T, R> matcher) {
        return object == null ? null : matcher.apply(object);
    }

    public static <T, R> R letIf(T object, boolean value, Function<? super T, ? extends R> mapper, Function<? super T, ? extends R> otherwiseMapper) {
        return letIf(object, o -> value, mapper, otherwiseMapper);
    }

    public static <T> T letIf(T object, Predicate<? super T> predicate, Function<? super T, ? extends T> mapper) {
        return letIf(object, predicate, mapper, Function.identity());
    }

    public static <T> T letIf(T object, boolean value, Function<? super T, ? extends T> mapper) {
        return letIf(object, o -> value, mapper, Function.identity());
    }

    public static <T, R> R letLast(List<T> list, Function<? super T, ? extends R> mapper) {
        if (isEmptyOrNull(list)) {
            return null; // NOPMD on purpose null
        }
        return let(last(list), mapper);
    }

    public static <T, R> R letLast(T[] array, Function<? super T, ? extends R> mapper) {
        return isEmptyOrNull(array) ? null : let(last(array), mapper); // NOPMD on purpose null
    }

    public static <T> T letLast(List<T> list) {
        return isEmptyOrNull(list) ? null : last(list); // NOPMD on purpose null
    }

    public static <T> T letLast(T[] array) { // NOPMD no varargs
        return isEmptyOrNull(array) ? null : last(array); // NOPMD on purpose null
    }

    public static <T, R> R letOr(T object, Function<? super T, ? extends R> mapping, R other) {
        return takeOr(let(object, mapping), other);
    }

    public static <T, R> R letOrGet(T object, Function<? super T, ? extends R> mapping, Supplier<R> other) {
        return takeOrGet(let(object, mapping), other);
    }

    public static <T, U extends AutoCloseable, R, X extends Throwable> R letResource(
        T object,
        AutoCloseableFunction<? super T, ? extends U, ? extends X> autoCloseableMapper,
        Function<? super U, ? extends R> action) {
        return letResource(object, autoCloseableMapper, ExceptionMapper.wrapChecked(), action);
    }

    @SuppressWarnings({"checkstyle:ParenPad"})
    public static <T, U extends AutoCloseable, R, X extends Throwable, Y extends RuntimeException> R letResource(
        T object,
        AutoCloseableFunction<? super T, ? extends U, ? extends X> autoCloseableMapper,
        BiFunction<? super T, ? super X, ? extends Y> autoCloseableExceptionMapper,
        Function<? super U, ? extends R> action) throws Y { // NOPMD
        return object == null ? null : of(autoCloseableMapper, autoCloseableExceptionMapper).map(action).apply(object);
    }

    public static <T, L, R> Pair<L, R> letToPair(T object, Function<? super T, L> keyMapper, Function<? super T, R> valueMapper) {
        return object == null ? null : product(keyMapper, valueMapper).apply(object); // NOPMD on purpose null
    }

    public static <T, U, X extends Throwable> U letTry(
        T object,
        ThrowingFunction<? super T, ? extends U, ? extends X> throwingMapper) {
        return letTry(object, throwingMapper, ExceptionMapper.wrapChecked());
    }

    @SuppressWarnings({"checkstyle:ParenPad"})
    public static <T, U, X extends Throwable, Y extends RuntimeException> U letTry( // NOPMD Complex
        T object,
        ThrowingFunction<? super T, ? extends U, ? extends X> throwingMapper,
        ExceptionMapper<? super T, ? extends Y> exceptionMapper) throws Y { // NOPMD
        if (object == null) {
            return null;
        }
        return uFunction(throwingMapper, exceptionMapper).apply(object);
    }

    public static <T> T letUnless(T object, Predicate<? super T> predicate, Function<? super T, ? extends T> mapper) {
        return letIf(object, predicate.negate(), mapper, Function.identity());
    }

    public static <T, R> R letUnless(T object, Predicate<? super T> predicate, Function<? super T, ? extends R> mapper, Function<? super T, ? extends R> otherwiseMapper) {
        return letIf(object, predicate.negate(), mapper, otherwiseMapper);
    }

    public static <T, U, R> R letWith(T object, U object2, BiFunction<? super T, ? super U, ? extends R> mapper) {
        return object == null ? null : mapper.apply(object, object2);
    }

    public static <T, U> T letWith(T object, U object2, Predicate<U> combinedPredicate, BiFunction<? super T, ? super U, T> biFunction) {
        return object == null ? null : combinedPredicate.test(object2) ? biFunction.apply(object, object2) : object;
    }

    public static <T, U, V> List<T> takeEachAlsoIn(Collection<T> collection, Collection<U> otherCollection, Function<T, V> collectionKeyExtractor, Function<U, V> otherCollectionKeyExtractor) {
        var set = toSet(otherCollection, otherCollectionKeyExtractor);
        return takeEachIf(collection, m -> set.contains(collectionKeyExtractor.apply(m)));
    }

    public static <T, U> List<T> takeEachAlsoIn(Collection<T> collection, Collection<T> otherCollection, Function<T, U> collectionKeyExtractor) {
        return takeEachAlsoIn(collection, otherCollection, collectionKeyExtractor, collectionKeyExtractor);
    }

    public static <T> List<T> takeEachAlsoIn(Collection<T> collection, Collection<T> otherCollection) {
        return takeEachAlsoIn(collection, otherCollection, Function.identity());
    }

    public static <K, V, E extends Entry<K, V>> List<E> takeEachEntryIf(Collection<E> collection, BiPredicate<? super K, ? super V> biPredicate) {
        return collection == null ? null : collection.stream().filter(e -> biPredicate.test(e.getKey(), e.getValue())).toList(); // NOPMD On purpose
    }

    public static <T> List<T> takeEachIf(Collection<T> collection, Predicate<? super T> predicate) {
        return streamEachIf(collection, predicate).toList(); // NOPMD On purpose
    }

    public static <K, V> Map<K, V> takeEachIfValue(Map<K, V> map, Predicate<V> predicate) {
        return toMap(streamEntries(map).filter(e -> predicate.test(e.getValue())));
    }

    public static <T> List<T> takeEachUnless(Collection<T> collection, Predicate<? super T> predicate) {
        return takeEachIf(collection, predicate.negate());
    }

    public static <T> List<T> takeEachUnlessIn(Collection<T> collection, Collection<T> otherCollection) {
        return takeEachUnlessIn(collection, otherCollection, Function.identity());
    }

    public static <T, U> List<T> takeEachUnlessIn(Collection<T> collection, Collection<T> otherCollection, Function<T, U> collectionKeyExtractor) {
        return takeEachUnlessIn(collection, otherCollection, collectionKeyExtractor, collectionKeyExtractor);
    }

    public static <T, U, V> List<T> takeEachUnlessIn(Collection<T> collection, Collection<U> otherCollection, Function<T, V> collectionKeyExtractor, Function<U, V> otherCollectionKeyExtractor) {
        return difference(collection, otherCollection, collectionKeyExtractor, otherCollectionKeyExtractor);
    }

    public static <K, V> Map<K, V> takeEachUnlessValue(Map<K, V> map, Predicate<V> predicate) {
        return takeEachIfValue(map, predicate.negate());
    }

    public static <T, U> List<T> takeEachWhen(Collection<T> collection, Function<? super T, ? extends U> extractor, Predicate<? super U> predicate) {
        return streamEach(collection)
            .filter(v -> predicate.test(extractor.apply(v)))
            .toList();
    }

    public static <T> T takeIf(T object, Predicate<? super T> predicate) {
        return takeIfOr(object, predicate, null);
    }

    public static <T> T takeIf(T object, boolean value) {
        return takeIfOr(object, o -> value, null);
    }

    public static <T> T takeIfOr(T object, Predicate<? super T> predicate, T other) {
        return object != null && predicate.test(object) ? object : other;
    }

    /**
     * Returns "it" if predicate is matched. Otherwise, returns operator mapped "it".
     *
     * @param object
     * @param predicate
     * @param other
     * @param <T>
     * @return
     */
    public static <T> T takeIfOrApply(T object, Predicate<? super T> predicate, UnaryOperator<T> other) {
        return object != null && predicate.test(object) ? object : other.apply(object);
    }

    /**
     * Returns "it" if predicate is matched. Otherwise, returns supplier evaluation.
     *
     * @param object
     * @param predicate
     * @param other
     * @param <T>
     * @return
     */
    public static <T> T takeIfOrGet(T object, Predicate<? super T> predicate, Supplier<T> other) {
        return object != null && predicate.test(object) ? object : other.get();
    }

    /**
     * Returns "it" if predicate is matched. Otherwise, throws exception.
     *
     * @param object
     * @param predicate
     * @param exceptionMapper
     * @param <T>
     * @param <X>
     * @return
     * @throws X
     */
    public static <T, X extends Throwable> T takeIfOrThrow(T object, Predicate<? super T> predicate, Function<? super T, X> exceptionMapper) throws X {
        if (object != null && predicate.test(object)) {
            return object;
        }
        throw exceptionMapper.apply(object);
    }

    /**
     * Returns "it" if value is true. Otherwise, throws exception.
     *
     * @param object
     * @param value
     * @param exceptionMapper
     * @param <T>
     * @param <X>
     * @return
     * @throws X
     */
    public static <T, X extends Throwable> T takeIfOrThrow(T object, boolean value, Function<? super T, X> exceptionMapper) throws X {
        return takeIfOrThrow(object, o -> value, exceptionMapper);
    }

    /**
     * Returns "it" or other value if null
     *
     * @param object
     * @param other
     * @param <T>
     * @return
     */
    public static <T> T takeOr(T object, T other) {
        return object != null ? object : other;
    }

    /**
     * Returns "it" or result of supplier evaluation if null
     *
     * @param object
     * @param other
     * @param <T>
     * @return
     */
    public static <T> T takeOrGet(T object, Supplier<T> other) {
        return object != null ? object : other.get();
    }

    /**
     * Returns "it" or throws exception if null
     *
     * @param object
     * @param exceptionSupplier
     * @param <T>
     * @param <X>
     * @return
     * @throws X
     */
    public static <T, X extends Throwable> T takeOrThrow(T object, Supplier<X> exceptionSupplier) throws X {
        if (object != null) {
            return object;
        }
        throw exceptionSupplier.get();
    }

    /**
     * Returns "it" if predicate is not matched. Otherwise, returns null.
     *
     * @param object
     * @param predicate
     * @param <T>
     * @return
     */
    public static <T> T takeUnless(T object, Predicate<? super T> predicate) {
        return takeUnlessOr(object, predicate, null);
    }

    /**
     * Returns "it" if predicate is not matched. Otherwise, returns other.
     *
     * @param object
     * @param predicate
     * @param other
     * @param <T>
     * @return
     */
    public static <T> T takeUnlessOr(T object, Predicate<? super T> predicate, T other) {
        return object != null && predicate.negate().test(object) ? object : other;
    }

    /**
     * Returns "it" if predicate is not matched. Otherwise, returns operator mapped "it".
     *
     * @param object
     * @param predicate
     * @param other
     * @param <T>
     * @return
     */
    public static <T> T takeUnlessOrApply(T object, Predicate<? super T> predicate, UnaryOperator<T> other) {
        return object != null && predicate.negate().test(object) ? object : other.apply(object);
    }

    /**
     * Returns "it" if predicate is not matched. Otherwise, returns supplier evaluation.
     *
     * @param object
     * @param predicate
     * @param other
     * @param <T>
     * @return
     */
    public static <T> T takeUnlessOrGet(T object, Predicate<? super T> predicate, Supplier<T> other) {
        return object != null && predicate.negate().test(object) ? object : other.get();
    }

    /**
     * Returns "it" if predicate is not matched. Otherwise, returns throws exception.
     *
     * @param object
     * @param predicate
     * @param exceptionMapper
     * @param <T>
     * @param <X>
     * @return
     * @throws X
     */
    public static <T, X extends Throwable> T takeUnlessOrThrow(T object, Predicate<? super T> predicate, Function<? super T, X> exceptionMapper) throws X {
        if (object != null && predicate.negate().test(object)) {
            return object;
        }
        throw exceptionMapper.apply(object);
    }

}
