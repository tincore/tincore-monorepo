package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.FunctionExtension;
import com.tincore.util.lang.function.ThrowingFunction;
import com.tincore.util.lang.function.ThrowingRunnable;
import com.tincore.util.lang.function.ThrowingSupplier;
import lombok.experimental.ExtensionMethod;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.*;
import java.util.stream.Stream;

import static com.tincore.util.lang.ExceptionMapper.isChecked;
import static java.util.Objects.requireNonNull;

@ExtensionMethod(FunctionExtension.class)
public abstract sealed class Either<L, R> implements Map.Entry<L, R> { // NOPMD No god

    public static final String EITHER_RUNNABLE_SUCCESS = "RUNNABLE_SUCCESS";

    public static <L, R> Either<L, R> of(Supplier<L> leftSupplier, Supplier<R> rightSupplier) { // NOPMD
        var rightValue = rightSupplier.get();
        return rightValue != null ? ofRight(rightValue) : ofLeft(leftSupplier.get());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <L, R> Either<L, R> of(Optional<? extends R> optional, L leftWhenNotPresent) { // NOPMD
        return optional
            .map(Either::<L, R>ofRight)
            .orElseGet(() -> ofLeft(leftWhenNotPresent));
    }

    public static <L, R> Either<L, R> ofLeft(L value) {
        return new Left<>(value);
    }

    /**
     * Instantiate reducing type inference declaration verbosity.
     * alternative to Either.&lt;typeA, typeB&gt;.left(obj)
     *
     * @param value
     * @param rightType
     * @param <L>
     * @param <R>
     * @return
     */
    public static <L, R> Either<L, R> ofLeft(L value, Class<R> rightType) {
        return ofLeft(value);
    }

    public static <L, R> Either<L, ? extends List<R>> ofMerge(Either<? extends L, ? extends R>... eithers) {
        return ofMerge(List.of(eithers));
    }

    @SuppressWarnings("unchecked")
    public static <L, R> Either<L, ? extends List<R>> ofMerge(List<Either<? extends L, ? extends R>> eithers) {
        return eithers.stream()
            .filter(Either::isLeft)
            .findFirst()
            .map(e -> Either.<L, List<R>>ofLeft(e.getLeft()))
            .orElseGet(() -> ofRight((List<R>) eithers.stream().filter(Either::isRight).map(Either::getRight).toList()));
    }

    public static <L, R> Either<L, R> ofRight(R value) {
        return new Right<>(value);
    }

    /**
     * Instantiate reducing type inference declaration verbosity
     * alternative to Either.&lt;typeA, typeB&gt;.right(obj)
     *
     * @param value
     * @param leftType
     * @param <L>
     * @param <R>
     * @return
     */
    public static <L, R> Either<L, R> ofRight(R value, Class<L> leftType) {
        return ofRight(value);
    }

    public static <T, R, X extends Throwable> Function<T, Either<X, R>> tryEither(ThrowingFunction<? super T, R, X> function) {
        return tryEither(function, false);
    }

    public static <T, R, X extends Throwable> Function<T, Either<X, R>> tryEither(
        ThrowingFunction<? super T, R, X> function,
        boolean catchAll) {

        return t -> function.applyEither(t, ExceptionMapper.<T, X>wrapNone().applyPartial(t).lift(x -> catchAll || isChecked(x)));
    }

    public static <T, L, R, X extends Throwable> Function<T, Either<L, R>> tryEither(
        ThrowingFunction<? super T, R, X> function,
        Function<? super X, ? extends L> exceptionMapper) {
        return tryEither(function, exceptionMapper, false);
    }

    public static <T, L, R, X extends Throwable> Function<T, Either<L, R>> tryEither(
        ThrowingFunction<? super T, R, X> function,
        Function<? super X, ? extends L> exceptionMapper,
        boolean catchAll) {
        return Either.<T, R, X>tryEither(function, catchAll).andThen(it -> it.mapLeft(exceptionMapper));
    }

    public static <T, L, R, X extends Throwable> Function<T, Either<L, R>> tryEither(
        ThrowingFunction<? super T, R, ? extends X> function,
        BiFunction<? super T, ? super X, L> exceptionMapper) {
        return tryEither(function, exceptionMapper, false);
    }

    public static <T, L, R, X extends Throwable> Function<T, Either<L, R>> tryEither(
        ThrowingFunction<? super T, R, ? extends X> function,
        BiFunction<? super T, ? super X, L> exceptionMapper,
        boolean catchAll) {

        return t -> function.applyEither(t, exceptionMapper.applyPartial(t).lift(x -> catchAll || isChecked((Throwable) x)));
    }

    public static <T, C extends AutoCloseable, V, X extends Throwable, Y extends RuntimeException> Function<T, Either<Y, V>> tryEither(ResourceFunctor<T, C, V, X, Y> resourceFunctor) {
        return resourceFunctor::applyEither;
    }

    public static <R, X extends Throwable> Supplier<Either<X, R>> tryEither(ThrowingSupplier<R, X> supplier) {
        return tryEither(supplier, false);
    }

    public static <R, X extends Throwable> Supplier<Either<X, R>> tryEither(
        ThrowingSupplier<R, X> supplier,
        boolean catchAll) {
        return supplier.getEitherSupplier(ExceptionMapper.<X, X>wrapNone().applyPartial(null).lift(x -> catchAll || isChecked(x)));
    }

    public static <L, R, X extends Throwable> Supplier<Either<L, R>> tryEither(
        ThrowingSupplier<R, X> supplier,
        Function<? super X, ? extends L> exceptionMapper) {
        return tryEither(supplier, exceptionMapper, false);
    }

    public static <L, R, X extends Throwable> Supplier<Either<L, R>> tryEither(
        ThrowingSupplier<R, X> supplier,
        Function<? super X, ? extends L> exceptionMapper,
        boolean catchAll) {
        return () -> tryEither(supplier, catchAll).get().mapLeft(exceptionMapper);
    }

    public static <R, X extends Throwable> Supplier<Either<X, R>> tryEither(
        ThrowingRunnable<? extends X> runnable,
        R success,
        boolean catchAll) {
        return tryEither(() -> {
            runnable.run();
            return success;
        }, catchAll);
    }

    /**
     * Returns value if right and predicate satisfied. Otherwise returns alternative right.
     *
     * @param rightPredicate
     * @param alternativeRightValue
     * @return
     */
    public Either<L, R> filterOrElse(Predicate<? super R> rightPredicate, R alternativeRightValue) {
        if (isLeft() || rightPredicate.test(getValue())) {
            return this;
        }
        return ofRight(alternativeRightValue);
    }

    public Either<L, R> filterOrElseGet(Predicate<? super R> rightPredicate, Supplier<R> rightValueSupplier) {
        if (isLeft() || rightPredicate.test(getRight())) {
            return this;
        }
        return ofRight(rightValueSupplier.get());
    }

    /**
     * Returns value if right and predicate satisfied. Otherwise, returns left.
     *
     * @param rightPredicate
     * @param leftValue
     * @return
     */
    public Either<L, R> filterOrElseLeft(Predicate<? super R> rightPredicate, L leftValue) {
        if (isLeft() || rightPredicate.test(getRight())) {
            return this;
        }
        return ofLeft(leftValue);
    }

    /**
     * Returns value if right and predicate satisfied. Otherwise, returns left.
     *
     * @param predicate
     * @param leftMapper
     * @return
     */
    public Either<L, R> filterOrElseLeft(Predicate<? super R> predicate, Function<? super R, L> leftMapper) {
        if (isLeft() || predicate.test(getRight())) {
            return this;
        }
        return ofLeft(leftMapper.apply(getRight()));
    }

    public abstract Optional<Either<L, R>> findIf(Predicate<? super R> rightPredicate);

    public abstract Optional<Either<L, R>> findIfLeft(Predicate<? super L> leftPredicate);

    public Optional<Either<L, R>> findIfNot(Predicate<? super R> rightPredicate) {
        return findIf(rightPredicate.negate());
    }

    public abstract Optional<Either<L, R>> findIfRight(Predicate<? super R> rightPredicate);

    public abstract <U> Either<L, U> flatMap(Function<? super R, ? extends Either<L, ? extends U>> mapper);

    public abstract <U> Either<L, U> flatMap(Function<? super R, ? extends Optional<? extends U>> mapper, L leftIfNotPresent);

    public abstract <T, U> Either<T, U> flatMap(Function<? super L, Either<T, U>> leftMapper, Function<? super R, Either<T, U>> rightMapper);

    public abstract <U> Either<U, R> flatMapLeft(Function<? super L, Either<U, R>> mapper);

    public abstract <U> Either<L, U> flatMapRight(Function<? super R, Either<L, U>> mapper);

    public abstract <T> T fold(Function<? super L, ? extends T> leftMapper, Function<? super R, ? extends T> rightMapper);

    public R get() {
        return getRight();
    }

    @Override
    public L getKey() {
        return getLeft();
    }

    public abstract L getLeft();

    public abstract Optional<L> getLeftOption();

    public abstract R getRight();

    public abstract Optional<R> getRightOption();

    /**
     * Entry interface implementation
     *
     * @return
     */
    @Override
    public R getValue() {
        return getRight();
    }

    public abstract void ifLeft(Consumer<L> leftAction);

    public void ifPresent(Consumer<R> rightAction) {
        ifRight(rightAction);
    }

    public abstract void ifRight(Consumer<R> rightAction);

    public abstract void ifRightOrElse(Consumer<R> rightAction, Consumer<L> leftAction);

    public abstract <X extends Throwable> void ifRightOrElseThrow(Consumer<R> rightAction, Function<L, ? extends X> exceptionSupplier) throws X;

    public abstract boolean isLeft();

    public abstract boolean isRight();

    public abstract <T, U> Either<T, U> map(Function<? super L, ? extends T> leftMapper, Function<? super R, ? extends U> rightMapper);

    public <U> Either<L, U> map(Function<? super R, ? extends U> mapper) {
        return mapRight(mapper);
    }

    public <U> Either<U, R> mapLeft(Function<? super L, ? extends U> mapper) {
        return map(mapper, Function.identity());
    }

    public <U> Either<L, U> mapRight(Function<? super R, ? extends U> mapper) {
        return map(Function.identity(), mapper);
    }

    public <L extends S, S> Either<S, R> narrowLeft(Class<S> type) {
        return (Either<S, R>) this;
    }

    public Optional<Either<L, R>> option() {
        return Optional.of(this);
    }

    public Either<? extends L, ? extends R> or(Supplier<? extends Either<? extends L, ? extends R>> supplier) { // NOPMD method name
        if (isRight()) {
            return this;
        }
        var alternative = supplier.get();
        return alternative.isRight() ? alternative : this;
    }

    @SuppressWarnings("unchecked")
    public final Either<L, R> orElse(Either<? extends L, ? extends R> other) {
        return isRight() ? this : (Either<L, R>) other;
    }

    public final R orElse(R other) {
        return isRight() ? this.getRight() : other;
    }

    @SuppressWarnings("unchecked")
    public final Either<L, R> orElseGet(Supplier<? extends Either<? extends L, ? extends R>> supplier) {
        return isRight() ? this : (Either<L, R>) supplier.get();
    }

    public final R orElseGetValue(Supplier<? extends R> supplier) {
        return isRight() ? this.getRight() : supplier.get();
    }

    public <X extends Throwable> R orElseThrow(Function<L, X> exceptionMapper) throws X {
        return orElseThrow(() -> exceptionMapper.apply(getLeft()));
    }

    public <X extends Throwable> R orElseThrow(Supplier<X> exceptionSupplier) throws X {
        return getRightOption().orElseThrow(exceptionSupplier);
    }

    public Either<L, R> peek(Consumer<R> rightAction) {
        return peekRight(rightAction);
    }

    public Either<L, R> peek(Consumer<L> leftAction, Consumer<R> rightAction) {
        ifRightOrElse(rightAction, leftAction);
        return this;
    }

    public Either<L, R> peekLeft(Consumer<L> leftAction) {
        ifLeft(leftAction);
        return this;
    }

    public Either<L, R> peekRight(Consumer<R> rightAction) {
        ifRight(rightAction);
        return this;
    }

    @SuppressWarnings("unchecked")
    public final Either<L, R> recover(Function<? super L, ? extends Either<? extends L, ? extends R>> mapper) {
        return isRight() ? this : (Either<L, R>) mapper.apply(getLeft());
    }

    @Override
    public final R setValue(final R value) {
        throw new UnsupportedOperationException("Either is immutable. Method present just to provide Entry interface implementation.");
    }

    public abstract Stream<R> stream();

    public abstract Either<R, L> swap();

    private static final class Left<L, R> extends Either<L, R> {

        private final L value;

        private Left(L value) {
            super();
            this.value = requireNonNull(value);
        }

        @Override
        public boolean equals(Object obj) {
            return this == obj || obj instanceof Left<?, ?> && value.equals(((Left<?, ?>) obj).value);
        }

        @Override
        public Optional<Either<L, R>> findIf(Predicate<? super R> rightPredicate) {
            return Optional.empty();
        }

        public Optional<Either<L, R>> findIfLeft(Predicate<? super L> leftPredicate) {
            return getLeftOption().filter(leftPredicate).isPresent() ? Optional.of(this) : Optional.empty();
        }

        public Optional<Either<L, R>> findIfRight(Predicate<? super R> rightPredicate) {
            return Optional.empty();
        }

        @Override
        public <T, U> Either<T, U> flatMap(Function<? super L, Either<T, U>> leftMapper, Function<? super R, Either<T, U>> rightMapper) {
            return leftMapper.apply(this.value);
        }

        @Override
        @SuppressWarnings("unchecked")
        public <U> Either<L, U> flatMap(Function<? super R, ? extends Either<L, ? extends U>> mapper) {
            return (Either<L, U>) this;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <U> Either<L, U> flatMap(Function<? super R, ? extends Optional<? extends U>> mapper, L leftIfNotPresent) {
            return (Either<L, U>) this;
        }

        @Override
        public <U> Either<U, R> flatMapLeft(Function<? super L, Either<U, R>> mapper) {
            return mapper.apply(this.value);
        }

        @Override
        public <U> Either<L, U> flatMapRight(Function<? super R, Either<L, U>> mapper) {
            return ofLeft(this.value);
        }

        @Override
        public <T> T fold(Function<? super L, ? extends T> leftMapper, Function<? super R, ? extends T> rightMapper) {
            return leftMapper.apply(getLeft());
        }

        @Override
        public L getLeft() {
            return this.value;
        }

        @Override
        public Optional<L> getLeftOption() {
            return Optional.of(this.value);
        }

        @Override
        public R getRight() {
            throw new NoSuchElementException("This is not Either.Right");
        }

        @Override
        public Optional<R> getRightOption() {
            return Optional.empty();
        }

        @Override
        public int hashCode() {
            return this.value.hashCode();
        }

        @Override
        public void ifLeft(Consumer<L> leftAction) {
            leftAction.accept(this.value);
        }

        @Override
        public void ifRight(Consumer<R> rightAction) {
            // Do nothing
        }

        @Override
        public void ifRightOrElse(Consumer<R> rightAction, Consumer<L> leftAction) {
            ifLeft(leftAction);
        }

        @Override
        public <X extends Throwable> void ifRightOrElseThrow(Consumer<R> rightAction, Function<L, ? extends X> exceptionSupplier) throws X {
            throw exceptionSupplier.apply(value);
        }

        @Override
        public boolean isLeft() {
            return true;
        }

        @Override
        public boolean isRight() {
            return false;
        }

        @Override
        public <T, U> Either<T, U> map(Function<? super L, ? extends T> leftMapper, Function<? super R, ? extends U> rightMapper) {
            return ofLeft(leftMapper.apply(this.value));
        }

        @Override
        public Stream<R> stream() {
            return Stream.empty();
        }

        @Override
        public Either<R, L> swap() {
            return new Right<>(this.value);
        }

        @Override
        public String toString() {
            return "Left(" + value + ")";
        }
    }

    private static final class Right<L, R> extends Either<L, R> {

        private final R value;

        private Right(R value) {
            super();
            this.value = requireNonNull(value);
        }

        @Override
        public boolean equals(Object obj) {
            return this == obj || obj instanceof Right<?, ?> && value.equals(((Right<?, ?>) obj).value);
        }

        @Override
        public Optional<Either<L, R>> findIf(Predicate<? super R> rightPredicate) {
            return findIfRight(rightPredicate);
        }

        public Optional<Either<L, R>> findIfLeft(Predicate<? super L> leftPredicate) {
            return Optional.empty();
        }

        public Optional<Either<L, R>> findIfRight(Predicate<? super R> rightPredicate) {
            return getRightOption().filter(rightPredicate).isPresent() ? Optional.of(this) : Optional.empty();
        }

        @Override
        public <T, U> Either<T, U> flatMap(Function<? super L, Either<T, U>> leftMapper, Function<? super R, Either<T, U>> rightMapper) {
            return rightMapper.apply(this.value);
        }

        @Override
        @SuppressWarnings("unchecked")
        public <U> Either<L, U> flatMap(Function<? super R, ? extends Either<L, ? extends U>> mapper) {
            return (Either<L, U>) mapper.apply(getRight());
        }

        @Override
        public <U> Either<L, U> flatMap(Function<? super R, ? extends Optional<? extends U>> mapper, L leftIfNotPresent) {
            return of(mapper.apply(getRight()), leftIfNotPresent);
        }

        @Override
        public <U> Either<U, R> flatMapLeft(Function<? super L, Either<U, R>> mapper) {
            return ofRight(value);
        }

        @Override
        public <U> Either<L, U> flatMapRight(Function<? super R, Either<L, U>> mapper) {
            return mapper.apply(value);
        }

        @Override
        public <T> T fold(Function<? super L, ? extends T> leftMapper, Function<? super R, ? extends T> rightMapper) {
            return rightMapper.apply(getRight());
        }

        @Override
        public L getLeft() {
            throw new NoSuchElementException("This is not Either.Left");
        }

        @Override
        public Optional<L> getLeftOption() {
            return Optional.empty();
        }

        @Override
        public R getRight() {
            return this.value;
        }

        @Override
        public Optional<R> getRightOption() {
            return Optional.of(value);
        }

        @Override
        public int hashCode() {
            return this.value.hashCode();
        }

        @Override
        public void ifLeft(Consumer<L> leftAction) {
            // Do nothing
        }

        @Override
        public void ifRight(Consumer<R> rightAction) {
            rightAction.accept(this.value);
        }

        @Override
        public void ifRightOrElse(Consumer<R> rightAction, Consumer<L> leftAction) {
            ifRight(rightAction);
        }

        @Override
        public <X extends Throwable> void ifRightOrElseThrow(Consumer<R> rightAction, Function<L, ? extends X> exceptionSupplier) {
            ifRight(rightAction);
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        @Override
        public boolean isRight() {
            return true;
        }

        @Override
        public <T, U> Either<T, U> map(Function<? super L, ? extends T> leftMapper, Function<? super R, ? extends U> rightMapper) {
            return ofRight(rightMapper.apply(this.value));
        }

        @Override
        public Stream<R> stream() {
            return Stream.of(this.value);
        }

        @Override
        public Either<R, L> swap() {
            return new Left<>(this.value);
        }

        @Override
        public String toString() {
            return "Right(" + value + ")";
        }
    }
}
