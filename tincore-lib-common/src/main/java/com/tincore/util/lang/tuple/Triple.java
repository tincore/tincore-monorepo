package com.tincore.util.lang.tuple;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.TriConsumer;
import com.tincore.util.lang.function.TriFunction;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Triple<L, M, R> extends Tuple<L, Pair<M, R>, Triple<L, M, R>> {

    int ARITY = 3;

    static <L, M, R> Triple<L, M, R> of(final L left, final M middle, final R right) { // NOPMD
        return new Triple.Immutable<>(left, middle, right);
    }

    static <L, M, R> Triple<L, M, R> of(Map.Entry<L, M> pair, R right) { // NOPMD short method name
        return of(pair.getKey(), pair.getValue(), right);
    }

    static <T> Triple<T, T, T> ofIterable(Iterable<T> iterable) {
        return ofIterable(iterable, false);
    }

    static <T> Triple<T, T, T> ofIterable(Iterable<T> iterable, boolean autoFill) {
        T v0 = null;
        T v1 = null;
        T v2 = null;

        Iterator<T> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            v0 = iterator.next();
            if (iterator.hasNext()) {
                v1 = iterator.next();
                if (iterator.hasNext()) {
                    v2 = iterator.next();
                } else {
                    iterator = null;
                }
            } else {
                iterator = null;
            }
        } else {
            iterator = null;
        }

        if (!autoFill && (iterator == null || iterator.hasNext())) {
            throw new IllegalArgumentException("Invalid iterator length. Expected length=" + ARITY);
        }

        return of(v0, v1, v2);
    }

    static <L, M, R> Triple<L, M, R> ofMiddle(Map.Entry<L, R> pair, M middle) { // NOPMD short method name
        return of(pair.getKey(), middle, pair.getValue());
    }

    static <L, M, R> Triple.Mutable<L, M, R> ofMutable(final L left, final M middle, final R right) {
        return new Triple.Mutable<>(left, middle, right);
    }

    default void accept(TriConsumer<L, M, R> consumer) {
        consumer.accept(getLeft(), getMiddle(), getRight());
    }

    default <U> U apply(TriFunction<L, M, R, U> mapper) {
        return mapper.apply(getLeft(), getMiddle(), getRight());
    }

    @Override
    default int arity() {
        return ARITY;
    }

    @Override
    default int compareTo(final Triple<L, M, R> other) {
        return new CompareToBuilder().append(getLeft(), other.getLeft())
            .append(getMiddle(), other.getMiddle())
            .append(getRight(), other.getRight())
            .toComparison();
    }

    default <U, V, W> Triple<U, V, W> flatMap(TriFunction<L, M, R, Triple<U, V, W>> mapper) {
        return mapper.apply(getLeft(), getMiddle(), getRight());
    }

    @Override
    default L getHead() {
        return getLeft();
    }

    L getLeft();

    M getMiddle();

    R getRight();

    @Override
    default Pair<M, R> getTail() {
        return Pair.of(getMiddle(), getRight());
    }

    default <U, V, W> Triple<U, V, W> map(Function<L, U> mapperL, Function<M, V> mapperM, Function<R, W> mapperR) {
        return of(mapperL.apply(getLeft()), mapperM.apply(getMiddle()), mapperR.apply(getRight()));
    }

    default <U> Triple<U, M, R> mapLeft(Function<L, U> mapper) {
        return of(mapper.apply(getLeft()), getMiddle(), getRight());
    }

    default <U> Triple<L, U, R> mapMiddle(Function<M, U> mapper) {
        return of(getLeft(), mapper.apply(getMiddle()), getRight());
    }

    default <U> Triple<L, M, U> mapRight(Function<R, U> mapper) {
        return of(getLeft(), getMiddle(), mapper.apply(getRight()));
    }

    @Override
    default Stream<Object> stream() {
        return Stream.of(getLeft(), getMiddle(), getRight());
    }

    @Override
    default Triple<L, M, R> withHead(L value) {
        return withLeft(value);
    }

    Triple<L, M, R> withLeft(L value);

    Triple<L, M, R> withMiddle(M value);

    Triple<L, M, R> withRight(R value);

    abstract class Abstract<L, M, R> implements Triple<L, M, R>, Serializable {

        @Override
        public boolean equals(final Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof Triple<?, ?, ?> other) {
                return Objects.equals(getLeft(), other.getLeft()) && Objects.equals(getMiddle(), other.getMiddle())
                    && Objects.equals(getRight(), other.getRight());
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(getLeft()) ^ Objects.hashCode(getMiddle()) ^ Objects.hashCode(getRight());
        }

        @Override
        public String toString() {
            return "(%s,%s,%s)".formatted(getLeft(), getMiddle(), getRight());
        }

        public String toString(final String format) {
            return String.format(format, getLeft(), getMiddle(), getRight());
        }
    }

    @Getter
    @FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    @With
    @Builder(toBuilder = true)
    final class Immutable<L, M, R> extends Triple.Abstract<L, M, R> {
        private final L left;
        private final M middle;
        private final R right;

        @Override
        public Immutable<L, M, R> withTail(Pair<M, R> pair) {
            return new Immutable<>(this.left, pair.getLeft(), pair.getRight());
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @With
    @Builder(toBuilder = true)
    class Mutable<L, M, R> extends Triple.Abstract<L, M, R> {
        private L left;
        private M middle;
        private R right;

        @Override
        public Mutable<L, M, R> withTail(Pair<M, R> pair) {
            return new Mutable<>(this.left, pair.getLeft(), pair.getRight());
        }
    }
}
