package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.FunctionExtension;
import lombok.experimental.ExtensionMethod;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.function.BiFunction;
import java.util.function.Function;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ExceptionMapper<T, Y> extends BiFunction<T, Throwable, Y> {

    ExceptionMapper<Object, RuntimeException> DEFAULT = (o, throwable) -> {
        if (throwable instanceof Error e) {
            throw e;
            // return e;
        }
        if (throwable instanceof RuntimeException r) {
            return r;
        }
        if (throwable instanceof IOException i) {
            throw new UncheckedIOException(i);
        }
        return new UndeclaredThrowableException(throwable);
    };

    static Function<? extends Throwable, Exception> asException() {
        return x -> (Exception) x;
    }

    static boolean isChecked(Throwable throwable) {
        return throwable instanceof Exception && !(throwable instanceof RuntimeException);
    }

    @SuppressWarnings("unchecked")
    static <T, X extends Throwable, Y extends RuntimeException> ExceptionMapper<T, Y> wrapAll(Function<? super X, ? extends Y> exceptionMapper) {
        return (t, x) -> exceptionMapper.apply((X) x);
    }

    @SuppressWarnings("unchecked")
    static <T, X extends Throwable, Y extends RuntimeException> ExceptionMapper<T, Y> wrapAll(BiFunction<? super T, ? super X, ? extends Y> exceptionMapper) {
        return (t, x) -> exceptionMapper.apply(t, (X) x);
    }

    static <T> ExceptionMapper<T, ? extends RuntimeException> wrapChecked() {
        return wrapChecked(e -> new RuntimeException(e));
    }

    @SuppressWarnings("unchecked")
    static <T, X extends Throwable, Y extends RuntimeException> ExceptionMapper<T, ? extends RuntimeException> wrapChecked(Function<? super X, ? extends Y> exceptionMapper) {
        return (t, x) -> x instanceof RuntimeException r ? r : exceptionMapper.apply((X) x);
    }

    @SuppressWarnings("unchecked")
    static <T, X extends Throwable, Y extends RuntimeException> ExceptionMapper<T, ? extends RuntimeException> wrapChecked(BiFunction<? super T, ? super X, ? extends Y> exceptionMapper) {
        return (t, x) -> x instanceof RuntimeException r ? r : exceptionMapper.apply(t, (X) x);
    }

    static RuntimeException wrapChecked(Throwable throwable) {
        return throwable instanceof RuntimeException r ? r : new RuntimeException("Unmapped checked exception", throwable);
    }

    static <T, X extends Throwable> BiFunction<T, X, X> wrapNone() {
        return (t, e) -> e;
    }

}
