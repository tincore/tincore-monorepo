package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import lombok.experimental.ExtensionMethod;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ThrowingBiConsumer<T, U, X extends Throwable> {
    static <T, U, X extends Throwable> BiConsumer<T, U> uBiConsumer(
        ThrowingBiConsumer<? super T, ? super U, ? extends X> consumer) {
        return uBiConsumer(consumer, ExceptionMapper.wrapChecked());
    }

    static <T, U, X extends Throwable, Y extends RuntimeException> BiConsumer<T, U> uBiConsumer(
        ThrowingBiConsumer<? super T, ? super U, ? extends X> consumer,
        Function<? super X, ? extends Y> exceptionMapper) {
        return uBiConsumer(consumer, ExceptionMapper.wrapChecked(exceptionMapper));
    }

    static <T, U, X extends Throwable, Y extends RuntimeException> BiConsumer<T, U> uBiConsumer(
        ThrowingBiConsumer<? super T, ? super U, ? extends X> consumer,
        ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        return (t, u) -> consumer.acceptUnchecked(t, u, exceptionMapper.applyPartial(t));
    }

    void accept(T t, U u) throws X;

    default <Y extends RuntimeException> void acceptUnchecked(T t, U u, Function<Throwable, ? extends Y> throwableMapper) {
        try {
            accept(t, u);
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD
            throw throwableMapper.apply(e);
        }
    }

    default ThrowingBiConsumer<T, U, X> andThen(final ThrowingBiConsumer<? super T, ? super U, X> after) {
        Objects.requireNonNull(after);
        return (t, u) -> {
            accept(t, u);
            after.accept(t, u);
        };
    }
}
