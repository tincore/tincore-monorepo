package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface QuadriFunction<T, U, V, W, R> {

    default <X> QuadriFunction<T, U, V, W, X> andThen(final Function<? super R, ? extends X> after) {
        Objects.requireNonNull(after);
        return (final T t, final U u, final V v, final W w) -> after.apply(apply(t, u, v, w));
    }

    R apply(T t, U u, V v, W w);

    default <X, Y, Z, A> QuadriFunction<X, Y, Z, A, R> compose(
        Function<? super X, ? extends T> beforeLeft,
        Function<? super Y, ? extends U> beforeMiddleLeft,
        Function<? super Z, ? extends V> beforeMiddleRight,
        Function<? super A, ? extends W> beforeRight) {
        Objects.requireNonNull(beforeLeft);
        Objects.requireNonNull(beforeMiddleLeft);
        Objects.requireNonNull(beforeMiddleRight);
        Objects.requireNonNull(beforeRight);
        return (X x, Y y, Z z, A a) -> apply(beforeLeft.apply(x), beforeMiddleLeft.apply(y), beforeMiddleRight.apply(z), beforeRight.apply(a));
    }

    default <X> QuadriFunction<X, U, V, W, R> composeLeft(Function<? super X, ? extends T> before) {
        Objects.requireNonNull(before);
        return (X x, U u, V v, W w) -> apply(before.apply(x), u, v, w);
    }

    default <X> QuadriFunction<T, X, V, W, R> composeMiddleLeft(Function<? super X, ? extends U> before) {
        Objects.requireNonNull(before);
        return (T t, X x, V v, W w) -> apply(t, before.apply(x), v, w);
    }

    default <X> QuadriFunction<T, U, X, W, R> composeMiddleRight(Function<? super X, ? extends V> before) {
        Objects.requireNonNull(before);
        return (T t, U u, X x, W w) -> apply(t, u, before.apply(x), w);
    }

    default <X> QuadriFunction<T, U, V, X, R> composeRight(Function<? super X, ? extends W> before) {
        Objects.requireNonNull(before);
        return (T t, U u, V v, X x) -> apply(t, u, v, before.apply(x));
    }
}
