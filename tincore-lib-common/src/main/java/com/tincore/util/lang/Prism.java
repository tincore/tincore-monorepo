package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

public final class Prism<T, P> implements Lens<T, Optional<P>> {
    private final Function<T, P> innerGetter;
    private final BiFunction<T, P, T> innerSetter;
    private final Function<T, Optional<P>> getter;
    private final BiFunction<T, Optional<P>, T> setter;

    public Prism(Function<T, P> getter, BiFunction<T, P, T> setter) {
        this.innerGetter = getter;
        this.innerSetter = setter;

        this.getter = t -> Optional.ofNullable(t).map(innerGetter);
        this.setter = (t, p) -> {
            if (t == null) {
                return null;
            }
            return isValueEquals(t, p) ? t : innerSetter.apply(t, p.orElse(null));
        };
    }

    public <V> Prism<T, V> andThenValue(Lens<P, V> lens) {
        return new Prism<>(t -> {
            if (t == null) {
                return null;
            }
            P p = innerGetter.apply(t);
            if (p == null) {
                return null;
            }
            return lens.getValue(p);
        }, (t, v) -> {
            if (t == null) {
                return null;
            }
            P p = innerGetter.apply(t);
            if (p == null) {
                return t;
            }
            return innerSetter.apply(t, lens.setValue(p, v));
        });
    }

    public <Q> Prism<T, Q> andThenValue(Prism<P, Q> prism) {
        return new Prism<>(t -> {
            if (t == null) {
                return null;
            }
            P p = innerGetter.apply(t);
            if (p == null) {
                return null;
            }
            return prism.getValue(p).orElse(null);
        }, (t, v) -> {
            if (t == null) {
                return null;
            }
            P p = innerGetter.apply(t);
            if (p == null) {
                return t;
            }
            return innerSetter.apply(t, prism.setValue(p, Optional.ofNullable(v)));
        });
    }

    @Override
    public Function<T, Optional<P>> getter() {
        return getter;
    }

    public T setValueNullable(T target, P value) { // NOPMD set linguistics
        return setValue(target, Optional.ofNullable(value));
    }

    @Override
    public BiFunction<T, Optional<P>, T> setter() {
        return setter;
    }
}
