package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.AutoCloseableFunction;
import com.tincore.util.lang.function.AutoCloseableSupplier;
import com.tincore.util.lang.function.ThrowingFunction;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

public final class ResourceFunctor<T, C extends AutoCloseable, R, X extends Throwable, Y extends Throwable> implements ThrowingFunction<T, R, Y> {

    private final AutoCloseableFunction<? super T, ? extends C, ? extends X> autoCloseableMapper;
    private final BiFunction<? super T, ? super X, ? extends Y> autoCloseableExceptionMapper;
    private final Function<? super C, ? extends R> action;

    private ResourceFunctor(AutoCloseableFunction<? super T, ? extends C, ? extends X> autoCloseableMapper, BiFunction<? super T, ? super X, ? extends Y> autoCloseableExceptionMapper, Function<? super C, ? extends R> action) {
        this.autoCloseableMapper = autoCloseableMapper;
        this.autoCloseableExceptionMapper = autoCloseableExceptionMapper;

        this.action = action;
    }

    public static <T, C extends AutoCloseable, X extends Throwable> ResourceFunctor<T, C, C, X, X> of(AutoCloseableFunction<? super T, ? extends C, ? extends X> autoCloseableMapper) { // NOPMD Short name
        return of(autoCloseableMapper, (n, e) -> e);
    }

    @SuppressWarnings({"checkstyle:ParenPad"})
    public static <T, C extends AutoCloseable, X extends Throwable, Y extends Throwable> ResourceFunctor<T, C, C, X, Y> of( // NOPMD
        AutoCloseableFunction<? super T, ? extends C, ? extends X> autoCloseableMapper,
        BiFunction<? super T, ? super X, ? extends Y> autoCloseableExceptionMapper) {
        return new ResourceFunctor<>(autoCloseableMapper, autoCloseableExceptionMapper, Function.identity());
    }

    public static <C extends AutoCloseable, X extends Throwable> ResourceFunctor<Void, C, C, X, X> ofSupplier(
        AutoCloseableSupplier<? extends C, ? extends X> autoCloseableSupplier) {
        return ofSupplier(autoCloseableSupplier, e -> e);
    }

    public static <C extends AutoCloseable, X extends Throwable, Y extends Throwable> ResourceFunctor<Void, C, C, X, Y> ofSupplier(
        AutoCloseableSupplier<? extends C, ? extends X> autoCloseableSupplier,
        Function<? super X, ? extends Y> autoCloseableExceptionMapper) {
        return of(n -> autoCloseableSupplier.get(), (n, x) -> autoCloseableExceptionMapper.apply(x));
    }

    public R get() throws Y { // NOPMD
        return apply(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public R apply(T t) throws Y { // NOPMD
        try (var closeable = autoCloseableMapper.apply(t)) {
            return action.apply(closeable);
        } catch (ExceptionXWrapper e) { // NOPMD Intentional
            throw autoCloseableExceptionMapper.apply(t, (X) e.getCause()); // NOPMD Can't be anything else
        } catch (ExceptionYWrapper e) { // NOPMD Intentional
            throw (Y) e.getCause(); // NOPMD
        } catch (Error | RuntimeException e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD Intentional
            throw autoCloseableExceptionMapper.apply(t, (X) e); // Can't be anything else. No case of UndeclaredThrowableException
        }
    }

    @SuppressWarnings("unchecked")
    public Either<Y, R> applyEither(T t) { // NOPMD
        try (var closeable = autoCloseableMapper.apply(t)) {
            return Either.ofRight(action.apply(closeable));
        } catch (ExceptionXWrapper e) { // NOPMD Intentional
            return Either.ofLeft(autoCloseableExceptionMapper.apply(t, (X) e.getCause()));
        } catch (ExceptionYWrapper e) { // NOPMD Intentional
            return Either.ofLeft((Y) e.getCause());
        } catch (Error | RuntimeException e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD Intentional
            return Either.ofLeft(autoCloseableExceptionMapper.apply(t, (X) e));
        }
    }

    public <S> ResourceFunctor<T, C, S, X, Y> map(Function<? super R, ? extends S> action) {
        Objects.requireNonNull(action);
        return new ResourceFunctor<>(autoCloseableMapper, autoCloseableExceptionMapper, this.action.andThen(action));
    }

    public <Z extends Throwable> ResourceFunctor<T, C, R, X, Z> mapException(Function<? super Y, ? extends Z> exceptionMapper) {
        Objects.requireNonNull(exceptionMapper);
        return new ResourceFunctor<>(autoCloseableMapper, this.autoCloseableExceptionMapper.andThen(exceptionMapper), this.action);
    }

    public <S, Z extends Throwable> ResourceFunctor<T, C, S, X, Y> mapTry(ThrowingFunction<? super R, ? extends S, ? extends Z> action, Function<? super Z, Y> exceptionMapper) {
        Objects.requireNonNull(action);
        return new ResourceFunctor<>(
            autoCloseableMapper,
            autoCloseableExceptionMapper,
            this.action.andThen(ThrowingFunction.uFunction(action, x -> new ExceptionYWrapper(exceptionMapper.apply(x)))));
    }

    public <S> ResourceFunctor<T, C, S, X, Y> mapTry(ThrowingFunction<? super R, ? extends S, X> action) {
        Objects.requireNonNull(action);
        return new ResourceFunctor<>(
            autoCloseableMapper,
            autoCloseableExceptionMapper,
            this.action.andThen(ThrowingFunction.uFunction(action, (c, x) -> x instanceof RuntimeException r ? r : new ExceptionXWrapper(x))));
    }

    private static final class ExceptionXWrapper extends RuntimeException {

        private ExceptionXWrapper(Throwable throwable) {
            super(throwable);
        }
    }

    private static final class ExceptionYWrapper extends RuntimeException {

        private ExceptionYWrapper(Throwable throwable) {
            super(throwable);
        }
    }

}
