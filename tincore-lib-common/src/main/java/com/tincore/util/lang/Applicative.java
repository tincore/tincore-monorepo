package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.tuple.Pair;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public sealed interface Applicative<T> {

    @SuppressWarnings("rawtypes")
    ValueOne EMPTY = new ValueOne<>(Collections.emptySet());

    @SuppressWarnings("unchecked")
    static <T> Value<T> empty() {
        return EMPTY;
    }

    @SuppressWarnings({"OptionalUsedAsFieldOrParameterType", "unchecked"})
    static <T> Value<T> of(Optional<T> optional) { // NOPMD
        return optional.map(Applicative::of).orElse((ValueOne<T>) EMPTY);
    }

    static <T> Value<T> of(T value) { // NOPMD
        Objects.requireNonNull(value);
        return new ValueOne<>(value);
    }

    @SafeVarargs
    static <T> Value<T> ofEach(T... elements) {
        return new ValueOne<>(Arrays.asList(elements));
    }

    static <T> Value<T> ofEach(Collection<T> collection) {
        return new ValueOne<>(collection);
    }

    @SuppressWarnings({"unchecked"})
    static <T> Value<T> ofEach(Stream<? extends T> stream) {
        return ofEach((List<T>) stream.toList());
    }

    static <T, R> ApplyOne<T, R> ofFunction(Function<? super T, ? extends R> value) { // NOPMD
        return new ApplyOne<>(value);
    }

    @SafeVarargs
    static <T, R> ApplyAll<T, R> ofFunctions(Function<? super T, ? extends R>... functions) {
        Stream<Function<? super T, ? extends R>> stream = Stream.of(functions);
        return ofFunctions(stream);
    }

    static <T, R> ApplyAll<T, R> ofFunctions(Collection<? extends Function<? super T, ? extends R>> collection) {
        return ofFunctions(collection.stream());
    }

    static <T, R> ApplyAll<T, R> ofFunctions(Stream<? extends Function<? super T, ? extends R>> stream) {
        return new ApplyAll<>(stream.toList());
    }

    static <T> ValueOne<T> ofNullable(T value) { // NOPMD
        return new ValueOne<>(value);
    }

    boolean isEmpty();

    Stream<T> stream();

    default <R> ValuePair<T, R> zip(Applicative<R> applicative) {
        return new ValuePair<>(this, applicative);
    }

    /**
     * Applicatives that contain one or multiple functions
     *
     * @param <T>
     * @param <R>
     */
    sealed interface Apply<T, R> extends Applicative<Function<? super T, ? extends R>> {
        default <V> ApplyAll<T, V> andThenAp(Applicative<? extends Function<? super R, ? extends V>> afterApplicative) {
            return ofFunctions(this.stream().flatMap(fa -> afterApplicative.stream().map(fa::andThen)));
        }

        @SuppressWarnings("unchecked")
        default <R> Value<R> ap(Applicative<T> applicative) { // NOPMD
            return ofEach(stream().flatMap(v -> applicative.stream().flatMap(t -> Stream.of(v.apply(t)))).filter(Objects::nonNull).map(r -> (R) r));
        }

        default <V> ApplyAll<T, V> map(Function<? super R, ? extends V> mapper) {
            return andThenAp(ofFunction(mapper));
        }
    }

    /**
     * Applicatives that contain just one or multiple values
     *
     * @param <T>
     */
    sealed interface Value<T> extends Applicative<T> {

        @SuppressWarnings("unchecked")
        default <R> Value<R> ap(Applicative<? extends Function<? super T, ? extends R>> functionApplicative) { // NOPMD
            var values = functionApplicative.stream().flatMap(m -> stream().map(m)).toList();
            if (values.isEmpty()) {
                return empty();
            }
            if (values.size() == 1) { // NOPMD
                return of(values.get(0));
            }
            return (Value<R>) ofEach(values); // NOPMD Necessary

        }

        default <R> Value<R> ap(Collection<? extends Applicative<? extends Function<? super T, ? extends R>>> functionApplicatives) { // NOPMD
            return ofEach(stream().flatMap(v -> functionApplicatives.stream().flatMap(fa -> fa.stream().map(f -> f.apply(v)))).filter(Objects::nonNull));
        }

        default <R> Value<R> apReverse(Collection<? extends Applicative<? extends Function<? super T, ? extends R>>> functionApplicatives) { // NOPMD
            return ofEach(functionApplicatives.stream().flatMap(fa -> fa.stream().flatMap(f -> stream().map(f))).filter(Objects::nonNull));
        }

        default <R> Value<R> map(Function<? super T, ? extends R> mapper) {
            return ap(ofFunction(mapper));
        }
    }

    /**
     * Single or multiple value containing applicative.
     *
     * @param <T>
     */
    final class ValueOne<T> implements Value<T> {

        private final Collection<T> collection;

        private ValueOne(T value) {
            this.collection = value != null ? Collections.singleton(value) : Collections.emptySet();
        }

        private ValueOne(Collection<T> collection) {
            Objects.requireNonNull(collection);
            this.collection = collection;
        }

        @Override
        public boolean isEmpty() {
            return collection.isEmpty();
        }

        @Override
        public Stream<T> stream() {
            return collection.stream();
        }
    }

    /**
     * Pair tuple containing applicative
     *
     * @param <T>
     * @param <U>
     */
    final class ValuePair<T, U> implements Value<Pair<T, U>> {
        private final Applicative<T> left;
        private final Applicative<U> right;

        public ValuePair(Applicative<T> left, Applicative<U> right) {
            this.left = left;
            this.right = right;
        }

        @Override
        public boolean isEmpty() {
            return left.isEmpty() && right.isEmpty();
        }

        @Override
        public Stream<Pair<T, U>> stream() {
            return SequenceExtension.zip(left.stream(), right.stream(), null, null, Pair::of);
        }
    }

    final class ApplyAll<T, R> implements Apply<T, R>, Applicative<Function<? super T, ? extends R>> {
        private final Collection<Function<? super T, ? extends R>> functions;

        @SuppressWarnings("unchecked")
        private ApplyAll(Collection<? extends Function<? super T, ? extends R>> functions) {
            Objects.requireNonNull(functions);
            this.functions = (Collection<Function<? super T, ? extends R>>) functions;
        }

        @Override
        public boolean isEmpty() {
            return functions.isEmpty();
        }

        @Override
        public Stream<Function<? super T, ? extends R>> stream() {
            return functions.stream();
        }
    }

    final class ApplyOne<T, R> implements Apply<T, R>, Function<T, R>, Applicative<Function<? super T, ? extends R>> {

        private final Function<? super T, ? extends R> function;

        private ApplyOne(Function<? super T, ? extends R> function) {
            this.function = function;
        }

        @Override
        public R apply(T o) {
            return function.apply(o);
        }

        @Override
        public boolean isEmpty() {
            return function == null;
        }

        @Override
        public Stream<Function<? super T, ? extends R>> stream() {
            return Stream.ofNullable(function);
        }
    }
}
