package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class LangExtension {

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <L, R> Either<L, R> either(Optional<? extends R> optional, L leftWhenNotPresent) {
        return Either.of(optional, leftWhenNotPresent);
    }

    // Enable if needed
    // public static <L, R> Either<L, R> eitherLeft(L value) {
    // return Either.ofLeft(value);
    // }

    // Enable if needed
    // public static <L, R> Either<L, R> eitherLeft(L value, Class<R> rightClass) {
    // return Either.ofLeft(value, rightClass);
    // }

    // Enable if needed
    // public static <L, R> Either<L, R> eitherRight(R value) {
    // return Either.ofRight(value);
    // }

    public static <L, R> Either<L, R> eitherRight(R value, Class<L> leftClass) {
        return Either.ofRight(value, leftClass);
    }

    public static <T> Optional<T> option(T object) {
        return Optional.ofNullable(object);
    }

    public static Optional<Integer> option(int integer) {
        return Optional.of(integer);
    }

    public static <T, R> Optional<R> option(T object, Function<? super T, ? extends R> mapper) {
        return option(object).map(mapper);
    }

    public static Optional<Integer> optionIf(int integer, IntPredicate filter) {
        return option(integer).filter(filter::test);
    }

    public static <T> Optional<T> optionIf(T object, Predicate<? super T> filter) {
        return option(object).filter(filter);
    }

    public static <T> Optional<T> optionUnless(T object, Predicate<? super T> filter) {
        return option(object).filter(filter.negate());
    }

    public static <T> Stream<T> streamNullable(T object) {
        return Stream.ofNullable(object);
    }

}
