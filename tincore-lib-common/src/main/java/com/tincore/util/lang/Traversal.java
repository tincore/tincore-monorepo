package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.*;
import java.util.stream.Stream;

import static com.tincore.util.lang.Iso.ofArrayToStream;
import static com.tincore.util.lang.Iso.ofListToStream;

public class Traversal<T, P, C> implements Lens<T, C> {

    private static final Iso LIST_TO_STREAM_ISO = ofListToStream();

    private final Function<T, C> getter;
    private final BiFunction<T, C, T> innerSetter;
    private final BiFunction<T, C, T> setter;
    private final BiPredicate<C, C> changeChecker;
    private final Iso<C, Stream<P>> streamConverter;

    public Traversal(
        Function<T, C> getter,
        BiFunction<T, C, T> setter,
        Iso<C, Stream<P>> streamConverter,
        BiPredicate<C, C> changeChecker) {

        this.getter = getter;
        this.innerSetter = setter;
        this.setter = (t, c) -> isValueEquals(t, c) ? t : innerSetter.apply(t, c);

        this.streamConverter = streamConverter;
        this.changeChecker = changeChecker;
    }

    public Traversal(Function<T, C> getter, BiFunction<T, C, T> setter, Iso<C, Stream<P>> streamConverter) {
        this(getter, setter, streamConverter, Objects::equals);
    }

    public static <T, P>
        ArrayTraversal<T, P>
        ofArray(Function<T, P[]> getter, BiFunction<T, P[], T> setter, Class<P> type) {
        return new ArrayTraversal<>(getter, setter, type);
    }

    public static <T> ArrayTraversal<T[], T> ofArray(Class<T> type) {
        return ofArray(t -> t, (t, v) -> v, type);
    }

    public static <T> ListTraversal<List<T>, T> ofList() {
        return new ListTraversal<>(t -> t, (t, v) -> v);
    }

    public static <T, P> ListTraversal<T, P> ofList(Function<T, List<P>> getter, BiFunction<T, List<P>, T> setter) {
        return new ListTraversal<>(getter, setter);
    }

    public <U, D> Traversal<T, U, D> andThenEach(Lens<P, U> lens, Iso<D, Stream<U>> streamConverter) {
        Function<T, D> getter = t -> streamConverter.reverseGet(this.streamValues(t).map(lens::getValue));
        BiFunction<T, D, T> setter
            = (t, u) -> setValue(t, transformValues(t, s -> SequenceExtension.zip(s, streamConverter.get(u)).map(pa -> {
                var p = pa.getLeft();
                var v = pa.getRight();
                return lens.isValueEquals(p, v) ? p : lens.setValue(p, v);
            })));

        return new Traversal<>(getter, setter, streamConverter);
    }

    public <U, D> Traversal<T, U, D> andThenEach(Traversal<P, U, D> traversal) {
        Function<T, D> getter = t -> traversal.collect(streamValues(t).flatMap(traversal::streamValues));
        BiFunction<T, D, T> setter = (t, v) -> {
            var iterator = traversal.stream(v).iterator();
            return setValue(t,
                transformValues(t,
                    d -> d.map(p -> traversal.setValue(p,
                        traversal.transformValues(p, s -> s.map(u -> iterator.hasNext() ? iterator.next() : u))))));
        };

        return new Traversal<>(getter, setter, traversal.streamConverter, Object::equals);
    }

    @SuppressWarnings("unchecked")
    public <U> Traversal<T, U, List<U>> andThenEach(Lens<P, U> lens) {
        return andThenEach(lens, LIST_TO_STREAM_ISO);
    }

    private C collect(Stream<P> stream) {
        return streamConverter.reverseGet(stream);
    }

    @Override
    public Function<T, C> getter() {
        return getter;
    }

    @Override
    public boolean isValueEquals(T target, C testValue) {
        return changeChecker.test(getValue(target), testValue);
    }

    public T modifyEach(T target, UnaryOperator<P> unaryOperator) {
        return setValue(target, collect(streamValues(target).map(unaryOperator)));
    }

    public T modifyEachIf(T target, Predicate<? super P> predicate, UnaryOperator<P> unaryOperator) {
        return setValue(target, collect(streamValues(target).map(p -> predicate.test(p) ? unaryOperator.apply(p) : p)));
    }

    public T modifyEachUnless(T target, Predicate<? super P> predicate, UnaryOperator<P> unaryOperator) {
        return modifyEachIf(target, predicate.negate(), unaryOperator);
    }

    @Override
    public BiFunction<T, C, T> setter() {
        return setter;
    }

    private Stream<P> stream(C value) {
        return streamConverter.get(value);
    }

    private Stream<P> streamValues(T t) {
        return stream(getValue(t));
    }

    private C transformValues(T t, UnaryOperator<Stream<P>> streamUnaryOperator) {
        return this.streamConverter.transform(getValue(t), streamUnaryOperator);
    }

    public static class ListTraversal<T, P> extends Traversal<T, P, List<P>> {

        @SuppressWarnings("unchecked")
        public ListTraversal(
            Function<T, List<P>> getter,
            BiFunction<T, List<P>, T> setter,
            BiPredicate<List<P>, List<P>> changeChecker) {
            super(getter, setter, LIST_TO_STREAM_ISO, changeChecker);
        }

        ListTraversal(Function<T, List<P>> getter, BiFunction<T, List<P>, T> setter) {
            this(getter, setter, Objects::equals);
        }
    }

    public static class ArrayTraversal<T, P> extends Traversal<T, P, P[]> {

        public ArrayTraversal(
            Function<T, P[]> getter,
            BiFunction<T, P[], T> setter,
            Class<P> type,
            BiPredicate<P[], P[]> changeChecker) {
            super(getter, setter, ofArrayToStream(type), changeChecker);
        }

        ArrayTraversal(Function<T, P[]> getter, BiFunction<T, P[], T> setter, Class<P> type) {
            this(getter, setter, type, Arrays::equals);
        }
    }

}
