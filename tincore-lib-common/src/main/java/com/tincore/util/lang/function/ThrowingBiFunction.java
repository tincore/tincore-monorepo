package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import lombok.experimental.ExtensionMethod;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ThrowingBiFunction<T, U, V, X extends Throwable> {

    static <T, U, R, X extends Throwable> BiFunction<T, U, R> uBiFunction(
        ThrowingBiFunction<? super T, ? super U, ? extends R, ? extends X> function) {
        Objects.requireNonNull(function);
        return uBiFunction(function, ExceptionMapper.wrapChecked());
    }

    static <T, U, R, X extends Throwable, Y extends RuntimeException> BiFunction<T, U, R> uBiFunction(
        ThrowingBiFunction<? super T, ? super U, ? extends R, ? extends X> function,
        Function<? super X, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(function);
        Objects.requireNonNull(exceptionMapper);
        return uBiFunction(function, ExceptionMapper.wrapChecked(exceptionMapper));
    }

    static <T, U, R, X extends Throwable, Y extends RuntimeException> BiFunction<T, U, R> uBiFunction(
        ThrowingBiFunction<? super T, ? super U, ? extends R, ? extends X> function,
        ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(function);
        Objects.requireNonNull(exceptionMapper);
        return (t, u) -> function.applyUnchecked(t, u, exceptionMapper.applyPartial(t));
    }

    default <W> ThrowingBiFunction<T, U, W, X> andThen(final ThrowingFunction<? super V, ? extends W, X> after) {
        Objects.requireNonNull(after);
        return (final T t, final U u) -> after.apply(apply(t, u));
    }

    V apply(T t, U u) throws X;

    default <Y extends RuntimeException> V applyUnchecked(T t, U u, Function<Throwable, ? extends Y> throwableMapper) {
        try {
            return apply(t, u);
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD
            throw throwableMapper.apply(e);
        }
    }

    default <Y extends RuntimeException> V applyUnchecked(T t, U u, ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        return this.applyUnchecked(t, u, exceptionMapper.applyPartial(t));
    }
}
