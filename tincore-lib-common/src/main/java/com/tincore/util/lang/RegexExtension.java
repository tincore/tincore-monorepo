package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.tuple.Pair;
import com.tincore.util.lang.tuple.Triple;
import com.tincore.util.lang.tuple.TupleExtension;
import lombok.experimental.ExtensionMethod;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@ExtensionMethod({LangExtension.class, MonadicExtension.class, SequenceExtension.class, TupleExtension.class})
public class RegexExtension {

    public static final Pattern NAMED_GROUPS_PATTERN = Pattern.compile("\\(\\?<([a-zA-Z][a-zA-Z0-9]*)>");
    public static final Pattern VALUES_BETWEEN_BRACKETS_PATTERN = Pattern.compile("\\{([^\\}]+)\\}");
    public static final Function<Matcher, String> VALUES_BETWEEN_BRACKETS_MAPPING = m -> m.group(1);

    public static boolean isFound(String string, Pattern pattern) {
        return pattern.matcher(string).find();
    }

    public static boolean isMatch(String string, Pattern pattern) {
        return pattern.matcher(string).matches();
    }

    public static boolean isMatchWildcard(String string, String pattern, boolean caseSensitive) {
        return isMatchWildcard(string, pattern, caseSensitive, Locale.getDefault());
    }

    public static boolean isMatchWildcard(String string, String pattern, boolean caseSensitive, Locale locale) {
        return isMatchWildcard(caseSensitive ? string : string.toLowerCase(locale), caseSensitive ? pattern : pattern.toLowerCase(locale));
    }

    public static boolean isMatchWildcard(String string, String pattern) { // NOPMD It is as complex as it needs to
        var stringLength = string.length();
        var patternLength = pattern.length();

        var startIndex = -1;
        var match = 0;

        var i = 0;
        var j = 0;
        while (i < stringLength) {
            if (j < patternLength && (pattern.charAt(j) == '?' || pattern.charAt(j) == string.charAt(i))) {
                i++;
                j++;
            } else if (j < patternLength && pattern.charAt(j) == '*') {
                startIndex = j;
                match = i;
                j++;
            } else if (startIndex == -1) {
                return false;
            } else {
                j = startIndex + 1;
                match++;
                i = match;
            }
        }
        while (j < patternLength && pattern.charAt(j) == '*') {
            j++;
        }
        return j == patternLength;
    }

    public static String replaceAll(String string, Pattern pattern, Function<Matcher, String> matcherToValueMapper) {
        Set<String> replacedGroups = new HashSet<>();

        var replacedUrl = string;
        var matcher = pattern.matcher(replacedUrl);
        while (matcher.find()) {
            var group = matcher.group();
            if (!replacedGroups.contains(group)) {
                var replacementValue = matcherToValueMapper.apply(matcher);
                replacedUrl = replacedUrl.replace(group, replacementValue);
                replacedGroups.add(group);
                matcher = pattern.matcher(replacedUrl);
            }
        }
        return replacedUrl;
    }

    public static List<String> replaceNamedGroupMatches(String string, Pattern pattern, Map<String, String> replacement) {
        var matcher = pattern.matcher(string);
        var groups = toNamedGroups(matcher.pattern()).toList();

        var list = new ArrayList<String>();
        while (matcher.find()) {
            var substitutes = groups.streamEach(g -> Triple.of(g, matcher.start(g), matcher.end(g)))
                .filter(t -> t.getMiddle() >= 0)
                .filter(t -> replacement.containsKey(t.getLeft()))
                .sortBy(Triple::getMiddle)
                .toList();
            var lastIndex = 0;
            var output = new StringBuilder(); // NOPMD
            for (var s : substitutes) {
                output.append(string, lastIndex, s.getMiddle()).append(replacement.get(s.getLeft()));
                lastIndex = s.getRight();
            }
            if (lastIndex < string.length()) {
                output.append(string, lastIndex, string.length());
            }
            list.add(output.toString());
        }
        return list;
    }

    private static Pair<String, String> splitAtIndex(String string, int index) {
        return string.substring(0, index).to(string.substring(index));
    }

    /**
     * Splits text including each splitter and text between splitter and next splitter to the right
     *
     * @param text
     * @param splittingPattern
     * @return
     */
    public static List<String> splitIncludingRight(String text, Pattern splittingPattern) {
        return splittingPattern.matcher(text)
            .results()
            .toListReverse()
            .fold(Pair.of(text, Collections.<String>emptyList()),
                (p, m) -> splitAtIndex(p.getLeft(), m.start()).letEntry((l, r) -> Pair.of(l,
                    p.getRight()
                        .prepend(r))))
            .let(Pair::getRight);
    }

    public static Optional<Matcher> toMatched(String string, Pattern pattern) {
        return string.option().map(pattern::matcher).filter(Matcher::matches);
    }

    public static Optional<Map<String, String>> toMatchedGroupValues(String string, Pattern pattern) {
        return toMatched(string, pattern).map(m -> toNamedGroups(pattern).mapToPair(g -> g, m::group)
            .filterValue(Objects::nonNull)
            .toMap());
    }

    public static <T> Optional<T> toMatchedGroupValues(String string, Pattern pattern, Function<Map<String, String>, ? extends T> mapper) {
        return toMatchedGroupValues(string, pattern).map(mapper);
    }

    private static Stream<String> toNamedGroups(Pattern pattern) {
        return NAMED_GROUPS_PATTERN.matcher(pattern.pattern()).results().map(r -> r.group(1));
    }

    public static Pattern withSingleSpaceOrNone(Pattern pattern) {
        return pattern.pattern().let(p -> Pattern.compile("((?: " + p + "\\b)|(?:\\b" + p + " |(?:\\b" + p + "\\b)))"));
    }
}
