package com.tincore.util.lang.tuple;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.QuadriConsumer;
import com.tincore.util.lang.function.QuadriFunction;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Quadruple<L, M, N, R> extends Tuple<L, Triple<M, N, R>, Quadruple<L, M, N, R>> {

    int ARITY = 4;

    static <L, M, N, R> Quadruple<L, M, N, R> of(final L left, final M middleLeft, N middleRight, final R right) { // NOPMD
        return new Quadruple.Immutable<>(left, middleLeft, middleRight, right);
    }

    static <T> Quadruple<T, T, T, T> ofIterable(Iterable<T> iterable) {
        return ofIterable(iterable, false);
    }

    static <T> Quadruple<T, T, T, T> ofIterable(Iterable<T> iterable, boolean autoFill) {
        T v0 = null;
        T v1 = null;
        T v2 = null;
        T v3 = null;

        Iterator<T> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            v0 = iterator.next();
            if (iterator.hasNext()) {
                v1 = iterator.next();
                if (iterator.hasNext()) {
                    v2 = iterator.next();
                    if (iterator.hasNext()) {
                        v3 = iterator.next();
                    } else {
                        iterator = null;
                    }
                } else {
                    iterator = null;
                }
            } else {
                iterator = null;
            }
        } else {
            iterator = null;
        }

        if (!autoFill && (iterator == null || iterator.hasNext())) {
            throw new IllegalArgumentException("Invalid iterator length. Expected length=" + ARITY);
        }

        return of(v0, v1, v2, v3);
    }

    static <L, M, N, R>
        Quadruple.Mutable<L, M, N, R>
        ofMutable(final L left, final M middleLeft, final N middleRight, final R right) {
        return new Quadruple.Mutable<>(left, middleLeft, middleRight, right);
    }

    default void accept(QuadriConsumer<L, M, N, R> consumer) {
        consumer.accept(getLeft(), getMiddleLeft(), getMiddleRight(), getRight());
    }

    default <U> U apply(QuadriFunction<L, M, N, R, U> mapper) {
        return mapper.apply(getLeft(), getMiddleLeft(), getMiddleRight(), getRight());
    }

    @Override
    default int arity() {
        return ARITY;
    }

    @Override
    default int compareTo(final Quadruple<L, M, N, R> other) {
        return new CompareToBuilder().append(getLeft(), other.getLeft())
            .append(getMiddleLeft(), other.getMiddleLeft())
            .append(getMiddleRight(), other.getMiddleRight())
            .append(getRight(), other.getRight())
            .toComparison();
    }

    default <U, V, W, X> Quadruple<U, V, W, X> flatMap(QuadriFunction<L, M, N, R, Quadruple<U, V, W, X>> mapper) {
        return mapper.apply(getLeft(), getMiddleLeft(), getMiddleRight(), getRight());
    }

    @Override
    default L getHead() {
        return getLeft();
    }

    L getLeft();

    M getMiddleLeft();

    N getMiddleRight();

    R getRight();

    @Override
    default Triple<M, N, R> getTail() {
        return Triple.of(getMiddleLeft(), getMiddleRight(), getRight());
    }

    default <U, V, W, X>
        Quadruple<U, V, W, X>
        map(Function<L, U> mapperL, Function<M, V> mapperM, Function<N, W> mapperN, Function<R, X> mapperR) {
        return of(mapperL.apply(getLeft()),
            mapperM.apply(getMiddleLeft()),
            mapperN.apply(getMiddleRight()),
            mapperR.apply(getRight()));
    }

    default <U> Quadruple<U, M, N, R> mapLeft(Function<L, U> mapper) {
        return of(mapper.apply(getLeft()), getMiddleLeft(), getMiddleRight(), getRight());
    }

    default <U> Quadruple<L, U, N, R> mapMiddleLeft(Function<M, U> mapper) {
        return of(getLeft(), mapper.apply(getMiddleLeft()), getMiddleRight(), getRight());
    }

    default <U> Quadruple<L, M, U, R> mapMiddleRight(Function<N, U> mapper) {
        return of(getLeft(), getMiddleLeft(), mapper.apply(getMiddleRight()), getRight());
    }

    default <U> Quadruple<L, M, N, U> mapRight(Function<R, U> mapper) {
        return of(getLeft(), getMiddleLeft(), getMiddleRight(), mapper.apply(getRight()));
    }

    @Override
    default Stream<Object> stream() {
        return Stream.of(getLeft(), getMiddleLeft(), getMiddleRight(), getRight());
    }

    @Override
    default Quadruple<L, M, N, R> withHead(L value) {
        return withLeft(value);
    }

    Quadruple<L, M, N, R> withLeft(L value);

    Quadruple<L, M, N, R> withMiddleLeft(M value);

    Quadruple<L, M, N, R> withMiddleRight(N value);

    Quadruple<L, M, N, R> withRight(R value);

    abstract class Abstract<L, M, N, R> implements Quadruple<L, M, N, R>, Serializable {

        @Override
        public boolean equals(final Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof Quadruple<?, ?, ?, ?> other) {
                return Objects.equals(getLeft(), other.getLeft())
                    && Objects.equals(getMiddleLeft(), other.getMiddleLeft())
                    && Objects.equals(getMiddleRight(), other.getMiddleRight())
                    && Objects.equals(getRight(), other.getRight());
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(getLeft()) ^ Objects.hashCode(getMiddleLeft()) ^ Objects.hashCode(getMiddleRight())
                ^ Objects.hashCode(getRight());
        }

        @Override
        public String toString() {
            return "(%s,%s,%s,%s)".formatted(getLeft(), getMiddleLeft(), getMiddleRight(), getRight());
        }

        public String toString(final String format) {
            return String.format(format, getLeft(), getMiddleLeft(), getMiddleRight(), getRight());
        }
    }

    @Getter
    @FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    @With
    @Builder(toBuilder = true)
    final class Immutable<L, M, N, R> extends Quadruple.Abstract<L, M, N, R> {
        private final L left;
        private final M middleLeft;
        private final N middleRight;
        private final R right;

        @Override
        public Immutable<L, M, N, R> withTail(Triple<M, N, R> triple) {
            return new Immutable<>(this.left, triple.getLeft(), triple.getMiddle(), triple.getRight());
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @With
    @Builder(toBuilder = true)
    class Mutable<L, M, N, R> extends Quadruple.Abstract<L, M, N, R> {
        private L left;
        private M middleLeft;
        private N middleRight;
        private R right;

        @Override
        public Mutable<L, M, N, R> withTail(Triple<M, N, R> triple) {
            return new Mutable<>(this.left, triple.getLeft(), triple.getMiddle(), triple.getRight());
        }
    }
}
