package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.tuple.Pair;
import com.tincore.util.lang.tuple.Triple;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.*;

public class FunctionExtension { // NOPMD No God
    private static final BiPredicate<?, ?> BIPREDICATE_TAUTOLOGY = (t, u) -> true;
    private static final BiPredicate<?, ?> BIPREDICATE_CONTRADICTION = BIPREDICATE_TAUTOLOGY.negate();
    private static final Predicate<?> PREDICATE_TAUTOLOGY = t -> true;
    private static final Predicate<?> PREDICATE_CONTRADICTION = PREDICATE_TAUTOLOGY.negate();
    private static final Object LAZY_NO_VALUE = new Object();

    public static <T, R> Supplier<R> andThen(Supplier<T> supplier, Function<? super T, R> function) {
        return () -> function.apply(supplier.get());
    }

    public static <T, R> Supplier<R> applyPartial(Function<T, R> function, T t) {
        return () -> function.apply(t);
    }

    public static <T, U, R> Function<U, R> applyPartial(BiFunction<T, U, R> biFunction, T t) {
        return applyPartialLeft(biFunction, t);
    }

    public static <T, U, V, R> BiFunction<U, V, R> applyPartial(TriFunction<T, U, V, R> triFunction, T t) {
        return applyPartialLeft(triFunction, t);
    }

    public static <T, U extends AutoCloseable, X extends Throwable> AutoCloseableSupplier<U, X> applyPartial(AutoCloseableFunction<? super T, ? extends U, ? extends X> function, T t) {
        return () -> function.apply(t);
    }

    public static <T, U, R> Function<U, R> applyPartialLeft(BiFunction<T, U, R> biFunction, T t) {
        return v -> biFunction.apply(t, v);
    }

    public static <T, U, V, R> BiFunction<U, V, R> applyPartialLeft(TriFunction<T, U, V, R> triFunction, T t) {
        return (u, v) -> triFunction.apply(t, u, v);
    }

    public static <T, U, V, R> BiFunction<T, V, R> applyPartialMiddle(TriFunction<T, U, V, R> triFunction, U u) {
        return (t, v) -> triFunction.apply(t, u, v);
    }

    public static <T, U, R> Function<T, R> applyPartialRight(BiFunction<T, U, R> biFunction, U u) {
        return t -> biFunction.apply(t, u);
    }

    public static <T, U, V, R> BiFunction<T, U, R> applyPartialRight(TriFunction<T, U, V, R> triFunction, V v) {
        return (t, u) -> triFunction.apply(t, u, v);
    }

    @SuppressWarnings("unchecked")
    public static <T, U> BiPredicate<T, U> biContradiction() {
        return (BiPredicate<T, U>) BIPREDICATE_CONTRADICTION;
    }

    @SuppressWarnings("unchecked")
    public static <T, U> BiPredicate<T, U> biTautology() {
        return (BiPredicate<T, U>) BIPREDICATE_TAUTOLOGY;
    }

    public static <T> Consumer<T> c(Consumer<T> consumer) { // NOPMD
        return consumer;
    }

    public static <T, U, V, W> BiFunction<T, U, Pair<V, W>> combine(Function<? super T, ? extends V> leftMapper, Function<? super U, ? extends W> rightMapper) {
        return (t, u) -> Pair.of(leftMapper.apply(t), rightMapper.apply(u));
    }

    public static <T, U, V, W, X, Y> TriFunction<T, U, V, Triple<W, X, Y>> combine(Function<? super T, ? extends W> leftMapper, Function<? super U, ? extends X> middleMapper, Function<? super V, ? extends Y> rightMapper) {
        return (t, u, v) -> Triple.of(leftMapper.apply(t), middleMapper.apply(u), rightMapper.apply(v));
    }

    public static <T, U, R, V, W> BiFunction<V, W, R> compose(BiFunction<T, U, R> biFunction, Function<? super V, ? extends T> beforeLeft, Function<? super W, ? extends U> beforeRight) {
        Objects.requireNonNull(beforeRight);
        return (V v, W w) -> biFunction.apply(beforeLeft.apply(v), beforeRight.apply(w));
    }

    public static <T, U, V, W> BiPredicate<V, W> compose(BiPredicate<T, U> biPredicate, Function<? super V, ? extends T> beforeLeft, Function<? super W, ? extends U> beforeRight) {
        Objects.requireNonNull(beforeRight);
        return (V v, W w) -> biPredicate.test(beforeLeft.apply(v), beforeRight.apply(w));
    }

    public static <T, U, V, W> BiConsumer<V, W> compose(BiConsumer<T, U> biConsumer, Function<? super V, ? extends T> beforeLeft, Function<? super W, ? extends U> beforeRight) {
        Objects.requireNonNull(beforeRight);
        return (V v, W w) -> biConsumer.accept(beforeLeft.apply(v), beforeRight.apply(w));
    }

    public static <T, U, R, V> BiFunction<V, U, R> composeLeft(BiFunction<T, U, R> biFunction, Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v, U u) -> biFunction.apply(before.apply(v), u);
    }

    public static <T, U, V> BiPredicate<V, U> composeLeft(BiPredicate<T, U> biPredicate, Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v, U u) -> biPredicate.test(before.apply(v), u);
    }

    public static <T, U, V> BiConsumer<V, U> composeLeft(BiConsumer<T, U> biConsumer, Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v, U u) -> biConsumer.accept(before.apply(v), u);
    }

    public static <T, U, R, V> BiFunction<T, V, R> composeRight(BiFunction<T, U, R> biFunction, Function<? super V, ? extends U> before) {
        Objects.requireNonNull(before);
        return (T t, V v) -> biFunction.apply(t, before.apply(v));
    }

    public static <T, U, V> BiPredicate<T, V> composeRight(BiPredicate<T, U> biPredicate, Function<? super V, ? extends U> before) {
        Objects.requireNonNull(before);
        return (T t, V v) -> biPredicate.test(t, before.apply(v));
    }

    public static <T, U, V> BiConsumer<T, V> composeRight(BiConsumer<T, U> biConsumer, Function<? super V, ? extends U> before) {
        Objects.requireNonNull(before);
        return (T t, V v) -> biConsumer.accept(t, before.apply(v));
    }

    @SuppressWarnings("unchecked")
    public static <T> Predicate<T> contradiction() {
        return (Predicate<T>) PREDICATE_CONTRADICTION;
    }

    public static <T, U, V, R> Function<T, Function<U, Function<V, R>>> curry(TriFunction<T, U, V, R> triFunction) {
        return t -> u -> v -> triFunction.apply(t, u, v);
    }

    public static <T, U, R> Function<T, Function<U, R>> curry(BiFunction<T, U, R> biFunction) {
        return t -> applyPartialLeft(biFunction, t);
    }

    public static <T, U, R> BiFunction<T, U, R> expand(Function<Pair<T, U>, R> function) {
        return (t, u) -> function.apply(Pair.of(t, u));
    }

    public static <T, U, V, R> TriFunction<T, U, V, R> expandTriple(Function<Triple<T, U, V>, R> function) {
        return (t, u, v) -> function.apply(Triple.of(t, u, v));
    }

    public static <T> Supplier<T> f(Supplier<T> supplier) { // NOPMD
        return supplier;
    }

    public static <T, R> Function<T, R> f(Function<T, R> function) { // NOPMD
        return function;
    }

    public static <T, U, R> BiFunction<T, U, R> f(BiFunction<T, U, R> biFunction) { // NOPMD
        return biFunction;
    }

    public static <T, U, V, R> TriFunction<T, U, V, R> f(TriFunction<T, U, V, R> triFunction) { // NOPMD
        return triFunction;
    }

    @SuppressWarnings({"unchecked"})
    public static <T> Supplier<T> lazy(final Supplier<T> supplier) {
        return new Supplier<>() {
            private volatile T value = (T) LAZY_NO_VALUE; // NOPMD I use volatile because its fine here

            @Override
            public T get() {
                var result = value;
                if (result == LAZY_NO_VALUE) {
                    synchronized (this) {
                        result = value;
                        if (result == LAZY_NO_VALUE) {
                            result = supplier.get();
                            value = result;
                        }
                    }
                }
                return result;
            }
        };
    }

    public static <T, R> Function<T, Optional<R>> lift(Function<T, R> function) {
        return t -> {
            try {
                return Optional.ofNullable(function.apply(t));
            } catch (Exception e) { // NOPMD this is the whole purpose
                return Optional.empty();
            }
        };
    }

    public static <T, R, X extends Throwable> Function<T, Optional<R>> lift(ThrowingFunction<T, R, X> function) {
        return t -> {
            try {
                return Optional.ofNullable(function.apply(t));
            } catch (Error e) { // NOPMD this is the whole purpose
                throw e;
            } catch (Throwable e) { // NOPMD this is the whole purpose
                return Optional.empty();
            }
        };
    }

    public static <T, R> Function<T, Optional<R>> lift(Function<T, R> function, Predicate<T> definedAt) {
        return t -> definedAt.test(t) ? Optional.ofNullable(function.apply(t)) : Optional.empty();
    }

    public static <T, R> Function<T, Optional<R>> lift(PartialFunction<T, R> function) {
        return lift(function, function::isDefinedAt);
    }

    public static <T, U, R> BiFunction<T, U, Optional<R>> lift(BiFunction<T, U, R> function, BiPredicate<T, U> definedAt) {
        return (t, u) -> definedAt.test(t, u) ? Optional.ofNullable(function.apply(t, u)) : Optional.empty();
    }

    public static <T, U, R> BiFunction<T, U, Optional<R>> lift(PartialBiFunction<T, U, R> function) {
        return lift(function, function::isDefinedAt);
    }

    public static <T, U, V, R> TriFunction<T, U, V, Optional<R>> lift(TriFunction<T, U, V, R> function, TriPredicate<T, U, V> definedAt) {
        return (t, u, v) -> definedAt.test(t, u, v) ? Optional.ofNullable(function.apply(t, u, v)) : Optional.empty();
    }

    public static <T, U, V, R> TriFunction<T, U, V, Optional<R>> lift(PartialTriFunction<T, U, V, R> function) {
        return lift(function, function::isDefinedAt);
    }

    public static <T> Supplier<T> memoized(final Supplier<T> supplier) {
        return lazy(supplier);
    }

    public static <T, U> Function<T, U> memoized(final Function<T, U> function) {
        return new Function<>() {
            private final Map<T, U> memoizer = new ConcurrentHashMap<>();

            @Override
            public U apply(T t) {
                return memoizer.computeIfAbsent(t, function);
            }
        };
    }

    public static <T, U, V> BiFunction<T, U, V> memoized(final BiFunction<T, U, V> biFunction) {
        return new BiFunction<>() {
            private final Map<Pair<T, U>, V> memoizer = new ConcurrentHashMap<>();

            @Override
            public V apply(T t, U u) {
                return memoizer.computeIfAbsent(Pair.of(t, u), p -> biFunction.apply(p.getLeft(), p.getRight()));
            }
        };
    }

    public static <T, U, V, W> TriFunction<T, U, V, W> memoized(final TriFunction<T, U, V, W> triFunction) {
        return new TriFunction<>() {
            private final Map<Triple<T, U, V>, W> memoizer = new ConcurrentHashMap<>();

            @Override
            public W apply(T t, U u, V v) {
                return memoizer.computeIfAbsent(Triple.of(t, u, v), p -> triFunction.apply(p.getLeft(), p.getMiddle(), p.getRight()));
            }
        };
    }

    public static <T> Predicate<T> p(Predicate<T> predicate) { // NOPMD
        return predicate;
    }

    public static <T, R> PartialFunction<T, R> partial(Function<T, R> function, Predicate<T> definedAt) {
        return new PartialFunction<>() {
            @Override
            public R apply(T t) {
                return function.apply(t);
            }

            @Override
            public boolean isDefinedAt(T t) {
                return definedAt.test(t);
            }
        };
    }

    public static <T, U, R> PartialBiFunction<T, U, R> partial(BiFunction<T, U, R> function, BiPredicate<T, U> definedAt) {
        return new PartialBiFunction<>() {
            @Override
            public R apply(T t, U u) {
                return function.apply(t, u);
            }

            @Override
            public boolean isDefinedAt(T t, U u) {
                return definedAt.test(t, u);
            }
        };
    }

    public static <T, U, V, R> PartialTriFunction<T, U, V, R> partial(TriFunction<T, U, V, R> function, TriPredicate<T, U, V> definedAt) {
        return new PartialTriFunction<>() {
            @Override
            public R apply(T t, U u, V v) {
                return function.apply(t, u, v);
            }

            @Override
            public boolean isDefinedAt(T t, U u, V v) {
                return definedAt.test(t, u, v);
            }
        };
    }

    public static <T, U, V> Function<T, Pair<U, V>> product(Function<? super T, ? extends U> functionLeft, Function<? super T, ? extends V> functionRight) {
        var combine = FunctionExtension.<T, T, U, V>combine(functionLeft, functionRight);
        return t -> combine.apply(t, t);
    }

    public static <T, U, V, W> Function<T, Triple<U, V, W>> product(Function<? super T, ? extends U> functionLeft, Function<? super T, ? extends V> functionMiddle, Function<? super T, ? extends W> functionRight) {
        var combine = FunctionExtension.<T, T, T, U, V, W>combine(functionLeft, functionMiddle, functionRight);
        return t -> combine.apply(t, t, t);
    }

    @SuppressWarnings("unchecked")
    public static <T> Predicate<T> tautology() {
        return (Predicate<T>) PREDICATE_TAUTOLOGY;
    }

    @SuppressWarnings("unchecked")
    public static <T, U, V> TriPredicate<T, U, V> triContradiction() {
        return (TriPredicate<T, U, V>) TriPredicate.CONTRADICTION;
    }

    @SuppressWarnings("unchecked")
    public static <T, U, V> TriPredicate<T, U, V> triTautology() {
        return (TriPredicate<T, U, V>) TriPredicate.TAUTOLOGY;
    }

    public static <T, U, R> Function<Pair<T, U>, R> tupled(BiFunction<T, U, R> biFunction) {
        return t -> biFunction.apply(t.getLeft(), t.getRight());
    }

    public static <T, U, V, R> Function<Triple<T, U, V>, R> tupled(TriFunction<T, U, V, R> triFunction) {
        return t -> triFunction.apply(t.getLeft(), t.getMiddle(), t.getRight());
    }

    public static <T, U, R> BiFunction<T, U, R> uncurry(Function<? super T, ? extends Function<? super U, ? extends R>> function) {
        return (t, u) -> function.apply(t).apply(u);
    }

    public static <T, U, V, R> TriFunction<T, U, V, R> uncurryTriple(Function<? super T, ? extends Function<? super U, ? extends Function<? super V, ? extends R>>> function) {
        return (t, u, v) -> function.apply(t).apply(u).apply(v);
    }

    // static <T, X extends Throwable, Y extends RuntimeException> BiFunction<T, X, Y> narrow(BiFunction<? super T, ? super X, ? extends Y> exceptionMapper) {
    // return exceptionMapper::apply;
    // }

}
