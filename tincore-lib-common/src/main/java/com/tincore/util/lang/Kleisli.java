package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Kleisli abstracts composition of different types of functors or monads
 *
 * @param <C>
 * @param <T>
 * @param <R>
 */
public sealed interface Kleisli<C, T, R> {
    static <T> Kleisli<Optional<T>, T, T> of(T value) { // NOPMD
        return ofOptional(Optional.ofNullable(value));
    }

    static <T, R> ApplicativeKleisli<T, R> ofApplicative(Function<? super T, Applicative.Value<R>> valueFunction) {
        return new ApplicativeKleisli<>(valueFunction);
    }

    static <T> ApplicativeKleisli<T, T> ofApplicative() {
        return ofApplicative(Applicative::of);
    }

    @SafeVarargs
    static <T, R> ApplicativeKleisli<T, R> ofApplicativeAp(Function<? super T, ? extends R>... functions) {
        Objects.requireNonNull(functions);
        return ofApplicative(t -> Applicative.of(t).ap(Applicative.ofFunctions(functions)));
    }

    static <T, R> CollectionKleisli<T, R> ofCollection(Function<? super T, Collection<R>> function) {
        return new CollectionKleisli<>(function);
    }

    static <T, R> OptionalKleisli<T, R> ofOptional(Function<? super T, Optional<R>> function) {
        return new OptionalKleisli<>(function, Stream::findFirst);
    }

    static <T, R> OptionalKleisli<T, R> ofOptional(Function<? super T, Optional<R>> function, Function<Stream<R>, Optional<R>> aggregator) {
        return new OptionalKleisli<>(function, aggregator);
    }

    @SuppressWarnings({"OptionalUsedAsFieldOrParameterType"})
    static <T> OptionalKleisli<T, T> ofOptional(Optional<T> optional) { // NOPMD
        return ofOptional(s -> optional);
    }

    static <T> OptionalKleisli<T, T> ofOptional() {
        return ofOptional(Optional::ofNullable);
    }

    static <T> OptionalKleisli<T, T> ofOptionalAggregation(Function<Stream<T>, Optional<T>> aggregator) {
        return new OptionalKleisli<>(Optional::ofNullable, aggregator);
    }

    static <T, R> OptionalMergerKleisli<T, R> ofOptionalMerging(Function<Stream<? super T>, Optional<R>> function) {
        return new OptionalMergerKleisli<>(function);
    }

    static <T, R> StreamKleisli<T, R> ofStream(Function<? super T, Stream<R>> function) {
        return new StreamKleisli<>(function);
    }

    <B, V> Kleisli<B, T, V> andThen(Kleisli<B, R, V> other);

    C apply(T t);

    <B, V> Kleisli<C, V, R> compose(Kleisli<B, V, T> before);

    abstract sealed class Abstract<C, T, R> implements Kleisli<C, T, R> {

        @Override
        public <D, V> Kleisli<D, T, V> andThen(Kleisli<D, R, V> after) {
            var afterKleisli = (Abstract<D, R, V>) after;
            return afterKleisli.withStreamFunction(this.sendOutgoing().andThen(afterKleisli.processIncoming()));
        }

        @Override
        public <D, V> Kleisli<C, V, R> compose(Kleisli<D, V, T> before) {
            return withStreamFunction(((Abstract<D, V, T>) before).sendOutgoing().andThen(this.processIncoming()));
        }

        Function<Stream<T>, Stream<R>> processIncoming() {
            return t -> t.flatMap(this.sendOutgoing());
        }

        abstract Function<? super T, Stream<R>> sendOutgoing();

        abstract <V> Kleisli<C, V, R> withStreamFunction(Function<? super V, Stream<R>> streamFunction);
    }

    final class OptionalKleisli<T, R> extends Abstract<Optional<R>, T, R> {

        private final Function<? super T, Optional<R>> function;
        private final Function<Stream<R>, Optional<R>> aggregator;

        private OptionalKleisli(Function<? super T, Optional<R>> function, Function<Stream<R>, Optional<R>> aggregator) {
            super();
            Objects.requireNonNull(function);
            Objects.requireNonNull(aggregator);
            this.function = function;
            this.aggregator = aggregator;
        }

        @Override
        public Optional<R> apply(T t) {
            return function.apply(t);
        }

        public <V> OptionalKleisli<T, V> map(Function<? super R, ? extends V> mapper) {
            return ofOptional(this.function.andThen(s -> s.map(mapper)));
        }

        @Override
        Function<? super T, Stream<R>> sendOutgoing() {
            return function.andThen(Optional::stream);
        }

        @Override
        <V> OptionalKleisli<V, R> withStreamFunction(Function<? super V, Stream<R>> streamFunction) {
            return ofOptional(streamFunction.andThen(aggregator), aggregator);
        }
    }

    final class OptionalMergerKleisli<T, R> extends Abstract<Optional<R>, T, R> {

        private final Function<Stream<? super T>, Optional<R>> function;

        private OptionalMergerKleisli(Function<Stream<? super T>, Optional<R>> function) {
            super();
            Objects.requireNonNull(function);
            this.function = function;
        }

        @Override
        public Optional<R> apply(T t) {
            return function.apply(Stream.ofNullable(t));
        }

        @Override
        Function<Stream<T>, Stream<R>> processIncoming() {
            return t -> function.apply(t).stream();
        }

        @Override
        Function<? super T, Stream<R>> sendOutgoing() {
            return ((Function<T, Optional<R>>) this::apply).andThen(Optional::stream);
        }

        @Override
        @SuppressWarnings("unchecked")
        <V> OptionalMergerKleisli<V, R> withStreamFunction(Function<? super V, Stream<R>> streamFunction) {
            return ofOptionalMerging(vs -> vs.map(v -> (V) v).flatMap(streamFunction).findAny()); // Will need revisit
        }
    }

    final class StreamKleisli<T, R> extends Abstract<Stream<R>, T, R> {

        private final Function<? super T, Stream<R>> function;

        private StreamKleisli(Function<? super T, Stream<R>> function) {
            super();
            Objects.requireNonNull(function);
            this.function = function;
        }

        @Override
        public Stream<R> apply(T t) {
            return function.apply(t);
        }

        public <V> StreamKleisli<T, V> map(Function<? super R, ? extends V> mapper) {
            return ofStream(sendOutgoing().andThen(s -> s.map(mapper)));
        }

        @Override
        Function<? super T, Stream<R>> sendOutgoing() {
            return function;
        }

        @Override
        <V> StreamKleisli<V, R> withStreamFunction(Function<? super V, Stream<R>> streamFunction) {
            return ofStream(streamFunction);
        }
    }

    final class CollectionKleisli<T, R> extends Abstract<Collection<R>, T, R> {

        private final Function<? super T, Collection<R>> function;

        private CollectionKleisli(Function<? super T, Collection<R>> function) {
            super();
            Objects.requireNonNull(function);
            this.function = function;
        }

        @Override
        public Collection<R> apply(T t) {
            return function.apply(t);
        }

        public <V> CollectionKleisli<T, V> map(Function<? super R, ? extends V> mapper) {
            return ofCollection(sendOutgoing().andThen(s -> s.map(mapper)).andThen(s -> s.map(v -> (V) v)).andThen(Stream::toList)); // NOPMD Necessary
        }

        @Override
        Function<? super T, Stream<R>> sendOutgoing() {
            return function.andThen(Collection::stream);
        }

        @Override
        <V> CollectionKleisli<V, R> withStreamFunction(Function<? super V, Stream<R>> streamFunction) {
            return ofCollection(streamFunction.andThen(Stream::toList));
        }

    }

    final class ApplicativeKleisli<T, R> extends Abstract<Applicative.Value<R>, T, R> {

        private final Function<? super T, Applicative.Value<R>> function;

        private ApplicativeKleisli(Function<? super T, Applicative.Value<R>> function) {
            super();
            Objects.requireNonNull(function);
            this.function = function;
        }

        @Override
        public Applicative.Value<R> apply(T t) {
            return function.apply(t);
        }

        public <V> ApplicativeKleisli<T, V> map(Function<? super R, ? extends V> mapper) {
            return ofApplicative(this.function.andThen(s -> s.map(mapper)));
        }

        @Override
        Function<? super T, Stream<R>> sendOutgoing() {
            return function.andThen(Applicative::stream);
        }

        @Override
        <V> ApplicativeKleisli<V, R> withStreamFunction(Function<? super V, Stream<R>> streamFunction) {
            return ofApplicative(streamFunction.andThen(Applicative::ofEach));
        }
    }

}
