package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.Matcher.Getting;

import java.util.*;
import java.util.function.*;
import java.util.stream.IntStream;

public interface Lens<T, P> {

    static <T, P> Lens<T, P> of(Function<T, P> get, BiFunction<T, P, T> set) { // NOPMD short method name
        return of(get, set, Objects::equals);
    }

    @SuppressWarnings({"checkstyle:ParenPad"})
    static <T, P> Lens<T, P> of( // NOPMD
        Function<T, P> getter,
        BiFunction<T, P, T> setter,
        BiPredicate<P, P> changeChecker) {
        return new SimpleLens<>(getter, setter, changeChecker);
    }

    static <T, P> Lens<T, P[]> ofArray(Function<T, P[]> getter, BiFunction<T, P[], T> setter) {
        return of(getter, setter, Arrays::equals);
    }

    static <T> List<? super AppliedLens<List<T>, T>> ofEachItem(List<T> list) {
        return list == null ? Collections.emptyList()
            : IntStream.range(0, list.size()).mapToObj(i -> Lens.<T>ofList(i).toApplied(list)).toList();
    }

    static <T> Lens<List<T>, T> ofList(int index) {
        return ofList(s -> index);
    }

    static <T> Lens<List<T>, T> ofList(Function<List<T>, Integer> indexMapper) {
        return of(l -> {
            int index = indexMapper.apply(l);
            return index >= 0 && index < l.size() ? l.get(index) : null;
        }, (l, v) -> {
            int index = indexMapper.apply(l);
            if (index >= 0 && index < l.size()) {
                l.set(index, v);
            }
            return l;
        });
    }

    static <T> Lens<List<T>, T> ofListFirst() {
        return ofList(0);
    }

    static <T> Lens<List<T>, T> ofListLast() {
        return ofList(s -> s.size() - 1);
    }

    static <T, P> Lens<Map<T, P>, P> ofMap(T keyValue) {
        return of(m -> m.get(keyValue), (m, v) -> {
            m.put(keyValue, v);
            return m;
        });
    }

    static <T, P> Lens<T, P> ofMutable(Function<T, P> getter, BiConsumer<T, P> setter) { // NOPMD short method name
        return new MutableLens<>(getter, setter);
    }

    static <T, P> Lens<T, P> ofNullable(Function<T, P> getter, BiFunction<T, P, T> setter) {
        return new NullableLens<>(getter, setter);
    }

    default <Q> Lens<T, Q> andThen(Lens<P, Q> lens) {
        return of(getter().andThen(lens.getter()), (t, v) -> setValue(t, lens.setValue(getValue(t), v)));
    }

    default <S> Lens<S, P> compose(final Lens<S, T> lens) {
        return lens.andThen(this);
    }

    default Lens<T, P> filterSet(Predicate<P> predicate) {
        return of(getter(), (t, v) -> predicate.test(v) ? setValue(t, v) : t);
    }

    default P getValue(T target) {
        return getter().apply(target);
    }

    Function<T, P> getter();

    default boolean isValueEquals(T target, P testValue) {
        return Objects.equals(getValue(target), testValue);
    }

    default Lens<T, P> map(UnaryOperator<P> unaryOperator) {
        return of(getter().andThen(unaryOperator), (t, v) -> this.setValue(t, unaryOperator.apply(v)));
    }

    default Lens<T, P> mapGet(UnaryOperator<P> unaryOperator) {
        return of(getter().andThen(unaryOperator), setter());
    }

    default Lens<T, P> mapSet(UnaryOperator<P> unaryOperator) {
        return of(getter(), (t, v) -> this.setValue(t, unaryOperator.apply(v)));
    }

    default T modifyValue(T target, UnaryOperator<P> unaryOperator) {
        return setValue(target, getValue(target), unaryOperator);
    }

    default T modifyValueIf(T target, Predicate<? super P> predicate, UnaryOperator<P> unaryOperator) {
        P currentValue = getValue(target);
        return predicate.test(currentValue) ? setValue(target, currentValue, unaryOperator) : target;
    }

    default T modifyValueUnless(T target, Predicate<? super P> predicate, UnaryOperator<P> unaryOperator) {
        return modifyValueIf(target, predicate.negate(), unaryOperator);
    }

    private T setValue(T target, P value, UnaryOperator<P> unaryOperator) { // NOPMD No linguistic antipattern here
        return setValue(target, unaryOperator.apply(value));
    }

    default T setValue(T target, P value) { // NOPMD fluent
        return setter().apply(target, value);
    }

    BiFunction<T, P, T> setter();

    default AppliedLens<T, P> toApplied(T target) {
        return new FocusedLens<>(getter(), setter(), target);
    }

    default Lens<T, P> toNullable() {
        return ofNullable(getter(), setter());
    }

    default Prism<T, P> toPrism() {
        return new Prism<>(getter(), setter());
    }

    @SuppressWarnings("unchecked")
    default <W> Lens<W, P> upcast(Class<W> type) {
        // Java does not support setting a lower bound for W
        return (Lens<W, P>) this;
    }

    interface AppliedLens<U, V> extends Lens<U, V> {

        V getValue();

        default U modifyValue(UnaryOperator<V> unaryOperator) {
            return setValue(unaryOperator.apply(getValue()));
        }

        default U modifyValueIf(Predicate<V> predicate, UnaryOperator<V> unaryOperator) {
            V value = getValue();
            return setValue(predicate.test(value) ? unaryOperator.apply(value) : value);
        }

        default U modifyValueUnless(Predicate<V> predicate, UnaryOperator<V> unaryOperator) {
            return modifyValueIf(predicate.negate(), unaryOperator);
        }

        U setValue(V value); // NOPMD Linguistic does not apply here
    }

    class FocusedLens<T, P> implements AppliedLens<T, P> {
        private final Function<T, P> getter;
        private final BiFunction<T, P, T> setter;

        public FocusedLens(Function<T, P> getter, BiFunction<T, P, T> setter, T target) {
            this.getter = t -> getter.apply(target);
            this.setter = (u, p) -> setter.apply(target, p);
        }

        @Override
        public P getValue() {
            return getValue(null);
        }

        @Override
        public Function<T, P> getter() {
            return getter;
        }

        @Override
        public T setValue(P value) {
            // NOPMD fluent
            return setValue(null, value);
        }

        @Override
        public BiFunction<T, P, T> setter() {
            return setter;
        }
    }

    class SimpleLens<T, P> implements Lens<T, P> {
        private final Function<T, P> getter;
        private final BiFunction<T, P, T> setter;
        private final BiPredicate<P, P> changeChecker;

        public SimpleLens(Function<T, P> getter, BiFunction<T, P, T> setter, BiPredicate<P, P> changeChecker) {
            this.getter = getter;
            this.setter = (t, p) -> isValueEquals(t, p) ? t : setter.apply(t, p);
            this.changeChecker = changeChecker;
        }

        @Override
        public Function<T, P> getter() {
            return getter;
        }

        @Override
        public boolean isValueEquals(T target, P testValue) {
            return changeChecker.test(getValue(target), testValue);
        }

        @Override
        public BiFunction<T, P, T> setter() {
            return setter;
        }
    }

    abstract class CompositeLens<T, P> implements Lens<T, P> {

        private final Function<T, P> getter
            = t -> getLens(t).map(Lens::getter).orElseThrow(() -> new IllegalStateException("Unmatched lens")).apply(t);
        private final BiFunction<T, P, T> setter = (t, p) -> getLens(t).map(Lens::setter)
            .orElseThrow(() -> new IllegalStateException("Unmatched lens"))
            .apply(t, p);

        public abstract Optional<Lens<T, P>> getLens(T t);

        @Override
        public Function<T, P> getter() {
            return getter;
        }

        @Override
        public BiFunction<T, P, T> setter() {
            return setter;
        }
    }

    class MatcherCompositeLens<T, P> extends CompositeLens<T, P> {
        private final Getting<T, Lens<T, P>> matcher = new Getting<>();

        @Override
        public Optional<Lens<T, P>> getLens(T t) {
            return matcher.find(t);
        }

        public MatcherCompositeLens<T, P> otherwise(Lens<T, P> lens) {
            matcher.otherwise(lens);
            return this;
        }

        public MatcherCompositeLens<T, P> otherwiseDummy() {
            return otherwise(of(o -> null, (o, p) -> o));
        }

        public MatcherCompositeLens<T, P> when(Predicate<T> matchPredicate, Lens<T, P> lens) {
            matcher.when(matchPredicate, lens);
            return this;
        }

        @SuppressWarnings("unchecked")
        public <W extends T> MatcherCompositeLens<T, P> when(Class<W> type, Lens<W, P> lens) {
            return when(type::isInstance, (Lens<T, P>) lens);
        }
    }

    class MutableLens<T, P> implements Lens<T, P> {
        private final Function<T, P> getter;
        private final BiFunction<T, P, T> setter;

        public MutableLens(Function<T, P> getter, BiConsumer<T, P> setter) {
            this.getter = getter;
            this.setter = (t, p) -> {
                if (!isValueEquals(t, p)) {
                    setter.accept(t, p);
                }
                return t;
            };
        }

        @Override
        public Function<T, P> getter() {
            return getter;
        }

        @Override
        public BiFunction<T, P, T> setter() {
            return setter;
        }
    }

    final class NullableLens<T, P> implements Lens<T, P> {
        private final Function<T, P> getter;
        private final BiFunction<T, P, T> setter;

        public NullableLens(Function<T, P> getter, BiFunction<T, P, T> setter) {
            this.getter = t -> t == null ? null : getter.apply(t);
            this.setter = (t, p) -> t == null || isValueEquals(t, p) ? t : setter.apply(t, p);
        }

        @Override
        public <V> Lens<T, V> andThen(Lens<P, V> lens) {
            return of((T t) -> {
                var p = getValue(t);
                return p == null ? null : lens.getValue(p);
            }, (t, v) -> t == null ? null : setValue(t, lens.setValue(getValue(t), v)));
        }

        @Override
        public Function<T, P> getter() {
            return getter;
        }

        @Override
        public BiFunction<T, P, T> setter() {
            return setter;
        }
    }
}
