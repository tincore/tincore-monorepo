package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.*;

public abstract class PileDual<I extends Pile, T> implements Pile<I, T> {
    @SafeVarargs
    public static <T> PileDual.Mutable<T> mutableOf(T... items) { // NOPMD Type
        var pile = new PileDual.Mutable<T>();
        if (items != null) {
            List<T> a = Arrays.asList(items);
            Collections.reverse(a);
            a.forEach(pile::push);
        }
        return pile;
    }

    @SafeVarargs
    public static <T> PileDual.Immutable<T> of(T... items) { // NOPMD Type
        var pile = new PileDual.Immutable<T>();
        if (items != null) {
            List<T> a = Arrays.asList(items);
            Collections.reverse(a);
            for (T t : a) {
                pile = pile.push(t);
            }
        }
        return pile;
    }

    @Override
    public boolean add(T t) {
        return getMainPile().add(t);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return getMainPile().addAll(c);
    }

    @Override
    public void clear() {
        getMainPile().clear();
        getStashPile().clear();
    }

    @Override
    public boolean contains(Object o) {
        return getMainPile().contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return getMainPile().contains(c);
    }

    @Override
    public T element() {
        return getMainPile().element();
    }

    @Override
    public int getIndexOf(Object o) {
        return getMainPile().getIndexOf(o);
    }

    protected abstract Pile<?, T> getMainPile();

    protected abstract Pile<?, T> getStashPile();

    public int getStashSize() {
        return getStashPile().size();
    }

    @Override
    public Iterator<T> iterator() {
        return getMainPile().iterator();
    }

    @Override
    public boolean offer(T t) {
        return getMainPile().offer(t);
    }

    @Override
    public T peek() {
        return getMainPile().element();
    }

    @Override
    public T poll() {
        return getMainPile().poll();
    }

    @Override
    public boolean remove(Object o) {
        return getMainPile().remove(o);
    }

    @Override
    public T remove() {
        return getMainPile().remove();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return getMainPile().removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return getMainPile().retainAll(c);
    }

    @Override
    public int size() {
        return getMainPile().size();
    }

    abstract I stash();

    abstract I stashClear();

    abstract I stashDrop();

    abstract I stashPop();

    @Override
    public Object[] toArray() {
        return getMainPile().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return getMainPile().toArray(a);
    }

    public static class Immutable<T> extends PileDual<Immutable<T>, T> { // NOPMD Types

        private final Pile.Immutable<T> mainPile;
        private final Pile.Immutable<T> stashPile;

        public Immutable(Pile.Immutable<T> mainPile, Pile.Immutable<T> stashPile) {
            super();
            this.mainPile = mainPile;
            this.stashPile = stashPile;
        }

        public Immutable() {
            this(Pile.Immutable.empty(), Pile.Immutable.empty());
        }

        @Override
        protected Pile<?, T> getMainPile() {
            return mainPile;
        }

        @Override
        protected Pile<?, T> getStashPile() {
            return stashPile;
        }

        @Override
        public PileDual.Immutable<T> pop() {
            return new PileDual.Immutable<>(mainPile.pop(), stashPile);
        }

        @Override
        public PileDual.Immutable<T> popAll() {
            return new PileDual.Immutable<>(mainPile.popAll(), stashPile);
        }

        @Override
        public PileDual.Immutable<T> push(T value) {
            return new PileDual.Immutable<>(mainPile.push(value), stashPile);
        }

        @Override
        public PileDual.Immutable<T> stash() {
            T transfer = mainPile.element();
            return new PileDual.Immutable<>(mainPile.pop(), stashPile.push(transfer));
        }

        @Override
        public PileDual.Immutable<T> stashClear() {
            return new PileDual.Immutable<>(mainPile, stashPile.popAll());
        }

        @Override
        public PileDual.Immutable<T> stashDrop() {
            return new PileDual.Immutable<>(mainPile, stashPile.drop());
        }

        @Override
        public PileDual.Immutable<T> stashPop() {
            T transfer = stashPile.element();
            return new PileDual.Immutable<>(mainPile.push(transfer), stashPile.pop());
        }
    }

    public static class Mutable<T> extends PileDual<Mutable<T>, T> { // NOPMD Types

        private final Pile.Mutable<T> mainPile; // NOPMD Types
        private final Pile.Mutable<T> stashPile; // NOPMD Types

        public Mutable() {
            super();
            this.mainPile = new Pile.Mutable<>();
            this.stashPile = new Pile.Mutable<>();
        }

        @Override
        protected Pile<?, T> getMainPile() {
            return mainPile;
        }

        @Override
        protected Pile<?, T> getStashPile() {
            return stashPile;
        }

        @Override
        public PileDual.Mutable<T> pop() {
            mainPile.pop();
            return this;
        }

        @Override
        public PileDual.Mutable<T> popAll() {
            mainPile.popAll();
            return this;
        }

        @Override
        public PileDual.Mutable<T> push(T value) {
            mainPile.push(value);
            return this;
        }

        @Override
        public PileDual.Mutable<T> stash() {
            stashPile.push(mainPile.remove());
            return this;
        }

        @Override
        public PileDual.Mutable<T> stashClear() {
            stashPile.popAll();
            return this;
        }

        @Override
        public PileDual.Mutable<T> stashDrop() {
            stashPile.drop();
            return this;
        }

        @Override
        public PileDual.Mutable<T> stashPop() {
            mainPile.push(stashPile.remove());
            return this;
        }
    }
}
