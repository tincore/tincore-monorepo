package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import lombok.experimental.ExtensionMethod;

import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Function;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ThrowingBiPredicate<T, U, X extends Throwable> {
    ThrowingBiPredicate<?, ?, ?> TAUTOLOGY = (a, b) -> true;
    ThrowingBiPredicate<?, ?, ?> CONTRADICTION = (a, b) -> false;

    @SuppressWarnings("unchecked")
    static <T, U, X extends Throwable> ThrowingBiPredicate<T, U, X> contradiction() {
        return (ThrowingBiPredicate<T, U, X>) CONTRADICTION;
    }

    @SuppressWarnings("unchecked")
    static <T, U, X extends Throwable> ThrowingBiPredicate<T, U, X> tautology() {
        return (ThrowingBiPredicate<T, U, X>) TAUTOLOGY;
    }

    static <T, U, X extends Throwable> BiPredicate<T, U> uBiPredicate(
        ThrowingBiPredicate<? super T, ? super U, ? extends X> biPredicate) {
        Objects.requireNonNull(biPredicate);
        return uBiPredicate(biPredicate, ExceptionMapper.wrapChecked());
    }

    static <T, U, X extends Throwable, Y extends RuntimeException> BiPredicate<T, U> uBiPredicate(
        ThrowingBiPredicate<? super T, ? super U, ? extends X> biPredicate,
        Function<? super X, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(biPredicate);
        Objects.requireNonNull(exceptionMapper);
        return uBiPredicate(biPredicate, ExceptionMapper.wrapChecked(exceptionMapper));
    }

    static <T, U, X extends Throwable, Y extends RuntimeException> BiPredicate<T, U> uBiPredicate(
        ThrowingBiPredicate<? super T, ? super U, ? extends X> biPredicate,
        ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(biPredicate);
        Objects.requireNonNull(exceptionMapper);
        return (t, u) -> biPredicate.testUnchecked(t, u, exceptionMapper.applyPartial(t));
    }

    default ThrowingBiPredicate<T, U, X> and(final ThrowingBiPredicate<? super T, ? super U, X> other) {
        Objects.requireNonNull(other);
        return (final T t, final U u) -> test(t, u) && other.test(t, u);
    }

    default ThrowingBiPredicate<T, U, X> negate() {
        return (final T t, final U u) -> !test(t, u);
    }

    default ThrowingBiPredicate<T, U, X> or(final ThrowingBiPredicate<? super T, ? super U, X> other) { // NOPMD
        Objects.requireNonNull(other);
        return (final T t, final U u) -> test(t, u) || other.test(t, u);
    }

    boolean test(T t, U u) throws X;

    default <Y extends RuntimeException> boolean testUnchecked(T t, U u, Function<Throwable, ? extends Y> throwableMapper) {
        try {
            return test(t, u);
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD
            throw throwableMapper.apply(e);
        }
    }

    default <Y extends RuntimeException> boolean testUnchecked(T t, U u, ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        return this.testUnchecked(t, u, exceptionMapper.applyPartial(t));
    }
}
