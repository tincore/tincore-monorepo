package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import lombok.experimental.ExtensionMethod;

import java.util.function.Function;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ThrowingRunnable<X extends Throwable> {

    static <X extends Throwable, Y extends RuntimeException> void uRun(
        ThrowingRunnable<? extends X> throwingRunnable,
        Function<? super X, ? extends Y> exceptionMapper) {
        uRunnable(throwingRunnable, exceptionMapper).run();
    }

    static <X extends Throwable> Runnable uRunnable(ThrowingRunnable<? extends X> throwingRunnable) {
        return uRunnable(throwingRunnable, ExceptionMapper.wrapChecked());
    }

    static <X extends Throwable, Y extends RuntimeException> Runnable uRunnable(
        ThrowingRunnable<? extends X> throwingRunnable,
        Function<? super X, ? extends Y> exceptionMapper) {
        return uRunnable(throwingRunnable, ExceptionMapper.wrapChecked(exceptionMapper));
    }

    static <X extends Throwable, Y extends RuntimeException> Runnable uRunnable(
        ThrowingRunnable<? extends X> throwingRunnable,
        ExceptionMapper<?, ? extends Y> exceptionMapper) {
        return () -> throwingRunnable.runUnchecked(exceptionMapper.applyPartial(null));
    }

    void run() throws X;

    default <Y extends RuntimeException> void runUnchecked(Function<Throwable, ? extends Y> throwableMapper) {
        try {
            run();
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD
            throw throwableMapper.apply(e);
        }
    }
}
