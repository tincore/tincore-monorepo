package com.tincore.util.lang.tuple;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Pair<L, R> extends Tuple<L, Unit<R>, Pair<L, R>>, Map.Entry<L, R> {

    int ARITY = 2;

    static <L, R> Pair<L, R> of(final L left, final R right) { // NOPMD
        return new Pair.Immutable<>(left, right);
    }

    static <T> Pair<T, T> ofIterable(Iterable<T> iterable) {
        return ofIterable(iterable, false);
    }

    static <T> Pair<T, T> ofIterable(Iterable<T> iterable, boolean autoFill) {
        T v0 = null;
        T v1 = null;

        Iterator<T> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            v0 = iterator.next();
            if (iterator.hasNext()) {
                v1 = iterator.next();
            } else {
                iterator = null;
            }
        } else {
            iterator = null;
        }

        if (!autoFill && (iterator == null || iterator.hasNext())) {
            throw new IllegalStateException("Invalid iterator length. Expected length=" + ARITY);
        }

        return of(v0, v1);
    }

    static <L, R> Pair.Mutable<L, R> ofMutable(final L left, final R right) {
        return new Pair.Mutable<>(left, right);
    }

    static <L, R> Pair<R, L> swap(Map.Entry<L, R> entry) {
        return swap(entry.getKey(), entry.getValue());
    }

    static <L, R> Pair<R, L> swap(L left, R right) {
        return of(right, left);
    }

    default void accept(BiConsumer<L, R> consumer) {
        consumer.accept(getLeft(), getRight());
    }

    default <U> U apply(BiFunction<L, R, U> mapper) {
        return mapper.apply(getLeft(), getRight());
    }

    @Override
    default int arity() {
        return ARITY;
    }

    @Override
    default int compareTo(final Pair<L, R> other) {
        return new CompareToBuilder().append(getLeft(), other.getLeft())
            .append(getRight(), other.getRight())
            .toComparison();
    }

    default <U, V> Pair<U, V> flatMap(BiFunction<L, R, Pair<U, V>> mapper) {
        return mapper.apply(getLeft(), getRight());
    }

    @Override
    default L getHead() {
        return getLeft();
    }

    @Override
    default L getKey() {
        return getLeft();
    }

    L getLeft();

    R getRight();

    @Override
    default Unit<R> getTail() {
        return Unit.of(getRight());
    }

    @Override
    default R getValue() {
        return getRight();
    }

    default <U, V> Pair<U, V> map(Function<L, U> mapperL, Function<R, V> mapperR) {
        return of(mapperL.apply(getLeft()), mapperR.apply(getRight()));
    }

    default <U> Pair<U, R> mapLeft(Function<L, U> mapper) {
        return of(mapper.apply(getLeft()), getRight());
    }

    default <U> Pair<L, U> mapRight(Function<R, U> mapper) {
        return of(getLeft(), mapper.apply(getRight()));
    }

    @Override
    default Stream<Object> stream() {
        return Stream.of(getLeft(), getRight());
    }

    Pair<R, L> swap();

    @Override
    default Pair<L, R> withHead(L value) {
        return withLeft(value);
    }

    Pair<L, R> withLeft(L value);

    Pair<L, R> withRight(R value);

    @Override
    default Pair<L, R> withTail(Unit<R> unit) {
        return withRight(unit.getValue());
    }

    abstract class Abstract<L, R> implements Pair<L, R>, Serializable {

        @Override
        public boolean equals(final Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof Pair<?, ?> other) {
                return Objects.equals(getLeft(), other.getLeft()) && Objects.equals(getRight(), other.getRight());
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(getLeft()) ^ Objects.hashCode(getRight());
        }

        @Override
        public String toString() {
            return "(%s,%s)".formatted(getLeft(), getRight());
        }

        public String toString(final String format) {
            return String.format(format, getLeft(), getRight());
        }
    }

    @Getter
    @FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    @With
    @Builder(toBuilder = true)
    final class Immutable<L, R> extends Pair.Abstract<L, R> {
        private final L left;
        private final R right;

        @Override
        public R setValue(R value) {
            throw new UnsupportedOperationException("Immutable implementation");
        }

        @Override
        public Pair<R, L> swap() {
            return new Immutable<>(right, left);
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @With
    @Builder(toBuilder = true)
    class Mutable<L, R> extends Pair.Abstract<L, R> {
        private L left;
        private R right;

        @Override
        public R setValue(R value) {
            R current = getRight();
            setRight(value);
            return current;
        }

        @Override
        public Pair<R, L> swap() {
            return new Mutable<>(right, left);
        }
    }
}
