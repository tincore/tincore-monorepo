package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.PartialFunction;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Map.Entry;

public interface Matcher {

    static <T> Accepting<T> accepting() {
        return new Accepting<>();
    }

    static <T, R> Applying<T, R> applying() {
        return new Applying<>();
    }

    static <T, R> Getting<T, R> getting() {
        return new Getting<>();
    }

    sealed class AbstractMatcher<T, R, S> {
        private final Map<Predicate<T>, R> valueByPredicate = new LinkedHashMap<>();
        private R otherwise;

        Optional<R> find(T t) {
            return valueByPredicate.entrySet().stream().filter(e -> e.getKey().test(t)).findFirst().map(Entry::getValue).or(() -> Optional.ofNullable(otherwise));
        }

        @SuppressWarnings("unchecked")
        S otherwise(R value) {
            this.otherwise = value;
            return (S) this;
        }

        @SuppressWarnings("unchecked")
        S put(Predicate<T> matchPredicate, R value) {
            valueByPredicate.putIfAbsent(matchPredicate, value);
            return (S) this;
        }
    }

    final class Getting<T, R> extends AbstractMatcher<T, R, Getting<T, R>> implements Function<T, R> {

        private boolean otherwiseSet;

        @Override
        public R apply(T t) {
            R r = find(t).orElse(null);
            if (r != null || otherwiseSet) {
                return r;
            }
            throw new IllegalStateException("Can't resolve case, value=" + t);
        }

        @Override
        Getting<T, R> otherwise(R value) {
            this.otherwiseSet = true;
            return super.otherwise(value);
        }

        public <W extends T> Getting<T, R> when(Class<W> type, R value) {
            return when(type::isInstance, value);
        }

        public Getting<T, R> when(Predicate<T> matchPredicate, R value) {
            put(matchPredicate, value);
            return this;
        }

    }

    final class Accepting<T> extends AbstractMatcher<T, Consumer<? super T>, Accepting<T>> implements Consumer<T> {

        @Override
        public void accept(T t) {
            var consumer = find(t).orElseThrow(() -> new IllegalStateException("Can't resolve case, value=" + t));
            consumer.accept(t);
        }

        public Accepting<T> when(Predicate<T> matchPredicate, Consumer<? super T> value) {
            return put(matchPredicate, value);
        }

        @SuppressWarnings("unchecked")
        public <W extends T> Accepting<T> when(Class<W> type, Consumer<W> value) {
            return when(type::isInstance, t -> value.accept((W) t));
        }
    }

    final class Applying<T, R> extends AbstractMatcher<T, Function<T, R>, Applying<T, R>> implements PartialFunction<T, R> {
        public R apply(T t) {
            return find(t).orElseThrow(() -> new IllegalStateException("Can't resolve case, value=%s".formatted(t))).apply(t);
        }

        @Override
        public boolean isDefinedAt(T t) {
            // Should probably provide its own lift implementation to be more efficient
            return find(t).isPresent();
        }

        public Applying<T, R> when(Predicate<T> matchPredicate, Function<T, R> value) {
            return put(matchPredicate, value);
        }

        @SuppressWarnings("unchecked")
        public <W extends T> Applying<T, R> when(Class<W> type, Function<W, R> value) {
            return when(type::isInstance, f -> value.apply((W) f));
        }
    }

}
