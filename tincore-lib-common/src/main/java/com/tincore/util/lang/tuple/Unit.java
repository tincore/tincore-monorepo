package com.tincore.util.lang.tuple;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Unit<T> extends Tuple<T, Void, Unit<T>> {

    int ARITY = 1;

    static <T> Unit<T> of(final T value) { // NOPMD
        return new Unit.Immutable<>(value);
    }

    static <T> Unit<T> ofIterable(Iterable<T> iterable) {
        return ofIterable(iterable, false);
    }

    static <T> Unit<T> ofIterable(Iterable<T> iterable, boolean autoFill) {
        T v0 = null;

        Iterator<T> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            v0 = iterator.next();
        } else {
            iterator = null;
        }

        if (!autoFill && (iterator == null || iterator.hasNext())) {
            throw new IllegalArgumentException("Invalid iterator length. Expected length=" + ARITY);
        }

        return of(v0);
    }

    static <T> Unit.Mutable<T> ofMutable(final T value) {
        return new Unit.Mutable<>(value);
    }

    default void accept(Consumer<T> consumer) {
        consumer.accept(getValue());
    }

    default <U> U apply(Function<T, U> mapper) {
        return mapper.apply(getValue());
    }

    @Override
    default int arity() {
        return ARITY;
    }

    @Override
    default int compareTo(final Unit<T> other) {
        return new CompareToBuilder().append(getValue(), other.getValue()).toComparison();
    }

    default <U> Unit<U> flatMap(Function<T, Unit<U>> mapper) {
        return mapper.apply(getValue());
    }

    @Override
    default T getHead() {
        return getValue();
    }

    @Override
    default Void getTail() {
        return null;
    }

    T getValue();

    default <U> Unit<U> map(Function<T, U> mapper) {
        return of(mapper.apply(getValue()));
    }

    @Override
    default Stream<T> stream() {
        return Stream.of(getValue());
    }

    @Override
    default Unit<T> withHead(T value) {
        return withValue(value);
    }

    @Override
    default Unit<T> withTail(Void value) {
        return this;
    }

    Unit<T> withValue(T value);

    abstract class Abstract<R> implements Unit<R>, Serializable {

        @Override
        public boolean equals(final Object obj) {
            return obj == this || obj instanceof Unit<?> other && Objects.equals(getValue(), other.getValue());
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(getValue());
        }

        @Override
        public String toString() {
            return "(%s)".formatted(getValue());
        }

        public String toString(final String format) {
            return String.format(format, getValue());
        }
    }

    @Getter
    @FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    @With
    @Builder(toBuilder = true)
    final class Immutable<V> extends Unit.Abstract<V> {
        private final V value;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @With
    @Builder(toBuilder = true)
    class Mutable<V> extends Unit.Abstract<V> {
        private V value;

        @Override
        public <U> Unit<U> map(Function<V, U> mapper) {
            return ofMutable(mapper.apply(getValue()));
        }
    }
}
