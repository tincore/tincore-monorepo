package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface SexiFunction<T, U, V, W, X, Y, R> {
    default <Z> SexiFunction<T, U, V, W, X, Y, Z> andThen(final Function<? super R, ? extends Z> after) {
        Objects.requireNonNull(after);
        return (final T t, final U u, final V v, final W w, X x, Y y) -> after.apply(apply(t, u, v, w, x, y));
    }

    R apply(T t, U u, V v, W w, X x, Y y);
}
