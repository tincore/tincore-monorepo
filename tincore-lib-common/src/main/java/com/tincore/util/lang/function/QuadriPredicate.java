package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface QuadriPredicate<T, U, V, W> {

    QuadriPredicate<?, ?, ?, ?> TAUTOLOGY = (t, u, v, w) -> true;
    QuadriPredicate<?, ?, ?, ?> CONTRADICTION = TAUTOLOGY.negate();

    default QuadriPredicate<T, U, V, W> and(QuadriPredicate<? super T, ? super U, ? super V, ? super W> other) { // NOPMD
        Objects.requireNonNull(other);
        return (T t, U u, V v, W w) -> test(t, u, v, w) && other.test(t, u, v, w);
    }

    default <X, Y, Z, A> QuadriPredicate<X, Y, Z, A> compose(
        Function<? super X, ? extends T> beforeLeft,
        Function<? super Y, ? extends U> beforeMiddleLeft,
        Function<? super Z, ? extends V> beforeMiddleRight,
        Function<? super A, ? extends W> beforeRight) {
        Objects.requireNonNull(beforeLeft);
        Objects.requireNonNull(beforeMiddleLeft);
        Objects.requireNonNull(beforeMiddleRight);
        Objects.requireNonNull(beforeRight);
        return (X x, Y y, Z z, A a) -> test(beforeLeft.apply(x), beforeMiddleLeft.apply(y), beforeMiddleRight.apply(z), beforeRight.apply(a));
    }

    default <X> QuadriPredicate<X, U, V, W> composeLeft(Function<? super X, ? extends T> before) {
        Objects.requireNonNull(before);
        return (X x, U u, V v, W w) -> test(before.apply(x), u, v, w);
    }

    default <X> QuadriPredicate<T, X, V, W> composeMiddleLeft(Function<? super X, ? extends U> before) {
        Objects.requireNonNull(before);
        return (T t, X x, V v, W w) -> test(t, before.apply(x), v, w);
    }

    default <X> QuadriPredicate<T, U, X, W> composeMiddleRight(Function<? super X, ? extends V> before) {
        Objects.requireNonNull(before);
        return (T t, U u, X x, W w) -> test(t, u, before.apply(x), w);
    }

    default <X> QuadriPredicate<T, U, V, X> composeRight(Function<? super X, ? extends W> before) {
        Objects.requireNonNull(before);
        return (T t, U u, V v, X x) -> test(t, u, v, before.apply(x));
    }

    default QuadriPredicate<T, U, V, W> negate() {
        return (T t, U u, V v, W w) -> !test(t, u, v, w);
    }

    default QuadriPredicate<T, U, V, W> or(QuadriPredicate<? super T, ? super U, ? super V, ? super W> other) { // NOPMD
        Objects.requireNonNull(other);
        return (T t, U u, V v, W w) -> test(t, u, v, w) || other.test(t, u, v, w);
    }

    boolean test(T t, U u, V v, W w);
}
