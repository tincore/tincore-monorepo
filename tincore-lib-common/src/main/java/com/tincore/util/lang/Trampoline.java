package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public sealed interface Trampoline<T> {
    static <T, U> BiFunction<T, U, Trampoline<T>> builder(BiFunction<? super T, ? super U, ? extends Map.Entry<T, U>> next, BiPredicate<? super T, ? super U> completedWhen) {
        return (t, u) -> recurse(t, u, next, completedWhen);
    }

    static <T> Function<T, Trampoline<T>> builder(Function<? super T, ? extends T> next, Predicate<? super T> completedWhen) {
        return t -> recurse(t, next, completedWhen);
    }

    static <T> Trampoline<T> complete(T value) {
        return new Complete<>(value);
    }

    static <T> Trampoline<T> next(Supplier<Trampoline<T>> trampolineSupplier) {
        return new Next<>(trampolineSupplier);
    }

    static <T> Trampoline<T> next(T value, Supplier<Trampoline<T>> trampolineSupplier) {
        return new Next<>(value, trampolineSupplier);
    }

    static <T> Trampoline<T> next(T value, Function<? super T, Trampoline<T>> trampolineMapper) {
        return next(value, () -> trampolineMapper.apply(value));
    }

    static <T> Trampoline<T> recurse(T seed, Function<? super T, ? extends T> next, Predicate<? super T> completedWhen) {
        if (completedWhen.test(seed)) {
            return complete(seed);
        }

        return next(seed, () -> recurse(next.apply(seed), next, completedWhen));
    }

    static <T, C> Trampoline<T> recurse(T seed, C control, BiFunction<? super T, ? super C, ? extends Map.Entry<? extends T, ? extends C>> next, BiPredicate<? super T, ? super C> completedWhen) {
        if (completedWhen.test(seed, control)) {
            return complete(seed);
        }

        return next(seed, () -> {
            var nextValues = next.apply(seed, control);
            return recurse(nextValues.getKey(), nextValues.getValue(), next, completedWhen);
        });
    }

    static <T, C> Trampoline<T> recurseUnseeded(C control, Function<? super C, ? extends Map.Entry<? extends T, ? extends C>> next, Predicate<? super C> completedWhen) {
        return recurseUnseeded(control, (v, c) -> next.apply(c), (v, c) -> completedWhen.test(c));
    }

    static <T, C> Trampoline<T> recurseUnseeded(C control, BiFunction<? super T, ? super C, ? extends Map.Entry<? extends T, ? extends C>> next, BiPredicate<? super T, ? super C> completedWhen) {
        return recurseUnseeded(c -> next.apply(null, c), control, next, completedWhen);
    }

    static <T, C> Trampoline<T> recurseUnseeded(
        Function<? super C, ? extends Map.Entry<? extends T, ? extends C>> seedInitializer,
        C control,
        BiFunction<? super T, ? super C, ? extends Map.Entry<? extends T, ? extends C>> next,
        BiPredicate<? super T, ? super C> completedWhen) {
        var nextValues = seedInitializer.apply(control);
        return recurse(nextValues.getKey(), nextValues.getValue(), next, completedWhen);
    }

    Trampoline<T> bounce();

    T get();

    boolean isComplete();

    default T reduce() {
        var trampoline = this;

        while (!trampoline.isComplete()) {
            trampoline = trampoline.bounce();
        }

        return trampoline.get();
    }

    /**
     * Returns stream of intermediate values
     *
     * @return
     */
    default Stream<T> runningReduce() {
        return scan();
    }

    default Stream<T> scan() {
        return stream().map(Trampoline::get);
    }

    default Stream<Trampoline<T>> stream() { // NOPMD
        return StreamSupport.stream(new Spliterators.AbstractSpliterator<>(Long.MAX_VALUE, Spliterator.ORDERED | Spliterator.IMMUTABLE) {
            private Trampoline<T> prev;
            private boolean started;
            private boolean completeEmitted;
            private boolean finished;

            @Override
            public void forEachRemaining(Consumer<? super Trampoline<T>> action) {
                Objects.requireNonNull(action);
                if (finished) {
                    return;
                }
                finished = true;
                Trampoline<T> t = started ? getNext(prev) : Trampoline.this;
                prev = null;
                while (hasNext(t)) {
                    action.accept(t);
                    t = getNext(t);
                }
            }

            private Trampoline<T> getNext(Trampoline<T> trampoline) {
                if (trampoline.isComplete() && !completeEmitted) {
                    completeEmitted = true;
                    return trampoline;
                }

                return trampoline.bounce();
            }

            private boolean hasNext(Trampoline<T> trampoline) {
                return !completeEmitted || !trampoline.isComplete();
            }

            @Override
            public boolean tryAdvance(Consumer<? super Trampoline<T>> action) {
                Objects.requireNonNull(action);
                if (finished) {
                    return false;
                }
                Trampoline<T> t;
                if (started) {
                    t = getNext(prev);
                } else {
                    t = Trampoline.this;
                    started = true;
                }
                if (!hasNext(t)) {
                    prev = null;
                    finished = true;
                    return false;
                }
                prev = t;
                action.accept(prev);
                return true;
            }
        }, false);
    }

    abstract sealed class AbstractTrampoline<T> implements Trampoline<T> {
        private final T value;

        protected AbstractTrampoline(T value) {
            this.value = value;
        }

        @Override
        public T get() {
            return value;
        }
    }

    final class Complete<T> extends AbstractTrampoline<T> {

        private Complete(T value) {
            super(value);
        }

        @Override
        public Trampoline<T> bounce() {
            return this;
        }

        @Override
        public boolean isComplete() {
            return true;
        }
    }

    final class Next<T> extends AbstractTrampoline<T> {

        private final Supplier<Trampoline<T>> trampolineSupplier;

        private Next(T value, Supplier<Trampoline<T>> trampolineSupplier) {
            super(value);
            this.trampolineSupplier = trampolineSupplier;
        }

        private Next(Supplier<Trampoline<T>> trampolineSupplier) {
            this(null, trampolineSupplier);
        }

        public Trampoline<T> bounce() {
            return trampolineSupplier.get();
        }

        @Override
        public boolean isComplete() {
            return false;
        }
    }
}
