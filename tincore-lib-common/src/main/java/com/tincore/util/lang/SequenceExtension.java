package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.Matcher.Applying;
import com.tincore.util.lang.function.ThrowingFunction;
import com.tincore.util.lang.function.ThrowingPredicate;
import com.tincore.util.lang.function.TriConsumer;
import com.tincore.util.lang.function.TriFunction;
import com.tincore.util.lang.tuple.Pair;
import com.tincore.util.lang.tuple.Triple;
import com.tincore.util.lang.tuple.TupleExtension;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.*;
import java.util.stream.*;
import java.util.stream.Collector.Characteristics;

import static com.tincore.util.lang.Either.tryEither;
import static com.tincore.util.lang.function.FunctionExtension.combine;
import static com.tincore.util.lang.function.FunctionExtension.product;
import static com.tincore.util.lang.function.ThrowingFunction.uFunction;
import static com.tincore.util.lang.function.ThrowingPredicate.uPredicate;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;

public class SequenceExtension { // NOPMD No god

    private static final long FIND_LAST_LIMIT = Integer.MAX_VALUE;

    public static <T> boolean all(Collection<T> collection, Predicate<? super T> predicate) {
        return streamEach(collection).allMatch(predicate);
    } // NOPMD

    public static <T> boolean all(T[] array, Predicate<? super T> predicate) {
        return streamEach(array).allMatch(predicate);
    } // NOPMD

    public static <T> boolean any(Collection<T> collection, Predicate<? super T> predicate) { // NOPMD
        return streamEach(collection).anyMatch(predicate);
    }

    public static <T> boolean any(T[] array, Predicate<? super T> predicate) { // NOPMD
        return streamEach(array).anyMatch(predicate);
    }

    @SuppressWarnings("unchecked")
    public static <R, T extends R> List<R> asContravariant(List<T> list, Class<R> type) {
        return (List<R>) list;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> asContravariant(List<? extends T> list) {
        // Relatively dangerous if list is mutable. Think about wrapping in immutable
        return (List<T>) list;
    }

    @SuppressWarnings("unchecked")
    public static <R, T extends R> Stream<R> asContravariant(Stream<T> stream, Class<R> type) {
        return (Stream<R>) stream;
    }

    @SuppressWarnings("unchecked")
    public static <T> Stream<T> asContravariant(Stream<? extends T> stream) {
        return (Stream<T>) stream;
    }

    @SuppressWarnings({"unchecked", "OptionalUsedAsFieldOrParameterType"})
    public static <T> Optional<T> asContravariant(Optional<? extends T> optional) { // NOPMD
        return (Optional<T>) optional;
    }

    @SuppressWarnings("unchecked")
    public static <K, V> List<Entry<K, V>> asContravariantEntry(List<? extends Entry<? extends K, ? extends V>> list) {
        return (List<Entry<K, V>>) list;
    }

    @SuppressWarnings("unchecked")
    public static <K, V> Stream<Entry<K, V>> asContravariantEntry(Stream<? extends Entry<? extends K, ? extends V>> stream) {
        return (Stream<Entry<K, V>>) stream;
    }

    public static <T, U, V> Stream<Pair<T, U>> associate(Stream<T> stream, Stream<U> otherStream, Function<T, V> keyExtractor, Function<U, V> otherKeyExtractor) {
        var associatedBy = toMap(otherStream, otherKeyExtractor, s -> s);
        return stream.map(p -> Pair.of(p, associatedBy.get(keyExtractor.apply(p))));
    }

    @SuppressWarnings("raw")
    public static <T, U, V> List<Entry<T, U>> associate(Collection<T> collection, Collection<U> otherCollection, Function<T, V> keyExtractor, Function<U, V> otherKeyExtractor) {
        var associatedBy = toMap(otherCollection, otherKeyExtractor, s -> s);
        return asContravariantEntry(streamEach(collection).map(p -> Pair.of(p, associatedBy.get(keyExtractor.apply(p))))).toList();
    }

    public static <T> Stream<Stream<T>> chunked(Stream<T> stream, int chunkSize) {
        return windowed(stream, chunkSize, chunkSize, true);
    }

    public static <T> List<List<T>> chunked(Collection<T> collection, int chunkSize) {
        return windowed(collection, chunkSize, chunkSize, true);
    }

    @SafeVarargs
    public static <T> Stream<T> concat(Stream<? extends T>... steams) {
        return Stream.of(steams).flatMap(s -> s);
    }

    public static <T> Stream<T> concatWith(Stream<? extends T> stream, Collection<? extends T> collection) {
        return Stream.concat(stream, collection.stream());
    }

    @SafeVarargs
    public static <T> Stream<T> concatWith(Stream<? extends T> stream, Stream<? extends T>... steams) {
        return Stream.concat(stream, Stream.of(steams).flatMap(s -> s));
    }

    @SafeVarargs
    public static <T> Stream<T> concatWith(Stream<? extends T> stream, T... items) {
        return Stream.concat(stream, Stream.of(items));
    }

    public static <T> List<T> difference(Collection<T> collection, Collection<T> otherCollection) {
        return difference(collection, otherCollection, Function.identity());
    }

    public static <T, V> List<T> difference(Collection<T> collection, Collection<T> otherCollection, Function<T, V> keyExtractor) {
        return difference(collection, otherCollection, keyExtractor, keyExtractor);
    }

    public static <T, U, V> List<T> difference(Collection<T> collection, Collection<U> otherCollection, Function<T, V> keyExtractor, Function<U, V> otherKeyExtractor) {
        return difference(streamEach(collection), streamEach(otherCollection), keyExtractor, otherKeyExtractor).toList();
    }

    public static <T, U, V> Stream<T> difference(Stream<T> stream, Stream<U> otherStream, Function<T, V> keyExtractor, Function<U, V> otherKeyExtractor) {
        var set = toSet(otherStream, otherKeyExtractor);
        return stream.filter(m -> !set.contains(keyExtractor.apply(m)));
    }

    public static <T> Stream<T> distinctBy(Stream<T> stream, Predicate<? super T> predicate) {
        return stream.filter(predicate);
    }

    public static <T> List<T> distinctBy(Collection<T> collection, Predicate<? super T> predicate) {
        return distinctBy(streamEach(collection), predicate).toList();
    }

    public static <T> Stream<T> distinctByKey(Stream<T> stream, Function<? super T, ?> extractor) {
        return distinctBy(stream, Distinctors.extracting(extractor));
    }

    public static <T> List<T> distinctByKey(Collection<T> collection, Function<? super T, ?> extractor) {
        return distinctByKey(streamEach(collection), extractor).toList();
    }

    public static <K, V, E extends Entry<K, V>> Stream<E> filterEntry(Stream<E> entryStream, BiPredicate<? super K, ? super V> biPredicate) {
        return entryStream.filter(e -> biPredicate.test(e.getKey(), e.getValue()));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V, E extends Entry<K, V>> Optional<E> filterEntry(Optional<E> entryOptional, BiPredicate<? super K, ? super V> biPredicate) {
        return entryOptional.filter(e -> biPredicate.test(e.getKey(), e.getValue()));
    }

    public static <K, V> Map<K, V> filterEntry(Map<K, V> map, BiPredicate<? super K, ? super V> biPredicate) {
        return toMap(filterEntry(streamEntries(map), biPredicate));
    }

    public static <T> Stream<T> filterIndexed(Stream<T> stream, BiPredicate<? super T, Integer> predicate) {
        return zipWithIndex(stream).filter(e -> predicate.test(e.getKey(), e.getValue())).map(Entry::getKey);
    }

    @SuppressWarnings("unchecked")
    public static <T> Stream<T> filterInstanceOf(Stream<?> stream, Class<T> type) {
        return stream.filter(o -> type.isAssignableFrom(o.getClass())).map(o -> (T) o);
    }

    public static <T> Optional<T> filterInstanceOf(Optional<?> optional, Class<T> type) {
        return optional.filter(o -> type.isAssignableFrom(o.getClass())).map(o -> (T) o);
    }

    public static <K, V> Map<K, V> filterKey(Map<K, V> map, Predicate<? super K> keyPredicate) {
        return toMap(filterKey(streamEntries(map), keyPredicate));
    }

    public static <K, V, E extends Entry<K, V>> Stream<E> filterKey(Stream<E> entryStream, Predicate<? super K> keyPredicate) {
        return entryStream.filter(e -> keyPredicate.test(e.getKey()));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V, E extends Entry<K, V>> Optional<E> filterKey(Optional<E> optional, Predicate<? super K> keyPredicate) {
        return optional.filter(e -> keyPredicate.test(e.getKey()));
    }

    @SuppressWarnings("unchecked")
    public static <K, V, R> Stream<Entry<R, V>> filterKeyInstanceOf(Stream<? extends Entry<? extends K, ? extends V>> entryStream, Class<R> type) {
        return entryStream.filter(Objects::nonNull)
            .filter(e -> type.isAssignableFrom(e.getKey().getClass()))
            .map(e -> Pair.of((R) e.getKey(), e.getValue()));
    }

    public static <T> Stream<T> filterNonNull(Stream<T> stream) {
        return stream.filter(Objects::nonNull);
    }

    public static <T, U> Stream<T> filterNonNull(Stream<T> stream, Function<? super T, ? extends U> extractor) {
        return stream.filter(v -> extractor.apply(v) != null);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, U> Optional<T> filterNonNull(Optional<T> optional, Function<? super T, ? extends U> extractor) {
        return optional.filter(v -> extractor.apply(v) != null);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, X extends Throwable> Optional<T> filterTry(Optional<T> optional, ThrowingPredicate<T, ? extends X> throwingPredicate) {
        return filterTry(optional, throwingPredicate, ExceptionMapper.wrapChecked());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, X extends Throwable, Y extends RuntimeException> Optional<T> filterTry(Optional<T> optional, ThrowingPredicate<T, ? extends X> throwingPredicate, ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        return optional.filter(uPredicate(throwingPredicate, exceptionMapper));
    }

    public static <T, X extends Throwable> Stream<T> filterTry(Stream<T> stream, ThrowingPredicate<T, ? extends X> throwingPredicate) {
        return filterTry(stream, throwingPredicate, ExceptionMapper.wrapChecked());
    }

    public static <T, X extends Throwable, Y extends RuntimeException> Stream<T> filterTry(Stream<T> stream, ThrowingPredicate<T, ? extends X> throwingPredicate, ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        return stream.filter(uPredicate(throwingPredicate, exceptionMapper));
    }

    public static <T> Stream<T> filterUnless(Stream<T> stream, Predicate<? super T> predicate) {
        return stream.filter(predicate.negate());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> Optional<T> filterUnless(Optional<T> optional, Predicate<? super T> predicate) {
        return optional.filter(predicate.negate());
    }

    public static <T, U> Stream<T> filterUnlessWhen(Stream<T> stream, Function<? super T, ? extends U> extractor, Predicate<? super U> predicate) {
        return filterWhen(stream, extractor, predicate.negate());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, U> Optional<T> filterUnlessWhen(Optional<T> optional, Function<? super T, ? extends U> extractor, Predicate<? super U> predicate) {
        return filterWhen(optional, extractor, predicate.negate());
    }

    public static <K, V> Map<K, V> filterValue(Map<K, V> map, Predicate<? super V> valuePredicate) {
        return toMap(filterValue(streamEntries(map), valuePredicate));
    }

    public static <K, V, E extends Entry<K, V>> Stream<E> filterValue(Stream<E> entryStream, Predicate<? super V> valuePredicate) {
        return entryStream.filter(e -> valuePredicate.test(e.getValue()));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V, E extends Entry<K, V>> Optional<E> filterValue(Optional<E> optional, Predicate<? super V> valuePredicate) {
        return optional.filter(e -> valuePredicate.test(e.getValue()));
    }

    @SuppressWarnings("unchecked")
    public static <K, V, R> Stream<Entry<K, R>> filterValueInstanceOf(Stream<? extends Entry<? extends K, ? extends V>> entryStream, Class<R> type) {
        var stream = entryStream.filter(Objects::nonNull).filter(e -> type.isAssignableFrom(e.getValue().getClass()));
        return mapValue(stream, v -> (R) v);
    }

    public static <K, V> Map<K, V> filterValueUnless(Map<K, V> map, Predicate<? super V> valuePredicate) {
        return filterValue(map, valuePredicate.negate());
    }

    public static <K, V, E extends Entry<K, V>> Stream<E> filterValueUnless(Stream<E> entryStream, Predicate<? super V> valuePredicate) {
        return filterValue(entryStream, valuePredicate.negate());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V, E extends Entry<K, V>> Optional<E> filterValueUnless(Optional<E> optional, Predicate<? super V> valuePredicate) {
        return optional.filter(e -> valuePredicate.negate().test(e.getValue()));
    }

    public static <T, U> Stream<T> filterWhen(Stream<T> stream, Function<? super T, ? extends U> extractor, Predicate<? super U> predicate) {
        return stream.filter(v -> predicate.test(extractor.apply(v)));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, U> Optional<T> filterWhen(Optional<T> optional, Function<? super T, ? extends U> extractor, Predicate<? super U> predicate) {
        return optional.filter(v -> predicate.test(extractor.apply(v)));
    }

    public static <K, V> Optional<V> find(Map<K, V> map, K key) {
        return Optional.ofNullable(map).map(m -> m.get(key));
    }

    // Add when needed
    public static <T> Optional<T> findFirst(List<T> list) {
        return Optional.ofNullable(list).filter(l -> !l.isEmpty()).map(SequenceExtension::first);
    }

    public static <T> Optional<T> findFirst(T[] array) { // NOPMD No varargs
        return Optional.ofNullable(array).filter(a -> a.length > 0).map(SequenceExtension::first);
    }

    public static <T> Optional<T> findFirst(Stream<T> stream) {
        return stream.findFirst();
    }

    public static <T> Optional<T> findFirst(Stream<T> stream, Predicate<? super T> predicate) {
        return findFirst(stream.filter(predicate));
    }

    public static <T> Optional<T> findFirst(T[] array, Predicate<? super T> predicate) {
        return findFirst(streamEach(array), predicate);
    }

    public static <T> Optional<T> findFirst(Collection<T> collection, Predicate<? super T> predicate) {
        return findFirst(streamEach(collection), predicate);
    }

    public static <K, V> Optional<Entry<K, V>> findFirst(Map<K, V> map, Predicate<? super V> valuePredicate) {
        return filterValue(streamEntries(map), valuePredicate).findFirst();
    }

    public static <K, V> Optional<Entry<K, V>> findFirst(Map<K, V> map, BiPredicate<? super K, ? super V> biPredicate) {
        return filterEntry(streamEntries(map), biPredicate).findFirst();
    }

    public static <T> Optional<T> findFirstNonNull(Stream<T> stream) {
        return filterNonNull(stream).findFirst();
    }

    public static <T> Optional<T> findFirstPresent(Stream<Optional<T>> stream) {
        return mapPresent(stream).findFirst();
    }

    public static <T, R> Optional<R> findFirstPresent(Stream<T> stream, Function<? super T, Optional<R>> mapper) {
        return findFirstPresent(stream.map(mapper));
    }

    public static <T> Optional<T> findFirstPresent(Collection<Optional<T>> collection) {
        return findFirstPresent(collection.stream());
    }

    public static <T, R> Optional<R> findFirstPresent(Collection<T> collection, Function<? super T, Optional<R>> mapper) {
        return findFirstPresent(collection.stream(), mapper);
    }

    public static <T> Optional<T> findLast(List<T> list) {
        return Optional.ofNullable(list).filter(l -> !l.isEmpty()).map(SequenceExtension::last);
    }

    /**
     * Returns stream last element if stream not empty. empty otherwise.
     * <p>
     * Infinite streams are capped at Integer.MAX_VALUE
     *
     * @param stream
     * @param <T>
     * @return last element
     */
    public static <T> Optional<T> findLast(Stream<T> stream) {
        return stream.limit(FIND_LAST_LIMIT).reduce((a, b) -> b);
    }

    public static <T> T first(List<T> list) {
        return list.get(0);
    }

    public static <T> T first(T[] array) {  // NOPMD No varargs
        return array[0];
    }

    public static <T, U, R> Stream<R> flatMapComprehension(Stream<T> stream, Function<? super T, ? extends Stream<? extends U>> mapper, BiFunction<? super T, ? super U, ? extends Stream<? extends R>> mapper2) {
        return stream.flatMap(s -> mapper.apply(s).flatMap(s2 -> mapper2.apply(s, s2)));
    }

    public static <T, U, R> Stream<R> flatMapMap(Stream<T> stream, Function<? super T, ? extends Stream<? extends U>> mapper, BiFunction<? super T, ? super U, ? extends R> mapper2) {
        return stream.flatMap(s -> mapper.apply(s).map(s2 -> mapper2.apply(s, s2)));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, R, X extends Throwable, Y extends RuntimeException>
        Optional<R>
        flatMapTry(Optional<T> optional, ThrowingFunction<? super T, Optional<R>, ? extends X> throwingMapper, ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        return optional.flatMap(uFunction(throwingMapper, exceptionMapper));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, R, X extends Throwable> Optional<R> flatMapTry(Optional<T> optional, ThrowingFunction<? super T, Optional<R>, ? extends X> throwingMapper) {
        return flatMapTry(optional, throwingMapper, ExceptionMapper.wrapChecked());
    }

    public static <T, R, X extends Throwable, Y extends RuntimeException> Stream<R> flatMapTry(Stream<T> stream, ThrowingFunction<? super T, Stream<R>, ? extends X> throwingMapper, ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        return stream.flatMap(uFunction(throwingMapper, exceptionMapper));
    }

    public static <T, R, X extends Throwable> Stream<R> flatMapTry(Stream<T> stream, ThrowingFunction<? super T, Stream<R>, ? extends X> throwingMapper) {
        return flatMapTry(stream, throwingMapper, ExceptionMapper.wrapChecked());
    }

    public static <T> Stream<T> flatten(Stream<? extends Collection<T>> stream) {
        return stream.flatMap(Collection::stream);
    }

    public static <T> List<T> flatten(Collection<? extends Collection<T>> collections) {
        return flatten(collections.stream()).toList();
    }

    public static <T, R> Stream<R> flatten(Stream<T> stream, Function<? super T, ? extends Collection<R>> mapper) {
        return stream.flatMap(t -> streamEach(mapper.apply(t)));
    }

    public static <T, R> List<R> flatten(Collection<T> collection, Function<? super T, ? extends Collection<R>> mapper) {
        return flatten(streamEach(collection), mapper).toList();
    }

    public static <T, R> Stream<R> flattenAll(Stream<T> stream, Function<? super T, R[]> mapper) {
        return stream.flatMap(t -> streamEach(mapper.apply(t)));
    }

    public static <K, V> Stream<V> flattenValues(Stream<Map<K, V>> stream) {
        return stream.flatMap(SequenceExtension::streamValues);
    }

    public static <T, K, V> Stream<V> flattenValues(Stream<T> stream, Function<T, Map<K, V>> mapMapper) {
        return flattenValues(stream.map(mapMapper));
    }

    public static <T, R> R fold(Collection<T> collection, R identity, BiFunction<R, ? super T, R> accumulator) {
        return fold(streamEach(collection), identity, accumulator);
    }

    public static <T, R> R fold(Stream<T> stream, R identity, BiFunction<R, ? super T, R> accumulator) {
        return stream.reduce(identity,
            accumulator,
            (a0, a1) -> {
                throw new IllegalStateException("ParallelUnsupported");
            });
    }

    public static <T, U> void forEachComprehension(Stream<T> stream, Function<? super T, ? extends U> mapper, BiConsumer<? super T, ? super U> consumer) {
        stream.forEach(t -> consumer.accept(t, mapper.apply(t)));
    }

    public static <T, U, V> void forEachComprehension(Stream<T> stream, Function<? super T, ? extends U> mapper1, BiFunction<? super T, ? super U, ? extends V> mapper2, TriConsumer<? super T, ? super U, ? super V> consumer) {
        stream.forEach(t -> {
            U u = mapper1.apply(t);
            V v = mapper2.apply(t, u);
            consumer.accept(t, u, v);
        });
    }

    public static <T, U> void forEachComprehension(Iterable<T> iterable, Function<? super T, ? extends U> mapper, BiConsumer<? super T, ? super U> consumer) {
        iterable.forEach(t -> consumer.accept(t, mapper.apply(t)));
    }

    public static <T, U, V> void forEachComprehension(Iterable<T> iterable, Function<? super T, ? extends U> mapper1, BiFunction<? super T, ? super U, ? extends V> mapper2, TriConsumer<? super T, ? super U, ? super V> consumer) {
        iterable.forEach(t -> {
            U u = mapper1.apply(t);
            V v = mapper2.apply(t, u);
            consumer.accept(t, u, v);
        });
    }

    public static <K, V> void forEachEntry(Stream<? extends Entry<? extends K, ? extends V>> entryStream, BiConsumer<? super K, ? super V> biConsumer) {
        entryStream.forEach(e -> biConsumer.accept(e.getKey(), e.getValue()));
    }

    public static <K, V> void forEachEntry(Collection<? extends Entry<? extends K, ? extends V>> entryCollection, BiConsumer<? super K, ? super V> biConsumer) {
        entryCollection.forEach(e -> biConsumer.accept(e.getKey(), e.getValue()));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V> void forEachEntry(Optional<Map<K, V>> optional, BiConsumer<? super K, ? super V> biConsumer) {
        streamEntries(optional).forEach(e -> biConsumer.accept(e.getKey(), e.getValue()));
    }

    public static <K, V> void forEachEntry(Stream<Entry<K, V>> entryStream, boolean parallel, BiConsumer<? super K, ? super V> biConsumer) {
        (parallel ? entryStream.toList()
            .stream()
            .parallel() : entryStream).forEach(e -> biConsumer.accept(e.getKey(), e.getValue()));
    }

    public static <T> void forEachIndexed(Collection<T> collection, BiConsumer<? super T, Integer> biConsumer) {
        forEachIndexed(collection, false, biConsumer);
    }

    public static <T> void forEachIndexed(Collection<T> collection, boolean parallel, BiConsumer<? super T, Integer> biConsumer) {
        forEachEntry(zipWithIndex(streamEach(collection)), parallel, biConsumer);
    }

    public static <T> void forEachIndexed(Stream<T> stream, BiConsumer<? super T, Integer> biConsumer) {
        zipWithIndex(stream).forEach(e -> biConsumer.accept(e.getKey(), e.getValue()));
    }

    public static <T, K> Map<K, List<T>> groupBy(Collection<T> collection, Function<? super T, ? extends K> classifier) {
        return groupBy(streamEach(collection), classifier);
    }

    public static <T, K> Map<K, List<T>> groupBy(Iterable<T> iterable, Function<? super T, ? extends K> classifier) {
        return groupBy(streamEach(iterable), classifier);
    }

    public static <T, K> Map<K, List<T>> groupBy(T[] array, Function<? super T, ? extends K> classifier) {
        return groupBy(Stream.of(array), classifier);
    }

    public static <T, K, V> Map<K, List<V>> groupBy(Collection<? extends T> collection, Function<T, K> classifier, Function<T, V> valueMapper) {
        return groupBy(streamEach(collection), classifier, valueMapper);
    }

    public static <T, K, V> Map<K, List<V>> groupBy(Stream<? extends T> stream, Function<T, K> classifier, Function<T, V> valueMapper) {
        return stream.collect(groupingBy(classifier, Collectors.mapping(valueMapper, Collectors.toList())));
    }

    public static <T, K> Map<K, List<T>> groupBy(Stream<T> stream, Function<? super T, ? extends K> classifier) {
        return stream.collect(groupingBy(classifier));
    }

    public static <T, K, V> Map<K, List<V>> groupByIdentity(Collection<? extends T> collection, Function<T, K> classifier, Function<T, V> valueMapper) {
        return streamEach(collection).collect(groupingBy(classifier, IdentityHashMap::new, Collectors.mapping(valueMapper, Collectors.toList())));
    }

    public static <E extends Entry<K, V>, K, V> Map<K, List<V>> groupByKey(Stream<E> stream) {
        return groupBy(stream, Entry::getKey, Entry::getValue);
    }

    public static <T> List<T> head(List<T> list) {
        return list.subList(0, 1);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> void ifEmpty(Optional<T> optional, Runnable emptyAction) {
        optional.ifPresentOrElse(t -> { // Do nothing
        }, emptyAction);
    }

    public static <T, U, V> List<Pair<T, U>> intersection(Collection<T> collection, Collection<U> otherCollection, Function<T, V> keyExtractor, Function<U, V> otherKeyExtractor) {
        return intersection(streamEach(collection), streamEach(otherCollection), keyExtractor, otherKeyExtractor).toList();
    }

    private static <T, U, V> Stream<Pair<T, U>> intersection(Stream<T> stream, Stream<U> otherStream, Function<T, V> keyExtractor, Function<U, V> otherKeyExtractor) {
        var map = groupBy(otherStream, otherKeyExtractor);
        return stream.flatMap(t -> streamEach(map.get(keyExtractor.apply(t))).map(u -> Pair.of(t, u)));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> boolean isPresent(Optional<T> optional, Predicate<? super T> predicate) {
        return optional.filter(predicate).isPresent();
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> boolean isPresent(Optional<T> optional) {
        return optional.isPresent();
    }

    public static String joinWith(CharSequence[] array, String delimiter) {
        return array == null ? null : String.join(delimiter, array);
    }

    public static String joinWith(Iterable<? extends CharSequence> iterable, String delimiter) {
        return iterable == null ? null : String.join(delimiter, iterable);
    }

    public static String joinWith(Iterable<? extends CharSequence> iterable) {
        return joinWith(iterable, "");
    }

    public static String joinWith(Collection<? extends CharSequence> collection, CharSequence delimiter, CharSequence prefix, CharSequence suffix) {
        return joinWith(streamEach(collection), delimiter, prefix, suffix);
    }

    public static String joinWith(Stream<? extends CharSequence> stream, CharSequence delimiter) {
        return stream.collect(joining(delimiter));
    }

    public static String joinWith(Stream<? extends CharSequence> stream, CharSequence delimiter, CharSequence prefix, CharSequence suffix) {
        return stream.collect(joining(delimiter, prefix, suffix));
    }

    public static String joinWith(Stream<? extends CharSequence> stream) {
        return stream.collect(joining());
    }

    public static <T> T last(List<T> list) {
        return list.get(list.size() - 1);
    }

    public static <T> T last(T[] array) {  // NOPMD No varargs
        return array[array.length - 1];
    }

    public static <T, U, R> Stream<R> mapComprehension(Stream<T> stream, Function<? super T, ? extends U> mapper1, BiFunction<? super T, ? super U, ? extends R> mapper2) {
        return stream.map(t -> mapper2.apply(t, mapper1.apply(t)));
    }

    public static <T, U, V, R>
        Stream<R>
        mapComprehension(Stream<T> stream, Function<? super T, ? extends U> mapper1, BiFunction<? super T, ? super U, ? extends V> mapper2, TriFunction<? super T, ? super U, ? super V, ? extends R> mapper3) {
        return stream.map(t -> {
            U u = mapper1.apply(t);
            V v = mapper2.apply(t, u);
            return mapper3.apply(t, u, v);
        });
    }

    public static <K, V, R> Applicative<R> mapEntry(Applicative.Value<? extends Entry<? extends K, ? extends V>> entryApplicative, BiFunction<? super K, ? super V, ? extends R> biFunction) {
        return entryApplicative.map(e -> biFunction.apply(e.getKey(), e.getValue()));
    }

    public static <K, V, L, R> Either<L, R> mapEntry(Either<L, ? extends Entry<? extends K, ? extends V>> entryEither, BiFunction<? super K, ? super V, ? extends R> biFunction) {
        return entryEither.map(e -> biFunction.apply(e.getKey(), e.getValue()));
    }

    public static <K, V, R> Stream<R> mapEntry(Stream<? extends Entry<? extends K, ? extends V>> entryStream, BiFunction<? super K, ? super V, ? extends R> biFunction) {
        return entryStream.map(e -> biFunction.apply(e.getKey(), e.getValue()));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V, R> Optional<R> mapEntry(Optional<? extends Entry<? extends K, ? extends V>> entryStream, BiFunction<? super K, ? super V, ? extends R> biFunction) {
        return entryStream.map(e -> biFunction.apply(e.getKey(), e.getValue()));
    }

    /**
     * Bimap
     *
     * @param entryStream
     * @param keyMapper
     * @param valueMapper
     * @param <K>
     * @param <V>
     * @param <U>
     * @param <W>
     * @return
     */
    public static <K, V, U, W> Stream<Pair<U, W>> mapEntryToPair(Stream<? extends Entry<? extends K, ? extends V>> entryStream, Function<? super K, ? extends U> keyMapper, Function<? super V, ? extends W> valueMapper) {
        return mapEntry(entryStream, combine(keyMapper, valueMapper));
    }

    public static <L, K, V, U, W> Either<L, Pair<U, W>> mapEntryToPair(Either<L, ? extends Entry<? extends K, ? extends V>> entryEither, Function<? super K, ? extends U> keyMapper, Function<? super V, ? extends W> valueMapper) {
        return mapEntry(entryEither, combine(keyMapper, valueMapper));
    }

    public static <T, K> Stream<Entry<K, List<T>>> mapGroupingBy(Stream<T> stream, Function<? super T, ? extends K> classifier) {
        return streamEntries(groupBy(stream, classifier));
    }

    public static <T, R> Stream<R> mapIndexed(Stream<T> stream, BiFunction<? super T, Integer, ? extends R> mapper) {
        return zipWithIndex(stream).map(e -> mapper.apply(e.getKey(), e.getValue()));
    }

    public static <K, V, R> Stream<Entry<R, V>> mapKey(Stream<? extends Entry<? extends K, ? extends V>> entryStream, Function<? super K, ? extends R> function) {
        return entryStream.map(e -> Pair.of(function.apply(e.getKey()), e.getValue()));
    }

    public static <K, V> Stream<K> mapKey(Stream<? extends Entry<? extends K, ? extends V>> entryStream) {
        return entryStream.map(Entry::getKey);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V> Optional<K> mapKey(Optional<? extends Entry<? extends K, ? extends V>> optionalEntry) {
        return optionalEntry.map(Entry::getKey);
    }

    public static <T, R> StreamMatcherBuilder<T, R> mapMatcher(Stream<T> stream, Class<R> type) {
        return new StreamMatcherBuilder<>(stream);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, R> Optional<R> mapMatcher(Optional<T> option, Applying<T, R> applying) {
        return option.map(applying);
    }

    public static <T, R> Stream<R> mapMatcher(Stream<T> stream, Applying<T, R> applying) {
        return stream.map(applying);
    }

    public static <T, R> Stream<R> mapNonNull(Stream<? extends T> stream, Function<? super T, R> mapper) {
        return stream.map(mapper).filter(Objects::nonNull);
    }

    public static <T> Stream<T> mapPresent(Stream<Optional<T>> stream) {
        return stream.filter(Optional::isPresent).map(Optional::get);
    }

    public static <T, R> Stream<R> mapPresent(Stream<T> stream, Function<? super T, Optional<R>> mapper) {
        return mapPresent(stream.map(mapper));
    }

    public static <L, R, K, V> Either<L, Pair<K, V>> mapToPair(Either<L, R> either, Function<? super R, ? extends K> keyMapper, Function<? super R, ? extends V> valueMapper) {
        return either.map(product(keyMapper, valueMapper));
    }

    public static <T, K, V> Stream<Pair<K, V>> mapToPair(Stream<T> stream, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return stream.map(product(keyMapper, valueMapper));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, K, V> Optional<Pair<K, V>> mapToPair(Optional<T> optional, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return optional.map(product(keyMapper, valueMapper));
    }

    public static <L, R, U, X extends Throwable> Either<L, U> mapTry(Either<L, R> either, ThrowingFunction<R, U, X> throwingMapper, BiFunction<R, X, L> exceptionMapper) {
        return either.flatMap(tryEither(throwingMapper, exceptionMapper, false));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, R, X extends Throwable> Optional<R> mapTry(Optional<T> optional, ThrowingFunction<? super T, ? extends R, ? extends X> throwingMapper) {
        return mapTry(optional, throwingMapper, ExceptionMapper.wrapChecked());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, R, X extends Throwable, Y extends RuntimeException> Optional<R> mapTry(Optional<T> optional, ThrowingFunction<? super T, ? extends R, ? extends X> throwingMapper, ExceptionMapper<T, ? extends Y> exceptionMapper) {
        return optional.map(uFunction(throwingMapper, exceptionMapper));
    }

    public static <T, R, X extends Throwable> Stream<R> mapTry(Stream<T> stream, ThrowingFunction<? super T, ? extends R, ? extends X> throwingMapper) {
        return mapTry(stream, throwingMapper, ExceptionMapper.wrapChecked());
    }

    public static <T, R, X extends Throwable, Y extends RuntimeException> Stream<R> mapTry(Stream<T> stream, ThrowingFunction<? super T, ? extends R, ? extends X> throwingMapper, ExceptionMapper<T, ? extends Y> exceptionMapper) {
        return stream.map(uFunction(throwingMapper, exceptionMapper));
    }

    public static <K, V, R> Stream<Entry<K, R>> mapValue(Stream<? extends Entry<? extends K, ? extends V>> entryStream, Function<? super V, ? extends R> function) {
        return entryStream.map(e -> Pair.of(e.getKey(), function.apply(e.getValue())));
    }

    public static <K, V> Stream<V> mapValue(Stream<? extends Entry<? extends K, ? extends V>> entryStream) {
        return entryStream.map(Entry::getValue);
    }

    public static <T> int maxAsScalar(Collection<T> collection, ToIntFunction<T> mapper) {
        return maxAsScalar(streamEach(collection), mapper);
    }

    private static <T> int maxAsScalar(Stream<T> stream, ToIntFunction<T> mapper) {
        return stream.mapToInt(mapper).max().orElse(0);
    }

    public static <T> Optional<T> maxBy(Collection<T> collection, ToIntFunction<T> mapper) {
        return maxBy(collection, Comparator.comparingInt(mapper));
    }

    public static <T> Optional<T> maxBy(Collection<T> collection, Comparator<? super T> comparator) {
        return streamEach(collection).max(comparator);
    }

    public static <T, U> Map<T, U> mergeEach(Map<T, U> base, Map<T, U> merge) {
        var result = new HashMap<T, U>();
        if (base != null) {
            result.putAll(base);
        }
        if (merge != null) {
            result.putAll(merge);
        }
        return result;
        // return toMap(mergeEach(streamEntries(base), e -> merge == null || !merge.containsKey(e.getKey()), streamEntries(merge)));
    }

    public static <T> Stream<T> mergeEach(Stream<T> baseStream, Predicate<T> baseStreamPredicate, Stream<T> mergeStream) {
        return Stream.concat(baseStream.filter(baseStreamPredicate), mergeStream);
    }

    public static <T> Optional<T> minBy(Collection<T> collection, ToIntFunction<T> mapper) {
        return minBy(collection, Comparator.comparingInt(mapper));
    }

    public static <T> Optional<T> minBy(Collection<T> collection, Comparator<? super T> comparator) {
        return streamEach(collection).min(comparator);
    }

    public static <T> boolean none(Collection<T> collection, Predicate<? super T> predicate) { // NOPMD
        return streamEach(collection).noneMatch(predicate);
    }

    public static <T> boolean none(T[] array, Predicate<? super T> predicate) { // NOPMD
        return streamEach(array).noneMatch(predicate);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> Optional<T> orGet(Optional<T> optional, Supplier<? extends T> supplier) {
        return optional.or(() -> Optional.ofNullable(supplier.get()));
    }

    public static <T, U, V> Triple<List<T>, List<Pair<T, U>>, List<U>> partition(Collection<T> collection, Collection<U> otherCollection, Function<T, V> keyExtractor, Function<U, V> otherKeyExtractor) {
        // Can be optimized
        return Triple.ofMiddle(symmetricDifference(collection, otherCollection, keyExtractor, otherKeyExtractor), intersection(collection, otherCollection, keyExtractor, otherKeyExtractor));
    }

    public static <T> Pair<List<T>, List<T>> partition(Collection<T> collection, Predicate<? super T> predicate) {
        // Could create IOR for this
        var parts = streamEach(collection).collect(Collectors.partitioningBy(predicate));
        return Pair.of(parts.get(true), parts.get(false));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> Optional<T> peekIf(Optional<T> optional, Predicate<? super T> predicate, Consumer<? super T> action) {
        optional.filter(predicate).ifPresent(action);
        return optional;
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> Optional<T> peekIf(Optional<T> optional, Predicate<? super T> predicate, Consumer<? super T> action, Consumer<? super T> otherwiseAction) {
        if (optional.isPresent()) {
            optional.filter(predicate).ifPresentOrElse(action, () -> otherwiseAction.accept(optional.get()));
        }
        return optional;
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> Optional<T> peekIf(Optional<T> optional, boolean value, Consumer<? super T> action) {
        return peekIf(optional, v -> value, action);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> Optional<T> peekValue(Optional<T> optional, Consumer<? super T> action) {
        optional.ifPresent(action);
        return optional;
    }

    @SafeVarargs
    public static <T> List<T> plus(Collection<? extends T> collection, T... items) {
        return Stream.concat(streamEach(collection), streamEach(items)).toList();
    }

    public static <T> List<T> plus(Collection<? extends T> collection, Collection<? extends T> otherCollection) {
        return streamWith(collection, otherCollection).toList();
    }

    public static <T> List<T> plus(Collection<? extends T> collection, Stream<? extends T> stream) {
        return streamWith(collection, stream).toList();
    }

    public static <K, V> Map<K, V> plus(Map<K, V> map, Map<K, V> otherMap) {
        return Stream.concat(streamEntries(map), streamEntries(otherMap))
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (a, b) -> b));
    }

    @SafeVarargs
    public static <T> List<T> prepend(List<? extends T> list, T... items) {
        return Stream.concat(streamEach(items), list.stream()).toList();
    }

    public static <T> List<T> prepend(List<? extends T> list, Collection<? extends T> collection) {
        return streamWith(collection, list).toList();
    }

    public static <T> List<T> prepend(List<? extends T> list, Stream<? extends T> stream) {
        return concatWith(stream, list).toList();
    }

    public static void repeat(IntConsumer consumer, int times) {
        IntStream.range(0, times).forEach(consumer);
    }

    public static <T, U extends Comparable<? super U>> List<T> sortBy(Collection<T> collection, Function<? super T, ? extends U> keyExtractor) {
        return sortWith(collection, Comparator.comparing(keyExtractor));
    }

    public static <T, U extends Comparable<? super U>> Stream<T> sortBy(Stream<T> stream, Function<? super T, ? extends U> keyExtractor) {
        return sortWith(stream, Comparator.comparing(keyExtractor));
    }

    public static <T> List<T> sortWith(Collection<T> collection, Comparator<T> comparator) {
        return streamEach(collection).sorted(comparator).toList();
    }

    public static <T> Stream<T> sortWith(Stream<T> stream, Comparator<T> comparator) {
        return stream.sorted(comparator);
    }

    public static <T> Stream<T> streamEach(Collection<T> collection) {
        return collection == null ? Stream.empty() : collection.stream();
    }

    public static <T> Stream<T> streamEach(Iterable<T> iterable) {
        return streamEach(iterable, false);
    }

    public static <T> Stream<T> streamEach(Iterable<T> iterable, boolean parallel) {
        return iterable == null ? Stream.empty() : StreamSupport.stream(iterable.spliterator(), parallel);
    }

    public static <T> Stream<T> streamEach(T[] array) { // NOPMD varargs
        return array == null ? Stream.empty() : Stream.of(array);
    }

    public static <T, R> Stream<R> streamEach(T[] array, Function<? super T, ? extends R> mapper) {
        return streamEach(array).map(mapper);
    }

    public static <T, R> Stream<R> streamEach(Collection<T> collection, Function<? super T, ? extends R> mapper) {
        return streamEach(collection).map(mapper);
    }

    public static <T> Stream<T> streamEach(Iterator<T> iterator) {
        return streamEach(iterator, false);
    }

    public static <T> Stream<T> streamEach(Iterator<T> iterator, boolean parallel) {
        if (iterator == null) {
            return Stream.empty();
        }
        return StreamSupport.stream(((Iterable<T>) () -> iterator).spliterator(), parallel);
    }

    public static <T> Stream<T> streamEachIf(Collection<T> collection, Predicate<? super T> filter) {
        return streamEach(collection).filter(filter);
    }

    public static <T, L, R> Stream<Pair<L, R>> streamEachToPair(Collection<T> collection, Function<? super T, L> keyMapper, Function<? super T, R> valueMapper) {
        return mapToPair(streamEach(collection), keyMapper, valueMapper);
    }

    public static <T, L, R> Stream<Pair<L, R>> streamEachToPair(Iterable<T> iterable, Function<? super T, L> keyMapper, Function<? super T, R> valueMapper) {
        return mapToPair(streamEach(iterable), keyMapper, valueMapper);
    }

    public static <K, V, L, R> Stream<Pair<L, R>> streamEachToPair(Map<K, V> map, Function<? super K, L> keyMapper, Function<? super V, R> valueMapper) {
        return mapEntryToPair(streamEntries(map), keyMapper, valueMapper);
    }

    public static <T> Stream<T> streamEachUnless(Collection<T> collection, Predicate<? super T> filter) {
        return streamEach(collection).filter(filter.negate());
    }

    public static <K, V> Stream<Entry<K, V>> streamEntries(Map<K, V> map) {
        return map == null ? Stream.empty() : map.entrySet().stream();
    }

    public static <K, V, R> Stream<R> streamEntries(Map<K, V> map, BiFunction<? super K, ? super V, ? extends R> mapper) {
        return streamEntries(map).map(e -> mapper.apply(e.getKey(), e.getValue()));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V> Stream<Entry<K, V>> streamEntries(Optional<Map<K, V>> optional) {
        return optional.stream().flatMap(m -> m.entrySet().stream());
    }

    public static <K, V, R> Stream<R> streamEntries(Optional<Map<K, V>> optional, BiFunction<? super K, ? super V, ? extends R> mapper) {
        return streamEntries(optional).map(e -> mapper.apply(e.getKey(), e.getValue()));
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> Stream<T> streamFlatten(Optional<? extends Collection<T>> optional) {
        return flatten(optional.stream());
    }

    public static <V> IntStream streamIndexes(Collection<V> collection) {
        return collection == null ? IntStream.empty() : IntStream.range(0, collection.size());
    }

    public static <K, V> Stream<K> streamKeys(Map<K, V> map) {
        return map == null ? Stream.empty() : map.keySet().stream();
    }

    public static <K, V> Stream<V> streamValues(Map<K, V> map) {
        return map == null ? Stream.empty() : map.values().stream();
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <K, V> Stream<V> streamValues(Optional<Map<K, V>> optional) {
        return optional.stream().flatMap(m -> m.values().stream());
    }

    public static <T extends Enum<T>> Stream<T> streamValues(Class<T> enumType) {
        return Stream.of(enumType.getEnumConstants());
    }

    public static <T> Stream<T> streamWith(Collection<? extends T> collection1, Collection<? extends T> collection2) {
        return streamWith(collection1, collection2.stream());
    }

    public static <T> Stream<T> streamWith(Collection<? extends T> collection, Stream<? extends T> stream) {
        return Stream.concat(streamEach(collection), stream);
    }

    public static <T> Stream<Entry<T, Integer>> streamWithIndex(Collection<T> collection) {
        return collection == null ? Stream.empty() : zipWithIndex(collection.stream());
    }

    public static <T> Pair<List<T>, List<T>> symmetricDifference(Collection<T> collection, Collection<T> otherCollection) {
        return symmetricDifference(collection, otherCollection, Function.identity());
    }

    public static <T, V> Pair<List<T>, List<T>> symmetricDifference(Collection<T> collection, Collection<T> otherCollection, Function<T, V> keyExtractor) {
        return symmetricDifference(collection, otherCollection, keyExtractor, keyExtractor);
    }

    public static <T, U, V> Pair<List<T>, List<U>> symmetricDifference(Collection<T> collection, Collection<U> otherCollection, Function<T, V> keyExtractor, Function<U, V> otherKeyExtractor) {
        // Can be optimized
        return Pair.of(difference(collection, otherCollection, keyExtractor, otherKeyExtractor), difference(otherCollection, collection, otherKeyExtractor, keyExtractor));
    }

    public static <T> List<T> tail(List<T> list) {
        return list.subList(1, list.size());
    }

    public static <T> T[] toArray(T... array) {
        return array;
    }

    public static <T> Deque<T> toDeque(Iterable<T> iterable) {
        return toDeque(streamEach(iterable));
    }

    public static <T> Deque<T> toDeque(T[] array) { // NOPMD varargs
        return array == null ? new LinkedList<>() : toDeque(Arrays.stream(array));
    }

    public static <T> Deque<T> toDeque(Stream<T> stream) {
        return stream.collect(Collectors.toCollection(LinkedList::new));
    }

    public static <T, R> Deque<R> toDeque(T[] array, Function<? super T, ? extends R> mapper) {
        return toDeque(streamEach(array), mapper);
    }

    public static <T, R> Deque<R> toDeque(Stream<T> stream, Function<? super T, ? extends R> mapper) {
        return toDeque(stream.map(mapper));
    }

    public static List<Integer> toList(int[] array) { // NOPMD varargs
        return array == null ? Collections.emptyList() : Arrays.stream(array).boxed().toList();
    }

    public static <T> List<T> toList(Iterable<T> iterable) {
        return streamEach(iterable).toList();
    }

    public static <T> List<T> toList(T[] array) { // NOPMD varargs
        return array == null ? Collections.emptyList() : Arrays.stream(array).toList();
    }

    public static <T> List<T> toList(Stream<T> stream) {
        return stream.toList();
    }

    public static <T, R> List<R> toList(T[] array, Function<? super T, ? extends R> mapper) {
        return toList(streamEach(array), mapper);
    }

    @SuppressWarnings("unchecked")
    public static <T, R> List<R> toList(Stream<T> stream, Function<? super T, ? extends R> mapper) {
        return (List<R>) stream.map(mapper).toList(); // NOPMD Necessary cast (immutable)
    }

    public static List<Integer> toListReverse(int... values) {
        return values == null ? Collections.emptyList() : Arrays.stream(values).boxed().collect(toListReverse());
    }

    public static <T> List<T> toListReverse(Iterable<T> iterable) {
        return streamEach(iterable).collect(toListReverse());
    }

    public static <T> List<T> toListReverse(Stream<T> stream) {
        return stream.collect(toListReverse());
    }

    public static <T> List<T> toListReverse(T[] array) { // NOPMD varargs
        return array == null ? Collections.emptyList() : Arrays.stream(array).collect(toListReverse());
    }

    public static <T> Collector<T, ?, List<T>> toListReverse() {
        return Collector.of(LinkedList::new, (BiConsumer<LinkedList<T>, T>) Deque::addFirst, (left, right) -> { // NOPMD Deque+List
            right.descendingIterator().forEachRemaining(left::addFirst);
            return left;
        }, Collections::unmodifiableList, Characteristics.IDENTITY_FINISH);
    }

    public static <K, V> Map<K, V> toMap(Stream<? extends Entry<? extends K, ? extends V>> entryStream) {
        return toMap(entryStream, Entry::getKey, Entry::getValue);
    }

    public static <T, K, V> Map<K, V> toMap(Stream<T> stream, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return stream.collect(Collectors.toMap(keyMapper, valueMapper)); // NOPMD false positive
    }

    public static <K, V> Map<K, V> toMap(Entry<K, V>[] entryArray) {  // NOPMD No varargs
        return toMap(streamEach(entryArray));
    }

    public static <T, K, V> Map<K, V> toMap(Collection<T> collection, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return toMap(streamEach(collection), keyMapper, valueMapper);
    }

    public static <T, K, V> Map<K, V> toMap(T[] array, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return toMap(streamEach(array), keyMapper, valueMapper);
    }

    public static <T> Map<T, T> toMapPairing(T[] array) { // NOPMD varargs
        return toMapPairing(Function.identity(), Function.identity(), array);
    }

    public static <T, K, V> Map<K, V> toMapPairing(T[] array, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return toMapPairing(keyMapper, valueMapper, array);
    }

    public static <T, K, V> Map<K, V> toMapPairing(Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper, T... values) {
        return values == null ? Collections.emptyMap() : IntStream.range(0, values.length / 2)
            .boxed()
            .collect(Collectors.toMap(i -> keyMapper.apply(values[i * 2]), i -> valueMapper.apply(values[i * 2 + 1])));
    }

    public static <T> List<T> toMutableList(Stream<T> stream) {
        return stream.collect(Collectors.toList()); // NOPMD false positive
    }

    public static <T, K, V> Map<K, V> toMutableMap(Collection<T> collection, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return toMutableMap(streamEach(collection), keyMapper, valueMapper);
    }

    public static <K, V> Map<K, V> toMutableMap(Stream<? extends Entry<? extends K, ? extends V>> entryStream) {
        return toMutableMap(entryStream, Entry::getKey, Entry::getValue);
    }

    public static <T, K, V> Map<K, V> toMutableMap(Stream<T> stream, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return stream.collect(Collectors.toMap(keyMapper, valueMapper)); // NOPMD false positive
    }

    public static <K, V> Map<K, V> toMutableMap(Entry<K, V>... entries) {
        return toMutableMap(streamEach(entries));
    }

    public static <T, K, V> Map<K, V> toMutableMap(T[] array, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return toMutableMap(streamEach(array), keyMapper, valueMapper);
    }

    @SuppressWarnings("unchecked")
    public static <T, U> Map<T, U> toMutableMapOf(T k, U v) {
        return toMutableMap(Pair.of(k, v));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2, T k3, U v3) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7, T k8, U v8) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7), Pair.of(k8, v8));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7, T k8, U v8, T k9, U v9) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7), Pair.of(k8, v8), Pair.of(k9, v9));
    }

    @SuppressWarnings("unchecked")
    static <T, U> Map<T, U> toMutableMapOf(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7, T k8, U v8, T k9, U v9, T k10, U v10) {
        return toMutableMap(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7), Pair.of(k8, v8), Pair.of(k9, v9), Pair.of(k10, v10));
    }

    public static <T> Map<T, T> toMutableMapPairing(T[] array) { // NOPMD varargs
        return toMutableMapPairing(Function.identity(), Function.identity(), array);
    }

    public static <T, K, V> Map<K, V> toMutableMapPairing(T[] array, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return toMutableMapPairing(keyMapper, valueMapper, array);
    }

    public static <T, K, V> Map<K, V> toMutableMapPairing(Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper, T... values) {
        return values == null ? Collections.emptyMap() : IntStream.range(0, values.length / 2)
            .boxed()
            .collect(Collectors.toMap(i -> keyMapper.apply(values[i * 2]), i -> valueMapper.apply(values[i * 2 + 1])));
    }

    public static <T> Set<T> toSet(Collection<T> collection) {
        return new HashSet<>(collection);
    }

    public static <T, R> Set<R> toSet(Collection<T> collection, Function<? super T, ? extends R> mapper) {
        return toSet(streamEach(collection), mapper);
    }

    public static <T, R> Set<R> toSet(Stream<T> stream, Function<? super T, ? extends R> mapper) {
        return toSet(stream.map(mapper));
    }

    public static <T> Set<T> toSet(Stream<T> stream) {
        return stream.collect(Collectors.toUnmodifiableSet()); // NOPMD false positive
    }

    public static <T, K, V> SortedMap<K, V> toSortedMap(Stream<T> stream, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return stream.collect(Collectors.toMap(keyMapper,
            valueMapper,
            (v1, v2) -> {
                throw new IllegalStateException("Duplicate key for values %s and %s".formatted(v1, v2));
            },
            TreeMap::new));
    }

    public static <T, K, V> SortedMap<K, V> toSortedMap(T[] array, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return toSortedMap(streamEach(array), keyMapper, valueMapper);
    }

    public static <T, K, V> SortedMap<K, V> toSortedMap(Collection<T> collection, Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper) {
        return toSortedMap(streamEach(collection), keyMapper, valueMapper);
    }

    public static <K, V> SortedMap<K, V> toSortedMap(Stream<? extends Entry<? extends K, ? extends V>> entryStream) {
        return toSortedMap(entryStream, Entry::getKey, Entry::getValue);
    }

    public static <T> List<T> unfold(T seed, Function<? super T, ? extends Collection<? extends T>> expander) {
        return unfold(seed, expander, (v, p) -> v, ArrayList::new);
    }

    public static <T, R> List<R> unfold(T seed, Function<? super T, ? extends Collection<? extends T>> expander, BiFunction<? super T, ? super T, ? extends R> mapper) {
        return unfold(seed, expander, mapper, ArrayList::new);
    }

    public static <T, R, C extends Collection<R>> C unfold(T seed, Function<? super T, ? extends Collection<? extends T>> expander, BiFunction<? super T, ? super T, ? extends R> mapper, Supplier<C> collectionFactory) {
        var resultAccumulator = collectionFactory.get();
        resultAccumulator.add(mapper.apply(seed, null));
        var remaining = new LinkedList<T>();
        remaining.add(seed);
        while (!remaining.isEmpty()) {
            var current = remaining.pop();
            var children = expander.apply(current);
            if (children != null) {
                remaining.addAll(children);
                children.stream().map(c -> mapper.apply(c, current)).forEach(resultAccumulator::add);
            }
        }
        return resultAccumulator;
    }

    @SuppressWarnings("unchecked")
    public static <T> Stream<Stream<T>> windowed(Stream<T> stream, int windowSize, int step, boolean partialWindows) { // NOPMD
        return StreamSupport.stream(new Spliterator<>() {
            private final Spliterator<T> source = stream.spliterator();
            private Deque<T> buffer;

            @Override
            public int characteristics() {
                return source.characteristics() & ~(SIZED | ORDERED);
            }

            @Override
            public long estimateSize() {
                var sourceSize = source.estimateSize();
                return sourceSize == 0 ? 0 : sourceSize <= windowSize ? 1 : sourceSize - windowSize;
            }

            @Override
            public boolean tryAdvance(Consumer<? super Stream<T>> action) {
                if (buffer == null) {
                    // Preload
                    buffer = new LinkedList<>();
                    var missingSize = windowSize;
                    while (source.tryAdvance(buffer::add)) {
                        missingSize--;
                        if (missingSize <= 0) {
                            break;
                        }
                    }
                }
                if (buffer.isEmpty() || !partialWindows && buffer.size() < windowSize) {
                    return false;
                }

                action.accept(Arrays.stream((T[]) buffer.toArray(new Object[0])));

                // Slide
                IntStream.range(0, step).takeWhile(i -> !buffer.isEmpty()).forEach(i -> {
                    buffer.removeFirst();
                    source.tryAdvance(buffer::add);
                });
                return true;

            }

            @Override
            public Spliterator<Stream<T>> trySplit() {
                return null; // No parallel!
            }
        }, false);
    }

    public static <T> Stream<Stream<T>> windowed(Stream<T> stream, int windowSize, int step) {
        return windowed(stream, windowSize, step, false);
    }

    public static <T> Stream<Stream<T>> windowed(Stream<T> stream, int windowSize) {
        return windowed(stream, windowSize, 1);
    }

    public static <T> List<List<T>> windowed(Collection<T> collection, int windowSize, int step, boolean partialWindows) {
        return windowed(collection, windowSize, step, partialWindows, Stream::toList);
    }

    private static <T, R> List<R> windowed(Collection<T> collection, int windowSize, int step, boolean partialWindows, Function<Stream<T>, R> windowContentMapper) {
        return windowed(streamEach(collection), windowSize, step, partialWindows, windowContentMapper).toList();
    }

    private static <T, R> Stream<R> windowed(Stream<T> stream, int windowSize, int step, boolean partialWindows, Function<Stream<T>, R> windowContentMapper) {
        return windowed(stream, windowSize, step, partialWindows).map(windowContentMapper);
    }

    public static <T> List<List<T>> windowed(Collection<T> collection, int windowSize, int step) {
        return windowed(collection, windowSize, step, false);
    }

    public static <T> List<List<T>> windowed(Collection<T> collection, int windowSize, boolean partialWindows) {
        return windowed(collection, windowSize, 1, partialWindows);
    }

    public static <T> List<List<T>> windowed(Collection<T> collection, int windowSize) {
        return windowed(collection, windowSize, 1);
    }

    private static <T, U, R> Stream<R> zip(Stream<? extends T> stream1, Stream<? extends U> stream2, BiFunction<? super T, ? super U, ? extends R> zipper, boolean all, T missing1, U missing2) { // NOPMD
        Objects.requireNonNull(zipper);
        var spliterator1 = Objects.requireNonNull(stream1).spliterator();
        var spliterator2 = Objects.requireNonNull(stream2).spliterator();

        var characteristics = spliterator1.characteristics() & spliterator2.characteristics() & ~(Spliterator.DISTINCT | Spliterator.SORTED);

        long zipSize;
        if ((characteristics & Spliterator.SIZED) == 0) {
            zipSize = -1;
        } else {
            if (all) {
                zipSize = Math.max(spliterator1.getExactSizeIfKnown(), spliterator2.getExactSizeIfKnown());
            } else {
                zipSize = Math.min(spliterator1.getExactSizeIfKnown(), spliterator2.getExactSizeIfKnown());
            }
        }

        Iterator<T> iterator1 = Spliterators.iterator(spliterator1);
        Iterator<U> iterator2 = Spliterators.iterator(spliterator2);

        return StreamSupport.stream(Spliterators.spliterator(new Iterator<>() {
            @Override
            public boolean hasNext() {
                if (all) {
                    return iterator1.hasNext() || iterator2.hasNext();
                } else {
                    return iterator1.hasNext() && iterator2.hasNext();
                }
            }

            @Override
            public R next() {
                if (all) {
                    return zipper.apply(iterator1.hasNext() ? iterator1.next() : missing1, iterator2.hasNext() ? iterator2.next() : missing2);
                } else {
                    return zipper.apply(iterator1.next(), iterator2.next());
                }
            }
        }, zipSize, characteristics), stream1.isParallel() || stream2.isParallel());
    }

    public static <T, U, R> Stream<R> zip(Stream<? extends T> stream1, Stream<? extends U> stream2, T missing1, U missing2, BiFunction<? super T, ? super U, ? extends R> zipper) {
        return zip(stream1, stream2, zipper, true, missing1, missing2);
    }

    public static <T, U, R> Stream<R> zip(Stream<? extends T> stream1, Stream<? extends U> stream2, BiFunction<? super T, ? super U, ? extends R> zipper) {
        return zip(stream1, stream2, zipper, false, null, null);
    }

    public static <L, R> Stream<Pair<L, R>> zip(Stream<? extends L> stream1, Stream<? extends R> stream2) {
        return zip(stream1, stream2, TupleExtension::to);
    }

    @SuppressWarnings("unchecked")
    public static <T, U, R> List<R> zip(Collection<T> collection1, Collection<U> collection2, BiFunction<? super T, ? super U, ? extends R> zipper) {
        return (List<R>) zip(collection1.stream(), collection2.stream(), zipper).toList(); // NOPMD Necessary cast for immutable
    }

    public static <L, R> List<? extends Pair<L, R>> zip(Collection<L> collection1, Collection<R> collection2) {
        return zip(collection1, collection2, TupleExtension::to);
    }

    public static <L, R> List<? extends Pair<L, R>> zip(Collection<L> collection1, Collection<R> collection2, L missing1, R missing2) {
        return zip(collection1, collection2, missing1, missing2, TupleExtension::to);
    }

    @SuppressWarnings("unchecked")
    public static <T, U, R> List<R> zip(Collection<T> collection1, Collection<U> collection2, T missing1, U missing2, BiFunction<? super T, ? super U, ? extends R> zipper) {
        return (List<R>) zip(collection1.stream(), collection2.stream(), missing1, missing2, zipper).toList(); // NOPMD Necessary cast for immutable
    }

    public static <T, R> Stream<R> zipWithIndex(Stream<T> stream, BiFunction<? super T, Integer, ? extends R> zipper) {
        return zip(stream, IntStream.generate(new IntSupplier() {
            private int value;

            @Override
            public int getAsInt() {
                return value++;
            }
        }).boxed(), zipper);
    }

    public static <T> Stream<Entry<T, Integer>> zipWithIndex(Stream<T> stream) {
        return zipWithIndex(stream, Pair::of);
    }

    @SuppressWarnings("unchecked")
    public static <T, R> List<R> zipWithIndex(Collection<T> collection, BiFunction<? super T, Integer, ? extends R> zipper) {
        return (List<R>) zipWithIndex(collection.stream(), zipper).toList(); // NOPMD Necessary cast for immutable
    }

    public static <T> List<Entry<T, Integer>> zipWithIndex(Collection<T> collection) {
        return zipWithIndex(collection.stream()).toList();
    }

    public static <T> List<? extends Entry<T, T>> zipWithNext(Collection<T> collection) {
        return zipWithNext(collection.stream()).toList();
    }

    public static <T> Stream<Entry<T, T>> zipWithNext(Stream<T> stream) {
        return windowed(stream, 2, 2, true, s -> s.collect(new PairCollector<>()));
    }

    public static <T> Stream<Entry<T, T>> zipWithNext(T[] array) { // NOPMD varargs
        return zipWithNext(streamEach(array));
    }

    private static final class PairCollector<T> implements Collector<T, List<T>, Pair<T, T>> {
        private static final Set<Characteristics> CHARACTERISTICS = Collections.emptySet(); // NOPMD

        @Override
        public BiConsumer<List<T>, T> accumulator() {
            return List::add;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return CHARACTERISTICS;
        }

        @Override
        public BinaryOperator<List<T>> combiner() {
            return (a, b) -> {
                a.addAll(b);
                return a;
            };
        }

        @Override
        public Function<List<T>, Pair<T, T>> finisher() {
            return i -> Pair.ofIterable(i, true);
        }

        @Override
        public Supplier<List<T>> supplier() {
            return ArrayList::new;
        }
    }

    public static class StreamMatcherBuilder<T, R> {

        private final Stream<T> stream;
        private final Applying<T, R> matcher;

        public StreamMatcherBuilder(Stream<T> stream) {
            this.stream = stream;
            this.matcher = Matcher.applying();
        }

        public Stream<R> build() {
            return stream.map(matcher);
        }

        public StreamMatcherBuilder<T, R> otherwise(Function<T, R> value) {
            matcher.otherwise(value);
            return this;
        }

        public StreamMatcherBuilder<T, R> when(Predicate<T> matchPredicate, Function<T, R> value) {
            matcher.when(matchPredicate, value);
            return this;
        }
    }

}
