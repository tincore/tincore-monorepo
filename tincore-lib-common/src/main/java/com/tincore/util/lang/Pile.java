package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.function.TriFunction;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Forth inspired stack
 *
 * @param <I>
 * @param <T>
 */
public interface Pile<I extends Pile, T> extends Queue<T> {
    @SafeVarargs
    static <T> Mutable<T> mutableOf(T... elements) { // NOPMD type
        var mutable = new Mutable<T>();
        if (elements != null) {
            List<T> a = Arrays.asList(elements);
            Collections.reverse(a);
            mutable.addAll(a);
        }
        return mutable;
    }

    @SafeVarargs
    static <T> Immutable<T> of(T... elements) { // NOPMD type
        var pile = Immutable.<T>empty();
        if (elements != null) {
            List<T> a = Arrays.asList(elements);
            Collections.reverse(a);
            for (T t : a) {
                pile = pile.push(t);
            }
        }
        return pile;
    }

    @SuppressWarnings("unchecked")
    default I apply(Function<I, I> mapping) {
        return mapping.apply((I) this);
    }

    @SuppressWarnings("unchecked")
    default I apply(Predicate<I> predicate, Function<I, I> mapping) {
        return apply(predicate, mapping, p -> p);
    }

    @SuppressWarnings("unchecked")
    default I apply(Predicate<I> predicate, Function<I, I> mapping, Function<I, I> otherwiseMapping) {
        return predicate.test((I) this) ? mapping.apply((I) this) : otherwiseMapping.apply((I) this);
    }

    @Override
    default boolean contains(Object o) {
        return getIndexOf(o) >= 0;
    }

    @Override
    default boolean containsAll(Collection<?> c) {
        return c.stream().allMatch(this::contains);
    }

    default I drop() {
        return pop();
    }

    @SuppressWarnings("unchecked")
    default I drop2() {
        return (I) drop().drop();
    }

    @SuppressWarnings("unchecked")
    default I dup(Predicate<T> predicate) {
        T a = element();
        if (predicate.test(a)) {
            return push(a);
        }
        return (I) this;
    }

    default I dup() {
        return push(element());
    }

    @SuppressWarnings("unchecked")
    default I dup2() {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        return (I) p.push(a).push(b).push(a);
    }

    @SuppressWarnings("unchecked")
    default I dup2(BiPredicate<T, T> predicate) {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        return (I) (predicate.test(a, b) ? p.push(a).push(b).push(a) : this);
    }

    int getIndexOf(Object o);

    @Override
    default boolean isEmpty() {
        return size() == 0;
    }

    @SuppressWarnings("unchecked")
    default I map(Function<? super T, ? extends T> mapper) {
        T a = element();
        return (I) pop().push(mapper.apply(a));
    }

    @SuppressWarnings("unchecked")
    default I map(BiFunction<? super T, ? super T, ? extends T> mapper) {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        p = (I) p.pop();
        return (I) p.push(mapper.apply(a, b));
    }

    @SuppressWarnings("unchecked")
    default I map(TriFunction<? super T, ? super T, ? super T, ? extends T> mapper) {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        p = (I) p.pop();
        T c = (T) p.element();
        p = (I) p.pop();
        return (I) p.push(mapper.apply(a, b, c));
    }

    @SuppressWarnings("unchecked")
    default I nip() {
        return (I) swap().drop();
    }

    @SuppressWarnings("unchecked")
    default I nip2() {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        p = (I) p.pop();
        return (I) p.drop2().push(b).push(a);
    }

    @SuppressWarnings("unchecked")
    default I over() {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        p = (I) p.pop();
        return (I) p.push(a).push(b).push(a);
    }

    @SuppressWarnings("unchecked")
    default I over2() {
        T a1 = element();
        I p = pop();
        T a2 = (T) p.element();
        p = (I) p.pop();
        T b1 = (T) p.element();
        p = (I) p.pop();
        T b2 = (T) p.element();
        p = (I) p.pop();
        return (I) p.push(a2).push(a1).push(b2).push(b1).push(a2).push(a1);
    }

    I pop();

    I popAll();

    I push(T value);

    @SuppressWarnings("unchecked")
    default I rot() {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        p = (I) p.pop();
        T c = (T) p.element();
        p = (I) p.pop();
        return (I) p.push(a).push(c).push(b);
    }

    @SuppressWarnings("unchecked")
    default I rot2() {
        T a1 = element();
        I p = pop();
        T a2 = (T) p.element();
        p = (I) p.pop();
        T b1 = (T) p.element();
        p = (I) p.pop();
        T b2 = (T) p.element();
        p = (I) p.pop();
        T c1 = (T) p.element();
        p = (I) p.pop();
        T c2 = (T) p.element();
        p = (I) p.pop();
        return (I) p.push(a2).push(a1).push(c2).push(c1).push(b2).push(b1);
    }

    @SuppressWarnings("unchecked")
    default I rotN() {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        p = (I) p.pop();
        T c = (T) p.element();
        p = (I) p.pop();
        return (I) p.push(b).push(a).push(c);
    }

    @SuppressWarnings("unchecked")
    default I rotN2() {
        T a1 = element();
        I p = pop();
        T a2 = (T) p.element();
        p = (I) p.pop();
        T b1 = (T) p.element();
        p = (I) p.pop();
        T b2 = (T) p.element();
        p = (I) p.pop();
        T c1 = (T) p.element();
        p = (I) p.pop();
        T c2 = (T) p.element();
        return (I) p.pop().push(b2).push(b1).push(a2).push(a1).push(c2).push(c1);
    }

    @SuppressWarnings("unchecked")
    default I swap() {
        T a = element();
        I p = pop();
        T b = (T) p.element();
        return (I) p.pop().push(a).push(b);
    }

    @SuppressWarnings("unchecked")
    default I swap2() {
        T a1 = element();
        I p = pop();
        T a2 = (T) p.element();
        p = (I) p.pop();
        T b1 = (T) p.element();
        p = (I) p.pop();
        T b2 = (T) p.element();
        p = (I) p.pop();
        return (I) p.push(a2).push(a1).push(b2).push(b1);
    }

    default List<T> toList() {
        return stream().toList();
    }

    @SuppressWarnings("unchecked")
    default I tuck() {
        return (I) swap().over();
    }

    @SuppressWarnings("unchecked")
    default I tuck2() {
        return (I) swap2().over2();
    }

    interface Immutable<T> extends Pile<Immutable<T>, T> {

        Immutable EMPTY = new Empty<>();

        static <T> Immutable<T> empty() {
            return EMPTY;
        }

        @Override
        default boolean add(T value) {
            throw new UnsupportedOperationException();
        }

        @Override
        default boolean addAll(Collection<? extends T> values) {
            throw new UnsupportedOperationException();
        }

        @Override
        default void clear() {
            throw new UnsupportedOperationException();
        }

        @Override
        default boolean offer(T t) {
            throw new UnsupportedOperationException();
        }

        @Override
        default Immutable<T> popAll() {
            return empty();
        }

        @Override
        default Immutable<T> push(T value) {
            return new Node<>(value, this);
        }

        @Override
        default boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        default T remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        default boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        default boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        class Node<T> implements Immutable<T> {

            private final T value;
            private final Immutable<T> tail;

            private final int size;

            public Node(final T value, final Immutable<T> tail) {
                this.value = value;
                this.tail = tail;

                this.size = tail.size() + 1;
            }

            private void assembleArray(Object[] array) { // NOPMD
                Immutable<T> node = this;
                int i = 0;
                while (node instanceof Node<T> n) { // NOPMD type
                    array[i++] = n.value;
                    node = n.tail;
                }
            }

            @Override
            public T element() {
                return peek();
            }

            @Override
            public int getIndexOf(Object o) {
                int index = 0;
                Immutable<T> node = this;
                while (node instanceof Immutable.Node<T> n) { // NOPMD type
                    if (Objects.equals(n.value, o)) {
                        return index;
                    }
                    index++;
                    node = n.tail;
                }
                return -1;
            }

            @Override
            public java.util.Iterator<T> iterator() {
                return new Iterator<>(this);
            }

            @Override
            public T peek() {
                return this.value;
            }

            @Override
            public T poll() {
                pop();
                return value;
            }

            @Override
            public Immutable<T> pop() {
                return this.tail;
            }

            @Override
            public int size() {
                return size;
            }

            @Override
            public Object[] toArray() {
                Object[] result = new Object[size];
                assembleArray(result);
                return result;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T2> T2[] toArray(T2[] a) { // NOPMD
                T2[] adjustedArray = a;
                if (adjustedArray.length < size) {
                    adjustedArray = (T2[]) Array.newInstance(adjustedArray.getClass().getComponentType(), size);
                }
                assembleArray(adjustedArray);
                if (adjustedArray.length > size) {
                    adjustedArray[size] = null;
                }

                return adjustedArray;
            }

        }

        class Empty<T> implements Immutable<T> {

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public T element() {
                throw new NoSuchElementException();
            }

            @Override
            public int getIndexOf(Object o) {
                return -1;
            }

            @Override
            public java.util.Iterator<T> iterator() {
                return new Iterator<>(this);
            }

            @Override
            public T peek() {
                return null;
            }

            @Override
            public T poll() {
                return null;
            }

            @Override
            public Immutable<T> pop() {
                throw new IllegalStateException("Empty");
            }

            @Override
            public T remove() {
                throw new NoSuchElementException();
            }

            @Override
            public int size() {
                return 0;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return (T[]) Array.newInstance(a.getClass().getComponentType(), 0);
            }
        }

        class Iterator<T> implements java.util.Iterator<T> {
            private Immutable<T> pile;

            public Iterator(final Immutable<T> pile) {
                this.pile = pile;
            }

            @Override
            public boolean hasNext() {
                return !this.pile.isEmpty();
            }

            @Override
            public T next() {
                T result = this.pile.peek();
                this.pile = this.pile.pop();
                return result;
            }
        }
    }

    class Mutable<T> implements Pile<Mutable<T>, T> { // NOPMD Type

        private Node<T> head;
        private int size;
        private int modCount;

        @Override
        public boolean add(T value) {
            push(value);
            return true;
        }

        @Override
        public boolean addAll(Collection<? extends T> values) {
            values.forEach(this::add);
            return true;
        }

        private void assembleArray(Object[] array) { // NOPMD
            Node<T> x = head;
            int i = 0;
            while (x != null) {
                array[i++] = x.value;
                x = x.next;
            }
        }

        @Override
        public void clear() {
            popAll();
        }

        @Override
        public T element() {
            Node<T> f = head;
            if (f == null) {
                throw new NoSuchElementException();
            }
            return f.value;
        }

        public int getIndexOf(Object o) {
            int index = 0;
            Node<T> currentNode = head;
            while (currentNode != null) {
                if (Objects.equals(currentNode.value, o)) {
                    return index;
                }
                index++;
                currentNode = currentNode.next;
            }
            return -1;
        }

        private Node<T> getNode(int index) {
            Node<T> node = head;
            for (int i = 0; i < index; i++) {
                node = node.next;
            }
            return node;
        }

        @Override
        public java.util.Iterator<T> iterator() {
            return new Iterator(0);
        }

        private void link(Node<T> node) {
            node.next = head;
            head = node;
            size++;
            modCount++;
        }

        @Override
        public boolean offer(T t) {
            return add(t);
        }

        public T peek() {
            return Optional.ofNullable(head).map(n -> n.value).orElse(null);
        }

        @Override
        public T poll() {
            return Optional.ofNullable(head).map(node -> unlink(node, null)).map(n -> n.value).orElse(null);
        }

        @Override
        public Mutable<T> pop() {
            remove();
            return this;
        }

        @Override
        public Mutable<T> popAll() {
            unlinkAll(head);
            head = null;

            size = 0;
            modCount++;

            return this;
        }

        private Node<T> popNode() {
            return unlink(head, null);
        }

        public Mutable<T> push(T value) {
            link(new Node<>(value, null));
            return this;
        }

        @Override
        public boolean remove(Object o) {
            if (size == 0) {
                return false;
            }
            Node<T> nodePredecessor = null;
            for (Node<T> current = head; current != null; current = current.next) {
                if (Objects.equals(current.value, o)) {
                    unlink(current, nodePredecessor);
                    current.value = null;
                    return true;
                }
                nodePredecessor = current;
            }
            return false;
        }

        @Override
        public T remove() {
            if (head == null) {
                throw new NoSuchElementException();
            }
            return popNode().value;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public Spliterator<T> spliterator() {
            return toSpliterator(iterator());
        }

        public Stream<T> stream(boolean drain) {
            if (drain) {
                return StreamSupport.stream(toSpliterator(new DrainingIterator()), false);
            }
            return stream();
        }

        @Override
        public Object[] toArray() {
            Object[] result = new Object[size];
            assembleArray(result);
            return result;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T[] toArray(T[] a) { // NOPMD
            T[] adjustedArray = a;
            if (adjustedArray.length < size) {
                adjustedArray = (T[]) Array.newInstance(adjustedArray.getClass().getComponentType(), size);
            }
            assembleArray(adjustedArray);
            if (adjustedArray.length > size) {
                adjustedArray[size] = null;
            }

            return adjustedArray;
        }

        private Spliterator<T> toSpliterator(java.util.Iterator<T> iterator) {
            return Spliterators.spliterator(iterator, size, Spliterator.ORDERED & Spliterator.SIZED);
        }

        private Node<T> unlink(Node<T> node, Node<T> nodePredecessor) {
            Node<T> next = node.next;
            node.next = null;
            if (nodePredecessor == null) {
                head = next;
            } else {
                nodePredecessor.next = next;
            }
            size--;
            modCount++;
            return node;
        }

        private void unlinkAll(Node<T> node) {
            Node<T> currentNode = node;
            while (currentNode != null) {
                Node<T> next = currentNode.next;
                currentNode.next = null;
                currentNode = next;
            }
        }

        private static class Node<T> {
            private T value;
            private Node<T> next;

            Node(T value, Node<T> next) {
                this.value = value;
                this.next = next;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        private class Iterator implements java.util.Iterator<T> {
            private final int expectedModCount = modCount;
            private Node<T> node;
            private int nextIndex;

            Iterator(int index) {
                node = index >= size ? null : getNode(index);
                nextIndex = index;
            }

            final void checkForComodification() {
                if (modCount != expectedModCount) {
                    throw new ConcurrentModificationException();
                }
            }

            @Override
            public boolean hasNext() {
                return nextIndex < size;
            }

            @Override
            public T next() {
                checkForComodification();
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                Node<T> lastVisited = node;
                node = node.next;
                nextIndex++;
                return lastVisited.value;
            }
        }

        private final class DrainingIterator implements java.util.Iterator<T> {
            @Override
            public boolean hasNext() {
                return !isEmpty();
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                return Mutable.this.remove();
            }
        }

    }
}
