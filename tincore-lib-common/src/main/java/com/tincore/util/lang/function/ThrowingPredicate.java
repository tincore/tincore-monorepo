package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.ExceptionMapper;
import lombok.experimental.ExtensionMethod;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ThrowingPredicate<T, X extends Throwable> {
    ThrowingPredicate<Object, RuntimeException> TAUTOLOGY = a -> true;
    ThrowingPredicate<Object, RuntimeException> CONTRADICTION = a -> false;

    @SuppressWarnings("unchecked")
    static <T, X extends Throwable> ThrowingPredicate<T, X> contradiction() {
        return (ThrowingPredicate<T, X>) CONTRADICTION;
    }

    @SuppressWarnings("unchecked")
    static <T, X extends Throwable> ThrowingPredicate<T, X> tautology() {
        return (ThrowingPredicate<T, X>) TAUTOLOGY;
    }

    static <T, X extends Throwable> Predicate<T> uPredicate(ThrowingPredicate<? super T, ? extends X> predicate) {
        Objects.requireNonNull(predicate);
        return uPredicate(predicate, ExceptionMapper.wrapChecked());
    }

    static <T, X extends Throwable, Y extends RuntimeException> Predicate<T> uPredicate(
        ThrowingPredicate<? super T, ? extends X> predicate,
        Function<? super X, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(predicate);
        Objects.requireNonNull(exceptionMapper);
        return uPredicate(predicate, ExceptionMapper.wrapChecked(exceptionMapper));
    }

    static <T, X extends Throwable, Y extends RuntimeException> Predicate<T> uPredicate(
        ThrowingPredicate<? super T, ? extends X> predicate,
        ExceptionMapper<? super T, ? extends Y> exceptionMapper) {
        Objects.requireNonNull(predicate);
        Objects.requireNonNull(exceptionMapper);
        return t -> predicate.testUnchecked(t, exceptionMapper.applyPartial(t));
    }

    default ThrowingPredicate<T, X> and(final ThrowingPredicate<? super T, X> other) {
        Objects.requireNonNull(other);
        return t -> test(t) && other.test(t);
    }

    default ThrowingPredicate<T, X> negate() {
        return t -> !test(t);
    }

    default ThrowingPredicate<T, X> or(final ThrowingPredicate<? super T, X> other) { // NOPMD
        Objects.requireNonNull(other);
        return t -> test(t) || other.test(t);
    }

    boolean test(T t) throws X;

    default <Y extends RuntimeException> boolean testUnchecked(T t, BiFunction<? super T, Throwable, ? extends Y> exceptionMapper) {
        return testUnchecked(t, exceptionMapper.applyPartial(t));
    }

    default <Y extends RuntimeException> boolean testUnchecked(T t, Function<Throwable, ? extends Y> throwableMapper) {
        try {
            return test(t);
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD
            throw throwableMapper.apply(e);
        }
    }

}
