package com.tincore.util.lang.function;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.lang.Either;
import com.tincore.util.lang.ExceptionMapper;
import lombok.experimental.ExtensionMethod;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.tincore.util.lang.ExceptionMapper.wrapChecked;

@FunctionalInterface
@ExtensionMethod(FunctionExtension.class)
public interface ThrowingSupplier<T, X extends Throwable> {

    static <T, X extends Throwable> T uGet(ThrowingSupplier<T, ? extends X> throwingSupplier) {
        return uSupplier(throwingSupplier).get();
    }

    static <T, X extends Throwable, Y extends RuntimeException> T uGet(
        ThrowingSupplier<T, ? extends X> throwingSupplier,
        ExceptionMapper<Void, ? extends Y> exceptionMapper) {
        return uSupplier(throwingSupplier, exceptionMapper).get();
    }

    static <T, X extends Throwable> Supplier<T> uSupplier(ThrowingSupplier<T, ? extends X> throwingSupplier) {
        return uSupplier(throwingSupplier, wrapChecked());
    }

    static <T, X extends Throwable, Y extends RuntimeException> Supplier<T> uSupplier(
        ThrowingSupplier<T, ? extends X> throwingSupplier,
        Function<? super X, ? extends Y> exceptionMapper) {
        return uSupplier(throwingSupplier, wrapChecked(exceptionMapper));
    }

    static <T, X extends Throwable, Y extends RuntimeException> Supplier<T> uSupplier(
        ThrowingSupplier<T, ? extends X> throwingSupplier,
        ExceptionMapper<Void, ? extends Y> exceptionMapper) {
        return () -> throwingSupplier.getUnchecked(exceptionMapper.applyPartial(null));
    }

    default <U> ThrowingSupplier<U, X> andThen(Function<? super T, ? extends U> function) {
        Objects.requireNonNull(function);
        return () -> function.apply(get());
    }

    T get() throws X;

    @SuppressWarnings("unchecked")
    default Either<X, T> getEither(Function<? super X, Optional<X>> liftedExceptionMapper) {
        try {
            return Either.ofRight(get());
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD
            return liftedExceptionMapper.apply((X) e)
                .map(Either::<X, T>ofLeft)
                .orElseThrow(() -> wrapChecked(e));
        }
    }

    default Supplier<Either<X, T>> getEitherSupplier(Function<? super X, Optional<X>> exceptionMapper) {
        return () -> getEither(exceptionMapper);
    }

    default <X extends Throwable, Y extends RuntimeException> T getUnchecked(Function<Throwable, ? extends Y> throwableMapper) {
        try {
            return get();
        } catch (Error e) { // NOPMD Intentional
            throw e;
        } catch (Throwable e) { // NOPMD
            throw throwableMapper.apply(e);
        }
    }
}
