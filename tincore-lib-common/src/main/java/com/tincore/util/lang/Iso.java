package com.tincore.util.lang;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public interface Iso<T, U> { // NOPMD short name

    static <T, U> Iso<T, U> of(Function<T, U> getter, Function<U, T> reverseGetter) { // NOPMD
        return new Iso<>() {
            @Override
            public U get(T t) {
                return getter.apply(t);
            }

            @Override
            public T reverseGet(U u) {
                return reverseGetter.apply(u);
            }
        };
    }

    static <T> Iso<T[], Stream<T>> ofArrayToStream(Class<T> type) {
        return of(Arrays::stream, s -> s.toArray(a -> (T[]) Array.newInstance(type, a)));
    }

    static <T> Iso<List<T>, Stream<T>> ofListToStream() {
        return of(List::stream, Stream::toList);
    }

    default <V> Iso<T, V> andThen(Iso<U, V> iso) {
        return of(t -> iso.get(this.get(t)), v -> this.reverseGet(iso.reverseGet(v)));
    }

    default <S> Iso<S, U> compose(Iso<S, T> iso) {
        return iso.andThen(this);
    }

    U get(T t);

    default Iso<U, T> reverse() {
        return new Iso<>() {
            @Override
            public T get(U u) {
                return Iso.this.reverseGet(u);
            }

            @Override
            public U reverseGet(T t) {
                return Iso.this.get(t);
            }
        };
    }

    T reverseGet(U u);

    default T transform(T t, UnaryOperator<U> uUnaryOperator) {
        return reverseGet(uUnaryOperator.apply(get(t)));
    }
}
