package com.tincore.util.form;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

// import org.mapstruct.AfterMapping;
// import org.mapstruct.MappingTarget;
// import org.springframework.data.domain.Page;
// import org.springframework.stereotype.Component;

// @Component
public class FormMapperUtil {

    // @Autowired
    // private ObjectMapper mapper;
    //
    // public String toString64(JsonNode jsonNode) {
    // return jsonNode != null ? jsonNode.toString64() : null;
    // }
    //
    // public JsonNode toString64(String string) {
    // if (string == null) {
    // return NullNode.getInstance();
    // }
    // try {
    // return mapper.readTree(string);
    // } catch (IOException e) {
    // throw new RuntimeException(e);
    // }
    // }

    // @AfterMapping
    // public void onAfterPage(Page<?> page, @MappingTarget PageForm<?> pageForm) {
    // pageForm.setSize(page.getSize());
    // pageForm.setNumber(page.getNumber());
    // pageForm.setTotalElements(page.getTotalElements());
    // pageForm.setTotalPages(page.getTotalPages());
    // }

}
