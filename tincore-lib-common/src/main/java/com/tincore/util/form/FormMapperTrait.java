package com.tincore.util.form;

/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public interface FormMapperTrait {

    default Date toDate(long value) {
        return new Date(value);
    }

    default LocalDate toLocalDate(String source) {
        return Optional.ofNullable(source).map(LocalDate::parse).orElse(null);
    }

    default long toLong(Date value) {
        return Optional.ofNullable(value).map(Date::getTime).orElse(0L);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    default String toString(Optional<String> value) {
        return Optional.ofNullable(value).map(value1 -> value1.orElse(null)).orElse(null);
    }

    default String toString(UUID value) {
        return Optional.ofNullable(value).map(UUID::toString).orElse(null);
    }

    default String toString(CharSequence value) {
        return Optional.ofNullable(value).map(CharSequence::toString).orElse(null);
    }

    default UUID toUuid(String value) {
        return Optional.ofNullable(value).map(UUID::fromString).orElse(null);
    }

    default ZonedDateTime toZonedDateTime(String source) {
        return Optional.ofNullable(source).map(ZonedDateTime::parse).orElse(null);
    }
}
