# Tincore Monorepo
[![pipeline status](https://gitlab.com/tincore/tincore-monorepo/badges/master/pipeline.svg)](https://gitlab.com/tincore/tincore-monorepo/-/commits/master)

This repository contains shared libraries and build infrastructure.

## JDK

Note that some JDK compiles much slower than others (there are some lombok extension methods). Corretto seems a good choice

## Projects

### Spring Boot Starter MQTT

Spring boot starter for MQTT.

#### Installation

Add maven dependency to activate

```
    <dependency>
        <groupId>com.tincore</groupId>
        <artifactId>spring-boot-starter-mqtt</artifactId>
        <version>2.4.5</version>
    </dependency>
```

#### Configuration

Add mqtt property in your yaml/properties file.

```
mqtt:
    enabled: true
    autostart: true
    autostart-delay: PT1S
    clients:
    - id: clientIdentification
      uris: tcp://127.0.0.1:1883 # Server Uri. Default value tcp://127.0.0.1:1883

# Alternatively you may declare multiple uris
#      uris: 
#        - tcp://127.0.0.1:1883
#        - tcp://otherserver:1883

      username: username # Server user name (Optional)
      password: password # Server password (Optional)
      persistenceDirectory: ~/somePath # Path to message persistence. If not set will use memory (Optional)
      enableSharedSubscription: true # Allow shared MQTT subscriptions (Optional)
      publishQos: AT_MOST_ONCE # One of AT_MOST_ONCE, AT_LEAST_ONCE, EXACTLY_ONCE
      subscribeQos: AT_MOST_ONCE # One of AT_MOST_ONCE, AT_LEAST_ONCE, EXACTLY_ONCE
      maxReconnectDelay: PT60S # Maximum time to wait between reconnects
      keepAliveInterval: PT60S
      connectionTimeout: PT30S
      executorServiceTimeout: PT10S
      cleanSession: true
      automaticReconnect: true
      lwt: # Last will testament (optional)
        topic: someLastWillTopic
        payload: some payload
        qos: AT_MOST_ONCE # One of AT_MOST_ONCE, AT_LEAST_ONCE, EXACTLY_ONCE
        retained: true # Mqtt retained        

    - id: otherClientIdentification
    ...
    
```

You can customize different aspects by providing your own implementation of the following beans:

```
    MqttAsyncClientFactory.class
    MqttConnectOptionsFactory.class
    MqttPublisher.class
    MqttPayloadReadConverterFactory.class
    MqttPayloadWriteConverter.class
```

#### Usage

##### Subscribe MQTT

###### Annotations

Subscribe on topic and get payload as string

```
        @MqttMapping("someTopic")
        public void onMessage(String payload) {
```

Subscribe on topic and get payload as byte array

```
        @MqttMapping("someTopic")
        public void onMessage(byte[] payload) {
```

Subscribe on topic filter and get topic and payload as string

```
        @MqttMapping("someTopic/+")
        public void onMessage(String topic, String payload) {
```

Subscribe and convert payload to json or other supported type. Uses registered converter.

```
        @MqttMapping("someTopic/+")
        public void onMessage(String topic, MyPojo payload) {

        @MqttMapping("someTopic/+")
        public void onMessage(String topic, UUID payload) {
```

Subscribe multiple mappings on same method

```
        @MqttMapping("someTopic/+")
        @MqttMapping("someOtherTopic/sub1/sub2")
        public void onMessage(String topic, MyPojo payload) {
```

Subscribe payload with different parameter name

```
        @MqttMapping("someTopic/+")
        public void onMessage(String topic, @MqttPayload MyPojo myPojo) {
```

Subscribe topic with different parameter name

```
        @MqttMapping("someTopic/+")
        public void onMessage(@MqttTopic String cause, MyPojo myPojo) {
```

Subscribe topic with filter and parameters

```
        @MqttMapping("someTopic/{parameter1}/sub/{parameter2}")
        public void onMessage(String parameter1, String parameter2, String payload) {
```

Subscribe topic with filter and parameters

```
        @MqttMapping("someTopic/{parameter1}/sub/{parameter2}")
        public void onMessage(String parameter1, String parameter2, String payload) {
```

Subscribe topic with filter and parameter with different name

```
        @MqttMapping("someTopic/{para1}/sub/#")
        public void onMessage(@MqttPathVariable("param1") String subTopic, String payload) {
```

Subscribe topic and bind MqttMessage

```
        @MqttMapping("someTopic/{para1}/sub/#")
        public void onMessage(@MqttPathVariable("param1") String subTopic, MqttMessage message) {
```

###### Programmatically

Inject reference to MqttSubscriberRegistry and MqttClientManager

```
    MqttSubscriberRegistry mqttSubscriberRegistry;
    MqttClientManager mqttClientManager;
```

Add subscribers to registry

```
        TestMqttReceivedMessageHandler mqttMessageHandler = new TestMqttReceivedMessageHandler();
       
        MqttSubscriber subscriber = new MqttSubscriber(List.of(new MqttSubscriberMapping(TOPIC_STRING_A)), (topic, mqttMessage, mqttSubscriberMapping, clientId) -> {
            // ....
        })
        
        mqttSubscriberRegistry.addMqttSubscriber(subscriber);
```

Send subscription updates to server

```
        mqttClientManager.sendSubscriptions();
```

##### Publish MQTT

Inject reference to MqttPublisher

```
    MqttPublisher mqttPublisher;
```

#### Other topics

##### Maven deploy to artifactory

Use artifactory profile (by default deploys to gitlab)

```
mvn deploy -Partifactory -Dartifactory.password=*****
```

## Docker Creator

```
docker run -it com.tincore/tincore-docker-constructor thrift -help
```

## Thrift

Parent projects to facilitate Thrift stub building


### Building generator binary

To build latest version from sources.

1. Download latest Thrift and unpack.

```
https://thrift.apache.org/download
```

2.a Just install client library. Anything from 0.13.0 seems to, at least, work (Jammy contains 0.16.0) 0.17.0 is most recent release atm
```
sudo apt-get install thrift-compiler
```

2.b Try to use /build/docker/README.md

2.c Try to compile locally
```
https://thrift.apache.org/docs/install/debian
```
```
sudo apt-get install automake bison flex g++ git libboost-all-dev libevent-dev libssl-dev libtool make pkg-config
```
For java do not forget gradle!
```
sudo apt-get install gradle
```

### Important

The project contains prebuilt versions that run on gitlabCI
There are some prebuilt versions that run on gitlabCI (libstd version problems)

More up to date docker images crash again. (maven 3.8+ java 17)!! 

##

### Other Tips

#### Git

Make sure that default commit author is set

```
git config -e --local
```

```
[user]
      name = username
      email = username@domain.com
```

To rename old/invalid author from history (just in case of mistake)

```
 git filter-branch --commit-filter '
        if [ "$GIT_AUTHOR_EMAIL" = "contact@tincore.com" ];
        then
                GIT_AUTHOR_NAME="Tincore";
                GIT_AUTHOR_EMAIL="contact@tincore.com";
                git commit-tree "$@";
        else
                git commit-tree "$@";
        fi' HEAD
```

#### Gitlab

##### Compare variables case normal build vs tag build

Tag
```
declare -x CI_COMMIT_TAG="2.4.3-20201215130351"
declare -x CI_COMMIT_REF_NAME="2.4.3-20201215130351"
declare -x CI_COMMIT_REF_PROTECTED="false"
declare -x CI_DEFAULT_BRANCH="master"
```
 
Normal push on master 
```
declare -x CI_COMMIT_BRANCH="master"
declare -x CI_COMMIT_REF_NAME="master"
declare -x CI_COMMIT_REF_PROTECTED="true"
declare -x CI_DEFAULT_BRANCH="master"
```

### GPG
Read this
    https://central.sonatype.org/publish/requirements/gpg/

Quick
    - gpg --gen-key
    - gpg --list-signatures --keyid-format 0xshort
    - gpg --keyserver keyserver.ubuntu.com --send-keys B21F42F6961178794C128A47977281C38D6AB2F8

PubKey B21F42F6961178794C128A47977281C38D6AB2F8

    gpg --keyserver keyserver.ubuntu.com --recv-keys B21F42F6961178794C128A47977281C38D6AB2F8

#### GitlabCI

    gpg --export-secret-key --armor > tincore.gitlab.com.private

## Maven

setting.xml sample

```
<settings>
    <servers>
        <server>
            <id>gitlab-maven</id>
            <configuration>
                <httpHeaders>

                    <property>
                        <name>Deploy-Token</name>
                        <value>[GITLAB_PROJECT OR PROJECT GROUP DEPLOY TOKEN_HERE]</value>
                    </property>
<!--
                    <property>
                        <name>Private-Token</name>
                        <value>[GITLAB_PRIVATETOKEN_HERE]</value>
                    </property>
-->
                </httpHeaders>
            </configuration>
        </server>
        <server>
            <id>gitlab-scm</id>
            <username>tincore.android</username>
            <password>[GITLAB_PRIVATETOKEN_HERE]</password>
        </server>
   		<server>
  			<id>ossrh</id>
  			<username>[TOKEN_USERNAME_HERE]</username>
  			<password>[TOKEN_PASSWORD_HERE]</password>
		</server>
    </servers>
    <profiles>
        <profile>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <gpg.keyname>[GPG_KEY_NAME_HERE]</gpg.keyname>
                <gpg.passphrase>[GPG_PASSWORD_HERE]</gpg.passphrase>
            </properties>
        </profile>
    </profiles>
</settings>
```

