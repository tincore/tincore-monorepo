# Principles

- Zero bug policy
- Any successful build is ready for production
- Development should always be possible without network dependencies

# Build and Test

These are maven projects.

[Maven documentation](http://maven.apache.org/guides/index.html)

## Java

In Ubuntu make sure that JAVA_HOME is set

```
sudo nano /etc/environment
```

Add

```
JAVA_HOME="/usr/lib/jvm/default-java"
```

Execute
```
source /etc/environment
```

## Native


apt-get update -qq && apt-get install -y -qq build-essential libz-dev zlib1g-dev && apt-get install -y flex bison

## CI/CD and Branches

TO_SETUP

## QA

As part of project default execution the following plugins are enabled.

- PMD
- Checkstyle
- Spotbugs (including secbugs)

Rule definitions for these plugins are stored under /qa folder.

### Sonar

If you with to start a local Sonar you can use the official image

```
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```

- Browse *<http://localhost:9000>* 
- Login with default user and password *admin:admin*
- Generate a token from tutorial or go to help
- Use generated token in a maven command like:
 
```
mvn sonar:sonar \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=7944340999e4da0bf2983d3a23124bf70ee0f541
```


## Documentation

Documentation is generated with [maven](https://maven.apache.org/plugins/maven-site-plugin/)


### Local access

You can generate documentation locally by running

```
mvn site site:stage
```

And then you can browse documentation at

```
target/staging
```

Alternatively, you can generate documentation in a fixed location in your filesystem.

For example
```
mvn site site:stage -DstagingDirectory=$HOME/docs/tincore-monorepo
```

If you do this with each of the repositories that inherit from tincore-parent then you 
will get aggregated documentation for all this projects. 
