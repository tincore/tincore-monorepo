#!/usr/bin/env bash
export CHROME_VERSION="114.0.5735.106-1"
export CHROME_DRIVER_VERSION="114.0.5735.90"
export FIREFOX_VERSION="105.0"
export GECKODRIVER_VERSION="v0.31.0"
export VAULT_VERSION="1.13.1"
export THRIFT_COMPILER_VERSION="0.19.0-2.1+b1"
#thrift-compiler_0.20.0-1_amd64.deb

wget -O /tmp/vault.zip https://releases.hashicorp.com/vault/$VAULT_VERSION/vault_${VAULT_VERSION}_linux_amd64.zip
wget -O /tmp/thrift-compiler.deb http://ftp.nl.debian.org/debian/pool/main/t/thrift/thrift-compiler_${THRIFT_COMPILER_VERSION}_amd64.deb
wget -O /tmp/chromedriver.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip
wget -O /tmp/firefox.tar.bz2 https://download-installer.cdn.mozilla.net/pub/firefox/releases/$FIREFOX_VERSION/linux-x86_64/en-US/firefox-$FIREFOX_VERSION.tar.bz2
wget -O /tmp/geckodriver.tar.gz https://github.com/mozilla/geckodriver/releases/download/$GECKODRIVER_VERSION/geckodriver-$GECKODRIVER_VERSION-linux64.tar.gz
wget -O /tmp/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 \
