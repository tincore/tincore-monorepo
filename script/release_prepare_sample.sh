#!/usr/bin/env bash
#mvn --batch-mode release:prepare -DautoVersionSubmodules -DcommitByProject -Pquick
#mvn --batch-mode release:rollback -DautoVersionSubmodules -DcommitByProject -Pquick
mvn --batch-mode release:prepare -DautoVersionSubmodules -DcommitByProject -Pquick
# -Dtag=my-proj-1.2 -DreleaseVersion=1.2 -DdevelopmentVersion=2.0-SNAPSHOT

#mvn --batch-mode release:update-versions

# Tags with current version
#mvn scm:tag

#mvn scm:tag -Dtag=CUSTOM_TAG