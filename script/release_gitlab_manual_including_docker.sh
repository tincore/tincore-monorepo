#!/usr/bin/env bash
cd ..
releaseId=$(date '+%Y%m%d%H%M')
mvn validate -PupdateReleaseId -DreleaseId=$releaseId
export CI_REGISTRY="registry.gitlab.com"
export CI_REGISTRY_IMAGE="registry.gitlab.com/tincore/tincore-monorepo"
export CI_PROJECT_ID="11418513"
export CI_COMMIT_REF_NAME="master"
mvn deploy -PciGitlab -DskipTests
mvn validate -PupdateReleaseId -DreleaseId=SNAPSHOT
