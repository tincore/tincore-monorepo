#!/usr/bin/env bash
cd ..
releaseId=$(date '+%Y%m%d%H%M')
mvn validate -PupdateReleaseId -DreleaseId=$releaseId
export CI_PROJECT_ID="11418513"
# Quick excludes docker
#mvn install -Pquick,ciGitlab
mvn deploy -Pquick,ciGitlab
mvn validate -PupdateReleaseId -DreleaseId=SNAPSHOT
