package com.tincore.test.support.selenium;

/*-
 * #%L
 * tincore-test-lib-functional
 * %%
 * Copyright (C) 2021 - 2022 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.springframework.context.ApplicationContext;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.openqa.selenium.net.PortProber.findFreePort;

@Slf4j
@RequiredArgsConstructor
public class SeleniumWebDriverService {
    private final SeleniumConfiguration seleniumConfiguration;
    private final List<WebDriver> webDrivers = new CopyOnWriteArrayList<>();
    private WebDriverManager webDriverManager;

    public WebDriver createWebDriver() {
        WebDriver webDriver;
        switch (seleniumConfiguration.getDriver()) {
            case CHROME:
                webDriverManager = WebDriverManager.chromedriver();
                setUpWebDriverManagerGeneralOptions(webDriverManager);
                webDriverManager.setup();
                webDriver = new ChromeDriver();
                break;
            case CHROME_DOCKER:
                webDriverManager = WebDriverManager.chromedriver().browserInDocker();
                setUpWebDriverManagerGeneralOptions(webDriverManager);
                if (seleniumConfiguration.isVncEnabled()) {
                    webDriverManager.enableVnc();
                }
                if (!seleniumConfiguration.getDockerExtraHosts().isEmpty()) {
                    webDriverManager.dockerExtraHosts(seleniumConfiguration.getDockerExtraHosts().toArray(String[]::new));
                }
                webDriver = webDriverManager.create();
                break;
            case CHROMIUM:
                webDriverManager = WebDriverManager.chromiumdriver();
                setUpWebDriverManagerGeneralOptions(webDriverManager);
                Path browserPath = webDriverManager.getBrowserPath().orElseThrow(() -> new IllegalStateException("Could not find browser for CHROMIUMN"));
                WebDriverManager.chromiumdriver().setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setBinary(browserPath.toFile());
                chromeOptions.addArguments("--no-sandbox");
                chromeOptions.addArguments("--disable-gpu");
                chromeOptions.addArguments("--disable-dev-shm-usage");
                chromeOptions.addArguments("--remote-debugging-port=" + findFreePort());
//        chromeOptions.addArguments("start-maximized");
//        chromeOptions.addArguments("disable-infobars"); // disabling infobars
//        chromeOptions.addArguments("disable-extensions"); // disabling extensions
                if (seleniumConfiguration.getBrowser().isHeadless()) {
                    chromeOptions.addArguments("--headless=new");
                }

                webDriver = new ChromeDriver(chromeOptions);
                break;
            case EDGE:
                webDriverManager = WebDriverManager.edgedriver();
                setUpWebDriverManagerGeneralOptions(webDriverManager);
                webDriverManager.setup();

                webDriver = new EdgeDriver();
                break;
            case FIREFOX: {
                webDriverManager = WebDriverManager.firefoxdriver();
                setUpWebDriverManagerGeneralOptions(webDriverManager);
                webDriverManager.setup();

                FirefoxOptions firefoxOptions = new FirefoxOptions();
                seleniumConfiguration.getFirefoxOptionCapabilities().forEach(s -> firefoxOptions.setCapability(s, true));
                if (seleniumConfiguration.getBrowser().isHeadless()) {
                    firefoxOptions.addArguments("-headless");
                }

                webDriver = new FirefoxDriver(firefoxOptions);
            }
            break;
            case FIREFOX_DOCKER:
                webDriverManager = WebDriverManager.firefoxdriver().browserInDocker();
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                seleniumConfiguration.getFirefoxOptionCapabilities().forEach(s -> firefoxOptions.setCapability(s, true));
                webDriverManager.capabilities(firefoxOptions);

                setUpWebDriverManagerGeneralOptions(webDriverManager);
                if (seleniumConfiguration.isVncEnabled()) {
                    webDriverManager.enableVnc();
                }
                if (!seleniumConfiguration.getDockerExtraHosts().isEmpty()) {
                    webDriverManager.dockerExtraHosts(seleniumConfiguration.getDockerExtraHosts().toArray(String[]::new));
                }
                webDriver = webDriverManager.create();
                break;
            case OPERA:
                webDriverManager = WebDriverManager.operadriver();
                setUpWebDriverManagerGeneralOptions(webDriverManager);
                webDriver = webDriverManager.create();
                break;
            case SAFARI:
                webDriverManager = WebDriverManager.safaridriver();
                setUpWebDriverManagerGeneralOptions(webDriverManager);
                webDriverManager.getBrowserPath().orElseThrow(() -> new IllegalStateException("Could not find browser for SAFARI"));
                webDriver = webDriverManager.create();
                break;
            case HTMLUNIT:
            case DEFAULT:
            default:
                webDriverManager = WebDriverManager.getInstance();
                setUpWebDriverManagerGeneralOptions(webDriverManager);
                webDriver = webDriverManager.create();
                break;
        }
        webDrivers.add(webDriver);
        return webDriver;
    }

    @PreDestroy
    public void preDestroy() {
        webDrivers.stream()
            .filter(Objects::nonNull)
            .forEach(d -> {
                try {
                    d.quit();
                } catch (Exception e) { // NOPMD
                    log.warn("Error quitting driver ", e);
                }
            });

        webDriverManager.quit();
    }

    public void release(WebDriver webDriver, ApplicationContext applicationContext) {
        if (seleniumConfiguration.isCloseDriverAfterEachTest()) {
            webDriver.close();
        }
        if (seleniumConfiguration.isQuitDriverAfterEachTest()) {
            try {
                webDriverManager.quit(webDriver);
            } catch (Exception e) { // NOPMD
                // Do nothing
                log.info("WebDriver quit failed. Continuing.", e);
            }
        }
        if (seleniumConfiguration.isNewDriverEachTest()) {
            applicationContext.getBean(SeleniumFunctionalTestScope.class).reset();
        }
    }

    public void saveScreenshot(String fileName, File screenshot) throws IOException {
        Path targetPath = seleniumConfiguration.getScreenshotPath().resolve(fileName);
        Path parent = targetPath.getParent();
        if (parent != null) {
            Files.createDirectories(parent);
        }
        try {
            Files.copy(screenshot.toPath(), targetPath);
        } catch (FileSystemException e) {
            log.error("Could not save screenshot at path={}", targetPath, e);
        }
    }

    private void setUpWebDriverManagerGeneralOptions(WebDriverManager webDriverManager) {
        if (!seleniumConfiguration.isTracingEnabled()) {
            webDriverManager.disableTracing();
        }
        if (seleniumConfiguration.isRecordingEnabled()) {
            webDriverManager.enableRecording();
        }
    }
}
