package com.tincore.test.support.selenium;

/*-
 * #%L
 * tincore-test-lib-functional
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.Map;

@ComponentScan(basePackageClasses = AbstractSeleniumFunctionalWebITConfiguration.class)
public abstract class AbstractSeleniumFunctionalWebITConfiguration {

    @Bean
    public CustomScopeConfigurer customScopeConfigurer(SeleniumFunctionalTestScope seleniumFunctionalTestScope) {
        CustomScopeConfigurer scopeConfigurer = new CustomScopeConfigurer();
        Map<String, Object> scopes = new HashMap<>();
        scopes.put(SeleniumFunctionalTestScope.ID, seleniumFunctionalTestScope);
        scopeConfigurer.setScopes(scopes);
        return scopeConfigurer;
    }

    @Bean
    public SeleniumFunctionalTestScope getSeleniumFunctionalTestScope() {
        return new SeleniumFunctionalTestScope();
    }

    @Bean
    public abstract SeleniumConfiguration seleniumConfiguration();

    @Bean
    public SeleniumWebDriverService seleniumWebDriverService(SeleniumConfiguration seleniumConfiguration) {
        return new SeleniumWebDriverService(seleniumConfiguration);
    }

    @Bean
    @Scope(SeleniumFunctionalTestScope.ID)
    public WebDriver webDriver(SeleniumWebDriverService seleniumWebDriverService) {
        return seleniumWebDriverService.createWebDriver();
    }
}
