package com.tincore.test.support.selenium;

/*-
 * #%L
 * tincore-test-lib-functional
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.File;
import java.lang.annotation.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@TestExecutionListeners(listeners = {SeleniumTest.ExecutionListener.class,
        DependencyInjectionTestExecutionListener.class}, mergeMode = MERGE_WITH_DEFAULTS)
public @interface SeleniumTest {

    class ExecutionListener extends AbstractTestExecutionListener {

        @Override
        public void afterTestMethod(TestContext testContext) throws Exception {
            WebDriver webDriver = testContext.getApplicationContext().getBean(WebDriver.class);
            SeleniumWebDriverService seleniumWebDriverService =
                    testContext.getApplicationContext().getBean(
                            SeleniumWebDriverService.class);

            if (testContext.getTestException() != null && webDriver instanceof TakesScreenshot) {
                File screenshot = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
                String testName = testContext.getTestClass().getName();
                String methodName = testContext.getTestMethod().getName();
                String fileName = testName + "-" + StringUtils
                        .truncate(methodName, 255 - testName.length() - screenshot.getName().length() - 2) + "-"
                        + screenshot.getName();
                seleniumWebDriverService.saveScreenshot(fileName, screenshot);
                return;
            }

            seleniumWebDriverService.release(webDriver, testContext.getApplicationContext());
        }
    }
}
