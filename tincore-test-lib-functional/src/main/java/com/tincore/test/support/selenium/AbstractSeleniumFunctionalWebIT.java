package com.tincore.test.support.selenium;

/*-
 * #%L
 * tincore-test-lib-functional
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;
import java.util.stream.Stream;

@ExtendWith(SpringExtension.class)
@SeleniumTest
@ActiveProfiles("test")
public abstract class AbstractSeleniumFunctionalWebIT implements SeleniumTrait {

    public static final char PATH_SEPARATOR = '/';
    @Autowired
    private WebDriver driver;

    @Autowired
    private SeleniumConfiguration seleniumConfiguration;

    @Value("${test.server.scheme:http}")
    private String serverScheme;

    @Value("${test.server.host:localhost}")
    private String serverHost;

    @Value("${local.server.port}")
    private int serverPort;

    @Value("${test.server.contextPath:}")
    private String contextPath;


    public String getHost() {
        return seleniumConfiguration.getDriver().isDocker() ? seleniumConfiguration.getDockerReachableHost() : serverHost;
    }

    public void deleteAllCookies() {
        driver.manage().deleteAllCookies();
    }

    @Override
    public WebDriver getDriver() {
        return driver;
    }


    public void goTo(String subPath) {
        goTo(subPath, null);
    }

    public void goTo(String subPath, SeleniumPageEnhanced page) {
        goTo(serverPort, subPath, page);
    }


    public void goTo(int serverPort, String subPath, SeleniumPageEnhanced page) {
        goTo(getHost(), serverPort, subPath, page);
    }


    public void goTo(int serverPort, SeleniumPageEnhanced page) {
        goTo(getHost(), serverPort, page);
    }


    public void goTo(String serverHost, int serverPort, SeleniumPageEnhanced page) {
        goTo(serverHost, serverPort, "", page);
    }


    public void goTo(String serverHost, int serverPort, String subPath, SeleniumPageEnhanced page) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.newInstance()
                .scheme(serverScheme).host(serverHost).port(serverPort);
        Stream.of(contextPath, subPath)
                .filter(StringUtils::isNotBlank)
                .map(s -> StringUtils.removeStart(s, "/"))
                .forEach(uriComponentsBuilder::pathSegment);
        driver.get(uriComponentsBuilder.build().toUriString());
        if (page != null) {
            page.waitForVisibility();
        }
    }

    @BeforeEach
    public void setUp() {
        driver.manage().window().setSize(new Dimension(seleniumConfiguration.getBrowser().getWidth(), seleniumConfiguration.getBrowser().getHeight()));
        Optional.ofNullable(seleniumConfiguration.getImplicitWait())
                .ifPresent(d -> driver.manage().timeouts().implicitlyWait(d));
        Optional.ofNullable(seleniumConfiguration.getPageLoadTimeout())
                .ifPresent(d -> driver.manage().timeouts().pageLoadTimeout(d));
    }
}
