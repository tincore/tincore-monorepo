package com.tincore.test.support.selenium;

/*-
 * #%L
 * tincore-test-lib-functional
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.time.Duration;
import java.util.Collections;
import java.util.List;


@Slf4j
@Data
public class SeleniumConfiguration {
    public static final String DOCKER_REACHABLE_HOST_DEFAULT = "host.docker.internal";
    private static final String[] CHROME_BROWSER_LOCATIONS = {
        "/snap/bin/chromium",
        "/usr/bin/chromium-browser",
        "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
        "/opt/chrome/chrome",
        "/opt/opt/chrome/chrome"};
    private static final String[] CHROME_DRIVER_LOCATIONS = {
        "/snap/bin/chromium.chromedriver",
        "/usr/lib/chromium-browser/chromedriver",
        "/usr/local/bin/chromedriver",
        "/usr/bin/chromedriver"
    };
    private static final String[] GECKO_DRIVER_LOCATIONS = {
        "/usr/bin/geckodriver",
        "/usr/lib/geckodriver/geckodriver",
        "/usr/local/bin/geckodriver"
    };
    private Duration implicitWait;
    private Duration pageLoadTimeout;

    private Driver driver;

    private Path screenshotPath = Path.of("target/screenshots");
    private boolean tracingEnabled;
    private boolean recordingEnabled;
    private boolean vncEnabled;
    private List<String> dockerExtraHosts = List.of(DOCKER_REACHABLE_HOST_DEFAULT + ":host-gateway");
    private String dockerReachableHost = DOCKER_REACHABLE_HOST_DEFAULT;

    private boolean closeDriverAfterEachTest;
    private boolean quitDriverAfterEachTest;
    private boolean newDriverEachTest;

    private List<String> chromeDriverLocations = List.of(CHROME_DRIVER_LOCATIONS);
    private List<String> chromeBrowserLocations = List.of(CHROME_BROWSER_LOCATIONS);
    private boolean chromeBrowserLocationDiscover;
    private boolean chromeSandBoxed;

    private List<String> geckoDriverLocations = List.of(GECKO_DRIVER_LOCATIONS);
    // for example marionette
    private List<String> firefoxOptionCapabilities = Collections.emptyList();

    private BrowserConfiguration browser = new BrowserConfiguration();

    public enum Driver {
        CHROME,
        CHROME_DOCKER(true),
        CHROMIUM,
        EDGE,
        FIREFOX,
        FIREFOX_DOCKER(true),
        OPERA,
        SAFARI,
        HTMLUNIT,
        DEFAULT;

        private final boolean docker;

        Driver(boolean docker) {
            this.docker = docker;
        }

        Driver() {
            this(false);
        }

        public boolean isDocker() {
            return docker;
        }
    }

    @Data
    public static class BrowserConfiguration {

        private static final int DEFAULT_WIDTH = 1280;
        private static final int DEFAULT_HEIGHT = 1024;
        private boolean headless = true;
        private boolean picturesDisabled;
        private boolean crawlerMode;

        private int width = DEFAULT_WIDTH;
        private int height = DEFAULT_HEIGHT;
    }
}
