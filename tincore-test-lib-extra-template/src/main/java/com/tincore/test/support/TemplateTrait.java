package com.tincore.test.support;

/*-
 * #%L
 * tincore-test-lib-extra-template
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ResourceLoader;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public interface TemplateTrait {

    default InputStream getInputStream(String templateResourcePath) throws IOException {
        ResourceLoader resourceLoader = getResourceLoader();
        if (resourceLoader != null) {
            return resourceLoader.getResource(templateResourcePath).getInputStream();
        } else {
            return getClass().getResourceAsStream(templateResourcePath);
        }
    }

    default String getRendered(String templateResourcePath, Map<String, Object> model) {
        try {
            try (InputStream is = getInputStream(templateResourcePath)) {
                String templateContent = IOUtils.toString(is, StandardCharsets.UTF_8);
                try (Writer result = new StringWriter()) {
                    Template template = new Template("response-template", new StringReader(templateContent), new Configuration(new Version(2, 3, 22)));
                    template.process(model, result);
                    result.flush();
                    return result.toString();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    default ResourceLoader getResourceLoader() {
        return null;
    }
}
