package com.tincore.boot.mqtt;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.test.support.AbstractMqttApplicationTests;
import com.tincore.boot.mqtt.test.support.TestMqttMessageCollector;
import com.tincore.boot.mqtt.test.support.TestMqttReceivedMessageHandler;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class MqttProgrammaticSubscriberIntegrationTests extends AbstractMqttApplicationTests {

    private final String messagePayload = "Test A" + System.nanoTime();

    private void assertThatMessageReceivedSuccessfully(String topic, String messagePayload, TestMqttReceivedMessageHandler mqttMessageHandler) throws MqttException, InterruptedException {
        try (var testPublisher = createTestMqttPublisher(topic, messagePayload)) {
            awaitMqtt().until(() -> mqttMessageHandler.getMessageCollector().isMessageReceived());

            var receivedMqttMessage = mqttMessageHandler.getMessageCollector().getMessage();
            assertThat(receivedMqttMessage.getPayload()).isEqualTo(messagePayload.getBytes(UTF_8));
            assertThat(receivedMqttMessage.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);

            testPublisher.disconnect();
        }
    }

    private String createTopic() {
        return "testTopic/topic" + System.nanoTime();
    }

    @Test
    void test_sendSubscription_Given_sameTopicSentTwice_Then_subscriberReceivesStringPayload() throws Exception {
        var topic = createTopic();

        var mqttMessageHandler = new TestMqttReceivedMessageHandler();
        mqttSubscriberRegistry.addMqttSubscriber(new MqttSubscriber(List.of(new MqttSubscriberMapping(topic)), mqttMessageHandler));
        mqttClientManager.sendSubscriptions();

        mqttSubscriberRegistry.addMqttSubscriber(new MqttSubscriber(List.of(new MqttSubscriberMapping(topic)), mqttMessageHandler));
        mqttClientManager.sendSubscriptions();

        assertThatMessageReceivedSuccessfully(topic, messagePayload, mqttMessageHandler);
    }

    @Test
    void test_sendSubscription_Given_singleTopic_When_publisherSendsStringPayloadAndSameTopic_Then_subscriberReceivesStringPayload() throws Exception {

        var topic = createTopic();

        var mqttMessageHandler = new TestMqttReceivedMessageHandler();
        mqttSubscriberRegistry.addMqttSubscriber(new MqttSubscriber(List.of(new MqttSubscriberMapping(topic)), mqttMessageHandler));
        mqttClientManager.sendSubscriptions();

        assertThatMessageReceivedSuccessfully(topic, messagePayload, mqttMessageHandler);
    }

    @Test
    void test_sendSubscription_Given_topicSentAnd_Then_changed_Then_subscriberReceivesStringPayload() throws Exception {

        var topic = createTopic();
        var topicStringB = "testTopic/topicStringB";

        var mqttMessageHandler = new TestMqttReceivedMessageHandler();
        var mqttSubscriberB = new MqttSubscriber(List.of(new MqttSubscriberMapping(topicStringB)), mqttMessageHandler);
        mqttSubscriberRegistry.addMqttSubscriber(mqttSubscriberB);
        mqttClientManager.sendSubscriptions();

        mqttSubscriberRegistry.removeMqttSubscriber(mqttSubscriberB);
        mqttSubscriberRegistry.addMqttSubscriber(new MqttSubscriber(List.of(new MqttSubscriberMapping(topic)), mqttMessageHandler));
        mqttClientManager.sendSubscriptions();

        assertThatMessageReceivedSuccessfully(topic, messagePayload, mqttMessageHandler);
    }
}
