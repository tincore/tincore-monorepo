package com.tincore.boot.mqtt;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.annotation.MqttMapping;
import com.tincore.boot.mqtt.annotation.MqttPayload;
import com.tincore.boot.mqtt.test.support.AbstractMqttApplicationTests;
import com.tincore.boot.mqtt.test.support.TestMqttMessageCollector;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@Import({
    MqttSubscriberParametrizedIntegrationTests.TestUuidMqttMessageHandler.class,
    MqttSubscriberParametrizedIntegrationTests.TestStringMqttMessageHandler.class,
    MqttSubscriberParametrizedIntegrationTests.TestPrimitiveMqttMessageHandler.class
})
class MqttSubscriberParametrizedIntegrationTests extends AbstractMqttApplicationTests {

    @Autowired
    private TestUuidMqttMessageHandler testUuidMqttMessageHandler;

    @Autowired
    private TestStringMqttMessageHandler testStringMqttMessageHandler;

    @Autowired
    private TestPrimitiveMqttMessageHandler testPrimitiveMqttMessageHandler;

    @Test
    void test_subscription_Given_parameterOfTypeNumber_When_publisherSendsStringPayloadAndSameTopic_Then_subscriberReceivesStringPayload() throws Exception {
        var messagePayload = String.valueOf(System.nanoTime());
        var topicParameter = Math.abs(new Random().nextLong());
        var topic = "testTopicNumber/" + topicParameter;

        try (var testPublisher = createTestMqttPublisher(topic, messagePayload)) {

            awaitMqtt().until(() -> testPrimitiveMqttMessageHandler.collector.isMessageReceived());

            var receivedMqttMessage = testPrimitiveMqttMessageHandler.collector.getMessage();
            assertThat(testPrimitiveMqttMessageHandler.parameter).isEqualTo(topicParameter);
            assertThat(receivedMqttMessage.getPayload()).asString().isEqualTo(messagePayload);
            assertThat(receivedMqttMessage.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);

            testPublisher.disconnect();
        }
    }

    @Test
    void test_subscription_Given_parameterOfTypeString_When_publisherSendsStringPayloadAndSameTopic_Then_subscriberReceivesStringPayload() throws Exception {
        var messagePayload = "payload_" + System.nanoTime();
        var topicParameter = "topic_" + System.nanoTime();
        var topic = "testTopicString/" + topicParameter;

        try (var testPublisher = createTestMqttPublisher(topic, messagePayload)) {
            awaitMqtt().until(() -> testStringMqttMessageHandler.collector.isMessageReceived());

            var receivedMqttMessage = testStringMqttMessageHandler.collector.getMessage();
            assertThat(testStringMqttMessageHandler.parameter).isEqualTo(topicParameter);
            assertThat(receivedMqttMessage.getPayload()).asString().isEqualTo(messagePayload);
            assertThat(receivedMqttMessage.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);

            testPublisher.disconnect();
        }
    }

    @Test
    void test_subscription_Given_parameterOfTypeUuid_When_publisherSendsStringPayloadAndSameTopic_Then_subscriberReceivesStringPayload() throws Exception {
        var messagePayload = UUID.randomUUID().toString();
        var topicParameter = UUID.randomUUID();
        var topic = "testTopicUuid/" + topicParameter;

        try (var testPublisher = createTestMqttPublisher(topic, messagePayload)) {
            awaitMqtt().until(() -> testUuidMqttMessageHandler.collector.isMessageReceived());

            var receivedMqttMessage = testUuidMqttMessageHandler.collector.getMessage();
            assertThat(testUuidMqttMessageHandler.parameter).isEqualTo(topicParameter);
            assertThat(receivedMqttMessage.getPayload()).asString().isEqualTo(messagePayload);
            assertThat(receivedMqttMessage.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);

            testPublisher.disconnect();
        }
    }

    @Slf4j
    public static class TestUuidMqttMessageHandler {

        public final TestMqttMessageCollector collector = new TestMqttMessageCollector();

        public UUID parameter;

        @MqttMapping(value = "testTopicUuid/{parameter}")
        public void onMessage(String topic, MqttMessage message, UUID parameter, @MqttPayload UUID payload) {
            this.parameter = parameter;
            collector.addReceivedMessage(topic, message, payload);
        }
    }

    @Slf4j
    public static class TestStringMqttMessageHandler {

        public final TestMqttMessageCollector collector = new TestMqttMessageCollector();

        public String parameter;

        @MqttMapping(value = "testTopicString/{parameter}")
        public void onMessage(String topic, MqttMessage message, String parameter, String payload) {
            this.parameter = parameter;
            collector.addReceivedMessage(topic, message, payload);
        }
    }

    @Slf4j
    public static class TestPrimitiveMqttMessageHandler {

        public final TestMqttMessageCollector collector = new TestMqttMessageCollector();

        private long parameter;

        @MqttMapping(value = "testTopicNumber/{parameter}")
        public void onMessage(String topic, MqttMessage message, long parameter, @MqttPayload long payload) {
            this.parameter = parameter;
            collector.addReceivedMessage(topic, message, payload);
        }
    }
}
