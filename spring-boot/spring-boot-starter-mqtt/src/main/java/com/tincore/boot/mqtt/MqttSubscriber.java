package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class MqttSubscriber {

    private final List<MqttSubscriberMapping> mqttSubscriberMappings;
    private final MqttReceivedMessageHandler messageHandler;
    private final Set<String> clientIds;

    @Getter
    @Setter
    private int order;

    public MqttSubscriber(List<MqttSubscriberMapping> mqttSubscriberMappings, MqttReceivedMessageHandler messageHandler, Set<String> clientIds) {
        this.mqttSubscriberMappings = mqttSubscriberMappings;
        this.messageHandler = messageHandler;
        this.clientIds = clientIds;
    }

    public MqttSubscriber(List<MqttSubscriberMapping> mqttSubscriberMappings, MqttReceivedMessageHandler messageHandler) {
        this(mqttSubscriberMappings, messageHandler, null);
    }

    public MqttReceivedMessageHandler getMessageHandler() {
        return messageHandler;
    }

    public Optional<MqttSubscriberMapping> getMqttSubscriberMappingByTopicMatching(final String clientId, final String topic) {
        if (!isSubscriberByClientId(clientId)) {
            return Optional.empty();
        }

        return mqttSubscriberMappings.stream().filter(d -> d.isTopicMatch(topic)).findFirst();
    }

    public List<MqttSubscriberMapping> getMqttSubscriberMappings() {
        return mqttSubscriberMappings;
    }

    public boolean isSubscriberByClientId(String clientId) {
        if (this.clientIds == null || this.clientIds.isEmpty()) {
            return true;
        }
        return clientIds.contains(clientId);
    }
}
