package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.convert.support.GenericConversionService;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Slf4j
public class MqttPayloadConversionService extends GenericConversionService {

    public MqttPayloadConversionService(MqttPayloadWriteConverter mqttPayloadWriteConverter, MqttPayloadReadConverterFactory mqttPayloadReadConverterFactory) {
        super();

        DefaultConversionService.addDefaultConverters(this);

        addConverter(String.class, byte[].class, s -> s.getBytes(StandardCharsets.UTF_8)); // NOPMD call method

        addConverter(byte[].class, String.class, s -> new String(s, StandardCharsets.UTF_8)); // NOPMD call method
        addConverter(byte[].class, Boolean.class, s -> Boolean.parseBoolean(new String(s, StandardCharsets.UTF_8))); // NOPMD call method
        addConverter(byte[].class, Byte.class, s -> Byte.parseByte(new String(s, StandardCharsets.UTF_8))); // NOPMD
        addConverter(byte[].class, Short.class, s -> Short.parseShort(new String(s, StandardCharsets.UTF_8))); // NOPMD call method
        addConverter(byte[].class, Integer.class, s -> Integer.parseInt(new String(s, StandardCharsets.UTF_8))); // NOPMD call method
        addConverter(byte[].class, Long.class, s -> Long.parseLong(new String(s, StandardCharsets.UTF_8))); // NOPMD call method
        addConverter(byte[].class, Float.class, s -> Float.parseFloat(new String(s, StandardCharsets.UTF_8))); // NOPMD call method
        addConverter(byte[].class, Double.class, s -> Double.parseDouble(new String(s, StandardCharsets.UTF_8))); // NOPMD call method

        addConverter(byte[].class, UUID.class, s -> UUID.fromString(new String(s, StandardCharsets.UTF_8))); // NOPMD call method

        // Declare these as last
        addConverter(mqttPayloadWriteConverter); // NOPMD call method
        addConverterFactory(mqttPayloadReadConverterFactory); // NOPMD call method
    }
}
