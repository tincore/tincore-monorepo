<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!--
        Based on
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.x.RELEASE</version>

        When upgrade:
         copy and paste sections from spring file
         check for diff to replicate commented outs
    -->
    <artifactId>spring-boot23-app-parent</artifactId>
    <packaging>pom</packaging>

    <parent>
        <groupId>com.tincore</groupId>
        <artifactId>spring-boot23-lib-parent</artifactId>
        <version>3.2.5-SNAPSHOT</version>
        <relativePath>../spring-boot23-lib-parent</relativePath>
    </parent>

    <description>Tincore - Spring Boot 2.3.x APPLICATION Parent</description>

    <properties>
        <monorepo.root.relative.path>../../</monorepo.root.relative.path>

        <resource.delimiter>${spring.boot.resource.delimiter}</resource.delimiter>

        <docker.buildArgs.JAR_FILE>target/${project.build.finalName}-${spring.boot.jar.classifier}.jar
        </docker.buildArgs.JAR_FILE>

        <cf.skip>false</cf.skip>
        <cf.jar>${project.build.finalName}-${spring.boot.jar.classifier}.jar</cf.jar>
    </properties>

    <build>
        <finalName>${project.artifactId}-${project.version}${spring.artifact.qualifier}</finalName>

        <resources>
            <!-- ================================== -->
            <!-- = SPRING BOOT BELOW ============== -->
            <!-- ================================== -->
            <resource>
                <filtering>true</filtering>
                <directory>${basedir}/src/main/resources</directory>
                <includes>
                    <include>**/application*.yml</include>
                    <include>**/application*.yaml</include>
                    <include>**/application*.properties</include>
                </includes>
            </resource>
            <resource>
                <directory>${basedir}/src/main/resources</directory>
                <excludes>
                    <exclude>**/application*.yml</exclude>
                    <exclude>**/application*.yaml</exclude>
                    <exclude>**/application*.properties</exclude>
                </excludes>
            </resource>
        </resources>

        <pluginManagement>
            <plugins>
                <!-- =========================================== -->
                <!-- = SPRING BOOT BELOW (ATTENTION MODDED) ==== -->
                <!-- =========================================== -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <parameters>true</parameters>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>integration-test</goal>
                                <goal>verify</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <classesDirectory>${project.build.outputDirectory}</classesDirectory>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <configuration>
                        <archive>
                            <manifest>
                                <mainClass>${start-class}</mainClass>
                                <!--                                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>-->
                                <!--                                <addClasspath>true</addClasspath>-->
                                <classpathPrefix>${settings.localRepository}</classpathPrefix>
                                <classpathLayoutType>repository</classpathLayoutType>
                            </manifest>
                        </archive>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-war-plugin</artifactId>
                    <configuration>
                        <archive>
                            <manifest>
                                <mainClass>${start-class}</mainClass>
                                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            </manifest>
                        </archive>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <configuration>
                        <delimiters>
                            <delimiter>${resource.delimiter}</delimiter>
                        </delimiters>
                        <useDefaultDelimiters>false</useDefaultDelimiters>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>pl.project13.maven</groupId>
                    <artifactId>git-commit-id-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>revision</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <verbose>true</verbose>
                        <dateFormat>yyyy-MM-dd'T'HH:mm:ssZ</dateFormat>
                        <generateGitPropertiesFile>true</generateGitPropertiesFile>
                        <generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties
                        </generateGitPropertiesFilename>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <!-- MOD -->
                    <executions>
                        <execution>
                            <id>build-info</id>
                            <!-- Spring Boot Actuator displays build-related information if a META-INF/build-info.properties
                                file is present -->
                            <goals>
                                <goal>build-info</goal>
                            </goals>
                            <configuration>
                                <additionalProperties>
                                    <encoding.source>${project.build.sourceEncoding}</encoding.source>
                                    <encoding.reporting>${project.reporting.outputEncoding}</encoding.reporting>

                                    <java.source>${maven.compiler.source}</java.source>
                                    <java.target>${maven.compiler.target}</java.target>

                                    <scm.buildNumber>${scmBuildNumber}</scm.buildNumber>
                                    <scm.branch>${scmBranch}</scm.branch>
                                    <scm.timestamp>${scmTimestamp}</scm.timestamp>
                                </additionalProperties>
                            </configuration>
                        </execution>
                        <execution>
                            <id>repackage</id>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <classifier>${spring.boot.jar.classifier}</classifier>
                        <fork>true</fork>
                        <mainClass>${start-class}</mainClass>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-shade-plugin</artifactId>
                    <configuration>
                        <keepDependenciesWithProvidedScope>true</keepDependenciesWithProvidedScope>
                        <createDependencyReducedPom>true</createDependencyReducedPom>
                        <filters>
                            <filter>
                                <artifact>*:*</artifact>
                                <excludes>
                                    <exclude>META-INF/*.SF</exclude>
                                    <exclude>META-INF/*.DSA</exclude>
                                    <exclude>META-INF/*.RSA</exclude>
                                </excludes>
                            </filter>
                        </filters>
                    </configuration>
                    <dependencies>
                        <dependency>
                            <groupId>org.springframework.boot</groupId>
                            <artifactId>spring-boot-maven-plugin</artifactId>
                            <version>${spring-boot.version}</version>
                        </dependency>
                    </dependencies>
                    <executions>
                        <execution>
                            <phase>package</phase>
                            <goals>
                                <goal>shade</goal>
                            </goals>
                            <configuration>
                                <transformers>
                                    <transformer
                                            implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                                        <resource>META-INF/spring.handlers</resource>
                                    </transformer>
                                    <transformer
                                            implementation="org.springframework.boot.maven.PropertiesMergingResourceTransformer">
                                        <resource>META-INF/spring.factories</resource>
                                    </transformer>
                                    <transformer
                                            implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                                        <resource>META-INF/spring.schemas</resource>
                                    </transformer>
                                    <transformer
                                            implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer"/>
                                    <transformer
                                            implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                        <mainClass>${start-class}</mainClass>
                                    </transformer>
                                </transformers>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
</project>
