package com.tincore.boot.server.web.controller;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.server.web.form.DevJwtRequestForm;
import com.tincore.boot.util.security.jwt.JwtTokenService;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindingResult;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class DevControllerTest extends BehaviourAwareTest {

    @Spy
    private final JwtTokenService jwtTokenService = new JwtTokenService("authHeader", "Bearer ", "authCookie", "something", Duration.ofMinutes(1));

    @InjectMocks
    private final DevController devController = new DevController(jwtTokenService, Claims.SUBJECT, true);

    private final Map<String, Object> model = new HashMap<>();
    private final MockHttpServletResponse response = new MockHttpServletResponse();

    @Mock
    private BindingResult mockBindingResult;

    @Test
    void test_doJwtAut_Given_requestForm_Then_addsCookie() {
        devController.doJwtAut(DevJwtRequestForm.builder()
                .claims("role=ADMIN")
                .build(), mockBindingResult, model, response);

        assertThat(response.getCookie("authCookie")).isNotNull();
    }
}
