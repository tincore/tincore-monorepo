package com.tincore.boot.config.convert;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ZonedDateTimeConverterTest extends BehaviourAwareTest {

    private ZonedDateTimeConverter converter = new ZonedDateTimeConverter();

    @Test
    void test_convert_Given_emptyString_Then_throwsException() {
        assertThatExceptionOfType(Exception.class).isThrownBy(() -> converter.convert(""));
    }

    @Test
    void test_convert_Given_localDateAsIsoStringh_Then_returnsLocalDate() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        assertThat(converter.convert(zonedDateTime.toString())).isEqualTo(zonedDateTime);
    }
}
