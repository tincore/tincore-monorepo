package com.tincore.boot.util.security.jwt;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JwtUserDetailsServiceImplTest extends BehaviourAwareTest {

    private static final String AUT_1 = "aut1";
    private static final String AUT_2 = "aut2";
    private static final String AUT_3 = "aut3";
    private final JwtAuthorityMapping mapping1 = JwtAuthorityMapping.builder().claim("c1").value("v1").authorizations(Stream.of(AUT_1, AUT_2).collect(Collectors.toList())).build();
    private final JwtAuthorityMapping mapping2 = JwtAuthorityMapping.builder().claim("c2").value("v2").authorizations(Stream.of(AUT_2, AUT_3).collect(Collectors.toList())).build();
    private final List<JwtAuthorityMapping> authorityMappings = Stream.of(mapping1, mapping2).collect(Collectors.toList());
    private final String claimSubject = Claims.SUBJECT;
    private final String token = "token";
    private final DefaultClaims claims = new DefaultClaims();
    private JwtUserDetailsServiceImpl jwtUserDetailsServiceImpl;
    @Mock
    private JwtTokenService mockJwtTokenService;

    @BeforeEach
    public void setUp() {
        jwtUserDetailsServiceImpl = new JwtUserDetailsServiceImpl(mockJwtTokenService, authorityMappings, claimSubject);
    }

    public void setUpGetClaimsForTokenExpectations() {
        when(mockJwtTokenService.getClaimsFromToken(token)).thenReturn(claims);
    }

    @Test
    void test_claimKeyUserName_Given_static_Then_isSubject() {
        assertThat(new JwtUserDetailsServiceImpl(null, null, null).getClaimUserName()).isEqualTo(Claims.SUBJECT);
    }

    @Test
    void test_loadUserByUsername_Given_invalidJwtToken_Then_throwsUsernameNotFoundException() {
        when(mockJwtTokenService.getClaimsFromToken(token)).thenThrow(new JwtException(""));
        assertThatThrownBy(() -> jwtUserDetailsServiceImpl.loadUserByUsername(token)).isInstanceOf(UsernameNotFoundException.class);
    }

    @Test
    void test_loadUserByUsername_Given_validJwtTokenAndClaimKeyAndValueMatches_Then_userHasMatchedMappingAuthorities() {
        claims.setSubject("s");
        claims.put(mapping1.getClaim(), mapping1.getValue());
        setUpGetClaimsForTokenExpectations();

        assertThat(jwtUserDetailsServiceImpl.loadUserByUsername(token).getAuthorities().stream().map(GrantedAuthority::getAuthority)).contains(AUT_1, AUT_2);
    }

    @Test
    void test_loadUserByUsername_Given_validJwtTokenAndClaimKeyAndValueMultipleMatches_Then_userHasAllMatchedMappingAuthorities() {
        claims.setSubject("s");
        claims.put(mapping1.getClaim(), mapping1.getValue());
        claims.put(mapping2.getClaim(), mapping2.getValue());
        setUpGetClaimsForTokenExpectations();

        assertThat(jwtUserDetailsServiceImpl.loadUserByUsername(token).getAuthorities().stream().map(GrantedAuthority::getAuthority)).contains(AUT_1, AUT_2, AUT_3);
    }

    @Test
    void test_loadUserByUsername_Given_validJwtTokenAndClaimKeyExitsButValueDoesNotMatch_Then_userNoMappingAuthorities() {
        claims.setSubject("s");
        claims.put(mapping1.getClaim(), "otherValue1");
        claims.put(mapping2.getClaim(), "otherValue2");
        setUpGetClaimsForTokenExpectations();

        assertThat(jwtUserDetailsServiceImpl.loadUserByUsername(token).getAuthorities()).isNullOrEmpty();
    }

    @Test
    void test_loadUserByUsername_Given_validJwtToken_Then_userNameEqualsClaimSubject() {
        claims.setSubject("s");
        claims.put(mapping1.getClaim(), mapping1.getValue());
        claims.put(mapping2.getClaim(), "otherValue2");
        setUpGetClaimsForTokenExpectations();

        assertThat(jwtUserDetailsServiceImpl.loadUserByUsername(token).getAuthorities().stream().map(GrantedAuthority::getAuthority)).contains(AUT_1, AUT_2);
    }

    @Test
    void test_loadUserByUsername_Given_validJwtToken_When_tokenClaimsHasNoSubject_Then_throwsUsernameNotFoundException() {
        claims.setSubject("");
        setUpGetClaimsForTokenExpectations();
        assertThatThrownBy(() -> jwtUserDetailsServiceImpl.loadUserByUsername(token)).isInstanceOf(UsernameNotFoundException.class);
    }
}
