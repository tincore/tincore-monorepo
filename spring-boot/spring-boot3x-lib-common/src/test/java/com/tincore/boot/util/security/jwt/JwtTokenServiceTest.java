package com.tincore.boot.util.security.jwt;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import com.tincore.util.lang.SequenceExtension;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.impl.DefaultClaims;
import jakarta.servlet.http.Cookie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtendWith(MockitoExtension.class)
@BehaviourGroup(scenario = "Authentication management: JWT")
class JwtTokenServiceTest extends BehaviourAwareTest {

    @InjectMocks
    private final JwtTokenService jwtTokenService = new JwtTokenService("authHeader", "Bearer ", "authCookie", "something", Duration.ofMinutes(1));

    private final String username = "user1";
    private final String claimKey = "someKey";
    private final Object claimValue = "someValue";
    private final Map<String, Object> claims = SequenceExtension.toMutableMapOf(claimKey, claimValue);
    MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
    MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();

    @BeforeEach
    public void setUp() {

        // MockitoAnnotations.initMocks(this);
    }

    @Test
    void test_getClaimValueForToken_Given_tokenWithClaims_When_claimHasSameNameAsKey_Then_returnsClaimValue() {
        var claims = new DefaultClaims();
        claims.put(claimKey, claimValue);
        assertThat(claims.get(claimKey, String.class)).isEqualTo(claimValue);
    }

    //
    @Test
    void test_getClaimValueForToken_Given_tokenWithClaims_When_noClaimForKey_Then_returnsNull() {
        var claims = new DefaultClaims();
        assertThat(claims.get("not_key", String.class)).isNull();
    }

    @Test
    void test_getClaimsForToken_Given_expiredToken_Then_throwsExpiredJwtException() {
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put(JwtTokenService.CLAIM_KEY_CREATED_MILLIS, new Date(1));

        assertThatExceptionOfType(ExpiredJwtException.class).isThrownBy(() -> jwtTokenService.getClaimsFromToken(jwtTokenService.createToken(map)));
    }

    @Test
    void test_getClaimsForToken_Given_validToken_Then_returnsClaimsAndStoredClaimCanBeRetrieved() {
        var token = jwtTokenService.createToken(claims);
        var claims = jwtTokenService.getClaimsFromToken(token);
        assertThat(claims.keySet()).isNotEmpty();

        assertThat(claims.get(claimKey)).isEqualTo(claimValue);
    }

    @Test
    void test_getTokenValue_Given_notSetInHeaderAndNotSetInCookie_Then_returnsEmpty() {
        assertThat(jwtTokenService.getTokenValue(mockHttpServletRequest)).isNotPresent();
    }

    @Test
    void test_getTokenValue_Given_setInCookie_Then_returnsValueFromCookie() {
        var token = "tok";
        mockHttpServletRequest.setCookies(new Cookie(jwtTokenService.getAuthenticationCookieName(), token));

        assertThat(jwtTokenService.getTokenValue(mockHttpServletRequest)).contains(token);
    }

    @Test
    void test_getTokenValue_Given_setInHeaderAndSetInCookie_Then_returnsValueFromHeader() {
        var cookieToken = "cookieToken";
        mockHttpServletRequest.setCookies(new Cookie(jwtTokenService.getAuthenticationCookieName(), cookieToken));
        var headerToken = "headerToken";
        mockHttpServletRequest.addHeader(jwtTokenService.getAuthenticationHeaderName(), headerToken);
        assertThat(jwtTokenService.getTokenValue(mockHttpServletRequest)).contains(headerToken);
    }

    @Test
    void test_getTokenValue_Given_setInHeader_Then_returnsValueFromHeader() {
        var token = "tok";
        mockHttpServletRequest.addHeader(jwtTokenService.getAuthenticationHeaderName(), token);

        assertThat(jwtTokenService.getTokenValue(mockHttpServletRequest)).contains(token);
    }

    @Test
    void test_getTokenValue_Given_valueInHeaderAndIsPrefixed_Then_returnsValueFromHeader() {
        var token = "tok";
        mockHttpServletRequest.addHeader(jwtTokenService.getAuthenticationHeaderName(), jwtTokenService.getAuthenticationHeaderValuePrefix() + token);

        assertThat(jwtTokenService.getTokenValue(mockHttpServletRequest)).contains(token);
    }

    @Test
    void test_jwtToken_Given_comAuth0CreatedToken_Then_canBeParsedSuccessfully() throws IllegalArgumentException, UnsupportedEncodingException {
        var key = "SomeKey";

        final var algorithm = Algorithm.HMAC256(key);
        var token = JWT.create().withSubject("123").withClaim("role", "customer").withIssuer("myIssuer").withExpiresAt(new Date(System.currentTimeMillis() + 100000)).sign(algorithm);

        var claims = jwtTokenService.getClaimsFromToken(key, token);
        assertThat(claims.get("sub")).isEqualTo("123");
        assertThat(claims.get("role")).isEqualTo("customer");
    }

    @Test
    void test_refreshToken_Given_token_Then_generatesNewTokenWithRecentCreationDate() {

        claims.put("s1", "v1");
        claims.put(JwtTokenService.CLAIM_KEY_CREATED_MILLIS, new Date(System.currentTimeMillis() - 10000));

        var token = jwtTokenService.createToken(claims);

        var refreshedToken = jwtTokenService.refreshWebToken(token);
        var refreshedClaims = jwtTokenService.getClaimsJws(jwtTokenService.getSigningKey(), refreshedToken);

        assertThat(refreshedClaims.getBody().get("someKey")).isEqualTo(claims.get("someKey"));
        assertThat(refreshedClaims.getBody().get("s1")).isEqualTo(claims.get("s1"));
        assertThat(((Long) refreshedClaims.getBody().get(JwtTokenService.CLAIM_KEY_CREATED_MILLIS))).isGreaterThan(System.currentTimeMillis() - 1000);
    }

    @Test
    void test_setTokenCookie_Given_token_Then_addsCookieToResponseWithRootPathAndMaxAge() {
        var token = "someToken";

        jwtTokenService.setTokenCookie(token, mockHttpServletResponse);

        var cookie = mockHttpServletResponse.getCookie(jwtTokenService.getAuthenticationCookieName());
        assertThat(cookie.getMaxAge()).isEqualTo((int) (jwtTokenService.getDefaultExpiration().toMillis() / 1000));
        assertThat(cookie.getPath()).isEqualTo("/");
        assertThat(cookie.getValue()).isEqualTo(token);
    }
}
