package com.tincore.boot.util.security.jwt;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.util.security.TincoreUserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final String claimUserName;
    private final List<JwtAuthorityMapping> jwtAuthorityMappings;
    private final JwtTokenService jwtTokenService;

    public JwtUserDetailsServiceImpl(JwtTokenService jwtTokenService, List<JwtAuthorityMapping> jwtAuthorityMappings, String claimUserName) {
        this.jwtTokenService = jwtTokenService;
        this.jwtAuthorityMappings = Optional.ofNullable(jwtAuthorityMappings).orElse(new ArrayList<>());
        this.claimUserName = Optional.ofNullable(claimUserName).orElse(Claims.SUBJECT);
    }

    String getClaimUserName() {
        return claimUserName;
    }

    private UserDetails getUserDetails(Claims claims, String username) {
        List<SimpleGrantedAuthority> authorities = jwtAuthorityMappings.stream()
            .filter(m -> Optional.ofNullable(claims.get(m.getClaim(), String.class)).filter(v -> m.getValue().equals(v)).isPresent())
            .map(JwtAuthorityMapping::getAuthorizations)
            .flatMap(List::stream)
            .distinct()
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());
        return TincoreUserDetails.builder().username(username).authorities(authorities).build();
    }

    private UserDetails getUserDetailsByToken(String token) {
        try {
            Claims claims = jwtTokenService.getClaimsFromToken(token);

            String username = claims.get(getClaimUserName(), String.class);
            if (StringUtils.isBlank(username)) {
                throw new UsernameNotFoundException(String.format("No user found with name '%s'.", token));
            }

            return getUserDetails(claims, username);
        } catch (JwtException e) {
            throw new UsernameNotFoundException(String.format("No user found with name '%s'.", token), e);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException { // NOPMD Unchecked exception
        return getUserDetailsByToken(username);
    }
}
