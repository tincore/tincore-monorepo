package com.tincore.boot.server.web.controller;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.server.web.form.DevJwtRequestForm;
import com.tincore.boot.util.security.jwt.JwtTokenService;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

@Controller
@ConditionalOnProperty(value = "tincore.dev.enabled", havingValue = "true")
@RequestMapping("dev")
public class DevController {

    private final JwtTokenService jwtTokenService;
    private final String jwtClaimUserName;
    private final boolean jwtEnabled;

    public DevController(@Autowired JwtTokenService jwtTokenService, @Value("${tincore.sec.jwt.claimUsername:" + Claims.SUBJECT + "}") String jwtClaimUserName, @Value("${tincore.sec.jwt.enabled:false}") boolean jwtEnabled) {
        this.jwtTokenService = jwtTokenService;
        this.jwtClaimUserName = jwtClaimUserName;
        this.jwtEnabled = jwtEnabled;
    }


    @GetMapping
    public String doDisplay(@RequestParam(required = false) String r, Map<String, Object> model, @AuthenticationPrincipal UserDetails userDetails) {
        setUpModel(model);
        model.put("tokenRequestForm", DevJwtRequestForm.builder().redirectTo(r).build());
        model.put("userDetails", userDetails);
        return "dev";
    }

    @PostMapping("jwtAut")
    public String doJwtAut(@ModelAttribute @Valid DevJwtRequestForm tokenRequestForm, BindingResult bindingResult, Map<String, Object> model, HttpServletResponse response) {
        setUpModel(model);
        if (bindingResult.hasErrors()) {
            model.put("tokenRequestForm", tokenRequestForm);
            return "dev";
        }

        Map<String, Object> claims = new HashMap<>();
        claims.put(jwtClaimUserName, tokenRequestForm.getUsername());

        Map<String, String> claimsMap = Stream.of(tokenRequestForm.getClaims().split(","))
                .map(el -> el.split("="))
                .collect(toMap(a -> a[0], arr -> arr[1], (oldValue, newValue) -> oldValue));
        claims.putAll(claimsMap);

        jwtTokenService.setTokenCookie(jwtTokenService.createToken(claims), response, tokenRequestForm.isHttpOnly());

        if (StringUtils.isNotBlank(tokenRequestForm.getRedirectTo())) {
            return "redirect:" + tokenRequestForm.getRedirectTo();
        }

        return "redirect:../dev";
    }

    private void setUpModel(Map<String, Object> model) {

        if (!jwtEnabled) {
            model.put("message", model.getOrDefault("message", "") + "Jwt Aut disabled in config! ");
        }
    }
}
