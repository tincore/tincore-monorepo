package com.tincore.boot.util.security.jwt;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
@Getter
public class JwtTokenService {

    static final String CLAIM_KEY_CREATED_MILLIS = "created";
    private static final int MILLIS_IN_SECOND = 1000;

    private final SignatureAlgorithm signatureAlgorithm;
    private final String authenticationHeaderName;
    private final String authenticationHeaderValuePrefix;
    private final String authenticationCookieName;
    private final Duration defaultExpiration;
    private final String signingKey;

    public JwtTokenService(String authenticationHeaderName, String authenticationHeaderValuePrefix, String authenticationCookieName, String signingKey, Duration expiration, SignatureAlgorithm signatureAlgorithm) {
        this.signingKey = signingKey;
        this.defaultExpiration = expiration;

        this.signatureAlgorithm = signatureAlgorithm;
        this.authenticationHeaderName = authenticationHeaderName;
        this.authenticationHeaderValuePrefix = authenticationHeaderValuePrefix;
        this.authenticationCookieName = authenticationCookieName;
    }

    public JwtTokenService(String authenticationHeaderName, String authenticationHeaderValuePrefix, String authenticationCookieName, String signingKey, Duration defaultExpiration) {
        this(authenticationHeaderName, authenticationHeaderValuePrefix, authenticationCookieName, signingKey, defaultExpiration, SignatureAlgorithm.HS256);
    }
    public String createToken(Map<String, Object> claimsMap) {
        return createToken(claimsMap, defaultExpiration);
    }

    public String createToken(Map<String, Object> claims, Duration expiration) {
        var updatedClaims = new HashMap<>(claims);
        var createdDate = (Date) updatedClaims.computeIfAbsent(CLAIM_KEY_CREATED_MILLIS, k -> new Date());
        var expirationDate = new Date(createdDate.getTime() + expiration.toMillis());
        return Jwts.builder().setHeaderParam("typ", "JWT").setClaims(updatedClaims).setExpiration(expirationDate).signWith(signatureAlgorithm, TextCodec.BASE64.encode(signingKey)).compact();
    }

    Claims getClaimsFromToken(String signingKey, String token) {
        return getClaimsJws(signingKey, token).getBody();
    }

    Claims getClaimsFromToken(String token) {
        return getClaimsFromToken(signingKey, token);
    }

    Jws<Claims> getClaimsJws(String signingKey, String token) {
        return Jwts.parser().setSigningKey(TextCodec.BASE64.encode(signingKey)).parseClaimsJws(token);
    }

    private Optional<String> getCookieValueByName(String name, HttpServletRequest request) {
        if (request.getCookies() == null) {
            return Optional.empty();
        }

        return Stream.of(request.getCookies()).filter(c -> c.getName().equals(name)).map(Cookie::getValue).findFirst();
    }

    private Optional<String> getHeaderValueByName(String authenticationHeaderName, HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(authenticationHeaderName)).map(h -> h.startsWith(authenticationHeaderValuePrefix) ? h.substring(authenticationHeaderValuePrefix.length()) : h);
    }

    public Optional<String> getTokenValue(HttpServletRequest request) {
        return Optional.ofNullable(getHeaderValueByName(authenticationHeaderName, request).orElseGet(() -> getCookieValueByName(authenticationCookieName, request).orElse(null)));
    }

    String refreshWebToken(String token) {
        return refreshWebToken(token, defaultExpiration);
    }

    String refreshWebToken(String token, Duration expiration) {
        var claims = getClaimsFromToken(signingKey, token);
        claims.remove(CLAIM_KEY_CREATED_MILLIS);
        return createToken(claims, expiration);
    }

    public void setTokenCookie(String token, HttpServletResponse response) {
        setTokenCookie(token, response, true);
    }

    public void setTokenCookie(String token, HttpServletResponse response, boolean httpOnly) {
        var cookie = new Cookie(authenticationCookieName, token);
        cookie.setMaxAge((int) (defaultExpiration.toMillis() / MILLIS_IN_SECOND));
        cookie.setPath("/");
        cookie.setHttpOnly(httpOnly);
        response.addCookie(cookie);
    }
}
