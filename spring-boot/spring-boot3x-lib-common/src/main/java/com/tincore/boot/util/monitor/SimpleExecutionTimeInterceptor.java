package com.tincore.boot.util.monitor;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.interceptor.AbstractMonitoringInterceptor;

import java.util.Date;

public class SimpleExecutionTimeInterceptor extends AbstractMonitoringInterceptor {

    private static final int DEFAULT_MILLIS_WARN_THRESHOLD = 10;

    private final long millisWarnThreshold;

    public SimpleExecutionTimeInterceptor() {
        this(DEFAULT_MILLIS_WARN_THRESHOLD, false);
    }

    public SimpleExecutionTimeInterceptor(long millisWarnThreshold, boolean useDynamicLogger) {
        super();

        this.millisWarnThreshold = millisWarnThreshold;
        setUseDynamicLogger(useDynamicLogger); // NOPMD
    }

    @Override
    protected Object invokeUnderTrace(MethodInvocation invocation, Log log) throws Throwable {
        String name = createInvocationTraceName(invocation);
        long start = System.currentTimeMillis();
        if (log.isInfoEnabled()) {
            log.info(String.format("Method %s execution started at:%s", name, new Date()));
        }
        try {
            return invocation.proceed();
        } finally {
            long end = System.currentTimeMillis();
            long time = end - start;
            if (log.isInfoEnabled()) {
                log.info(String.format("Method %s execution lasted:%d ms", name, time));
                log.info(String.format("Method %s execution ended at:%s", name, new Date()));
            }

            if (time > millisWarnThreshold && log.isWarnEnabled()) {
                log.warn(String.format("Method execution longer than %d ms!", millisWarnThreshold));
            }
        }
    }
}
