package com.tincore.boot.util.security.jwt;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.util.security.TincoreUserDetails;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@BehaviourGroup(scenario = "Authentication management: JWT")
public class JwtAuthenticationTokenFilterTest extends BehaviourAwareTest {

    private final MockHttpServletRequest mockServletHttpRequest = new MockHttpServletRequest();
    private final MockHttpServletResponse mockServletHttpResponse = new MockHttpServletResponse();
    private final MockFilterChain mockFilterChain = new MockFilterChain();
    private final String token = "someToken";
    private final String username = "someUsername";

    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;
    @Mock
    private JwtTokenService mockJwtTokenService;
    @Mock
    private UserDetailsService mockUserDetailsService;
    @Mock
    private Authentication mockAuthentication;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        jwtAuthenticationTokenFilter = new JwtAuthenticationTokenFilter(mockJwtTokenService, mockUserDetailsService);
    }

    @AfterEach
    public void tearDown() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    void test_filterInternal_Given_authenticationInContext_Then_doesNotSetAuthentication() throws Exception {
        SecurityContextHolder.getContext().setAuthentication(mockAuthentication);

        jwtAuthenticationTokenFilter.doFilterInternal(mockServletHttpRequest, mockServletHttpResponse, mockFilterChain);

        assertThat(SecurityContextHolder.getContext().getAuthentication()).isEqualTo(mockAuthentication);
        verifyNoInteractions(mockJwtTokenService, mockUserDetailsService, mockAuthentication);
    }

    @Test
    void test_filterInternal_Given_noAuthenticationInContextAndNoTokenInRequest_Then_doesNotSetAuthentication() throws Exception {
        when(mockJwtTokenService.getTokenValue(mockServletHttpRequest)).thenReturn(Optional.empty());
        jwtAuthenticationTokenFilter.doFilterInternal(mockServletHttpRequest, mockServletHttpResponse, mockFilterChain);
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    void test_filterInternal_Given_noAuthenticationInContextAndTokenInRequest_When_tokenIsNotValid_Then_doesNotSetAuthentication() throws Exception {
        when(mockJwtTokenService.getTokenValue(mockServletHttpRequest)).thenReturn(Optional.of(token));
        when(mockUserDetailsService.loadUserByUsername(token)).thenThrow(new UsernameNotFoundException(""));

        jwtAuthenticationTokenFilter.doFilterInternal(mockServletHttpRequest, mockServletHttpResponse, mockFilterChain);
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    void test_filterInternal_Given_noAuthenticationInContextAndTokenInRequest_When_tokenValid_Then_setsAuthenticationWithMatchingUserDetails() throws Exception {
        UserDetails userDetails = TincoreUserDetails.builder().build();

        when(mockJwtTokenService.getTokenValue(mockServletHttpRequest)).thenReturn(Optional.of(token));
        when(mockUserDetailsService.loadUserByUsername(token)).thenReturn(userDetails);

        jwtAuthenticationTokenFilter.doFilterInternal(mockServletHttpRequest, mockServletHttpResponse, mockFilterChain);
        assertThat(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).isEqualTo(userDetails);
    }
}
