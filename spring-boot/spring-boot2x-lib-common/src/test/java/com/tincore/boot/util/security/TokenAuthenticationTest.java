package com.tincore.boot.util.security;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "Authentication management")
public class TokenAuthenticationTest extends BehaviourAwareTest {

    private UserDetails principal = TincoreUserDetails.builder().build();

    private TokenAuthentication tokenAuthentication = new TokenAuthentication(principal);

    @Test
    void test_getCredentials_Given_credentials_Then_credentialsContainsToken() {
        tokenAuthentication.setToken("tok");

        assertThat(tokenAuthentication.getCredentials()).isEqualTo(tokenAuthentication.getToken());
    }

    @Test
    void test_isAuthenticated_Given_token_Then_isTrue() {
        assertThat(tokenAuthentication.isAuthenticated()).isTrue();
    }
}
