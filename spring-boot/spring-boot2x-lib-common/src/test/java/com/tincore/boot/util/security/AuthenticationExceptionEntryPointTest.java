package com.tincore.boot.util.security;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "Authentication management")
class AuthenticationExceptionEntryPointTest extends BehaviourAwareTest {

    private MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
    private MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
    private AuthenticationException authException = null;

    @Test
    void test_commenced_Given_requestAndNoRedirectLocation_Then_setsResponseStatusAsUnauthorized() throws IOException {
        AuthenticationExceptionEntryPoint authenticationExceptionEntryPoint = new AuthenticationExceptionEntryPoint(null);
        authenticationExceptionEntryPoint.commence(mockHttpServletRequest, mockHttpServletResponse, authException);

        assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    void test_commenced_Given_requestAndRedirectLocationAndEmptyContentType_Then_setsResponseAsRedirectToRedirectLocation() throws IOException {
        AuthenticationExceptionEntryPoint authenticationExceptionEntryPoint = new AuthenticationExceptionEntryPoint("someRedirectLocation");
        authenticationExceptionEntryPoint.commence(mockHttpServletRequest, mockHttpServletResponse, authException);

        assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpServletResponse.SC_FOUND);
        assertThat(mockHttpServletResponse.getHeader("Location")).isEqualTo("someRedirectLocation");
    }

    @Test
    void test_commenced_Given_requestAndRedirectLocationAndNotEmptyContentType_Then_setsResponseStatusAsUnauthorized() throws IOException {
        AuthenticationExceptionEntryPoint authenticationExceptionEntryPoint = new AuthenticationExceptionEntryPoint("someRedirectLocation");
        mockHttpServletRequest.setContentType("some");
        authenticationExceptionEntryPoint.commence(mockHttpServletRequest, mockHttpServletResponse, authException);

        assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    void test_commenced_Given_requestAndRedirectLocationWithRefererAndEmptyContentType_Then_setsResponseAsRedirectToRedirectLocationIncludingReferer() throws IOException {
        AuthenticationExceptionEntryPoint authenticationExceptionEntryPoint = new AuthenticationExceptionEntryPoint("http://someRedirectLocation?r={referer}");
        authenticationExceptionEntryPoint.commence(mockHttpServletRequest, mockHttpServletResponse, authException);

        assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpServletResponse.SC_FOUND);
        assertThat(mockHttpServletResponse.getHeader("Location")).isEqualTo("http://someRedirectLocation?r=http://localhost");
    }
}
