package com.tincore.util.querydsl;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.querydsl.core.types.dsl.BooleanExpression;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.function.Function;

public class OptionalBooleanBuilder {

    private final BooleanExpression predicate;

    public OptionalBooleanBuilder(BooleanExpression predicate) {
        this.predicate = predicate;
    }

    public <T> OptionalBooleanBuilder andWhenNotEmpty(Function<Collection<T>, BooleanExpression> expressionFunction, Collection<T> collection) {
        if (!CollectionUtils.isEmpty(collection)) {
            return new OptionalBooleanBuilder(predicate.and(expressionFunction.apply(collection)));
        }
        return this;
    }

    public OptionalBooleanBuilder andWhenNotEmpty(Function<String, BooleanExpression> expressionFunction, String value) {
        if (!StringUtils.isEmpty(value)) {
            return new OptionalBooleanBuilder(predicate.and(expressionFunction.apply(value)));
        }
        return this;
    }

    public OptionalBooleanBuilder andWhenNotEmptyOrZero(Function<Long, BooleanExpression> expressionFunction, Long value) {
        if (value != null && value != 0) {
            return new OptionalBooleanBuilder(predicate.and(expressionFunction.apply(value)));
        }
        return this;
    }

    public <T> OptionalBooleanBuilder andWhenNotNull(Function<T, BooleanExpression> expressionFunction, T value) {
        if (value != null) {
            return new OptionalBooleanBuilder(predicate.and(expressionFunction.apply(value)));
        }
        return this;
    }

    public <T> OptionalBooleanBuilder andWhenNotNull(BooleanExpression booleanExpression) {
        if (booleanExpression != null) {
            return new OptionalBooleanBuilder(predicate.or(booleanExpression));
        }
        return this;
    }

    public BooleanExpression build() {
        return predicate;
    }

    public <T> OptionalBooleanBuilder orWhenNotNull(BooleanExpression booleanExpression) {
        if (booleanExpression != null) {
            return new OptionalBooleanBuilder(predicate.or(booleanExpression));
        }
        return this;
    }

    public <T> OptionalBooleanBuilder orWhenNotNull(Function<T, BooleanExpression> expressionFunction, T value) {
        if (value != null) {
            return new OptionalBooleanBuilder(predicate.or(expressionFunction.apply(value)));
        }
        return this;
    }
}
