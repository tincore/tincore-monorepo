package com.tincore.boot.config;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.config.ProfilesHelper;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile(ProfilesHelper.CLOUD)
public class DataSourceConfiguration {

    // @Bean
    // public Cloud cloud() {
    // return new CloudFactory().getCloud();
    // }
    //
    // @Bean
    // @ConfigurationProperties(DataSourceProperties.PREFIX)
    // public DataSource dataSource() {
    // return cloud().getSingletonServiceConnector(DataSource.class, null);
    // }

}
