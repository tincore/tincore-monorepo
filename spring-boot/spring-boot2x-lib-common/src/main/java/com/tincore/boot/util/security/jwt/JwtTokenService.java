package com.tincore.boot.util.security.jwt;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
public class JwtTokenService {

    static final String CLAIM_KEY_CREATED = "created";
    private static final int MILLIS_IN_SECOND = 1000;

    private final SignatureAlgorithm signatureAlgorithm;
    private final String authenticationHeaderName;
    private final String authenticationHeaderValuePrefix;
    private final String authenticationCookieName;
    private final Duration expiration;
    private final String signingKey;

    public JwtTokenService(String authenticationHeaderName, String authenticationHeaderValuePrefix, String authenticationCookieName, String signingKey, Duration expiration, SignatureAlgorithm signatureAlgorithm) {
        this.signingKey = signingKey;
        this.expiration = expiration;

        this.signatureAlgorithm = signatureAlgorithm;
        this.authenticationHeaderName = authenticationHeaderName;
        this.authenticationHeaderValuePrefix = authenticationHeaderValuePrefix;
        this.authenticationCookieName = authenticationCookieName;
    }

    public JwtTokenService(String authenticationHeaderName, String authenticationHeaderValuePrefix, String authenticationCookieName, String signingKey, Duration expiration) {
        this(authenticationHeaderName, authenticationHeaderValuePrefix, authenticationCookieName, signingKey, expiration, SignatureAlgorithm.HS256);
    }

    public String createToken(Map<String, Object> claimsMap) {
        final Date createdDate = (Date) claimsMap.computeIfAbsent(CLAIM_KEY_CREATED, k -> getCurrentDate());
        final Date expirationDate = new Date(expiration.toMillis() + createdDate.getTime());

        String encodedKey = TextCodec.BASE64.encode(signingKey);
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setClaims(claimsMap)
                .setExpiration(expirationDate)
                .signWith(signatureAlgorithm, encodedKey)
                .compact();
    }

    public String getAuthenticationCookieName() {
        return authenticationCookieName;
    }

    public String getAuthenticationHeaderName() {
        return authenticationHeaderName;
    }

    public String getAuthenticationHeaderValuePrefix() {
        return authenticationHeaderValuePrefix;
    }

    Claims getClaimsFromToken(String signingKey, String token) {
        String encodedKey = TextCodec.BASE64.encode(signingKey);
        return Jwts.parser().setSigningKey(encodedKey).parseClaimsJws(token).getBody();
    }

    Claims getClaimsFromToken(String token) {
        return getClaimsFromToken(signingKey, token);
    }

    private Optional<String> getCookieValueByName(String name, HttpServletRequest request) {
        if (request.getCookies() == null) {
            return Optional.empty();
        }

        return Stream.of(request.getCookies()).filter(c -> c.getName().equals(name)).map(Cookie::getValue).findFirst();
    }

    private Date getCurrentDate() {
        return new Date();
    }

    public Duration getExpiration() {
        return expiration;
    }

    private Optional<String> getHeaderValueByName(String authenticationHeaderName, HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(authenticationHeaderName))
                .map(h -> h.startsWith(authenticationHeaderValuePrefix) ? h.substring(authenticationHeaderValuePrefix.length()) : h);
    }

    public String getSigningKey() {
        return signingKey;
    }

    public Optional<String> getTokenValue(HttpServletRequest request) {
        return Optional.ofNullable(getHeaderValueByName(authenticationHeaderName, request).orElseGet(() -> getCookieValueByName(authenticationCookieName, request).orElse(null)));
    }

    String refreshWebToken(String token) {
        String refreshedToken;
        final Claims claims = getClaimsFromToken(signingKey, token);
        claims.remove(CLAIM_KEY_CREATED);
        refreshedToken = createToken(claims);
        return refreshedToken;
    }

    public void setTokenCookie(String token, HttpServletResponse response) {
        setTokenCookie(token, response, true);
    }

    public void setTokenCookie(String token, HttpServletResponse response, boolean httpOnly) {
        Cookie cookie = new Cookie(authenticationCookieName, token);
        cookie.setMaxAge((int) (expiration.toMillis() / MILLIS_IN_SECOND));
        cookie.setPath("/");
        cookie.setHttpOnly(httpOnly);
        response.addCookie(cookie);
    }
}
