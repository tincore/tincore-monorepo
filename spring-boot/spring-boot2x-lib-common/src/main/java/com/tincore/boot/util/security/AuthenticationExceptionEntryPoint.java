package com.tincore.boot.util.security;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class AuthenticationExceptionEntryPoint implements AuthenticationEntryPoint {

    private final String redirectLocationTemplate;

    public AuthenticationExceptionEntryPoint(String redirectLocationTemplate) {
        this.redirectLocationTemplate = redirectLocationTemplate;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        log.debug("Authentication failed {}", request);
        String contentType = request.getContentType();

        boolean redirect = StringUtils.isNotBlank(redirectLocationTemplate) && StringUtils.isEmpty(contentType);
        if (redirect) {
            String refererUrl = request.getRequestURL().toString();
            UriComponents uriComponents = UriComponentsBuilder.fromUriString(redirectLocationTemplate).buildAndExpand(refererUrl);
            String uriString = uriComponents.toUriString();
            response.sendRedirect(uriString);
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        }
    }
}
