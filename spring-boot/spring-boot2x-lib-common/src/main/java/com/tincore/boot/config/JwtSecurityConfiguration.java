package com.tincore.boot.config;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.util.security.jwt.JwtAuthenticationTokenFilter;
import com.tincore.boot.util.security.jwt.JwtAuthorityMapping;
import com.tincore.boot.util.security.jwt.JwtTokenService;
import com.tincore.boot.util.security.jwt.JwtUserDetailsServiceImpl;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.util.List;

@Data
@Configuration
@ConfigurationProperties("tincore.jwt")
@ConditionalOnProperty(value = "tincore.jwt.enabled", havingValue = "true")
public class JwtSecurityConfiguration {

    public static final Duration DEFAULT_EXPIRATION = Duration.ofHours(1);

    private String headerName = "Authorization";
    private String headerValuePrefix = "Bearer ";
    private String cookieName = "SO_AUTH_TOKEN";
    private String signingKey = "someKey";
    private Duration expiration = DEFAULT_EXPIRATION;

    private List<JwtAuthorityMapping> authorityMappings;

    private String claimUsername;

    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() {
        return new JwtAuthenticationTokenFilter(jwtTokenService(), jwtUserDetailsService());
    }

    @Bean
    public JwtTokenService jwtTokenService() {
        return new JwtTokenService(headerName, headerValuePrefix, cookieName, signingKey, expiration);
    }

    @Bean
    public JwtUserDetailsServiceImpl jwtUserDetailsService() {
        return new JwtUserDetailsServiceImpl(jwtTokenService(), authorityMappings, claimUsername);
    }
}
