package com.tincore.boot.service;

/*-
 * #%L
 * spring-boot2x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.function.Supplier;

@NoRepositoryBean
public abstract class AbstractRepositoryServiceExtensionImpl<T, I extends Serializable> extends SimpleJpaRepository<T, I> implements RepositoryServiceExtension<T, I> {

    private final EntityManager entityManager;

    public AbstractRepositoryServiceExtensionImpl(Class<T> entityClass, EntityManager entityManager) {
        super(entityClass, entityManager);
        this.entityManager = entityManager;
    }

    public <T> T findOrCreate(Supplier<T> finder, Supplier<T> factory) {
        T value = finder.get();
        if (value != null) {
            return value;
        }
        EntityManager innerEntityManager = entityManager.getEntityManagerFactory().createEntityManager();
        innerEntityManager.getTransaction().begin();
        try {
            if (finder.get() == null) {
                T newInstance = factory.get();
                innerEntityManager.persist(newInstance);
            }
            innerEntityManager.getTransaction().commit();
        } catch (PersistenceException ex) {
            innerEntityManager.getTransaction().rollback();
            T entity = finder.get();
            if (entity == null) {
                throw ex;
            }
            // else {
            // Should be retrieved by outer
            // return entity;
            // }
        } catch (Throwable t) { // NOPMD
            innerEntityManager.getTransaction().rollback();
            throw t;
        } finally {
            innerEntityManager.close();
        }

        return finder.get();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
