package com.tincore.util.xml;

/*-
 * #%L
 * tincore-lib-common-xml
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import com.tincore.test.support.junit2bdd.junit5.BehaviourAwareTest;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "Java Utility")
class LocalDateAdapterTest extends BehaviourAwareTest {

    private final LocalDateAdapter localDateAdapter = new LocalDateAdapter();

    private final LocalDate date = LocalDate.now();

    @Test
    void test_marshal_Given_localDate_Then_returnsStringRepresentation() {
        assertThat(localDateAdapter.marshal(date)).isNotBlank().isEqualTo(date.toString());
    }

    @Test
    void test_unmarshal_Given_dateStringRepresentation_Then_retunsDate() {
        assertThat(localDateAdapter.unmarshal(date.toString())).isEqualTo(date);
    }
}
