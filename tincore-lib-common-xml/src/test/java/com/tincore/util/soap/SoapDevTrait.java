package com.tincore.util.soap;

/*-
 * #%L
 * tincore-lib-common-xml
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.w3c.dom.Document;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public interface SoapDevTrait {

    default <T> T fromSoap(String soapRequest, Class<T> clazz) {
        XMLInputFactory xif = XMLInputFactory.newFactory();
        JAXBElement<T> jb;
        try {
            XMLStreamReader xsr = xif.createXMLStreamReader(new StringReader(soapRequest));
            do {
                xsr.nextTag();
            } while (!xsr.getLocalName().equals("Body"));
            xsr.nextTag();
            JAXBContext jc = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            jb = unmarshaller.unmarshal(xsr, clazz);
            xsr.close();
        } catch (Exception e) {
            throw new RuntimeException(String.format("fromSoap failed class=%s/nrequest=%s", clazz, soapRequest), e);
        }

        return jb.getValue();
    }

    default <T> String toSoap(T object, String namespaceUri, String namespacePrefix) {
        return toSoap(object, namespaceUri, namespacePrefix, new HashMap<>());
    }

    default <T> String toSoap(T object, String namespaceUri, String namespacePrefix, Map<Class, String> map) {
        try {
            ByteArrayOutputStream byteArrayOutputStream;
            Class clazz = object.getClass();

            String responseRootTag = map.getOrDefault(clazz, clazz.getSimpleName());

            QName payloadName = new QName(namespaceUri, responseRootTag, namespacePrefix);

            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Marshaller objectMarshaller = jaxbContext.createMarshaller();

            JAXBElement<T> jaxbElement = new JAXBElement<>(payloadName, clazz, null, object);
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            objectMarshaller.marshal(jaxbElement, document);

            SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
            SOAPBody body = soapMessage.getSOAPPart().getEnvelope().getBody();
            body.addDocument(document);

            byteArrayOutputStream = new ByteArrayOutputStream();
            soapMessage.saveChanges();
            soapMessage.writeTo(byteArrayOutputStream);
            return byteArrayOutputStream.toString();
        } catch (Exception e) {
            throw new RuntimeException(String.format("Exception trying to serialize [%s] to a SOAP envelope", object), e);
        }
    }

    default XPath toXpath(Map<String, String> namespaceUris) {
        Map<String, String> uriMap = new HashMap<>();
        uriMap.put("xml", XMLConstants.XML_NS_URI);
        uriMap.put("soap", "http://schemas.xmlsoap.org/soap/envelope/");
        uriMap.putAll(namespaceUris);

        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(
            new NamespaceContext() {
                public String getNamespaceURI(String prefix) {
                    if (uriMap.containsKey(prefix)) {
                        return uriMap.get(prefix);
                    } else {
                        return XMLConstants.NULL_NS_URI;
                    }
                }

                public String getPrefix(String uri) {
                    throw new UnsupportedOperationException();
                }

                public Iterator getPrefixes(String uri) {
                    throw new UnsupportedOperationException();
                }
            });
        return xpath;
    }
}
